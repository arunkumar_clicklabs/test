
/**
 * Module dependencies.
 */


//process.env.NODE_ENV = 'localDevelopment';
//process.env.NODE_CONFIG_DIR = '/Users/clicklabs/Desktop/taximust-files/localDevelopment/taximust/config';
var path            = require('path');
process.env.NODE_ENV = 'review';
process.env.NODE_CONFIG_DIR = path.join(__dirname, 'config');


//process.env.NODE_ENV = 'distribution';        // For testing
//process.env.NODE_CONFIG_DIR = '/home/storx/taximust/taximust-test/config';

//process.env.NODE_ENV = 'production';
//process.env.NODE_CONFIG_DIR = '/home/taxihawk/taximust-app/config';

//process.env.NODE_ENV = 'review';
//process.env.NODE_CONFIG_DIR = '/home/taxihawk/taximust-app/config';


config = require('config');


var express         = require('express');
var http            = require('http');

var bodyParser      = require('body-parser');
var favicon         = require('serve-favicon');
var errorhandler    = require('errorhandler');
var logger          = require('morgan');
var methodOverride  = require('method-override');

var multipart       = require('connect-multiparty');
var multipartMiddleware = multipart();

connection          = undefined;
var mysqlLib        = require('./routes/mysqlLib');
var server          = require('./routes/server');
var ride_parallel_dispatcher = require('./routes/ride_parallel_dispatcher');
var manual_dispatcher = require('./routes/manual_dispatcher');
var userlogin       = require('./routes/user_login');
var rating          = require('./routes/rating');
var testing         = require('./routes/testing');
var service         = require('./routes/service');
var crone           = require('./routes/crone');
var user            = require('./routes/user');
var mailer          = require('./routes/mailer');
var marketing       = require('./routes/marketing');
var messenger       = require('./routes/messenger');
var analytics       = require('./routes/analytics');
var notifications   = require('./routes/notifications');
var heartbeat       = require('./routes/heartbeat');
var driver          = require('./routes/driver');
var driverLogin     = require('./routes/driver_login');
var stationing      = require('./routes/stationing');
var one_time_functions = require('./routes/one_time_functions');
var make_payments   = require('./routes/make_payments');
var split_fare      = require('./routes/split_fare');
var wait      = require('./routes/wait_time');
var utils = require('./routes/commonfunction');
var devapi = require('./routes/devapi');
var driver_dispatcher = require('./routes/driver_dispatcher');
alog = utils.log;

//eventPicBaseUrl='http://turnout-images.s3.amazonaws.com/event_images/';
var app = express();

//var fs = require('fs');
//var options = {
//    key: fs.readFileSync(__dirname + '/dev.jugnoo.in.key'),
//    cert: fs.readFileSync(__dirname + '/dev.jugnoo.in.crt')
//};

// all environments
app.set('port', process.env.PORT || config.get('PORT'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.use(bodyParser());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(favicon(__dirname + '/views/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(errorhandler());
}

//app.get('/', routes.index);
//app.get('/user', user.list);

app.get('/apitest', function (req, res) {
    res.render('test');
});
app.get('/kpidata', function (req, res) {
    res.render('kpidata');
});
app.get('/schedule_ride', function (req, res) {
    res.render('schedule_ride');
});
app.get('/notifications', function (req, res) {
    res.render('notifications');
});
app.get('/stationing', function(req, res){
    res.render('stationing');
});
app.get('/drivers_activity', function(req, res){
    res.render('drivers_activity');
});
app.get('/marketing_panel', function(req, res){
    res.render('marketing_panel');
});
app.get('/socketsExample', function(req, res) {
    res.sendFile(__dirname + '/views/socketsExample.html');
});
app.get('/pubnubExample', function(req, res) {
    res.sendFile(__dirname + '/views/pubnubExample.html');
});

app.get('/devapi', function (req, res) {
    res.render('devapi');
});

// Login / signup API's ===============================================================

app.post('/rating',                         rating.rating);
app.post('/email_login',                    userlogin.email_login);
app.post('/customer_fb_registeration_form', userlogin.login_fb);
app.use('/customer_registeration',          multipartMiddleware);
app.post('/customer_registeration',         userlogin.register_a_user);
app.post('/switch_to_driver_mode',          userlogin.switch_to_driver_mode);
app.post('/logout',                         userlogin.logout);
app.post('/forgot_password',                userlogin.forgotPasswordFromEmail);
app.post('/request_now',                    userlogin.request_now);
app.post('/make_me_driver_request',         userlogin.makeMeDriverRequest);
app.post('/send_otp_via_call',              userlogin.send_otp_via_call);
app.post('/get_missed_rides',               user.get_missed_rides);
app.post('/booking_history',                user.booking_history);
app.post('/get_driver_current_location',    user.get_driver_current_location);
app.post('/user_profile_information',       userlogin.UserProfileInformation);

app.use('/edit_customer_profile',           multipartMiddleware);
app.post('/edit_customer_profile',          userlogin.editCustomerProfile);
app.post('/reset_password',                 userlogin.resetPassword);
//app.post('/userData',               rating.userData);
//app.post('/driver_details',                 userlogin.get_driver_details);
//app.post('/fav_locations',          rating.fav_locations);
//app.post('/get_fav_locations',      rating.get_fav_locations);
//app.post('/delete_fav_locations',   rating.delete_fav_locations);
//app.post('/access_token',                   userlogin.access_token_login);
//app.post('/get_otp_using_number',           userlogin.get_otp_using_number);
//app.post('/logout2',                        userlogin.logout2);                             // For testing
//app.post('/invite_friends',                 user.invite_friends);
//app.post('/blacklist_email',                user.blacklist_email);
//app.head('/blacklist_email',                user.blacklist_email);
//app.post('/block_app_rating_popup',         user.blockRatingPopupForUser);
app.post('/update_manual_destination',          userlogin.updateManualDestinationInfo);         // By customer after confirmation of ride acceptance

// APIs for rating  ===============================================================

app.post('/rate_the_driver_or_customer',                rating.rateTheDriverOrCustomer);
app.post('/skip_rating_by_customer',        rating.skipRatingByCustomer);

// Driver specific APIs ===============================================================

app.post('/login_driver_via_email',             driverLogin.emailLogin);
app.post('/login_driver_via_access_token',      driverLogin.accessTokenLogin);
app.post('/logout_driver',                      driverLogin.logout);
//app.post('/forgot_password_driver',             driverLogin.forgotPassword);
app.post('/change_availability',			    driver.changeAvailability);
app.post('/update_driver_location',             driver.updateLocation);


app.use('/edit_driver_profile', multipartMiddleware);
app.post('/edit_driver_profile',            driverLogin.editDriverProfile);
//app.get('/report_dedicated_drivers_timeout',    driver.report_dedicated_drivers_timeout);
//app.post('/get_driver_activity_logs',           driver.getDriverActivityLogs);

// One time functions   ===============================================================

app.post('/run_once',       one_time_functions.runTheFunction);
    
// APIs for the crones running  ===============================================================

app.get('/crone',                               crone.crone_to_calculate_drivers_online_each_half_hour);
app.get('/remove_inactive_engagements',         crone.removeInactiveEngagements);
app.get('/crone_to_update',                     crone.update_lat_long_of_driver_to_zero);
app.get('/send_mail_for_offline_drivers',       crone.send_mail_for_offline_drivers);
app.get('/send_mail_for_all_drivers',           crone.send_mail_for_all_drivers);
app.get('/make_verification_calls',             crone.make_verification_calls);
app.get('/notify_dormant_users',                crone.sendNotificationToDormantUsers);
app.get('/notify_for_referrals',                crone.sendNotificationForReferrals);
app.get('/notify_about_expiring_coupons',       crone.sendNotificationForExpiringCoupons);
app.get('/expire_coupons_for_yesterday',        crone.expireCouponsForYesterday);
//app.get('/send_mail_for_unavailable_drivers',   crone.send_mail_for_unavailable_drivers);


// all the calls for new dispatcher should be here  ===============================================================

app.post('/find_a_driver',                  ride_parallel_dispatcher.find_driver_in_area);
app.post('/request_ride',                   ride_parallel_dispatcher.request_ride);
app.post('/acknowledge_request', 			ride_parallel_dispatcher.acknowledge_request);
app.post('/acknowledge_manual_engagement', 	ride_parallel_dispatcher.acknowledge_manual_engagement);
app.post('/accept_a_request',               ride_parallel_dispatcher.accept_a_request);
app.post('/reject_a_request',               ride_parallel_dispatcher.reject_a_request);
app.post('/cancel_the_request',             ride_parallel_dispatcher.cancel_the_request);
app.post('/cancel_the_ride',                ride_parallel_dispatcher.cancel_the_ride);
app.post('/start_ride',                     ride_parallel_dispatcher.start_ride);
app.post('/end_ride',                       ride_parallel_dispatcher.end_ride);
app.post('/get_current_user_status',        ride_parallel_dispatcher.get_current_user_status);


app.post('/create_manual_engagement',       ride_parallel_dispatcher.create_manual_engagement);         //support
app.post('/clear_engagements_for_driver',   ride_parallel_dispatcher.clear_engagements_for_driver);     //support
app.post('/clear_engagements_for_customer', ride_parallel_dispatcher.clear_engagements_for_customer);   //support
app.post('/update_in_ride_data',            ride_parallel_dispatcher.captureRideData);

// manual engagements   ===============================================================

app.post('/insert_pickup_schedule',     manual_dispatcher.insert_pickup_schedule);
app.post('/show_pickup_schedules',      manual_dispatcher.show_pickup_schedules);
app.post('/modify_pickup_schedule',     manual_dispatcher.modify_pickup_schedule);
app.post('/remove_pickup_schedule',     manual_dispatcher.remove_pickup_schedule);
app.get('/schedule_request_if_any',     manual_dispatcher.schedule_request_if_any);                     //support

// compound APIs    ===============================================================

app.post('/start_app_using_access_token', userlogin.start_app_using_access_token);

// Testing services ===============================================================

app.post('/test_sending_sms',           testing.test_sending_sms);                                          //support
app.post('/test_email_service',         testing.test_email_service);                                        //support
app.post('/check_push_notification',    testing.check_push_notification);                                    //support

// Analytics    ===============================================================

//app.post('/generate_report_for_driver', analytics.generate_report_for_driver);
//app.post('/get_location_updates',       analytics.get_location_updates);
//app.get('/get_data_for_rides',          analytics.get_data_for_rides);
//app.get('/get_revenues',                analytics.get_revenues);
//app.get('/get_demographic_rides_data',  analytics.get_demographic_rides_data);
//app.get('/get_device_rides_data',       analytics.get_device_rides_data);
//app.get('/get_user_registration_data',  analytics.get_user_registration_data);
//app.get('/service_quality_data',        analytics.service_quality_data);
//app.get('/hourly_service_quality_data', analytics.hourly_service_quality_data);

// KPI data ===============================================================

//app.get('/rides_completed_today',   analytics.rides_completed_today);
//app.get('/rides_today',             analytics.rides_today);
//app.get('/users_registered_today',  analytics.users_registered_today);

// Discounts and coupons    ===============================================================

app.post('/get_coupons',            user.get_coupons);
app.post('/enter_code',             user.enter_code);
//app.get('/give_coupons',            crone.give_coupons);

// Endpoints    ===============================================================

//app.post('/send_sms',                       messenger.sendMessage);
//app.post('/send_email',                     mailer.sendMail);
//app.post('/send_notification',              service.send_notification);
//app.post('/blast_notification_to_users',    notifications.blast_notification_to_users);
//app.get('/blast_message',           messenger.blast_message);
//app.get('/send_mail_for_free_ride', mailer.sendMailForFreeRide);

// Information for the app  ===============================================================

app.post('/get_information',        service.get_information);

// Admin services   ===============================================================

//app.post('/block_user',                 user.block_user);
//app.post('/disable_user',               user.disable_user);
//app.post('/toggle_location_updates',    user.toggle_location_updates);


// File uploads ===============================================================

var uploads = require('./routes/uploads');
app.use('/upload_file',     multipartMiddleware);
app.post('/upload_file',    uploads.upload_file);

// Pubnub module    ===============================================================

var publisher = require('./routes/publisher');
//app.post('/acknowledge_pubnub_message',     publisher.acknowledge_pubnub_message);
//app.post('/acknowledge_notification',       publisher.acknowledge_notification);
//app.get('/start_test_pings',                publisher.start_test_pings);
//app.get('/stop_test_pings',                 publisher.stop_test_pings);

// heartbeat    ===============================================================

app.post('/acknowledge_heartbeat',          heartbeat.acknowledge_heartbeat);

// Stationing   ===============================================================

app.post('/get_nearest_station',            stationing.getNearestAvailableStation);
app.post('/acknowledge_stationing',         stationing.acknowledgeStationingRequest);
//app.post('/check_stationing_sanity',        stationing.checkStationingSanity);
//app.post('/relocate_driver',                stationing.relocateDriver);
//app.post('remove_driver_from_station',      stationing.removeDriverFromStation);
//app.get('/report_unacknowledged_stationing',stationing.reportUnacknowledgedStationing);
//app.get('/remove_drivers_from_stations',    stationing.removeDriversFromStation);

// Marketing panel APIs  ===============================================================

//app.post('/give_coupons_to_dormant_users',  marketing.giveCouponsToDormantUsers);
//app.post('/give_coupons_to_paid_users',     marketing.giveCouponsToPaidUsers);

// Make Payment APIs  ===============================================================

app.post('/get_braintree_token',               make_payments.getBraintreeToken);
app.post('/add_credit_card',                   make_payments.addCreditCard);
app.post('/list_credit_cards',                 make_payments.listCreditCards);
app.post('/change_default_credit_card',        make_payments.ChangeDefaultCreditCard);
app.post('/delete_credit_card',                make_payments.DeleteCreditCard);
app.post('/payment_on_ride_completion',        make_payments.paymentOnRideCompletion);
//app.post('/check_payment_status',              make_payments.checkPaymentStatus);



// Split fare APIs  ===============================================================

app.post('/send_split_fare_request',            split_fare.sendSplitFareRequest);
app.post('/split_fare_screen',                  split_fare.splitFareScreen);
app.post('/user_response_to_split_fare_request',split_fare.userResponseToSplitFareRequest);
app.post('/resend_split_fare_request',          split_fare.resendSplitFareRequest);


// Wait time APIs  ===============================================================
app.post('/start_end_wait',                 wait.startEndWait);

// dev apis ===============================================================

app.post('/create_driver_dispatcher',        driver_dispatcher.create_driver_dispatcher);



if(process.env.NODE_ENV == "development")
{
app.post('/makeMeFree',                 devapi.makeMeFree);
app.post('/clearLastEngagement',        devapi.clearLastEngagement);
app.post('/getSomething',               devapi.getSomething);
app.post('/makeMeDriver',               devapi.makeMeDriver);
}

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});
