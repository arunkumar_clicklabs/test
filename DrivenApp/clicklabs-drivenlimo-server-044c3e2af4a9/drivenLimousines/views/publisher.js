/**
 * Created by harsh on 12/16/14.
 */


var constants   = require('./constants');
var logging     = require('./logging');
var utils       = require('./commonfunction');

var pubnub;
var driverToChannelMap;

// Initialize this module
(function initializePublisher(){
    pubnub = require('pubnub')({
        ssl           : true,
        publish_key   : 'pub-c-e5019ac7-0d3a-4920-84e4-cab1c954904d',
        subscribe_key : 'sub-c-95eece66-84e0-11e4-b769-02ee2ddab7fe'
    });

    // Initialize the driver to channel mapping
    //driverToChannelMap = new Map();

    // subscribe to the master channel
    subscribeToMasterChannel();

    // Start publishing on the test channel
    publishToTestChannel();
})();

function subscribeToMasterChannel(){
    pubnub.subscribe({
        channel     : 'driverAuthenticationChannel',
        message     : parseMasterChannelMessages,
        connect     : function(){console.log("Server CONNECTED  for channel driverAuthenticationChannel")},
        disconnect  : function(){console.log("Server DISCONNECT for channel driverAuthenticationChannel")},
        reconnect   : function(){console.log("Server RECONNECT  for channel driverAuthenticationChannel")},
        error       : function(){console.log("Server ERROR      for channel driverAuthenticationChannel")}
    });
}

function parseMasterChannelMessages(message){
    console.log('Message from the driverAuthenticationChannel: %s', message);

    var data = JSON.parse(message);

    if(data.flag === constants.pubnub){

    }
}


exports.openChannelForDriver = function(driverId){
};

exports.publishToDriver = function(driverId, message){
    var channel = driverToChannelMap.get(driverId);
    if(typeof channel === 'undefined'){
        return new Error('The channel for the driver with id: %s don\'t exist');
    }

    pubnub.publish({
        channel     : channel,
        message     : message,
        callback    : logPublishMessageStatus.bind(null, channel, message, 'success'),
        error       : logPublishMessageStatus.bind(null, channel, message, 'failure')
    });
};

function publishToTestChannel(){
    setInterval(
        function(){
            var uuid = pubnub.uuid();
            var date = new Date();

            pubnub.publish({
                channel     : 'testChannel',
                callback    : logPublishMessageStatus.bind(null, 'testChannel', { uuid : uuid, message : 'Test message being sent through pubnub' }, 'success'),
                error       : logPublishMessageStatus.bind(null, 'testChannel', { uuid : uuid, message : 'Test message being sent through pubnub' }, 'failure'),
                message     : {
                                uuid    : uuid,
                                message : 'Test message being sent through pubnub'
                              }
            });

            var insertPubnubLog =
                "INSERT INTO `tb_pubnub_logs` " +
                "(`message_uuid`, `published`) " +
                "VALUES (?, ?)";
            connection.query(insertPubnubLog, [uuid, date], function(err, result){
                if(err){
                    logging.logDatabaseQuery("Inserting the logs for the pubnub message", err, result);
                }
            });
        },
        10000
    );
}

function logPublishMessageStatus(channel , message, status){
    var stream = null;
    if(status === 'success'){
        stream = process.stdout;
    }
    else if(status === 'failure'){
        stream = process.stderr;
    }

    stream.write('Channel\t:' + channel + '\n');
    stream.write('Message\t:' + JSON.stringify(message) + '\n');
    stream.write('Status\t:' + status + '\n');
}

exports.acknowledge_pubnub_message = function(req, res){
    logging.startSection("acknowledge_pubnub_message");
    logging.logRequest(req);

    var uuid = req.body.uuid;
    var timestamp = req.body.timestamp;

    var logMessage =
        "UPDATE `tb_pubnub_logs` " +
        "SET `received` = ? " +
        "WHERE `message_uuid` LIKE ('" + uuid + "')";
    connection.query(logMessage, [timestamp], function(err, result){
        if(err){
            logging.logDatabaseQuery("Updating the received timestamp for the pubnub message", err, result);
        }
    });

    var response = {
        flag    : constants.responseFlags.ACTION_COMPLETE,
        message : 'Pubnub acknowledgement received'
    };
    res.send(response);
};