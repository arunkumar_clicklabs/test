/* Module for sending message using Twilio */

var request = require('request');

var logging     = require('./logging');
var constants   = require('./constants');
var utils       = require('./commonfunction');

var client = undefined;
exports.sendMessageByTwillio = sendMessageByTwillio;

function getClient(){
    var accountSid = config.get('twillioCredentials.accountSid');
    var authToken = config.get('twillioCredentials.authToken');

    //require the Twilio module and create a REST client
    client = require('twilio')(accountSid, authToken);
}

function sendMessageByTwillio(to, msg){
    if(client === undefined){
        getClient();
    }
    client.messages.create(
        {to: to, from: config.get('twillioCredentials.fromNumber'), body: msg},
        function(err, message) {
            console.log("Twilio error: " + JSON.stringify(err));
            console.log("Twilio message: " + JSON.stringify(message));
            //if(err){
            //    console.log("Error in sending message: " + JSON.stringify(err));
            //}
        }
    );
}


function sendTemplateMessageBy24x7(to, message){

    var url = config.get('sendTemplateMessageBy24x7.APIUrl') +
        "EmailID=" + config.get('sendTemplateMessageBy24x7.email') +
        "&Password=" + config.get('sendTemplateMessageBy24x7.password') +
        "&MobileNo=" + to.substr(1) + "" +
        "&SenderID=" + config.get('sendTemplateMessageBy24x7.senderId') +
        "&Message=" + message +
        "&ServiceName=" + config.get('sendTemplateMessageBy24x7.serviceName');
    console.log("Sending the message using the url: " + url);
    request(url, function(err, response, body){
        console.log("Sending message through 24x7, Error: " + JSON.stringify(err));
        console.log("Sending message through 24x7, Response: " + JSON.stringify(response));
        console.log("Sending message through 24x7, Body: " + JSON.stringify(body));
    });
}


exports.sendOTP = function(name, to, otp){
    var message = "Dear " + name.split(' ')[0] + ", Your One Time Password is " + otp + ".";
    sendMessageByTwillio(to, message);
};

exports.sendReasonForBlocking = function (to, reason){
    var message = "";
    var sendMessage = true;
    switch(reason){
        case constants.blockingReasons.SUSPECTED_NUMBER:
            message = "This number is suspected to not to belong to the actual owner and we are blocking its use in "+config.get('projectName')+". To unblock please send a mail to "+config.get('emailCredentials.senderEmail');
            break;
        case constants.blockingReasons.INVALID_EMAIL:
            message = "The email ID used with this number is invalid. To unblock please send a mail to "+config.get('emailCredentials.senderEmail')+"";
            break;
        case constants.blockingReasons.TERMS_VIOLATION:
            message = "We found the usage against the Terms of Use and we have blocked this number. To unblock please send a mail to "+config.get('emailCredentials.senderEmail');
            break;
        default:
            sendMessage = false;
    }
    if(sendMessage){
        sendMessageByTwillio(to, message);
    }
};

exports.sendMessageForScheduledRide = function(customerId, pickupTime, address){
    console.log("Sending the confirmation to the user that a schedule has been entered in the system.");

    var completeDate = new Date(utils.changeTimezoneFromUtcToIst(pickupTime));
    //completeDate.setTime(completeDate.getTime() + (3600000 * 5.5));
    var date = completeDate.getDate() + "-" + completeDate.getMonth();
    var time = completeDate.getHours() + ":" + completeDate.getMinutes() + " hrs";

    var customer_info = "SELECT `user_id`,`user_name`,`phone_no` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customerId], function (err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        if (customer.length > 0) {
            customer = customer[0];

            var msg =
                "You have scheduled a "+config.get('projectName')+" ride for " + address + " on " + date + " at " + time + ". " +
                "We will confirm your booking 20 minutes before the scheduled pickup time and you will see the driver details in the "+config.get('senderEmail')+" app. " +
                "We will send you a SMS as well.";
            sendMessageByTwillio(customer.phone_no, msg);
        }
    });
};

exports.sendMessageForManualFailure = function(customer_id, pickup_time) {
    console.log("Sending message to the user that the manual scheduling of auto has failed");

    var customer_info = "SELECT `user_id`,`user_name`,`phone_no` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customer_id], function (err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        if (customer.length > 0) {
            customer = customer[0];

            var msg_user = "We are sorry. Due to exceptional circumstances we could not provide you "+config.get('projectName')+" at your scheduled pickup time. We have cancelled your request.";
            sendMessageByTwillio(customer.phone_no, msg_user);

            var msg_support = "Couldn't provide pickup scheduled for " + customer.user_name + " at " + pickup_time + ".";
            sendMessageByTwillio("+918556921929", msg_support);
        }
    });
};


exports.sendDriverInformation = function(customer_id, driver_id, pickup_time){
    var customer_info = "SELECT `phone_no` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customer_id], function (err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        customer = customer[0];

        var driver_info = "SELECT `user_name`,`phone_no` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
        connection.query(driver_info, [driver_id], function (err, driver) {
            logging.logDatabaseQuery("Fetch information for driver", err, driver, null);

            driver = driver[0];
            var message =
                driver.user_name + " (" + driver.phone_no + ") will pick you up at your scheduled pickup time of " + pickup_time + ". " +
                "Please call the driver at his number and confirm the location. In case of issues please call at +91-9023-121-121.";
            sendMessageByTwillio(customer.phone_no, message);
        });
    });
};


exports.sendMessage = function(req, res){
    var user_id = req.body.user_id;
    var message = req.body.message;

    var customer_info = "SELECT `phone_no` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [user_id], function (err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        if (customer.length > 0) {
            customer = customer[0];

            sendMessage(customer.phone_no, message);
            res.send(JSON.stringify({log: 'message sent'}));
        }
        else{
            res.send(JSON.stringify({log: 'user doesn\'t exist'}));
        }
    });
};



exports.blast_message = function(req, res){
    logging.startSection("blast_message");
    logging.logRequest(req);

    var message = 'You have a free ride pending with '+config.get('projectName')+' to travel in tri city upto $100. ' +
        'Please use it as soon as possible. Update your app to see the free ride coupon.';

    var get_users =
        "SELECT `phone_no` FROM `tb_users` WHERE `date_registered` > '2014-10-31 00:00:00' && `user_id` IN " +
        "(" +
            "SELECT DISTINCT(`user_id`) FROM `tb_accounts` WHERE `status`=? && `user_id` NOT IN " +
            "(" +
                "SELECT DISTINCT(`user_id`) FROM `tb_session` WHERE `ride_acceptance_flag`=? && `date` >  timestamp(DATE_SUB(NOW(), INTERVAL 3 DAY))" +
            ")" +
        ")";
    connection.query(get_users, [constants.couponStatus.ACTIVE, constants.rideAcceptanceFlag.ACCEPTED], function(err, contacts){
        console.log("number of contacts: " + contacts.length);

        for(var i=0; i<contacts.length; i++){
            sendMessage(contacts[i].phone_no, message);
        }

        res.send("Sent the message to " + contacts.length + " users");
    });
};

exports.sendMessageForCouponsAdded = function (phone_no, number_of_coupons){
    var msg = "";
    if(number_of_coupons == 1){
        msg = "A free ride coupon has ";
    }
    else{
        msg = number_of_coupons + " free ride coupons have ";
    }
    msg += "been credited to your account. Now enjoy convenience at just a tap of a button. "+config.get('projectName');

    sendMessage(phone_no, msg);
};


exports.sendMessageForCouponsToInactiveUsers = function(phone_no){
    var msg = "Hi, You have not used "+config.get('projectName')+" for a while. We have given you another FREE Ride coupon so that you can try our services again."+config.get('emailCredentials.signature');
    sendMessage(phone_no, msg);
};

exports.sendMessageToReferringUser = function(friends_name, referred_by_code){
    var referring_user = undefined;

    function getReferringUser(){
        var get_user = "SELECT `user_name`, `phone_no`, `user_name` FROM `tb_users` WHERE `referral_code`=?";
        connection.query(get_user, [referred_by_code], function(err, user){
            logging.logDatabaseQuery("Getting the information for referring person", err, user);
            referring_user = user[0];
            callback();
        });
    }

    function sendMessage(){
        var to = referring_user.phone_no;
        var msg = "";

        console.log("Sending message to the REFERRING USER for the new user registered");
        //sendMessage(to, msg);
    }

    // Get the information for the referring user and send the email
    async.series([getReferringUser], sendMessage);

};


exports.sendMessageForFirstReferralRide = function(friends_name, referring_user){
    var to = referring_user.phone_no;
    var msg = "";

    console.log("Sending message to the REFERRING USER for the first ride taken");
    //sendMessage(to, msg);
};