/**
 * This module will be used for generating analytical reports for Jugnoo.
 */

var async = require('async');

var utils = require('./commonfunction');
var logging = require('./logging');
var constants = require('./constants');
var responses = require('./responses');

exports.generate_report_for_driver = function(req, res){
    var driver_id = req.body.driver_id;

    var manValues = [driver_id];
    var checkData = utils.checkBlank(manValues);
    if (checkData == 1)
    {
        sendParameterMissingResponse(res);
    }
    else
    {
        var get_driver_info = "SELECT `user_id`, `user_name`, `phone_no`, `date_registered`, `last_login`, `driver_date_registered`, " +
            "`todays_earnings`, `total_earnings`, `total_rating_got_user`, `total_rating_user`, `total_distance_as_driver` " +
            "FROM `tb_users` " +
            "WHERE `user_id`=?";
        connection.query(get_driver_info, [driver_id], function(err, result_driver){
            logging.logDatabaseQuery("Getting the information for the driver", err, result_driver, null);

            // Get all the engagements for the driver and generate analytics from them
            // var start_date = ;
            var get_all_engagements = "SELECT `pickup_latitude`, `pickup_location_address`, `pickup_longitude`, " +
                "`drop_latitude`, `drop_longitude`, `drop_location_address`, `distance_travelled`, `money_transacted`, " +
                "`pickup_time`, `drop_time`, `wait_time`, `current_time`, " +
                "`status`, `user_rating`, `driver_rating`, " +
                "`driver_accept_latitude`, `driver_accept_longitude`, `session_id` " +
                "FROM `tb_engagements` " +
                "WHERE `driver_id`=? ORDER BY `current_time` DESC";
            connection.query(get_all_engagements, [driver_id], function(err, result_engagements) {
                logging.logDatabaseQuery("Getting all the engagements for the driver", err, result_engagements, null);

                var num_engagements = result_engagements.length;
                var accepted_engagements = [];
                var fare_earned = 0;
                var distance_travelled = 0;
                var missed_engagements = [];
                var accepted_by_others = 0;
                var cancelled_by_customer = 0;
                var rejected_engagements = [];
                var rejected_rides = [];
                for(var i=0; i < result_engagements.length; i++){
                    var ariel_distance = utils.calculateDistance(
                        result_engagements[i].pickup_latitude, result_engagements[i].pickup_longitude, 
                        result_engagements[i].accept_latitude, result_engagements[i].accept_longitude) * Math.sqrt(2);
                    var engagement = {
                       pickup_latitude : result_engagements[i].pickup_longitude,
                       pickup_longitude : result_engagements[i].pickup_longitude,
                       pickup_address : result_engagements[i].pickup_location_address,
                       accept_latitude : result_engagements[i].driver_accept_latitude,
                       accept_longitude : result_engagements[i].driver_accept_longitude,
                       accept_distance : ariel_distance
                    };

                    switch(result_engagements[i].status){
                        case constants.engagementStatus.ENDED:
                            // Add some addition data to the engagement
                            engagement.drop_latitude = result_engagements[i].drop_latitude;
                            engagement.drop_longitude = result_engagements[i].drop_longitude;
                            engagement.drop_address = result_engagements[i].drop_location_address;
                            engagement.distance_travelled = result_engagements[i].distance_travelled;
                            engagement.money_transacted = result_engagements[i].money_transacted;
                            engagement.driver_rating = result_engagements[i].driver_rating;

                            fare_earned += engagement.money_transacted;
                            distance_travelled += engagement.distance_travelled;

                            accepted_engagements.push(engagement);
                            break;

                        case constants.engagementStatus.TIMEOUT :
                            missed_engagements.push(engagement);
                            break;
                        case constants.engagementStatus.REJECTED_BY_DRIVER :
                            rejected_engagements.push(engagement);
                            break;
                        case constants.engagementStatus.ACCEPTED_THEN_REJECTED :
                            rejected_rides.push(engagement);
                            break;
                        case constants.engagementStatus.CANCELLED_BY_CUSTOMER :
                            cancelled_by_customer++;
                            break;
                        case constants.engagementStatus.ACCEPTED_BY_OTHER_DRIVER :
                            accepted_by_others++;
                            break;
                        default :
                            break;
                    }
                }   

                var response = {
                    ride_requests : num_engagements,
                    rides_completed : accepted_engagements.length,
                    requests_missed : missed_engagements.length,
                    requests_rejected : rejected_engagements.length,
                    accpeted_and_cancelled : rejected_rides.length,
                    requests_cancelled : cancelled_by_customer,
                    accepted_by_others : accepted_by_others,
                    fare_earned : fare_earned,
                    distance_travelled : distance_travelled,
                    accepted_rides : accepted_engagements,
                    missed_rides : missed_engagements
                }
                res.send(JSON.stringify(response));
            });
        });
    }
};



exports.get_location_updates = function(req, res){
    logging.startSection("get_location_updates");
    logging.logRequest(req);

    var driver_id = req.body.driver_id;
    var start_time = req.body.start_time;
    var end_time = req.body.end_time;

    var manvalues = [driver_id, start_time, end_time];
    var checkblank = utils.checkBlank(manvalues);
    if(checkblank == 1){
        var response = {"error": "Some parameter missing", "flag": constants.responseFlags.PARAMETER_MISSING};
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    }
    else {
        var locations_query = "SELECT COUNT(`location_updated_timestamp`) AS count, CONVERT_TZ(`location_updated_timestamp`, '+00:00','+05:30') as time " +
            "FROM  `tb_updated_user_location` " +
            "WHERE  `user_id` = ? AND CONVERT_TZ(`location_updated_timestamp`, '+00:00','+05:30') BETWEEN ? AND ? " +
            "GROUP BY UNIX_TIMESTAMP(`location_updated_timestamp`) DIV 180";
        var values = [driver_id, start_time, end_time];
        connection.query(locations_query, values, function(err, result_locations) {
            logging.logDatabaseQuery("Getting the pings for the driver within an interval of 3 minutes", err, result_locations, null);
            var response = result_locations;
            logging.logResponse(response);
            res.send(JSON.stringify(result_locations));
        });
    }
};



// KPI data queries
exports.rides_completed_today = function(req, res){
    var query = "SELECT COUNT(*) as number_of_rides, SUM(`distance_travelled`) as total_distance_travelled, SUM(`money_transacted`) as total_money_transacted " +
        "FROM `tb_engagements` " +
        "WHERE `status` = ? AND DATE(`current_time`) = CURDATE()";
    var values = [constants.engagementStatus.ENDED];
    connection.query(query, values, function(err, result){
        logging.logDatabaseQuery("Getting the KPI data", err, result, null);
        res.send(JSON.stringify(result));
    });
};


exports.rides_today = function(req, res){
    var query = "SELECT COUNT(*) as number_of_rides, `status` " +
        "FROM `tb_engagements` " +
        "WHERE `status` IN (?, ?, ?) AND DATE(`current_time`) = CURDATE() GROUP BY `status`";
    var values = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED, constants.engagementStatus.ENDED];
    connection.query(query, values, function(err, result){
        logging.logDatabaseQuery("Getting the KPI data", err, result, null);

        for (var i=0; i<result.length; i++){
            result[i].status = utils.engagementStatusToString(result[i].status);
        }

        res.send(JSON.stringify(result));
    });
};


exports.users_registered_today = function(req, res){
    var query = "SELECT COUNT(*) as number_of_users_registered " +
        "FROM `tb_users` " +
        "WHERE DATE(`date_registered`) = CURDATE()";
    connection.query(query, [], function(err, result){
        logging.logDatabaseQuery("Getting the KPI data", err, result, null);
        res.send(JSON.stringify(result));
    });
};


/*
APIs FOR GETKOBOARD
 */

function getNumberOfRequests(requests, start_date, for_all_day, callback){
    var dates = [];

    var get_number_requests = "";
    if (for_all_day === 1){
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(`date`,'%Y-%m-%d') as `date` " +
            "FROM `tb_session` " +
            "WHERE DATE(`date`)>? GROUP BY DATE(`date`)";
    }
    else{
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(`date`,'%Y-%m-%d') as `date` " +
            "FROM `tb_session` " +
            "WHERE DATE(`date`)>? && TIME(`date`) < TIME(NOW()) GROUP BY DATE(`date`)";
    }
    connection.query(get_number_requests, [start_date], function(err, data) {
        logging.logDatabaseQuery("Getting the number of sessions grouped by date", err, data);

        var i = 0;
        for (i = 0; i < data.length; i++) {
            requests.push(data[i].number_requests);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(requests, dates, start_date, end_date);

        callback();
    });
}

function getRidesCompleted(rides_completed, start_date, for_all_day, callback){
    var dates = [];

    var get_rides_completed = "";
    if (for_all_day === 1){
        get_rides_completed =
            "SELECT COUNT(*) as `number_rides`, DATE_FORMAT(`current_time`,'%Y-%m-%d') as `date` " +
            "FROM `tb_engagements` " +
            "WHERE `status`=? && DATE(`current_time`)>? GROUP BY DATE(`current_time`)";
    }
    else{
        get_rides_completed =
            "SELECT COUNT(*) as `number_rides`, DATE_FORMAT(`current_time`,'%Y-%m-%d') as `date` " +
            "FROM `tb_engagements` " +
            "WHERE `status`=? && DATE(`current_time`)>? && TIME(`current_time`) < TIME(NOW()) GROUP BY DATE(`current_time`)";
    }
    connection.query(get_rides_completed, [constants.engagementStatus.ENDED, start_date], function(err, data){
        logging.logDatabaseQuery("Getting the number of rides completed grouped by date", err, data);

        var i = 0;
        for (i = 0; i < data.length; i++) {
            rides_completed.push(data[i].number_rides);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(rides_completed, dates, start_date, end_date);

        callback();
    });
}

function getPaidRidesCompleted(paid_rides, start_date, for_all_day, callback){
    var dates = [];

    var get_paid_rides_completed = "";
    if (for_all_day === 1){
        get_paid_rides_completed =
            "SELECT COUNT(*) as `number_rides`, DATE_FORMAT(`current_time`,'%Y-%m-%d') as `date` " +
            "FROM `tb_engagements` " +
            "WHERE `status`=? && DATE(`current_time`)>? && `account_id`=0 GROUP BY DATE(`current_time`)";
    }
    else{
        get_paid_rides_completed =
            "SELECT COUNT(*) as `number_rides`, DATE_FORMAT(`current_time`,'%Y-%m-%d') as `date` " +
            "FROM `tb_engagements` " +
            "WHERE `status`=? && DATE(`current_time`)>? && `account_id`=0  && TIME(`current_time`) < TIME(NOW()) GROUP BY DATE(`current_time`)";
    }
    connection.query(get_paid_rides_completed, [constants.engagementStatus.ENDED, start_date], function(err, data){
        logging.logDatabaseQuery("Getting the number of paid rides completed grouped by date", err, data);

        var i = 0;
        for (i = 0; i < data.length; i++) {
            paid_rides.push(data[i].number_rides);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(paid_rides, dates, start_date, end_date);

        callback();
    });
}

function getRidesAccepted(rides_accepted, start_date, for_all_day, callback){
    var dates = [];

    var required_status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED, constants.engagementStatus.ENDED,
        constants.engagementStatus.ACCEPTED_THEN_REJECTED, constants.engagementStatus.CANCELLED_ACCEPTED_REQUEST];
    var get_rides_accepted = "";
    if (for_all_day === 1){
        get_rides_accepted =
            "SELECT COUNT(*) as `number_rides`, DATE_FORMAT(`current_time`,'%Y-%m-%d') as `date` " +
            "FROM `tb_engagements` " +
            "WHERE `status` IN (" + required_status.toString() + ") && DATE(`current_time`)>? GROUP BY DATE(`current_time`)";
    }
    else{
        get_rides_accepted =
            "SELECT COUNT(*) as `number_rides`, DATE_FORMAT(`current_time`,'%Y-%m-%d') as `date` " +
            "FROM `tb_engagements` " +
            "WHERE `status` IN (" + required_status.toString() + ") && DATE(`current_time`)>? && TIME(`current_time`) < TIME(NOW()) GROUP BY DATE(`current_time`)";
    }
    var values = [start_date];
    connection.query(get_rides_accepted, values, function(err, data){
        logging.logDatabaseQuery("Getting the number of rides accepted grouped by date", err, data);

        var i = 0;
        for (i = 0; i < data.length; i++) {
            rides_accepted.push(data[i].number_rides);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(rides_accepted, dates, start_date, end_date);

        callback();
    });
}

function getRidesCancelled(rides_cancelled, start_date, for_all_day, callback){
    var dates = [];
    var required_status = [constants.engagementStatus.ACCEPTED_THEN_REJECTED, constants.engagementStatus.CANCELLED_ACCEPTED_REQUEST];
    var get_rides_rejected = "";
    if (for_all_day === 1){
        get_rides_rejected =
            "SELECT COUNT(*) as `number_rides`, DATE_FORMAT(`current_time`,'%Y-%m-%d') as `date` " +
            "FROM `tb_engagements` " +
            "WHERE `status` IN (" + required_status.toString() + ") && DATE(`current_time`)>? GROUP BY DATE(`current_time`)";
    }
    else{
        get_rides_rejected =
            "SELECT COUNT(*) as `number_rides`, DATE_FORMAT(`current_time`,'%Y-%m-%d') as `date` " +
            "FROM `tb_engagements` " +
            "WHERE `status` IN (" + required_status.toString() + ") && DATE(`current_time`)>? && TIME(`current_time`) < TIME(NOW()) GROUP BY DATE(`current_time`)";
    }
    connection.query(get_rides_rejected, [start_date], function(err, data){
        logging.logDatabaseQuery("Getting the number of rides rejected grouped by date", err, data);

        var i = 0;
        for (i = 0; i < data.length; i++) {
            rides_cancelled.push(data[i].number_rides);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(rides_cancelled, dates, start_date, end_date);

        callback();
    });
}

function getFirstTimeRides(first_time_rides, start_date, for_all_day, callback){
    var dates = [];

    // We are not providing this query for the format ("till this time of the day")
    if(for_all_day !== 1){
        callback(true);
    }
    else{
        var get_first_time_rides =
            "SELECT COUNT( * ) AS `number_rides` , DATE_FORMAT( `drop_time` , '%Y-%m-%d' ) AS `date` " +
            "FROM " +
            "(SELECT `drop_time` , `user_id` FROM `tb_engagements` WHERE `status`=? GROUP BY `user_id`) AS A " +
            "WHERE A.drop_time > ? GROUP BY DATE( A.drop_time )";
        connection.query(get_first_time_rides, [constants.engagementStatus.ENDED, start_date], function(err, data){
            logging.logDatabaseQuery("Getting the number of first time rides completed grouped by date", err, data);

            var i = 0;
            for (i = 0; i < data.length; i++) {
                first_time_rides.push(data[i].number_rides);
                dates.push(data[i].date);
            }

            var end_date = new Date();
            end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
            ensureConsistencyOfNumbers(first_time_rides, dates, start_date, end_date);

            callback();
        });
    }
}

function addDay(date){
    var newDate = new Date(date);
    newDate.setTime(newDate.getTime() + 86400000); // add a date
    var year = newDate.getFullYear().toString();
    var month = (newDate.getMonth() + 1).toString();
    month = month.length == 1 ? "0" + month : month;
    var date = newDate.getDate().toString();
    date = date.length == 1 ? "0" + date : date;
    return year + "-" + month + "-" + date;
}

function ensureConsistencyOfDates(dates, start_date, end_date){
    var i=0;
    if(dates.length == utils.timeDifferenceInDays(start_date, end_date)){
        return;
    }
    else{
        for(i=0; i<dates.length; i++){
            if(i === 0){
                if(utils.timeDifferenceInDays(start_date, dates[i]) > 0){
                    dates.splice(0, 0, start_date);
                }
            }

            if(utils.timeDifferenceInDays(dates[i], dates[i+1]) > 1){
                dates.splice(i+1, 0, addDay(dates[i]));
            }

            if(i === supplied_dates.length - 1){
                if(utils.timeDifferenceInDays(supplied_dates[i], end_date) > 0){
                    dates.splice(i+1, 0, addDay(dates[i]));
                }
            }
        }
    }
}

function getConsistentDates(dates, start_date, end_date){
    dates.push(addDay(start_date));

    var i=0;
    while(utils.timeDifferenceInDays(dates[i], end_date) > 0){
        dates.push(addDay(dates[i]));
        i++;
    }
}

function ensureConsistencyOfNumbers(supplied_numbers, supplied_dates, start_date, end_date){
    var i=0;
    if(supplied_dates.length == utils.timeDifferenceInDays(start_date, end_date)){
        return;
    }
    else{
        for(i = 0; i < supplied_dates.length; i++){
            if(i === 0){
                if(utils.timeDifferenceInDays(start_date, supplied_dates[i]) > 1){
                    supplied_dates.splice(0, 0, addDay(start_date));
                    supplied_numbers.splice(0, 0, 0);
                }
            }

            if(utils.timeDifferenceInDays(supplied_dates[i], supplied_dates[i+1]) > 1){
                supplied_dates.splice(i+1, 0, addDay(supplied_dates[i]));
                supplied_numbers.splice(i+1, 0, 0);
            }

            if(i === supplied_dates.length - 1){
                if(utils.timeDifferenceInDays(supplied_dates[i], end_date) > 0){
                    supplied_dates.splice(i+1, 0, addDay(supplied_dates[i]));
                    supplied_numbers.splice(i+1, 0, 0);
                }
            }
        }
    }
}

exports.get_data_for_rides = function(req, res){
    logging.startSection("get_data_for_rides");
    logging.logRequest(req);

    //var start_date = req.query.start_date;

    var till_now_flag = req.query.till_now;

    var request_flag = parseInt(req.query.requests);
    var rides_accepted_flag = parseInt(req.query.accepted);
    var rides_completed_flag = parseInt(req.query.completed);
    var rides_cancelled_flag = parseInt(req.query.cancelled);
    var paid_rides_flag = parseInt(req.query.paid_rides);
    var first_ride_flag = parseInt(req.query.first_ride);
    var recurring_rides_flag = parseInt(req.query.recurring_rides);

    var dates = [];
    var requests = [];
    var rides_accepted = [];
    var rides_completed = [];
    var rides_cancelled = [];
    var paid_rides = [];
    var first_time_rides = [];
    var recurring_rides = [];

    var start_date = '2014-10-27';
    var end_date = new Date();
    end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
    //ensureConsistencyOfDates(dates, start_date, end_date);
    getConsistentDates(dates, start_date, end_date);

    var asyncTasks = [];
    if(request_flag == 1)
        asyncTasks.push(getNumberOfRequests.bind(null, requests, start_date, 1 - till_now_flag));
    if(rides_accepted_flag == 1)
        asyncTasks.push(getRidesAccepted.bind(null, rides_accepted, start_date, 1 - till_now_flag));
    if(rides_cancelled_flag == 1)
        asyncTasks.push(getRidesCancelled.bind(null, rides_cancelled, start_date, 1 - till_now_flag));
    if(rides_completed_flag == 1 || recurring_rides_flag == 1)
        asyncTasks.push(getRidesCompleted.bind(null, rides_completed, start_date, 1 - till_now_flag));
    if(paid_rides_flag == 1)
        asyncTasks.push(getPaidRidesCompleted.bind(null, paid_rides, start_date, 1 - till_now_flag));
    if(first_ride_flag == 1 || recurring_rides_flag == 1){
        asyncTasks.push(getFirstTimeRides.bind(null, first_time_rides, start_date, 1 - till_now_flag));
    }

    async.parallel(asyncTasks, function(){
        var i=0;
        for (i=0; i<dates.length; i++){
            var date = dates[i].split('-');
            dates[i] = date[2] + '-' + date[1];
        }

        if(recurring_rides_flag == 1 && rides_completed.length != first_time_rides.length){
            console.log("ERROR IN ANALYTICS: " + "The length of arrays for rides completed and first time rides are not equal.");
        }
        else{
            for(i=0; i<rides_completed.length; i++){
                recurring_rides.push(rides_completed[i] - first_time_rides[i]);
            }
        }

        var response = {
            chart: {
                style: {
                    color: "#b9bbbb"
                },
                renderTo: "container",
                backgroundColor: "transparent",
                lineColor: "rgba(35,37,38,100)",
                plotShadow: false
            },
            credits: {
                enabled: false
            },
            title: {
                text: till_now_flag == 0 ? "Rides each day" : "Rides till now grouped by each day"
            },
            xAxis: {
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
                    text: "Days"
                },
                labels : {
                    rotation : 90
                },
                categories: dates
            },
            yAxis: {
                min: 0,
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
                    text: "Numbers"
                }
            },
            legend: {
                itemStyle: {
                    color: "#b9bbbb"
                },
                layout: "vertical",
                align: "right",
                verticalAlign: "middle",
                borderWidth: 0
            },
            series: []
        };

        if(request_flag == 1)
            response.series.push({color: "#108ec5", name: "Requests", data: requests});
        if(rides_completed_flag == 1)
            response.series.push({color: "#99CC33", name: "Completed", data: rides_completed});
        if(rides_accepted_flag == 1)
            response.series.push({color: "#ee5728", name: "Rides Accepted", data: rides_accepted});
        if(rides_cancelled_flag == 1)
            response.series.push({color: "#339900", name: "Rides Cancelled", data: rides_cancelled});
        if(paid_rides_flag == 1)
            response.series.push({color: "#009900", name: "Paid", data: paid_rides});
        if(first_ride_flag == 1)
            response.series.push({color: "#ff004c", name: "First Time", data: first_time_rides});
        if(recurring_rides_flag == 1)
            response.series.push({color: "#5e41d2", name: "Recurring", data: recurring_rides});

        //logging.logResponse(response);
        res.send(JSON.stringify(response));
    });
};





function getRevenues(all_rides_revenue, free_rides_revenue, percentages, start_date, callback_main){
    function getRevenueForAllRides(all_rides_revenue, callback){
        console.log("Getting revenues for all the rides");
        var dates = [];

        var get_all_rides_revenues =
            "SELECT SUM(`money_transacted`) as `money_transacted`, DATE_FORMAT(`current_time`,'%Y-%m-%d') as `date` " +
            "FROM `tb_engagements` " +
            "WHERE `status`=? && DATE(`current_time`)>? GROUP BY DATE(`current_time`)";
        connection.query(get_all_rides_revenues, [constants.engagementStatus.ENDED, start_date], function(err, data){
            //logging.logDatabaseQuery("Getting revenues for all the rides", err, data);

            if(err){
                return callback(err);
            }

            var i = 0;
            for (i = 0; i < data.length; i++) {
                all_rides_revenue.push(data[i].money_transacted);
                dates.push(data[i].date);
            }

            var end_date = new Date();
            end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
            ensureConsistencyOfNumbers(all_rides_revenue, dates, start_date, end_date);

            callback();
        });
    }

    function getRevenueForFreeRides(free_rides_revenue, callback){
        console.log("Getting revenues for all the free rides");

        var dates = [];

        var get_all_rides_revenues =
            "SELECT SUM(`money_transacted`) as `money_transacted`, DATE_FORMAT(`current_time`,'%Y-%m-%d') as `date` " +
            "FROM `tb_engagements` " +
            "WHERE `status`=? && `account_id`>0 && DATE(`current_time`)>? GROUP BY DATE(`current_time`)";
        connection.query(get_all_rides_revenues, [constants.engagementStatus.ENDED, start_date], function(err, data){
            //logging.logDatabaseQuery("Getting revenues for all the free rides", err, data);

            if(err){
                return callback(err);
            }

            var i = 0;
            for (i = 0; i < data.length; i++) {
                free_rides_revenue.push(data[i].money_transacted);
                dates.push(data[i].date);
            }

            var end_date = new Date();
            end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
            ensureConsistencyOfNumbers(free_rides_revenue, dates, start_date, end_date);

            callback();
        });
    }

    async.parallel(
        [
            getRevenueForAllRides.bind(null, all_rides_revenue),
            getRevenueForFreeRides.bind(null, free_rides_revenue)
        ],
        function(err){
            console.log("Called back with err: " + err);
            if(err){
                callback_main(err);
            }

            if(all_rides_revenue.length != free_rides_revenue.length){
                console.log("ERROR IN ANALYTICS: " + "The length of arrays for all rides revenue and free rides revenue are not equal.");
                callback_main(true);
            }
            else{
                var i=0;
                for( i=0; i<all_rides_revenue.length; i++){
                    if(all_rides_revenue[i] > 0){
                        percentages[i] = free_rides_revenue[i]/all_rides_revenue[i];
                    }
                    else{
                        percentages[i] = 0;
                    }
                }
                callback_main();
            }
        }
    );
}

exports.get_revenues = function(req, res) {
    logging.startSection("get_data_for_rides");
    logging.logRequest(req);

    var dates = [];
    var all_rides_revenue = [];
    var free_rides_revenue = [];
    var paid_rides_revenue = [];
    var percentages = [];

    var start_date = '2014-10-31';
    var end_date = new Date();
    end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
    getConsistentDates(dates, start_date, end_date);

    var asyncTasks = [];
    asyncTasks.push(getRevenues.bind(null, all_rides_revenue, free_rides_revenue, percentages, start_date));

    async.parallel(asyncTasks,
        function (err) {
            console.log("calling back with err: " + err);
            if (err) {
                res.send("An error occurred while fetching the data for revenues");
            }
            else {
                var i = 0;
                for (i = 0; i < dates.length; i++) {
                    var date = dates[i].split('-');
                    dates[i] = date[2] + '-' + date[1];
                }

                for (i = 0; i < all_rides_revenue.length; i++) {
                    paid_rides_revenue[i] = all_rides_revenue[i] - free_rides_revenue[i];
                }

                var response = {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Revenues per day'
                    },
                    xAxis: {
                        labels : {
                            rotation : 90
                        },
                        categories: dates
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Revenues in INR'
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: 'gray'
                            }
                        }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.x + '</b><br/>' +
                                this.series.name + ': ' + this.y + '<br/>' +
                                'Total: ' + this.point.stackTotal + '<br/>' +
                                'Percentage: ' + this.y/this.point.stackTotal;
                        },
                        valueDecimals: 2
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false,
                                color: 'white',
                                style: {
                                    textShadow: '0 0 3px black, 0 0 3px black'
                                }
                            }
                        }
                    },
                    series: [
                        //{
                        //    name: 'All rides',
                        //    data: all_rides_revenue
                        //},
                        {
                            name: 'Free',
                            data: free_rides_revenue
                        },
                        {
                            name: 'Paid',
                            data: paid_rides_revenue
                        }
                    ]
                };
                res.send(response);
            }
        }
    );
};



function getRidesByDemography(rides, area, start_date, callback){
    var dates = [];

    var get_rides_by_area = "SELECT COUNT(*) AS `number_rides` , DATE_FORMAT( `pickup_time` , '%Y-%m-%d' ) AS `date` " +
        "FROM `tb_engagements` " +
        "WHERE `status`=? && DATE(`pickup_time`) > ? && `pickup_location_address` LIKE '%" + area + "%' " +
        "GROUP BY DATE(`pickup_time`)";
    connection.query(get_rides_by_area, [constants.engagementStatus.ENDED, start_date], function(err, data){
        //logging.logDatabaseQuery("Getting the number of rides grouped by date from " + area, err, data);

        if(err){
            return callback(err);
        }

        var i = 0;
        for (i = 0; i < data.length; i++) {
            rides.push(data[i].number_rides);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(rides, dates, start_date, end_date);

        callback();
    });
}

exports.get_demographic_rides_data = function(req, res){
    logging.startSection("get_demographic_rides_data");
    logging.logRequest(req);

    var dates = [];
    var rides_chandigarh = [];
    var rides_panchkula = [];
    var rides_mohali = [];

    var start_date = '2014-10-31';
    var end_date = new Date();
    end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
    getConsistentDates(dates, start_date, end_date);

    var asyncTasks = [];
    asyncTasks.push(getRidesByDemography.bind(null, rides_chandigarh, "Chandigarh", start_date));
    asyncTasks.push(getRidesByDemography.bind(null, rides_panchkula, "Panchkula", start_date));
    asyncTasks.push(getRidesByDemography.bind(null, rides_mohali, "Mohali", start_date));

    async.parallel(asyncTasks, function() {
        var i = 0;
        for (i = 0; i < dates.length; i++) {
            var date = dates[i].split('-');
            dates[i] = date[2] + '-' + date[1];
        }

        var response = {
            chart: {
              //  type: 'column'
                style: {
                    color: "#b9bbbb"
                },
                renderTo: "container",
                backgroundColor: "transparent",
                lineColor: "rgba(35,37,38,100)",
                plotShadow: false
            },
            title: {
                text: 'Demographical distribution Origin of Rides'
            },
            xAxis: {
                labels : {
                    rotation : 90
                },
                categories: dates
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of rides'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: 'gray'
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y + '<br/>' +
                        'Total: ' + this.point.stackTotal + '<br/>';
                },
                valueDecimals: 2
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: 'white'
                    }
                }
            },
            series: [
                {
                    name: 'Chandigarh',
                    data: rides_chandigarh
                },
                {
                    name: 'Panchkula',
                    data: rides_panchkula
                },
                {
                    name: 'Mohali',
                    data: rides_mohali
                }
            ]
        };
        res.send(response);
    });
};



function getRidesByDevice(rides, device, start_date, callback){
    var dates = [];

    var get_rides_by_device = "SELECT COUNT(*) AS `number_rides` , DATE_FORMAT( engagements.pickup_time , '%Y-%m-%d' ) AS `date` " +
        "FROM " +
            "(SELECT `user_id`, `pickup_time` FROM `tb_engagements` WHERE `status`=? && DATE(`pickup_time`) > ?) as `engagements` " +
        "JOIN " +
            "(SELECT `user_id` FROM `tb_users` WHERE `device_type`=?) as `users` " +
        "WHERE engagements.user_id = users.user_id " +
        "GROUP BY DATE(engagements.pickup_time)";
    connection.query(get_rides_by_device, [constants.engagementStatus.ENDED, start_date, device], function(err, data){
        //logging.logDatabaseQuery("Getting the number of rides grouped by device type: " + device, err, data);

        if(err){
            return callback(err);
        }

        var i = 0;
        for (i = 0; i < data.length; i++) {
            rides.push(data[i].number_rides);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(rides, dates, start_date, end_date);

        callback();
    });
}

exports.get_device_rides_data = function(req, res){
    logging.startSection("get_device_rides_data");
    logging.logRequest(req);

    var dates = [];
    var rides_android = [];
    var rides_iOS = [];

    var start_date = '2014-10-31';
    var end_date = new Date();
    end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
    getConsistentDates(dates, start_date, end_date);

    var asyncTasks = [];
    asyncTasks.push(getRidesByDevice.bind(null, rides_android, constants.deviceType.ANDROID, start_date));
    asyncTasks.push(getRidesByDevice.bind(null, rides_iOS, constants.deviceType.iOS, start_date));

    async.parallel(asyncTasks, function() {
        var i = 0;
        for (i = 0; i < dates.length; i++) {
            var date = dates[i].split('-');
            dates[i] = date[2] + '-' + date[1];
        }

        var response = {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Rides by device type'
            },
            xAxis: {
                labels : {
                    rotation : 90
                },
                categories: dates
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of rides'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: 'gray'
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y + '<br/>' +
                        'Total: ' + this.point.stackTotal + '<br/>';
                },
                valueDecimals: 2
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: 'white'
                    }
                }
            },
            series: [
                {
                    name: 'Android',
                    data: rides_android
                },
                {
                    name: 'iOS',
                    data: rides_iOS
                }
            ]
        };
        res.send(response);
    });
};


function getRegisteredUserNumbers(num_users, start_date, registration_flag, device_flag, referral_flag, callback){
    var dates = [];

    var registration_status = [];
    var device_type = [];

    if(registration_flag == 0){
        registration_status.push(0);
        registration_status.push(1);
    }
    else{
        registration_status.push(1);
    }

    if(device_flag == 0 || device_flag == 1){
        device_type.push(device_flag);
    }
    else if(device_flag == 2){
        device_type.push(0);
        device_type.push(1);
    }

    var referral_condition = "";
    if(referral_flag == 1){
        referral_condition = " && referred_by!=0 ";
    }

    var get_registered_numbers = "SELECT COUNT(*) AS `num_users`, DATE_FORMAT( `date_registered` , '%Y-%m-%d' ) as `date` " +
        "FROM  `tb_users` " +
        "WHERE `verification_status` IN (" + registration_status.toString() + ") && `device_type` IN (" + device_type.toString() + ") && DATE(`date_registered`) > ?" + referral_condition +
        "GROUP BY DATE(`date_registered`)";
    connection.query(get_registered_numbers, [start_date], function(err, data){
        //logging.logDatabaseQuery("Getting the number of users registered " + registration_flag + " " + device_flag + " " + referral_flag, err, data);

        if(err){
            return callback(err);
        }

        var i = 0;
        for (i = 0; i < data.length; i++) {
            num_users.push(data[i].num_users);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(num_users, dates, start_date, end_date);

        callback();
    });
}

function getRegisteredByDrivers(num_users, start_date, callback){
    var dates = [];
    var get_registered_numbers =
        "SELECT COUNT(*) as `num_users`, DATE_FORMAT(users.date_registered, '%Y-%m-%d') as `date` " +
        "FROM " +
            "(SELECT `user_id`, `pickup_time` FROM `tb_engagements` WHERE `status`=3 && DATE(`pickup_time`)>?) as `engagements` " +
        "JOIN " +
            "(SELECT `user_id`, `date_registered` FROM `tb_users` WHERE `date_registered`>?) as `users` " +
        "WHERE engagements.user_id = users.user_id AND TIMESTAMPDIFF(MINUTE, users.date_registered, engagements.pickup_time) < 15 GROUP BY DATE(users.date_registered)";
    connection.query(get_registered_numbers, [start_date, start_date], function(err, data){
        //logging.logDatabaseQuery("Getting the number of users registered by drivers", err, data);

        if(err){
            return callback(err);
        }

        var i = 0;
        for (i = 0; i < data.length; i++) {
            num_users.push(data[i].num_users);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(num_users, dates, start_date, end_date);

        callback();
    });
}

/* 0 = all users, 1 = verified users */
exports.get_user_registration_data = function(req, res){
    logging.startSection("get_user_registration_data");
    logging.logRequest(req);

    var dates = [];
    var users_registered = [];
    var users_verified = [];
    var users_unverified = [];
    var users_registered_iOS = [];
    var referred_users = [];
    var driver_registrations = [];

    var start_date = '2014-10-31';
    var end_date = new Date();
    end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
    getConsistentDates(dates, start_date, end_date);

    var asyncTasks = [];
    asyncTasks.push(getRegisteredUserNumbers.bind(null, users_registered, start_date, 0 /* all users */, 2 /* all devices */, 0 /* all */));
    asyncTasks.push(getRegisteredUserNumbers.bind(null, users_verified, start_date, 1 /* verified users */, 2 /* all devices */, 0 /* all */));
    asyncTasks.push(getRegisteredUserNumbers.bind(null, users_registered_iOS, start_date, 1 /* all users */, 1 /* iOS */, 0 /* all */));
    asyncTasks.push(getRegisteredUserNumbers.bind(null, referred_users, start_date, 0 /* all users */, 2 /* all devices */, 1 /* referred by someone */));
    asyncTasks.push(getRegisteredByDrivers.bind(null, driver_registrations, start_date));

    async.parallel(asyncTasks, function() {
        var i = 0;
        for (i = 0; i < dates.length; i++) {
            var date = dates[i].split('-');
            dates[i] = date[2] + '-' + date[1];
        }

        for (i = 0; i < users_registered.length; i++) {
            users_unverified[i] = users_registered[i] - users_verified[i];
        }

        var response = {
            chart: {
                style: {
                    color: "#b9bbbb"
                },
                renderTo: "container",
                backgroundColor: "transparent",
                lineColor: "rgba(35,37,38,100)",
                plotShadow: false
            },
            credits: {
                enabled: false
            },
            title: {
                text: "User registrations"
            },
            xAxis: {
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
                    text: "Days"
                },
                labels : {
                    rotation : 90
                },
                categories: dates
            },
            yAxis: {
                min: 0,
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
                 //   text: "Numbers"
                }
            },
            legend: {
                itemStyle: {
                    color: "#b9bbbb"
                },
                layout: "vertical",
                align: "right",
                verticalAlign: "middle",
                borderWidth: 0
            },
            series: []
        };

        response.series.push({color: "#28a6d7", name: "Verified", data: users_verified});
      //  response.series.push({color: "#380002", name: "Registered and unverified", data: users_unverified});
     //   response.series.push({color: "#4749b8", name: "iOS registrations", data: users_registered_iOS});
        response.series.push({color: "#2dd251", name: "Referral", data: referred_users});
        response.series.push({color: "#363963", name: "Referral by driver", data: driver_registrations});

        //logging.logResponse(response);
        res.send(JSON.stringify(response));
    });
};



var sessionStatus = {
    UNDEFINED               : -1,
    NO_DRIVERS_AVAILABLE    : 1,
    ROGUE_CUSTOMER          : 2,
    CANCELLED_BY_USER       : 3,
    NOT_PROCESSED           : 4,
    RIDE_CANCELLED_BY_DRIVER: 5,
    RIDE_COMPLETED          : 6
};

function getSessionStatus(session, callback){
    // If the session was cancelled by the driver
    if(session.cancelled_by_user == 1){
        session.status = sessionStatus.CANCELLED_BY_USER;
        return callback(null);
    }

    // If the session timed out
    if(session.is_active == constants.sessionStatus.TIMED_OUT ||
        (session.is_active == constants.sessionStatus.INACTIVE && session.ride_acceptance_flag == constants.rideAcceptanceFlag.NOT_YET_ACCEPTED)){
        if(session.requested_drivers == 0){
            if(session.is_user_blocked == 1){
                session.status = sessionStatus.ROGUE_CUSTOMER;
            }
            else{
                session.status = sessionStatus.NO_DRIVERS_AVAILABLE;
            }
            return callback(null);
        }
        else{
            session.status = sessionStatus.NOT_PROCESSED;
            return callback(null);
        }
    }

    // If the session was accepted, check the status of the engagement that was active in that case
    if(session.is_active == constants.sessionStatus.INACTIVE && session.ride_acceptance_flag == constants.rideAcceptanceFlag.ACCEPTED) {
        session.status = sessionStatus.RIDE_COMPLETED;
        return callback(null);
    }

    if(session.is_active == constants.sessionStatus.INACTIVE && session.ride_acceptance_flag == constants.rideAcceptanceFlag.ACCEPTED_THEN_REJECTED) {
        session.status = sessionStatus.RIDE_CANCELLED_BY_DRIVER;
        return callback(null);
    }

    if(session.is_active == constants.sessionStatus.ACTIVE){
        session.status = sessionStatus.UNDEFINED;
        return callback(null);
    }
}

var userIDs = [];

function calculateServiceQualityData(date){
    console.log("Calculating the service quality data for " + date);
    var cancelledByDriver = 0;
    var driversUnavailable = 0;
    var requestUnprocessed = 0;
    var cancelledByUser = 0;
    var ridesCompleted = 0;

    var getSessions =
        "SELECT * " +
        "FROM " +
        "(SELECT `session_id`, `user_id`, `is_active`, `ride_acceptance_flag`, `cancelled_by_user` FROM `tb_session` WHERE DATE(`date`) = ?) AS `sessions` " +
        "JOIN " +
        "(SELECT `user_id`, `is_blocked` AS `is_user_blocked` FROM `tb_users`) AS `users` " +
        "WHERE sessions.user_id = users.user_id ORDER BY sessions.session_id ASC";
    connection.query(getSessions, [date], function(err, sessions) {
        if (err) {
            //logging.logDatabaseQuery("Getting all the sessions to get the quality data", err, null);
            return;
        }

        var i = 0;
        var getStatusTasks = [];
        var userSessions = {};
        for (i = 0; i < sessions.length; i++) {
            getStatusTasks.push(getSessionStatus.bind(null, sessions[i]));
        }
        async.parallel(getStatusTasks, function(err){
            if(err){
                console.log("Getting the status of all the session returned an error.");
            }
            else{
                userIDs = [];
                for (i = 0; i < sessions.length; i++) {
                    var userId = sessions[i].user_id;
                    if(userSessions.hasOwnProperty(userId.toString())){
                        var startTime = userSessions[userId.toString()].timeStarted;
                        if(utils.timeDifferenceInMinutes(startTime, new Date()) > constants.SERVICE_QUALITY_WINDOW){
                            processRequestsBatchStatus(userId, userSessions[userId.toString()].status);
                            deleteUserSession(userId);
                        }
                    }

                    if (!userSessions.hasOwnProperty(userId.toString())) {
                        insertNewUserSession(userId);
                    }

                    if (sessions[i].status > userSessions[userId.toString()].status){
                        userSessions[userId.toString()].status = sessions[i].status;
                    }

                    if (userSessions[userId.toString()].status == sessionStatus.RIDE_COMPLETED){
                        processRequestsBatchStatus(userId, userSessions[userId.toString()].status);
                        deleteUserSession(userId);
                    }
                }

                for (userId in userSessions) {
                    processRequestsBatchStatus(userId, userSessions[userId.toString()].status);
                    deleteUserSession(userId);
                }

                //console.log("Request not processed for " + userIDs);

                var totalRequests = cancelledByDriver + driversUnavailable + requestUnprocessed + cancelledByUser + ridesCompleted;

                var selectData = "SELECT * FROM `tb_service_quality_data` WHERE date=?";
                connection.query(selectData, [date], function(err, result){
                    //logging.logDatabaseQuery("Selecting service quality data for " + date, err, result);

                    if(result.length == 0){
                        var insertData =
                            "INSERT INTO `tb_service_quality_data` " +
                            "(`total_requests`, `cancelled_by_user`, `cancelled_by_driver`, `drivers_unavailable`, `not_processed`, `rides_completed`, `date`) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?)";
                        var values = [totalRequests, cancelledByUser, cancelledByDriver, driversUnavailable, requestUnprocessed, ridesCompleted, date];
                        connection.query(insertData, values, function(err, result){
                            //logging.logDatabaseQuery("Adding service quality data for " + date, err, result);
                        });
                    }
                    else{
                        var insertData =
                            "UPDATE `tb_service_quality_data` " +
                            "SET `total_requests` = ?, `cancelled_by_user` = ?, `cancelled_by_driver` = ?, `drivers_unavailable` = ?, `not_processed` = ?, `rides_completed` = ? " +
                            "WHERE `date` = ?";
                        var values = [totalRequests, cancelledByUser, cancelledByDriver, driversUnavailable, requestUnprocessed, ridesCompleted, date];
                        connection.query(insertData, values, function(err, result){
                            //logging.logDatabaseQuery("Updating service quality data for " + date, err, result);
                        });
                    }
                });
            }
        });

        function deleteUserSession(userId){
            if (userSessions.hasOwnProperty(userId.toString())) {
                delete userSessions[userId.toString()];
            }
        }

        function insertNewUserSession(userId){
            userSessions[userId.toString()] = {
                timeStarted: sessions[i].date,
                status: sessionStatus.UNDEFINED
            }
        }

        function processRequestsBatchStatus(userId, status){
            switch(status){
                case sessionStatus.UNDEFINED:
                case sessionStatus.ROGUE_CUSTOMER:
                    break;
                case sessionStatus.RIDE_CANCELLED_BY_DRIVER:
                    cancelledByDriver++;
                    break;
                case sessionStatus.NO_DRIVERS_AVAILABLE:
                    driversUnavailable++;
                    break;
                case sessionStatus.NOT_PROCESSED:
                    userIDs.push(userId);
                    requestUnprocessed++;
                    break;
                case sessionStatus.CANCELLED_BY_USER:
                    cancelledByUser++;
                    break;
                case sessionStatus.RIDE_COMPLETED:
                    ridesCompleted++;
                    break;
                default:
                    console.log("SUCH A CASE SHOULD NOT HAPPEN " + JSON.stringify(userSessions[sessions[i].user_id.toString()]));
            }
        }
    });
}

function generateServiceQualityData(startDate){
    var endDate = new Date();
    endDate = endDate.getFullYear() + "-" + (endDate.getMonth() + 1) + "-" + endDate.getDate();

    var date = startDate;
    do {
        calculateServiceQualityData(date);
        date = addDay(date);
    } while(utils.timeDifferenceInDays(date, endDate) >= 0);
}

exports.service_quality_data = function(req, res) {
    logging.startSection("service_quality_data");
    logging.logRequest(req);

    var dates = [];
    var totalRequests = [];
    var ridesCompleted = [];
    var userCancelRequests = [];
    var driverCancelRequests = [];
    var unprocessedRequests = [];
    var driversUnavailable = [];

    var startDate = '2014-10-31';
    var today = new Date();
    today = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
    //generateServiceQualityData(startDate);
    calculateServiceQualityData(today);

    getConsistentDates(dates, startDate, today);

    var getData = "SELECT * FROM `tb_service_quality_data` ORDER BY `data_id` ASC";
    connection.query(getData, [], function(err, dataRows){
        if(err){
            //logging.logDatabaseQuery("Getting the service quality data for Jugnoo", err, dataRows);
            responses.errorResponse(res);
            return;
        }

        var i = 0;
        for (i = 0; i < dates.length; i++){
            var date = dates[i].split('-');
            dates[i] = date[2] + '-' + date[1];
        }

        for(i = 0; i < dataRows.length; i++){
            totalRequests.push(dataRows[i].total_requests);
            ridesCompleted.push(dataRows[i].rides_completed);
            userCancelRequests.push(dataRows[i].cancelled_by_user);
            driverCancelRequests.push(dataRows[i].cancelled_by_driver);
            unprocessedRequests.push(dataRows[i].not_processed);
            driversUnavailable.push(dataRows[i].drivers_unavailable);
        }

        var response = {
            chart: {
                style: {
                    color: "#b9bbbb"
                },
                renderTo: "container",
                backgroundColor: "transparent",
                lineColor: "rgba(35,37,38,100)",
                plotShadow: false
            },
            credits: {
                enabled: false
            },
            title: {
                text: "Service quality data"
            },
            xAxis: {
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
                    text: "Days"
                },
                labels : {
                    rotation : 90
                },
                categories: dates
            },
            yAxis: {
                min: 0,
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
             //       text: "Numbers"
                }
            },
            legend: {
                itemStyle: {
                    color: "#b9bbbb"
                },
                layout: "vertical",
                align: "right",
                verticalAlign: "middle",
                borderWidth: 0
            },
            series: []
        };

        response.series.push({color: "#108ec5", name: "Requests", data: totalRequests});
        response.series.push({color: "#986e67", name: "Completed", data: ridesCompleted});
        response.series.push({color: "#99CC33", name: "User Cancelled", data: userCancelRequests});
        response.series.push({color: "#ee5728", name: "Driver Cancelled", data: driverCancelRequests});
        response.series.push({color: "#339900", name: "Not processed", data: unprocessedRequests});
     //   response.series.push({color: "#3bc4b4", name: "Drivers unavailable", data: driversUnavailable});

        //logging.logResponse(response);
        res.jsonp(response);
    });
};





exports.hourly_service_quality_data = function(req, res){
    logging.startSection('Getting the hourly service data for today and yesterday');
    logging.logRequest(req);

    var currentTimeUtc = new Date();
    var currentTimeIst = new Date(currentTimeUtc.getTime() + 5.5 * 3600000);

    var startTimeIst   = new Date(currentTimeIst.getTime() - 24 * 3600000);
    startTimeIst.setHours(0);
    startTimeIst.setMinutes(0);
    startTimeIst.setSeconds(0);
    startTimeIst.setMilliseconds(0);
    var startTimeUtc   = new Date(startTimeIst.getTime() - 5.5 * 3600000);

    console.log('Getting sessions between ' + startTimeUtc + ' and ' + currentTimeUtc);
    console.log('Getting sessions between ' + startTimeIst + ' and ' + currentTimeIst);

    var intervals = [];
    getConsistentHoursIntervals(startTimeIst, currentTimeIst, intervals);

    fetchExistingServiceData(startTimeIst, function(err, numbers){
        var totalRequestsYesterday = [];
        var totalRequestsToday = [];
        var notProcessedYesterday = [];
        var notProcessedToday = [];

        utils.sortByKeyAsc(numbers, 'start_time');

        var numbersMap = {};
        for(i = 0; i < numbers.length; i++){
            numbersMap[numbers[i].start_time.toISOString()] = numbers[i];
        }

        var i = 0;
        var intervalStartTime;
        var flag = 0;       // 0 for yesterday,
                            // 1 for today
        for(i = 0; i < intervals.length; i++){
            if(i === 24){
                flag = 1;
            }

            intervalStartTime = intervals[i];

            var totalRequests = 0;
            if(numbersMap.hasOwnProperty(intervalStartTime)){
                totalRequests = numbersMap[intervalStartTime].total_requests;
            }

            var notProcessed = 0;
            if(numbersMap.hasOwnProperty(intervalStartTime)) {
                notProcessed = numbersMap[intervalStartTime].not_processed;
            }

            if(flag === 0){
                totalRequestsYesterday.push(totalRequests);
                notProcessedYesterday.push(notProcessed);
            }
            else{
                totalRequestsToday.push(totalRequests);
                notProcessedToday.push(notProcessed);
            }
        }

        for(i = totalRequestsToday.length; i < 24; i++){
            totalRequestsToday.push(0);
        }
        for(i = notProcessedToday.length; i < 24; i++){
            notProcessedToday.push(0);
        }

        var categories = [];
        for(i = 0; i < intervals.length && i < 24; i++){
            var time = new Date(intervals[i]);
            categories.push(time.getHours() + ':00');
        }

        var response = {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Service quality data for the last 48 hours'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: 'Numbers'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            }
        };

        response.series = [];
        response.series.push({
            name   : 'Total requests yesterday',
            data   : totalRequestsYesterday
        });
        response.series.push({
            name   : 'Not processed yesterday',
            data   : notProcessedYesterday
        });
        response.series.push({
            name   : 'Total requests today',
            data   : totalRequestsToday
        });
        response.series.push({
            name   : 'Not processed today',
            data   : notProcessedToday
        });

        res.send(response);
    });

    // Calculate the service quality data for the current hour and the previous hour
    //calculateServiceQualityDataForHour(currentTimeUtc);
    //
    //var prevHourUtc = new Date(currentTimeUtc.getTime() - 3600000);
    //calculateServiceQualityDataForHour(prevHourUtc);

    // Calculate the hourly service quality data for the today and yesterday again
    // and persist the numbers in the database
    var numbers = {};
    calculateHourlyServiceQualityData(startTimeUtc, currentTimeUtc, numbers, function(err){
    });
};

// Calculate the service quality data on hourly basis with respect to the Indian standard time
function calculateHourlyServiceQualityData(startTime, endTime, numbers, callback){
    var getSessions =
        "SELECT * " +
        "FROM " +
            "(SELECT session_id, user_id, is_active, requested_drivers, ride_acceptance_flag, cancelled_by_user, TIMESTAMPADD(MINUTE, 330, `date`) as session_time_ist " +
            "FROM tb_session " +
            "WHERE `date` > ? AND `date` < ?) AS sessions " +
        "JOIN " +
            "(SELECT user_id, is_blocked AS is_user_blocked FROM tb_users) AS users " +
        "WHERE sessions.user_id = users.user_id AND users.is_user_blocked = 0 " +
        "ORDER BY sessions.session_id ASC";
    connection.query(getSessions, [startTime, endTime], function(err, sessions){
        logging.logDatabaseQuery("Getting all the sessions to get the hourly quality data", err, sessions.length);
        if (err) {
            return;
        }

        var i = 0;
        var getStatusTasks = [];
        var userSessions = {};
        for (i = 0; i < sessions.length; i++) {
            getSessionStatusSync(sessions[i]);
            if(sessions[i].user_id == 6604){
                console.log('SESSION FOR THE USER 6604');
                console.log(JSON.stringify(sessions[i], undefined, 2));
            }
        }
        console.log('Generated the status for all the sessions');

        for (i = 0; i < sessions.length; i++) {
            var userId = sessions[i].user_id;
            var sessionTime = sessions[i].session_time_ist;

            if(userSessions.hasOwnProperty(userId.toString())){
                var startTime = userSessions[userId.toString()].timeStarted;
                if(utils.timeDifferenceInMinutes(startTime, sessionTime) > constants.SERVICE_QUALITY_WINDOW){
                    processRequestsBatchStatus(userId, userSessions[userId.toString()].startTime, userSessions[userId.toString()].status);
                    deleteUserSession(userId);
                }
                else{
                    userSessions[userId.toString()].lastSessionAt = sessionTime;
                }
            }

            if (!userSessions.hasOwnProperty(userId.toString())) {
                insertNewUserSession(userId, sessionTime);
            }

            if (sessions[i].status > userSessions[userId.toString()].status){
                userSessions[userId.toString()].status = sessions[i].status;
            }

            if (userSessions[userId.toString()].status == sessionStatus.RIDE_COMPLETED){
                processRequestsBatchStatus(userId, userSessions[userId.toString()].startTime, userSessions[userId.toString()].status);
                deleteUserSession(userId);
            }
        }

        for (userId in userSessions) {
            processRequestsBatchStatus(userId, userSessions[userId.toString()].startTime, userSessions[userId.toString()].status);
            deleteUserSession(userId);
        }

        for(var time in numbers){
            numbers[time].totalRequests =
                numbers[time].cancelledByDriver + numbers[time].driversUnavailable + numbers[time].requestUnprocessed +
                numbers[time].cancelledByUser + numbers[time].ridesCompleted;
        }

        for(var time in numbers) {
            persistHourlyServiceQualityData(time, numbers[time]);
        }

        console.log(JSON.stringify(numbers, undefined, 2));

        return process.nextTick(callback.bind(null));

        function deleteUserSession(userId){
            if (userSessions.hasOwnProperty(userId.toString())) {
                delete userSessions[userId.toString()];
            }
        }

        function insertNewUserSession(userId, timeStarted){
            userSessions[userId.toString()] = {
                startTime     : timeStarted,
                lastSessionAt : timeStarted,
                status        : sessionStatus.UNDEFINED
            }
        }

        function processRequestsBatchStatus(userId, startTime, status){
            var time = new Date(startTime);
            time.setMinutes(0);
            time.setSeconds(0);
            time.setMilliseconds(0);
            if(!numbers.hasOwnProperty(time.toISOString())){
                numbers[time.toISOString()] = {
                    cancelledByDriver  : 0,
                    driversUnavailable : 0,
                    requestUnprocessed : 0,
                    cancelledByUser    : 0,
                    ridesCompleted     : 0,
                    notProcessedFor    : []
                };
            }

            switch(status){
                case sessionStatus.UNDEFINED:
                case sessionStatus.ROGUE_CUSTOMER:
                    break;
                case sessionStatus.RIDE_CANCELLED_BY_DRIVER:
                    numbers[time.toISOString()].cancelledByDriver++;
                    break;
                case sessionStatus.NO_DRIVERS_AVAILABLE:
                    numbers[time.toISOString()].driversUnavailable++;
                    break;
                case sessionStatus.NOT_PROCESSED:
                    userIDs.push(userId);
                    numbers[time.toISOString()].requestUnprocessed++;
                    numbers[time.toISOString()].notProcessedFor.push(userId);
                    break;
                case sessionStatus.CANCELLED_BY_USER:
                    numbers[time.toISOString()].cancelledByUser++;
                    break;
                case sessionStatus.RIDE_COMPLETED:
                    numbers[time.toISOString()].ridesCompleted++;
                    break;
                default:
                    console.log("SUCH A CASE SHOULD NOT HAPPEN " + JSON.stringify(userSessions[sessions[i].user_id.toString()]));
            }
        }
    });
}

function getSessionStatusSync(session){
    // If the session was cancelled by the driver
    if(session.cancelled_by_user == 1){
        session.status = sessionStatus.CANCELLED_BY_USER;
        return;
    }

    // If the session timed out
    if(session.is_active == constants.sessionStatus.TIMED_OUT ||
        (session.is_active == constants.sessionStatus.INACTIVE && session.ride_acceptance_flag == constants.rideAcceptanceFlag.NOT_YET_ACCEPTED)){
        if(session.requested_drivers == 0){
            if(session.is_user_blocked == 1){
                session.status = sessionStatus.ROGUE_CUSTOMER;
            }
            else{
                session.status = sessionStatus.NO_DRIVERS_AVAILABLE;
            }
            return;
        }
        else{
            session.status = sessionStatus.NOT_PROCESSED;
            return;
        }
    }

    // If the session was accepted, check the status of the engagement that was active in that case
    if(session.is_active == constants.sessionStatus.INACTIVE && session.ride_acceptance_flag == constants.rideAcceptanceFlag.ACCEPTED) {
        session.status = sessionStatus.RIDE_COMPLETED;
        return;
    }

    if(session.is_active == constants.sessionStatus.INACTIVE && session.ride_acceptance_flag == constants.rideAcceptanceFlag.ACCEPTED_THEN_REJECTED) {
        session.status = sessionStatus.RIDE_CANCELLED_BY_DRIVER;
        return;
    }

    if(session.is_active == constants.sessionStatus.ACTIVE){
        session.status = sessionStatus.UNDEFINED;
        return;
    }
}

function getConsistentHoursIntervals(startTime, endTime, intervals){
    var time = new Date(startTime);

    do{
        intervals.push(time.toISOString());
        time.setTime(time.getTime() + 3600000);
    } while(time.getTime() < endTime.getTime());
};

function persistHourlyServiceQualityData(time, numbers){
    var mysqlTime = time.replace(/T/, ' ').replace(/\..+/, '');

    var getExistingDataId =
        'SELECT data_id ' +
        'FROM tb_hourly_service_quality_data ' +
        'WHERE start_time = ?';
    connection.query(getExistingDataId, [mysqlTime], function(err, existing){
        logging.logDatabaseQuery('Getting the existing entry for the hour ' + mysqlTime, err, existing);

        var insertData = '';
        var values = [];
        if(existing.length > 0){
            insertData =
                'UPDATE tb_hourly_service_quality_data ' +
                'SET total_requests = ?, cancelled_by_user = ?, cancelled_by_driver = ?, ' +
                'drivers_unavailable = ?, not_processed = ?, rides_completed = ?, not_processed_for = ? ' +
                'WHERE data_id = ?';
            values = [numbers.totalRequests, numbers.cancelledByUser, numbers.cancelledByDriver,
                numbers.driversUnavailable, numbers.requestUnprocessed, numbers.ridesCompleted, numbers.notProcessedFor.join(','),
                existing[0].data_id];
        }
        else{
            insertData =
                'INSERT INTO tb_hourly_service_quality_data ' +
                '(total_requests, cancelled_by_user, cancelled_by_driver, ' +
                'drivers_unavailable, not_processed, rides_completed, not_processed_for, start_time) ' +
                'VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
            values = [numbers.totalRequests, numbers.cancelledByUser, numbers.cancelledByDriver,
                numbers.driversUnavailable, numbers.requestUnprocessed, numbers.ridesCompleted, numbers.notProcessedFor.join(','),
                mysqlTime];
        }
        console.log(JSON.stringify(values));
        connection.query(insertData, values, function(err, result){
            logging.logDatabaseQuery('Inserting hourly service quality data', err, result);
        });
    });
}

function fetchExistingServiceData(startTime, callback){
    var mysqlTime = startTime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
    console.log(mysqlTime);
    var getData =
        'SELECT * ' +
        'FROM tb_hourly_service_quality_data ' +
        'WHERE start_time >= ?';
    connection.query(getData, [mysqlTime], function(err, numbers){
        logging.logDatabaseQuery('Fetching the existing data for hourly service quality', err, numbers.length);

        return process.nextTick(callback.bind(null, err, numbers));
    });
}

function calculateServiceQualityDataForHour(forHour){
    var thisHour = new Date(forHour);

    var prevHour = new Date(thisHour.getTime() - 3600000);
    prevHour.setMinutes(0);
    prevHour.setSeconds(0);
    prevHour.setMilliseconds(0);
    var startMysqlDate = prevHour.toISOString().replace(/T/, ' ').replace(/\..+/, '');

    var thisHourIst = new Date(thisHour.getTime() + 5.5 * 3600000);
    thisHourIst.setMinutes(0);
    thisHourIst.setSeconds(0);
    thisHourIst.setMilliseconds(0);

    console.log('Calculating numbers for hour starting at ' + thisHourIst.toISOString().replace(/T/, ' ').replace(/\..+/, ''));

    var numbers = {};

    var getSessions =
        "SELECT * " +
        "FROM " +
        "(SELECT session_id, user_id, is_active, requested_drivers, ride_acceptance_flag, cancelled_by_user, TIMESTAMPADD(MINUTE, 330, `date`) as session_time_ist " +
        "FROM tb_session " +
        "WHERE `date` > ?) AS sessions " +
        "JOIN " +
        "(SELECT user_id, is_blocked AS is_user_blocked FROM tb_users) AS users " +
        "WHERE sessions.user_id = users.user_id AND users.is_user_blocked = 0 " +
        "ORDER BY sessions.session_id ASC";
    connection.query(getSessions, [startMysqlDate], function(err, sessions){
        logging.logDatabaseQuery("Getting all the sessions to get the hourly quality data", err, sessions.length);
        if (err) {
            return;
        }

        var i = 0;
        var userSessions = {};
        for (i = 0; i < sessions.length; i++) {
            getSessionStatusSync(sessions[i]);
        }
        console.log('Generated the status for all the sessions');

        for (i = 0; i < sessions.length; i++) {
            var userId = sessions[i].user_id;
            var sessionTime = sessions[i].session_time_ist;

            if(userSessions.hasOwnProperty(userId.toString())){
                var startTime = userSessions[userId.toString()].startTime;
                if(utils.timeDifferenceInMinutes(startTime, sessionTime) > constants.SERVICE_QUALITY_WINDOW){
                    if(startTime.getTime() - thisHourIst.getTime() >= 0) {
                        processRequestsBatchStatus(userId, userSessions[userId.toString()].startTime, userSessions[userId.toString()].status);
                    }
                    deleteUserSession(userId);
                }
                else{
                    userSessions[userId.toString()].lastSessionAt = sessionTime;
                }
            }

            if (!userSessions.hasOwnProperty(userId.toString())) {
                insertNewUserSession(userId, sessionTime);
            }

            if (sessions[i].status > userSessions[userId.toString()].status){
                userSessions[userId.toString()].status = sessions[i].status;
            }

            //console.log(JSON.stringify(userSessions[userId.toString()]));

            if (userSessions[userId.toString()].status == sessionStatus.RIDE_COMPLETED){
                if(userSessions[userId.toString()].startTime.getTime() - thisHourIst.getTime() >= 0){
                    processRequestsBatchStatus(userId, userSessions[userId.toString()].startTime, userSessions[userId.toString()].status);
                }
                deleteUserSession(userId);
            }
        }

        for (userId in userSessions) {
            if(userSessions[userId.toString()].startTime.getTime() - thisHourIst.getTime() >= 0){
                processRequestsBatchStatus(userId, userSessions[userId.toString()].startTime, userSessions[userId.toString()].status);
            }
            deleteUserSession(userId);
        }

        for(var time in numbers){
            numbers[time].totalRequests =
                numbers[time].cancelledByDriver + numbers[time].driversUnavailable + numbers[time].requestUnprocessed +
                numbers[time].cancelledByUser + numbers[time].ridesCompleted;
        }

        for(var time in numbers){
            persistHourlyServiceQualityData(time, numbers[time]);
        }

        console.log(numbers);

        function deleteUserSession(userId){
            if (userSessions.hasOwnProperty(userId.toString())) {
                delete userSessions[userId.toString()];
            }
        }

        function insertNewUserSession(userId, timeStarted){
            userSessions[userId.toString()] = {
                startTime     : timeStarted,
                lastSessionAt : timeStarted,
                status        : sessionStatus.UNDEFINED
            }
        }

        function processRequestsBatchStatus(userId, startTime, status){
            var time = new Date(startTime);
            time.setMinutes(0);
            time.setSeconds(0);
            time.setMilliseconds(0);
            if(!numbers.hasOwnProperty(time.toISOString())){
                numbers[time.toISOString()] = {
                    cancelledByDriver  : 0,
                    driversUnavailable : 0,
                    requestUnprocessed : 0,
                    cancelledByUser    : 0,
                    ridesCompleted     : 0,
                    notProcessedFor    : []};
            }

            switch(status){
                case sessionStatus.UNDEFINED:
                case sessionStatus.ROGUE_CUSTOMER:
                    break;
                case sessionStatus.RIDE_CANCELLED_BY_DRIVER:
                    numbers[time.toISOString()].cancelledByDriver++;
                    break;
                case sessionStatus.NO_DRIVERS_AVAILABLE:
                    numbers[time.toISOString()].driversUnavailable++;
                    break;
                case sessionStatus.NOT_PROCESSED:
                    numbers[time.toISOString()].notProcessedFor.push(userId);
                    numbers[time.toISOString()].requestUnprocessed++;
                    break;
                case sessionStatus.CANCELLED_BY_USER:
                    numbers[time.toISOString()].cancelledByUser++;
                    break;
                case sessionStatus.RIDE_COMPLETED:
                    numbers[time.toISOString()].ridesCompleted++;
                    break;
                default:
                    console.log("SUCH A CASE SHOULD NOT HAPPEN " + JSON.stringify(userSessions[sessions[i].user_id.toString()]));
            }
        }
    });
}