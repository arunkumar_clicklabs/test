/**
 * Created by harsh on 11/19/14.
 */

var async       = require('async');

var constants   = require('./constants');
var logging     = require('./logging');
var heartbeat   = require('./heartbeat');

var count = 0;
var successful = false;
var sessions_cleared = false;

function initServer(){
    //console.log(this);
    if(successful === false){
        console.log("************************************************************************");
        console.log(" **********              INITIALING THE SERVER              ***********");
        console.log("************************************************************************");
        console.log("Number of tries for initializing the server: " + count);
        // Since, the timeouts set for the session are not there now, we need to deactivate
        // these outstanding sessions to release the customers

        var asyncTasks = [];
        asyncTasks.push(clearOutActiveSessions);
        asyncTasks.push(heartbeat.initialize_heartbeat);

        async.parallel(asyncTasks, function(){
            if(sessions_cleared === true){
                successful === true;
            }
        });
        count++;
    }
};

initServer();

function clearOutActiveSessions(callback){
    /* Check for the database connection before making the query */
    console.log("******    Clearing all the non-accepted active sessions    ******");
    if(typeof(connection) === undefined){
        console.log("SETTING TIMEOUT SINCE THE DATABASE CONNECTION IS NOT THERE YET");
        setTimeout(clearOutActiveSessions, 1000);
    }
    else{
        var get_active_sessions =
            "SELECT `session_id` FROM `tb_session` " +
            "WHERE (`is_active` = ? && `ride_acceptance_flag` = ? && `date` > timestamp(DATE_SUB(NOW(), INTERVAL 2 HOUR))) " +
                    "|| (`is_active` = ? && `date` < timestamp(DATE_SUB(NOW(), INTERVAL 10 HOUR)))";
        var values =
            [constants.sessionStatus.ACTIVE, constants.rideAcceptanceFlag.NOT_YET_ACCEPTED,
                constants.sessionStatus.ACTIVE];
        connection.query(get_active_sessions, values, function(err, sessions){
            logging.logDatabaseQuery("Getting all the non-accepted active sessions", err, sessions);

            var i=0;
            var session_ids = [0];
            for( i=0; i<sessions.length; i++){
                session_ids.push(sessions[i].session_id);
            }

            var deactivate_sessions = "UPDATE `tb_session` SET `is_active`=? WHERE `session_id` IN (" + session_ids.toString() + ")";
            var deactivate_sessions_values = [constants.sessionStatus.INACTIVE];
            connection.query(deactivate_sessions, deactivate_sessions_values, function(err, result){
                logging.logDatabaseQuery("Deactivating the session", err, result, null);
            });

            var update_engagements = "UPDATE `tb_engagements` SET `status`=? WHERE `status`=? && `session_id` IN (" + session_ids.toString() + ")";
            var update_engagements_values = [constants.engagementStatus.TIMEOUT, constants.engagementStatus.REQUESTED];
            connection.query(update_engagements, update_engagements_values, function(err, result){
                logging.logDatabaseQuery("Timing out all engagements for the sessions made inactive", err, result);
            });

            sessions_cleared === true;
            callback();
        });
    }
}