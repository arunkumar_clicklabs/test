var constants = require('./constants');

var debugging_enabled = true;

// Logging for the database queries made during execution
exports.logDatabaseQuery = function (eventFired, error, result)
{
    if(debugging_enabled)
    {
        var stream = process.stdout;
        if(error){
            stream = process.stderr;
        }

        stream.write("Event: " + eventFired + '\n');
        stream.write("\tError: " + JSON.stringify(error) + '\n');
        stream.write("\tResult: " + JSON.stringify(result) + '\n');
    }
};

exports.logDatabaseQueryError = function (eventFired, error, result)
{
    if(debugging_enabled)
    {
        process.stderr.write("Event: " + eventFired);
        process.stderr.write("\tError: " + JSON.stringify(error));
        process.stderr.write("\tResult: " + JSON.stringify(result));
    }
};

exports.logNotificationEvent = function (device_type, message, flag, payload)
{
    if(debugging_enabled)
    {
        if(device_type == constants.deviceType.ANDROID) {
            console.log("payload: " + JSON.stringify(payload));
        }
        else if (device_type == constants.deviceType.iOS) {
            console.log("message: " + JSON.stringify(message));
            console.log("flag: " + JSON.stringify(flag));
            console.log("payload: " + JSON.stringify(payload));
        }
    }
};


exports.startSection = function(section)
{
    if(debugging_enabled)
    {
        console.log("=============   " + section + "   ==============");
    }
};


exports.logRequest = function(request)
{
    if(debugging_enabled)
    {
        console.log("REQUEST: " + JSON.stringify(request.body));
    }
};


exports.logResponse = function(response)
{
    if(debugging_enabled)
    {
        console.log("RESPONSE: " + JSON.stringify(response, undefined, 2));
    }
};


exports.endSection = function()
{
    if(debugging_enabled)
    {
        console.log("==================================");
    }
};


var log4js = require('log4js');
log4js.clearAppenders();

log4js.configure({
    appenders: [
        {
            type: 'dateFile',
            filename: __dirname + config.get('logFiles.allLogsFilePath'),
            pattern: '-yyyy-MM-dd',
            category: 'all_logs',
            alwaysIncludePattern : true
        },
        {
            type: 'dateFile',
            filename: __dirname + config.get('logFiles.curiousLogsFilePath'),
            pattern: '-yyyy-MM-dd',
            category: 'curious_logs',
            alwaysIncludePattern : true
        },
        {
            type: 'dateFile',
            filename: __dirname + config.get('logFiles.notificationLogsFilePath'),
            pattern: '-yyyy-MM-dd',
            category: 'notifications',
            alwaysIncludePattern : true
        }
    ]
});

var logger          = log4js.getLogger('all_logs');
var curiousLogger   = log4js.getLogger('curious_logs');
var notificationsLogger = log4js.getLogger('notifications');
logger.setLevel('INFO');
curiousLogger.setLevel('INFO');
notificationsLogger.setLevel('INFO');

// defining some house keeping for logging sessions
logger.sessions = {};
curiousLogger.curiousSessions = [];

exports.info = function (message) {
    logger.info(message);
};

exports.addSession = function(session_data){
    console.log("ADDING " + session_data.session_id.toString() + " TO THE LOGGER");
    logger.sessions[session_data.session_id.toString()] = session_data;
};

exports.flushSession = function(session_id){
    if(logger.sessions.hasOwnProperty(session_id.toString())){
        logger.info(JSON.stringify(logger.sessions[session_id.toString()], undefined, 2));

        if(curiousLogger.curiousSessions.indexOf(session_id) > 0){
            curiousLogger.debug(JSON.stringify(logger.sessions[session_id.toString()], undefined, 2));
        }
    }
};

exports.deleteSession = function(session_id){
    if(logger.sessions.hasOwnProperty(session_id.toString())) {
        delete logger.sessions[session_id.toString()];
    }
};

exports.addEventToSession = function(session_id, event){
    console.log(session_id + ": " + JSON.stringify(event, undefined, 2));
    if(logger.sessions.hasOwnProperty(session_id.toString())) {
        if(!logger.sessions[session_id.toString()].hasOwnProperty('events')){
            logger.sessions[session_id.toString()].events = [];
        }
        logger.sessions[session_id.toString()].events.push(event);
    }
};

exports.dump_session = function(req, res){
    logging.logRequest(req);

    var session_id = req.body.session_id;

    res.send(JSON.stringify(logger.sessions[session_id.toString()]));
};




exports.addNotificationBlast = function (event, users){
    var log = {
        event   : event,
        users   : users
    }
    notificationsLogger.info(JSON.stringify(log, undefined, 2));
};