
var uuidGenerator   = require('node-uuid');

var logging         = require('./logging');
var constants       = require('./constants');
var utils           = require('./commonfunction');

exports.initialize_heartbeat    = initializeHeartbeat;
exports.acknowledge_heartbeat   = acknowledgeHeartbeat;

var hearbeatId = 0;

function initializeHeartbeat(){
    if(config.get('heartbeat') === true && hearbeatId === 0){
        hearbeatId = setInterval(
            sendHeartbeatNotification,
            120000
        );
    }
}

function sendHeartbeatNotification(){
    var getDriverDeviceTokens =
        "SELECT drivers.user_id, drivers.device_type, drivers.user_device_token FROM " +
            "(SELECT `user_id`, `device_type`, `user_device_token` " +
            "FROM `tb_users` " +
            "WHERE `app_versioncode` >= 137 AND `reg_as` IN (1,2) AND `current_user_status` = 1) AS `drivers` " +
        "JOIN " +
            "`tb_timings` AS `timings` " +
        "ON drivers.user_id = timings.driver_id " +
        "WHERE timings.start_time < NOW() AND timings.end_time >= NOW()";
    connection.query(getDriverDeviceTokens, [], function(err, drivers){
        if(err){
            logging.logDatabaseQueryError("Getting the device token for all the drivers", err, drivers);
            return;
        }

        var i = 0;
        for (i = 0; i < drivers.length; i++){
            var uuid = uuidGenerator.v4();
            sendHeartbeat(drivers[i], uuid);
            logHeartbeat(drivers[i].user_id, uuid);
        }
    });
}

function sendHeartbeat(driver, uuid){
    var message     = 'Heartbeat';
    var flag        = 1;
    var payload     = {
        flag    : constants.notificationFlags.HEARTBEAT,
        uuid    : uuid
    };

    utils.sendNotificationToDevice(driver.device_type, driver.user_device_token, message, flag, payload);
}

function logHeartbeat(driverId, uuid){
    var logHearbeat =
        "INSERT INTO `tb_heartbeat_logs` " +
        "(`driver_id`, `uuid`) " +
        "VALUES (?, ?)";
    connection.query(logHearbeat, [driverId, uuid], function(err, result){
        if(err){
            logging.logDatabaseQueryError("Inserting a logging row for drivers", err, result);
        }
    });
}

function logHeartbeatAcknowledgement(uuid, timestamp, network){
    var logHearbeat =
        "UPDATE `tb_heartbeat_logs` " +
        "SET `received` = ?, `network` = ? " +
        "WHERE `uuid` = ?";
    connection.query(logHearbeat, [timestamp, network, uuid], function(err, result){
        if(err){
            logging.logDatabaseQueryError("Updating the received timestamp for the heartbeat", err, result);
        }
    });
}

function acknowledgeHeartbeat(req, res){
    //logging.startSection("acknowledge_heartbeat");
    //logging.logRequest(req);

    var uuid        = req.body.uuid;
    var timestamp   = req.body.timestamp;
    var network     = req.body.network_name;

    logHeartbeatAcknowledgement(uuid, timestamp, network);

    res.send({
        flag    : constants.responseFlags.ACTION_COMPLETE
    });
}

