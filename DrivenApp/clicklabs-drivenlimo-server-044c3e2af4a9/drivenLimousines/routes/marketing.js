

var constants   = require('./constants');
var logging     = require('./logging');
var utils       = require('./commonfunction');


exports.giveCouponsToDormantUsers   = giveCouponsToDormantUsers;
exports.giveCouponsToPaidUsers      = giveCouponsToPaidUsers;




function giveCouponsToDormantUsers(req, res){
    logging.startSection('Giving coupons to dormant users');
    logging.logRequest(req);

    res.send('Suspended for now. Contact the tech team');
    return;

    var dormantSince    = req.body.dormant_since;
    var couponValidity  = req.body.coupon_validity;
    var password        = req.body.password;

    if(password !== constants.SUPER_ADMIN_PASSWORD){
        res.send('You are not authorized to perform this action.');
        return;
    }

    // GIVE COUPONS TO USER WHO TOOK THEIR LAST RIDE(PAID/FREE) WITH US SOME DAYS BACK AND HAVE NO COUPON IN THEIR ACCOUNT
    var message = config.get('projectName')+' is missing you. 1 Free ride has been credited to your account which expires on Sunday midnight. HURRY, Call a '+config.get('projectName')+' today.';

    var getUsers =
        'SELECT dormant_users.user_id, dormant_users.phone_no, dormant_users.user_email, dormant_users.device_type, dormant_users.user_device_token FROM ' +
            '(SELECT `user_id`, `phone_no`, `user_email`, `device_type`, `user_device_token` ' +
            'FROM `tb_users` ' +
            'WHERE `is_blocked` = 0 AND (`last_ride_on` IS NOT NULL AND DATE(`last_ride_on`) < ?)) as `dormant_users` ' +
        'LEFT JOIN ' +
            '(SELECT `user_id`, COUNT(*) AS `num_coupons` FROM `tb_accounts` WHERE `status` = 1 GROUP BY `user_id`) as `users_with_coupons` ' +
        'ON dormant_users.user_id = users_with_coupons.user_id ' +
        'WHERE `num_coupons` IS NULL';

    var values = [dormantSince, dormantSince];

    connection.query(getUsers, values, function(err, users){
        logging.logDatabaseQuery('Getting the users dormant since ' + dormantSince, err, users.length);

        var i = 0;
        for(i = 0; i < users.length; i++){
            //giveCouponToDormantUser(users[i].user_id, couponValidity);
            sendNotificationToDormantUser(users[i], message);
        }

        sendUserInformation(users, res);
    });
}




function giveCouponsToPaidUsers(req, res){
    logging.startSection('Giving coupons to dormant users');
    logging.logRequest(req);

    var lastRideBefore  = req.body.last_ride_before;
    var couponValidity  = req.body.coupon_validity;
    var password        = req.body.password;

    if(password !== constants.SUPER_ADMIN_PASSWORD){
        res.send('You are not authorized to perform this action.');
        return;
    }

    // GIVE COUPONS TO USERS WHO TOOK A PAID RIDE WITH US AND TOOK A LAST RIDE SOME DAYS BACK AND HAVE NO COUPON IN THEIR ACCOUNT
    var message = "1 FREE "+config.get('projectName')+" ride has been credited to your account. HURRY, take your free ride before it expires on Sunday, 11th Jan, midnight.";

    var getUsers =
        'SELECT interesting_users.user_id, interesting_users.phone_no, interesting_users.user_email, interesting_users.device_type, interesting_users.user_device_token ' +
        'FROM ' +
        '(SELECT users.user_id, users.user_name, users.phone_no, users.user_email, users.device_type, users.user_device_token, users_with_paid_rides.num_paid_rides ' +
        'FROM ' +
        '(SELECT user_id, user_name, phone_no, user_email, device_type, user_device_token FROM tb_users WHERE is_blocked = 0 AND last_ride_on IS NOT NULL AND DATE(last_ride_on) < ?) as users ' +
        'LEFT JOIN ' +
        '(SELECT user_id, COUNT(*) AS num_paid_rides FROM tb_engagements WHERE status = 3 AND account_id = 0 AND DATE(pickup_time) < ? GROUP BY user_id) as users_with_paid_rides ' +
        'ON users.user_id = users_with_paid_rides.user_id WHERE users_with_paid_rides.num_paid_rides IS NOT NULL) as interesting_users ' +
        'LEFT JOIN ' +
        '(SELECT user_id, COUNT(*) AS num_coupons FROM tb_accounts WHERE status = 1 GROUP BY user_id) as users_with_coupons ON interesting_users.user_id = users_with_coupons.user_id ' +
        'WHERE num_coupons IS NULL';

    var values = [lastRideBefore, lastRideBefore];

    connection.query(getUsers, values, function(err, users){
        logging.logDatabaseQuery('Getting the paid users who took their last ride before ' + lastRideBefore, err, users.length);

        var i = 0;
        for(i = 0; i < users.length; i++){
            giveCouponToDormantUser(users[i].user_id, couponValidity);
            sendNotificationToDormantUser(users[i], message);
        }

        sendUserInformation(users, res);
    });
}




function giveCouponToDormantUser(userId, couponValidity){
    var addCoupons =
        'INSERT INTO `tb_accounts` ' +
        '(`user_id`, `coupon_id`, `expiry_date`, `reason`) ' +
        'VALUES (?, 1, concat_ws(\' \',DATE(DATE_ADD(NOW(), INTERVAL ? DAY)), \'18:29:59\'), \'marketing- dormant user\')';
    connection.query(addCoupons, [userId, couponValidity], function(err, result){
        logging.logDatabaseQuery('Adding coupon for user ' + userId, err, result);
    });
}

function sendNotificationToDormantUser(user, message){
    var payload = {
        flag    : constants.notificationFlags.DISPLAY_MESSAGE,
        persist : true,
        message : message};
    var flag = 1;

    console.log(message + ', ' + user.device_type + ', ' + user.user_device_token);
    if(user.user_device_token !== ''){
        utils.sendNotificationToDevice(user.device_type, user.user_device_token, message, flag, payload);
    }
};

function sendUserInformation(users, res){
    var userInfomation = '';

    for(var i = 0; i < users.length; i++){
        userInfomation +=
            '<p>' +
            '"' + users[i].user_id + '", ' +
            '"' + users[i].phone_no + '", ' +
            '"' + users[i].user_email + '"</p>';
    }

    res.send(userInfomation);
}