/**
 * Created by harsh on 12/3/14.
 */

var logging = require('./logging');
var utils = require('./commonfunction');
var constants = require('./constants');

exports.blast_notification_to_users = function(req, res){
    logging.logDatabaseQuery("blast_notification_to_users");
    logging.logRequest(req);

    var message = req.body.message;
    var filter = parseInt(req.body.filter);
    var password = req.body.password;

    var isBlank = utils.checkBlank([message, filter, password]);
    if(isBlank){
        var response = {"error": "some parameter missing", "flag": constants.responseFlags.PARAMETER_MISSING};
        logging.logResponse(response);
        res.send(JSON.stringify(response));
        return;
    }

    if (password != constants.NOTIFICATION_BLAST_PASSWORD) {
        res.send("The password is incorrect");
        return;
    }

    var getDevices = "";
    var values = [];
    switch(filter) {
        case constants.notificationFilters.TEST:
            getDevices = "SELECT `device_type`, `user_device_token` FROM `tb_users` WHERE `user_id` IN (117, 222, 408) && `user_device_token` NOT LIKE ''";
            values = [];
            break;
        case constants.notificationFilters.ALL:
            getDevices = "SELECT `device_type`, `user_device_token` FROM `tb_users` WHERE `is_blocked` = 0 && `reg_as` = 0 && `user_device_token` NOT LIKE '' GROUP BY `user_device_token`";
            values = [];
            break;
        case constants.notificationFilters.COUPON_BUT_NO_RIDE:
            getDevices =
                "SELECT * FROM " +
                "(SELECT `user_id`, `device_type`, `user_device_token` FROM `tb_users` WHERE `is_blocked` = 0 && `reg_as` = 0 && `user_device_token` NOT LIKE '') as `devices` " +
                "JOIN " +
                "(SELECT `user_id` FROM `tb_accounts` WHERE `status` = 1 and `user_id` NOT IN (SELECT `user_id` FROM `tb_engagements` WHERE `status` = 3 && DATE(`pickup_time`) >= DATE_SUB(NOW(), INTERVAL 3 DAY))) AS `users` " +
                "WHERE devices.user_id = users.user_id GROUP BY devices.user_device_token";
        case constants.notificationFilters.NO_COUPON_NO_RIDE:
            getDevices =
                "SELECT * FROM " +
                    "(SELECT `user_id`, `device_type`, `user_device_token` FROM `tb_users` WHERE `user_device_token` NOT LIKE '' " +
                    "AND `user_id` NOT IN (SELECT `user_id` FROM `tb_engagements` WHERE `status` = 3 AND DATE(`pickup_time`) > DATE_SUB(NOW(), INTERVAL 7 DAY))) AS `no_rides_users` " +
                "LEFT JOIN " +
                    "(SELECT `user_id`, count(*) AS `num_coupons` FROM `tb_accounts` WHERE `status` = 1 GROUP BY `user_id`) AS `users_with_coupons` " +
                "ON users_with_coupons.user_id = no_rides_users.user_id " +
                "WHERE ISNULL(users_with_coupons.num_coupons)";
            break;
        case constants.notificationFilters.COUPONS_EXPIRING:
            getDevices =
                'SELECT tb_users.device_type, tb_users.user_device_token FROM ' +
                    '(SELECT DISTINCT(`user_id`) FROM `tb_accounts` where DATE(`expiry_date`) = \'2014-12-31\' and `status` = 1) as `users_with_coupons` ' +
                'JOIN ' +
                    '`tb_users` ' +
                'ON users_with_coupons.user_id = tb_users.user_id ' +
                'WHERE tb_users.reg_as = 0 AND tb_users.is_blocked = 0 AND `user_device_token` NOT LIKE \'\'';
            break;
    }

    if(getDevices === "") {
        res.send("The filter that you are using is not in the system yet.");
        return;
    }

    connection.query(getDevices,values, function(err, devices){
        if(err){
            logging.logDatabaseQuery("Getting the device tokens for the users under the filter: " + filter, err, devices);
        }
        console.log("A total of " + devices.length + " devices will receive the notification");

        res.send("The notification will be sent to " + devices.length + " devices");

        var payload = {
            flag: constants.notificationFlags.DISPLAY_MESSAGE,
            message: message};
        var notificationFlag = 1;

        var i = 0;
        for(i = 0; i < devices.length; i++){
            sendNotification(i);
        }

        function sendNotification(i) {
            var j = i;
            var deviceTypeIn = devices[j].device_type;
            var userDeviceTokenIn = devices[j].user_device_token.slice(0);
            setTimeout(function() {
                console.log(deviceTypeIn + ", " + userDeviceTokenIn);
                utils.sendNotificationToDevice(deviceTypeIn, userDeviceTokenIn, message, notificationFlag, payload);
            }, 100*j);
        }

        return;
    });
};
