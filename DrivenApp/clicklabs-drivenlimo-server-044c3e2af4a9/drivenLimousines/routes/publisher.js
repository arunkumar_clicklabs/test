/**
 * Created by harsh on 12/16/14.
 */


var constants   = require('./constants');
var logging     = require('./logging');
var utils       = require('./commonfunction');

var pubnub;
var driverToChannelMap;
var intervalId = 0;

// Initialize this module
(function initializePublisher(){
    pubnub = require('pubnub')({
        ssl           : config.get('pubnubCredentials.ssl'),
        publish_key   : config.get('pubnubCredentials.publishKey'),
        subscribe_key : config.get('pubnubCredentials.subscribeKey')
    });

    // Initialize the driver to channel mapping
    //driverToChannelMap = new Map();

    // subscribe to the master channel
    subscribeToMasterChannel();

    // Start publishing on the test channel
    //publishToTestChannel();
})();

function subscribeToMasterChannel(){
    pubnub.subscribe({
        channel     : 'driverAuthenticationChannel',
        message     : parseMasterChannelMessages,
        connect     : function(){console.log("Server CONNECTED  for channel driverAuthenticationChannel")},
        disconnect  : function(){console.log("Server DISCONNECT for channel driverAuthenticationChannel")},
        reconnect   : function(){console.log("Server RECONNECT  for channel driverAuthenticationChannel")},
        error       : function(){console.log("Server ERROR      for channel driverAuthenticationChannel")}
    });
}

function parseMasterChannelMessages(message){
    console.log('Message from the driverAuthenticationChannel: %s', message);

    var data = JSON.parse(message);

    if(data.flag === constants.pubnub){

    }
}


exports.openChannelForDriver = function(driverId){
};

exports.publishToDriver = function(driverId, message){
    var channel = driverToChannelMap.get(driverId);
    if(typeof channel === 'undefined'){
        return new Error('The channel for the driver with id: %s don\'t exist');
    }

    pubnub.publish({
        channel     : channel,
        message     : message,
        callback    : logPublishMessageStatus.bind(null, channel, message, 'success'),
        error       : logPublishMessageStatus.bind(null, channel, message, 'failure')
    });
};

function publishMessageToChannel(channel, uuid){
    var date = new Date();
    pubnub.publish({
        channel     : channel,
        callback    : logPublishMessageStatus.bind(null, channel, { uuid : uuid, message : 'Test message being sent through pubnub', sent : date.toISOString() }, 'success'),
        error       : logPublishMessageStatus.bind(null, channel, { uuid : uuid, message : 'Test message being sent through pubnub', sent : date.toISOString() }, 'failure'),
        message     : {
            uuid    : uuid,
            message : 'Test message being sent through pubnub',
            sent    : date.toISOString().replace(/T/, ' ').replace(/\..+/, '')
        }
    });

    var insertPubnubLog =
        "INSERT INTO `tb_experimentation_logs` " +
        "(`uuid`, `published`, `type`) " +
        "VALUES (?, ?, 1)";
    connection.query(insertPubnubLog, [uuid, date], function(err, result){
        if(err){
            logging.logDatabaseQuery("Inserting the logs for the pubnub message", err, result);
        }
    });
}

function sendTestNotification(uuid, deviceToken){
    // Send notification too
    var date = new Date();

    var gcm = require('node-gcm');
    var message = new gcm.Message({
        delayWhileIdle: false,
        timeToLive: 2419200,
        data: {
            message: {
                uuid    : uuid,
                message : 'Test notification being sent from the server',
                sent    : date.toISOString().replace(/T/, ' ').replace(/\..+/, '')
            },
            brand_name: config.get('androidPushSettings.brandName')
        }
    });
    var sender = new gcm.Sender(config.get('androidPushSettings.gcmSender'));
    var registrationIds = [];
    registrationIds.push(deviceToken);
    sender.send(message, registrationIds, 4, function(err, result) {
        console.log("ANDROID NOTIFICATION RESULT: " + JSON.stringify(result));
        console.log("ANDROID NOTIFICATION ERROR: " + JSON.stringify(err));
    });

    var insertNotificationLog =
        "INSERT INTO `tb_experimentation_logs` " +
        "(`uuid`, `published`, `type`) " +
        "VALUES (?, ?, 2)";
    connection.query(insertNotificationLog, [uuid, date], function(err, result){
        if(err){
            logging.logDatabaseQuery("Inserting the logs for notification", err, result);
        }
    });
}

function publishToTestChannel(){
    intervalId = setInterval(
        function(){
            var uuid1 = pubnub.uuid();
            var uuid2 = pubnub.uuid();
            var uuid3 = pubnub.uuid();

            publishMessageToChannel('testChannel1', uuid1);
            publishMessageToChannel('testChannel2', uuid2);
            publishMessageToChannel('testChannel3', uuid3);

            sendTestNotification(uuid1, 'APA91bFuX-OjN1c_N7SNINQn56VmnWLqseEu-l7QslZwBQmcSX_0qCt3iBkz7JvsLfEKGaP3VlqXi2gz1WAhBbiKTpWg7YFl5sz6Qgw_H8rIZtID7OBC_VmFXGyW1DQoKw6nzKXfKJmQyLxDoMfLFHYWIbep2rztaH4S35j_6LZWO1Orb8fD6HE');
            sendTestNotification(uuid2, 'APA91bFeCRaetHgHqxf81DyxOmCktvxMevJGeDmW5auFysjyrUxkDZbkATV8ABHmZ-SFCJFrZR5_JbOe6Epz1tt0hlTJ1JtcScFKwsSOkGN5chwME6Z7eSYfFsNAkq0hwolp8hJJ4123SyvE2eXTgU_EiFnMUdXq847o2v-BuXTb_PnY9LflZ2I');
            sendTestNotification(uuid3, 'APA91bF246Y-j_odkwPM83jhOJISFISzh4tEJD0oh7vgG7eyyJ6OABSAXnf6KFhUhlAvxr3t-MQfVTSSEXGC7UzADQu1Epwh0zodhcAIj8zk09_4GerHC4xW9F-i_YZQQX18NFRuMbaNMCbwYiE25wRh6CPSa-aOR1jjsYtLiDvhdBONIuIieQc');
        },
        10000
    );
}

function logPublishMessageStatus(channel , message, status){
    var stream = null;
    if(status === 'success'){
        stream = process.stdout;
    }
    else if(status === 'failure'){
        stream = process.stderr;
    }

    stream.write('Channel\t:' + channel + '\n');
    stream.write('Message\t:' + JSON.stringify(message) + '\n');
    stream.write('Status\t:' + status + '\n');
}

exports.acknowledge_pubnub_message = function(req, res){
    var uuid = req.body.uuid;
    var timestamp = req.body.timestamp;

    var logMessage =
        "UPDATE `tb_experimentation_logs` " +
        "SET `received` = ? " +
        "WHERE `uuid` = ? AND `type` = 1";
    connection.query(logMessage, [timestamp, uuid], function(err, result){
        if(err){
            logging.logDatabaseQuery("Updating the received timestamp for the pubnub message", err, result);
        }
    });

    var response = {
        flag    : constants.responseFlags.ACTION_COMPLETE,
        message : 'Pubnub acknowledgement received'
    };
    res.send(response);
};

exports.acknowledge_notification = function(req, res){
    var uuid = req.body.uuid;
    var timestamp = req.body.timestamp;

    var logMessage =
        "UPDATE `tb_experimentation_logs` " +
        "SET `received` = ? " +
        "WHERE `uuid` = ? AND `type` = 2";
    connection.query(logMessage, [timestamp, uuid], function(err, result){
        if(err){
            logging.logDatabaseQuery("Updating the received timestamp for the notification", err, result);
        }
    });

    var response = {
        flag    : constants.responseFlags.ACTION_COMPLETE,
        message : 'notification acknowledgement received'
    };
    res.send(response);
};

exports.stop_test_pings = function(req, res){
    clearInterval(intervalId);
    intervalId = 0;
    res.send('Test pings from server stopped.')
};

exports.start_test_pings = function(req, res){
    if(intervalId === 0){
        publishToTestChannel();
        res.send('Test pings from server started.')
    }
    else{
        res.send('Already running.')
    }
};