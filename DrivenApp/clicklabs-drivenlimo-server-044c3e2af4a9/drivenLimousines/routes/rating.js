var constants = require('./constants');
var logging = require('./logging');
var responses = require('./responses');
var utils = require('./commonfunction');



exports.rateTheDriverOrCustomer = addRatingForUser;
exports.skipRatingByCustomer = skipRatingByCustomer;



function addRatingForUser(req, res) {
    logging.startSection('Adding rating for the driver');
    logging.logRequest(req);

    var accessToken    = req.body.access_token;
    var givenRating    = req.body.given_rating;
    var engagementId   = req.body.engagement_id;
    var userId         = req.body.user_id;
    var feedback       = req.body.feedback;
    var driverFlag     = req.body.driver_flag;

    if (typeof driverFlag === 'undefined') {
        driverFlag = 0;

    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.sendAuthenticationError(res);
            return;
        }

        var updateEngagement =
            'UPDATE `tb_engagements` ';

        if (driverFlag == 1) {

            updateEngagement += 'SET `user_rating` = ?, `user_feedback` = ? ';

        } else {


            updateEngagement += 'SET `driver_rating` = ?, `driver_feedback` = ? ';
        }
        updateEngagement += 'WHERE `engagement_id` = ?';
        connection.query(updateEngagement, [givenRating, feedback, engagementId], function(err, result) {
            logging.logDatabaseQuery('Updating the rating and feedback by customer for the engagement', err, result);
        });

        var updateDriverRating =
            'UPDATE `tb_users` ';

        if (driverFlag == 1) {

            updateDriverRating += 'SET `total_rating_got_user` = `total_rating_got_user` + 1, `total_rating_user` = `total_rating_user` + ? ';


        } else {

            updateDriverRating += 'SET `total_rating_got_driver` = `total_rating_got_driver` + 1, `total_rating_driver` = `total_rating_driver` + ? ';



        }

        updateDriverRating += 'WHERE `user_id` = ?';
        connection.query(updateDriverRating, [givenRating, userId], function(err, result) {
            logging.logDatabaseQuery('Updating the total rating for the driver', err, result);
        });

        var response = {
            flag: constants.responseFlags.ACTION_COMPLETE,
            log: 'Rating added.'
        };
        res.send(response);
    });
}




function skipRatingByCustomer(req, res) {
    logging.startSection('Skip the rating screen for the customer');
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var engagementId = req.body.engagement_id;

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.sendAuthenticationError(res);
            return;
        }

        var updateEngagement =
            'UPDATE `tb_engagements` ' +
            'SET `skip_rating_by_customer` = 1 ' +
            'WHERE `engagement_id` = ?';
        connection.query(updateEngagement, [engagementId], function(err, result) {
            logging.logDatabaseQuery('Skipping the rating by the customer for the engagement', err, result);
        });

        var response = {
            flag: constants.responseFlags.ACTION_COMPLETE,
            log: 'Rating skipped.'
        };
        res.send(response);
    });
}




exports.rating = function(req, res) {
    var access_token = req.body.access_token;
    var given_rating = req.body.given_rating;
    var engage_id = req.body.engagement_id;
    var rating_receiver_id = req.body.rating_receiver_id;
    var flag = req.body.flag; // 0 driver rating, 1 customer rating
    var manvalues = [access_token, given_rating, engage_id, flag];
    var checkblank = utils.checkBlank(manvalues);

    if (checkblank == 1) {
        response1 = {
            "error": "some parameter missing",
            "flag": 0
        }
        res.send(JSON.stringify(response1));
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                response1 = {
                    "error": "Invalid access token",
                    "flag": 1
                }
                res.send(JSON.stringify(response1));
            } else {
                // var userId;
                //= result[0].user_id;

                if (flag == 0) {

                    var userId = rating_receiver_id;
                    rating_receiver_id = result[0].user_id;
                    var sql = "UPDATE `tb_engagements` SET `user_rating`=? WHERE `engagement_id`=? LIMIT 1"
                    connection.query(sql, [given_rating, engage_id], function(err, result) {
                        response1 = {
                            "log": "Rated successfully"
                        }
                        res.send(JSON.stringify(response1));
                    });

                    var sql = "UPDATE `tb_users` SET `total_rating_got_user`=`total_rating_got_user`+?,`total_rating_user`=`total_rating_user`+? WHERE `user_id`=? LIMIT 1"

                    connection.query(sql, [1, given_rating, userId], function(err, results) {});

                } else if (flag == 1) {
                    var userId = result[0].user_id;
                    rating_receiver_id = rating_receiver_id;

                    var sql = "UPDATE `tb_engagements` SET `driver_rating`=? WHERE `engagement_id`=? LIMIT 1"
                    connection.query(sql, [given_rating, engage_id], function(err, result) {
                        response1 = {
                            "log": "Rated successfully"
                        }
                        res.send(JSON.stringify(response1));

                    });
                    var sql = "UPDATE `tb_users` SET `total_rating_got_driver`=`total_rating_got_driver`+?,`total_rating_driver`=`total_rating_driver`+? WHERE `user_id`=? LIMIT 1"
                    connection.query(sql, [1, given_rating, rating_receiver_id], function(err, results) {});
                }

            }

        });

    }
}


exports.fav_locations = function(req, res) {
    var access_token = req.body.access_token;
    var fav_latitude = req.body.fav_latitude;
    var fav_longitude = req.body.fav_longitude;
    var fav_name = req.body.fav_name;
    var manvalues = [access_token, fav_latitude, fav_longitude, fav_name];
    var checkblank = utils.checkBlank(manvalues);

    if (checkblank == 1) {
        response1 = {
            "error": "some parameter missing",
            "flag": 0
        }
        res.send(JSON.stringify(response1));
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                response1 = {
                    "error": "Invalid access token",
                    "flag": 1
                }
                res.send(JSON.stringify(response1));
            } else {
                var userId = result[0].user_id;

                var sql = "INSERT INTO `tb_fav_locations` (`user_id`,`fav_latitude`,`fav_longitude`,`fav_name`) VALUES (?,?,?,?)"
                connection.query(sql, [userId, fav_latitude, fav_longitude, fav_name], function(err, result) {

                    response1 = {
                        "log": "Added to favourite successfully"
                    }
                    res.send(JSON.stringify(response1));


                });
            }

        });

    }
}




exports.get_fav_locations = function(req, res) {
    var access_token = req.body.access_token;

    var manvalues = [access_token];
    var checkblank = utils.checkBlank(manvalues);

    if (checkblank == 1) {
        response1 = {
            "error": "some parameter missing",
            "flag": 0
        }
        res.send(JSON.stringify(response1));
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                response1 = {
                    "error": "Invalid access token",
                    "flag": 1
                }
                res.send(JSON.stringify(response1));
            } else {
                var userId = result[0].user_id;

                var sql = "SELECT `s_no`,`fav_latitude`,`fav_longitude`,`fav_name` FROM `tb_fav_locations` WHERE `user_id`= ?"
                connection.query(sql, [userId], function(err, result) {
                    if (result.length > 0) {
                        response1 = {
                            "favourite_data": result
                        }
                        res.send(JSON.stringify(response1));
                    } else {
                        response1 = {
                            "favourite_data": []
                        }
                        res.send(JSON.stringify(response1));
                    }
                });
            }

        });

    }
}




exports.delete_fav_locations = function(req, res) {
    var access_token = req.body.access_token;
    var serial_Num = req.body.s_no;

    var manvalues = [access_token, serial_Num];
    var checkblank = utils.checkBlank(manvalues);

    if (checkblank == 1) {
        response1 = {
            "error": "some parameter missing",
            "flag": 0
        }
        res.send(JSON.stringify(response1));
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                response1 = {
                    "error": "Invalid access token",
                    "flag": 1
                }
                res.send(JSON.stringify(response1));
            } else {
                var sql = "DELETE FROM `tb_fav_locations` WHERE `s_no`=? LIMIT 1"
                connection.query(sql, [serial_Num], function(err, result) {
                    response1 = {
                        "log": "Removed from favourite successfully"
                    }
                    res.send(JSON.stringify(response1));
                });
            }

        });

    }
}





exports.userData = function(req, res) {
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = utils.checkBlank(manvalues);

    if (checkblank == 1) {
        response1 = {
            "error": "some parameter missing",
            "flag": 0
        }
        res.send(JSON.stringify(response1));
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                response1 = {
                    "error": "Invalid access token",
                    "flag": 1
                }
                res.send(JSON.stringify(response1));
            } else {
                var sql = "SELECT `user_id`,`driver_id`,`engagement_id` FROM `tb_engagements` WHERE `status`= ?"
                connection.query(sql, [0], function(err, result) {

                    var userIdArray = [];
                    var userIdArrayCount = result.length;

                    for (var i = 0; i < userIdArrayCount; i++) {
                        userIdArray.push(result[i].user_id);
                        userIdArray.push(result[i].driver_id);

                    }

                    var userNameString = userIdArray.toString(',');

                    var sql = "SELECT `user_name`, `user_id` FROM `tb_users` WHERE `user_id` IN(" + userNameString + ")";
                    connection.query(sql, function(err, resultFromUserTable) {
                        var count = resultFromUserTable.length;
                        var userDataArray = [];
                        for (var i = 0; i < userIdArrayCount; i++) {

                            for (var j = 0; j < count; j++) {
                                if (result[i].user_id == resultFromUserTable[j].user_id) {
                                    var userName = (resultFromUserTable[j].user_name);
                                }
                                if (result[i].driver_id == resultFromUserTable[j].user_id) {
                                    var driverName = (resultFromUserTable[j].user_name);
                                }


                            }
                            userDataArray.push({
                                "user_id": result[i].user_id,
                                "driver_id": result[i].driver_id,
                                "engagement_id": result[i].engagement_id,
                                "user_name": userName,
                                "driver_name": driverName
                            });

                        }

                        response1 = {
                            "user_Data": userDataArray
                        }
                        res.send(JSON.stringify(response1));
                    });


                });

            }

        });
    }
}
