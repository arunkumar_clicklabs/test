var utils                               = require('./commonfunction');
var logging                             = require('./logging');
var constants                           = require('./constants');
var responses                           = require('./responses');
var messenger                           = require('./messenger');
var ride_parallel_dispatcher            = require('./ride_parallel_dispatcher');


exports.sendSplitFareRequest            = sendSplitFareRequest;
exports.splitFareScreen                 = splitFareScreen;
exports.userResponseToSplitFareRequest  = userResponseToSplitFareRequest;
exports.resendSplitFareRequest          = resendSplitFareRequest;
exports.isSplitFarePopup                = isSplitFarePopup;

function sendSplitFareRequest(req, res) {
    logging.startSection("send_split_fare_request");
    logging.logRequest(req);

    var accessToken     = req.body.access_token;
    var engagementId    = req.body.engagement_id;
    var phoneNo         = req.body.phone_no;

    var manValues   = [accessToken,engagementId,phoneNo];
    var isBlank     = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function (user){
        if (user == 0)
        {
            responses.sendAuthenticationError(res);
            return;
        }

        var senderUserId = user[0].user_id;
        var senderUserName = user[0].user_name;
        var senderUserImage = user[0].user_image;

        searchAUserByPhoneNumber(phoneNo,function(err,resultUser){

            if(err){

                responses.errorResponse(res);
                return;
            }

            getSplitFareKeyByEngagementId(engagementId, function(err,splitFareKey){
                if(err){

                    responses.errorResponse(res);
                    return;
                }

                if(resultUser.length == 0){

                    // Send SMS on phone
                    var msg = "Please download Taximust app";
                    messenger.sendMessageByTwillio(phoneNo, msg);

                    // Send response for not registered
                    sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.USER_NOT_REGISTERED, res);
                    return;
                }

                var userId = resultUser[0].user_id;


                if(userId == senderUserId){

                    // Send response for Blocked
                    sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.SENDING_REQUEST_TO_OWN, res);
                    return;
                }


                // Check user should not be blocked
                if(resultUser[0].is_blocked == constants.userBlockingStatus.BLOCKED){

                    // Send response for Blocked
                    sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.BLOCKED_USER, res);
                    return;
                }

                // Check if user is logged in
                if(resultUser[0].current_user_status != constants.userCurrentStatus.CUSTOMER_ONLINE){
                    // Send response for not registered
                    sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.USER_NOT_LOGGED_IN, res);
                    return;
                }

                // Check user's last engagement status...Must not be in ride
                var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED,constants.engagementStatus.ENDED];

                var sql = "SELECT `engagement_id`,`is_payment_successful`,`status` FROM `tb_engagements` WHERE `user_id`=? && `status` IN("+status.toString()+") ORDER BY `engagement_id` DESC LIMIT 1";
                connection.query(sql, [userId], function(err,resultLastEngagementPaymentSuccessful){
                    logging.logDatabaseQueryError("User's last active engagement info", err, resultLastEngagementPaymentSuccessful);
                    if(err){
                        responses.errorResponse(res);
                        return;
                    }

                    if(resultLastEngagementPaymentSuccessful.length > 0){

                        if(resultLastEngagementPaymentSuccessful[0].status == constants.engagementStatus.ENDED){

                            if(resultLastEngagementPaymentSuccessful[0].is_payment_successful == constants.PaymentStatus.UNSUCCESSFUL){

                                // Send response if user is in ride
                                sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.USER_ALREADY_IN_RIDE, res);
                                return;
                            }
                        }
                        else{

                            // Send response if user is in ride
                            sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.USER_ALREADY_IN_RIDE, res);
                            return;
                        }

                    }

                    // Check whether request has been already sent / accepted to this user or not for same split fare key


                    var status = [constants.splitFareRequestResponse.SPLITFARE_REQUEST_ACCEPTED,constants.splitFareRequestResponse.SPLITFARE_REQUEST_SENDING_SUCCESSFUL];

                    var sql = "SELECT `id` FROM `tb_split_fare_requests` WHERE `split_fare_key`=? && `sent_to`=? LIMIT 1";
                    connection.query(sql,[splitFareKey,userId],function(err,resultUserParticipatedInSplitfare){
                        logging.logDatabaseQueryError("Check whether user already got split fare request for same ride", err, resultUserParticipatedInSplitfare);
                        if(err){

                            responses.errorResponse(res);
                            return;
                        }
                        if(resultUserParticipatedInSplitfare.length > 0){

                            // Send response for request has been already sent
                            sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.SPLITFARE_REQUEST_SENT, res);
                            return;
                        }

                        // Else get the device token of user, send push

                        var sql = "INSERT INTO `tb_split_fare_requests`(`split_fare_key`,`sent_by`,`sent_to`,`phone_no`,`status`) VALUES(?,?,?,?,?)";
                        connection.query(sql,[splitFareKey, senderUserId, userId, phoneNo, constants.splitFareRequestStatus.SPLITFARE_REQUEST_SENDING_SUCCESSFUL], function(err,resultInsertSplitFareRequest){
                            logging.logDatabaseQueryError("Add split fare request to DB", err, resultInsertSplitFareRequest);
                            if(err){

                                responses.errorResponse(res);
                                return;
                            }

                            var split_id = resultInsertSplitFareRequest.insertId;

                            // Send response for successful request

                            sendSplitFareRequestResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.SPLITFARE_REQUEST_SENDING_SUCCESSFUL,res);

                            // Send push notification for split fare request

                            var message = "Split fare request";
                            var flag = constants.notificationFlags.SPLIT_FARE_REQUEST;
                            var payload = {"split_id":split_id,"user_name": senderUserName,
                                            "flag" : flag,"user_image": senderUserImage,"time":constants.splitFareRequestTimer.TIMEINSECONDS};
                            utils.sendNotification(userId, message, flag, payload);


                            return;
                        })

                    });

                });

            });

        });
    });
}



function splitFareScreen(req, res) {
    logging.startSection("send_split_fare_request");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var engagementId = req.body.engagement_id;

    var manValues = [accessToken, engagementId];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function (user) {
        if (user == 0) {
            responses.sendAuthenticationError(res);
            return;
        }

        var userId = user[0].user_id;

        getSplitFareKeyByEngagementId(engagementId, function (err, splitFareKey) {
            if (err) {

                responses.errorResponse(res);
                return;
            }
            splitFareScreenData(userId, splitFareKey, function (err, resultSplitFareScreenData) {
                if (err) {

                    responses.errorResponse(res);
                    return;
                }

                var response = {
                    "split_fare_data" : resultSplitFareScreenData
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));

            });
        });
    });
}



function userResponseToSplitFareRequest(req, res){
    logging.startSection("user_response_to_split_fare_request");
    logging.logRequest(req);

    var accessToken           = req.body.access_token;
    var splitId               = req.body.split_id;
    var userResponseFlag      = parseInt(req.body.user_response_flag);

    // userResponseFlag - CONSTANT.UserResponseToSplitFareRequest.ACCEPTED, CONSTANT.UserResponseToSplitFareRequest.REJECTED, CONSTANT.UserResponseToSplitFareRequest.TIMEOUT

    var manValues   = [accessToken,splitId,userResponseFlag];
    var isBlank     = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function (user) {
        if (user == 0) {
            responses.sendAuthenticationError(res);
            return;
        }

        var userId = user[0].user_id;

        switch(userResponseFlag){

            case constants.UserResponseToSplitFareRequest.ACCEPTED:

                console.log("ACCEPTED")

                // Get current status of split fare request
                getSplitFareRequestStatus(splitId, function(err, SplitFareRequestStatus){

                    if(SplitFareRequestStatus == constants.splitFareRequestStatus.SPLITFARE_REQUEST_ACCEPTED){
                        userResponseToSplitFareRequestResponse(constants.SplitFareRequestDisplayMessage.REQUESTALREADYACCEPTED, res)
                        return;
                    }

                    // Get complete engagement info from split_id
                    getEngagementInfoBySplitId(splitId, function(err, resultEngagement){
                        if (err) {
                            responses.errorResponse(res);
                            return;
                        }

                        if(resultEngagement.status === constants.engagementStatus.ENDED){

                            updateSplitFareRequestStatus(splitId, constants.splitFareRequestStatus.SPLITFARE_REQUEST_TIMEOUT);

                            userResponseToSplitFareRequestResponse(constants.SplitFareRequestDisplayMessage.RIDEALREADYENDED, res);

                            return;
                        }

                        var driverId    = resultEngagement.driver_id;
                        var sessionId   = resultEngagement.session_id;

                        // copy engagement info to create new engagement

                        console.log("userId")
                        console.log(userId)

                        copyEngagement(userId, resultEngagement, function(err, resultNewEngagementId){
                            if (err) {
                                responses.errorResponse(res);
                                return;
                            }

                            // Send response
                            ride_parallel_dispatcher.formRideAcceptanceMessageForCustomer(userId, driverId, resultNewEngagementId, sessionId, function(err, resultInRideResponseToCustomerOnAcceptanceOfSplitFareRequest) {
                                if (err) {
                                    responses.errorResponse(res);
                                    return;
                                }

                                resultInRideResponseToCustomerOnAcceptanceOfSplitFareRequest.flag = constants.responseFlags.RIDE_STARTED;

                                var response = {
                                    "in_ride_screen_data" : resultInRideResponseToCustomerOnAcceptanceOfSplitFareRequest
                                };
                                logging.logResponse(response);
                                res.send(JSON.stringify(response));

                                updateSplitFareRequestStatus(splitId, constants.splitFareRequestStatus.SPLITFARE_REQUEST_ACCEPTED);

                            });


                        });

                    });

                });

                break;

            case constants.UserResponseToSplitFareRequest.REJECTED:

                console.log("REJECTED")

                updateSplitFareRequestStatus(splitId, constants.splitFareRequestStatus.SPLITFARE_REQUEST_REJECTED);

                userResponseToSplitFareRequestResponse(constants.SplitFareRequestDisplayMessage.REJECTED,res);

                break;

            case constants.UserResponseToSplitFareRequest.TIMEOUT:

                console.log("TIMEOUT")

                updateSplitFareRequestStatus(splitId, constants.splitFareRequestStatus.SPLITFARE_REQUEST_TIMEOUT);

                userResponseToSplitFareRequestResponse(constants.SplitFareRequestDisplayMessage.TIMEOUT, res);

                break;
            default:

                console.log("DEFAULT")

                break;

        }


    });
}



function resendSplitFareRequest(req, res) {
    logging.startSection("resend_split_fare_request");
    logging.logRequest(req);

    var accessToken     = req.body.access_token;
    var splitId         = req.body.split_id;

    var manValues   = [accessToken,splitId];
    var isBlank     = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function (user){
        if (user == 0)
        {
            responses.sendAuthenticationError(res);
            return;
        }

        var senderUserId = user[0].user_id;
        var senderUserName = user[0].user_name;
        var senderUserImage = user[0].user_image;


        getUserInfoBySplitId(splitId, function(err,resultUserAndSplitFareKey){

            if(err){
                responses.errorResponse(res);
                return;
            }

            var userId          = resultUserAndSplitFareKey[0].user_id;
            var splitFareKey    = resultUserAndSplitFareKey[0].split_fare_key;

            // Check if user is logged in
            if(resultUserAndSplitFareKey[0].current_user_status != constants.userCurrentStatus.CUSTOMER_ONLINE){
                // Send response for not logged in
                sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.USER_NOT_LOGGED_IN, res);
                return;
            }

            // Check user's last engagement status...Must not be in ride
            var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED,constants.engagementStatus.ENDED];

            var sql = "SELECT `engagement_id`,`is_payment_successful`,`status` FROM `tb_engagements` WHERE `user_id`=? && `status` IN("+status.toString()+") ORDER BY `engagement_id` DESC LIMIT 1";
            connection.query(sql, [userId], function(err,resultLastEngagementPaymentSuccessful){
                logging.logDatabaseQueryError("User's last active engagement info", err, resultLastEngagementPaymentSuccessful);
                if(err){
                    responses.errorResponse(res);
                    return;
                }

                if(resultLastEngagementPaymentSuccessful.length > 0){

                    if(resultLastEngagementPaymentSuccessful[0].status == constants.engagementStatus.ENDED){

                        if(resultLastEngagementPaymentSuccessful[0].is_payment_successful == constants.PaymentStatus.UNSUCCESSFUL){

                            // Send response if user is in ride
                            sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.USER_ALREADY_IN_RIDE, res);
                            return;
                        }
                    }
                    else{

                        // Send response if user is in ride
                        sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.USER_ALREADY_IN_RIDE, res);
                        return;
                    }

                }

                // send push notification to user
                var sql = "UPDATE `tb_split_fare_requests` SET `status`=?,`request_sent_timestamp`=? WHERE `id`=? LIMIT 1";
                connection.query(sql,[constants.splitFareRequestStatus.SPLITFARE_REQUEST_SENDING_SUCCESSFUL, new Date(), splitId], function(err,resultResendSplitFareRequest){
                    logging.logDatabaseQueryError("Resend split fare request and update split fare id status in DB", err, resultResendSplitFareRequest);
                    if(err){

                        responses.errorResponse(res);
                        return;
                    }

                    // Send response for successful request
                    sendSplitFareRequestResponse(senderUserId, splitFareKey, constants.splitFareRequestResponse.SPLITFARE_REQUEST_SENDING_SUCCESSFUL,res);

                    // Send push notification for split fare request
                    var message = "Split fare request";
                    var flag = constants.notificationFlags.SPLIT_FARE_REQUEST;
                    var payload = {"split_id":splitId,"user_name": senderUserName,"user_image": senderUserImage,"time":constants.splitFareRequestTimer.TIMEINSECONDS};
                    utils.sendNotification(userId, message, flag, payload);


                    return;
                })



            });

        });

    });
}




function searchAUserByPhoneNumber(phone_no,callback){
    var sql = "SELECT `user_id`,`current_user_status`,`is_blocked`,`user_device_token` FROM `tb_users` WHERE `phone_no` LIKE '%"+phone_no+"' && `verification_status`=? && `reg_as`=? LIMIT 1";
    connection.query(sql,[constants.userVerificationStatus.Verified, constants.userRegistrationStatus.CUSTOMER],function(err,result){
       logging.logDatabaseQueryError("Get user from phone number", err, result);
       if(err){
           return callback(err,null);
       }
       callback(null,result);

    });
}

function sendSplitFareRequestErrorResponse(senderUserId, splitFareKey, responseMessage, res){

    splitFareScreenData(senderUserId, splitFareKey,function(err,resultSplitFareScreenData){
        var response = {
            "error" : responseMessage,
            "flag"  : constants.responseFlags.SHOW_ERROR_MESSAGE,
            "split_fare_data" : resultSplitFareScreenData
        };
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    });

}

function sendSplitFareRequestResponse(senderUserId, splitFareKey, responseMessage, res){

    splitFareScreenData(senderUserId, splitFareKey,function(err,resultSplitFareScreenData){

        if(err){
            responses.errorResponse(res);
            return;
        }

        var response = {
            "log" : responseMessage,
            "split_fare_data" : resultSplitFareScreenData
        };
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    });
}

function getSplitFareKeyByEngagementId(engagementId,callback){
    var sql = "SELECT `split_fare_key` FROM `tb_engagements` WHERE `engagement_id`=? LIMIT 1";
    connection.query(sql,[engagementId],function(err,resultSplitFareKey){

        logging.logDatabaseQueryError("Get split fare key from engagement_id", err, resultSplitFareKey);

        if(err){
            return callback(err,null);
        }
        callback(null,resultSplitFareKey[0].split_fare_key);
    })
}

function splitFareScreenData(userId, splitFareKey,callback){

    // Set all timed out request status
    var sql = "UPDATE `tb_split_fare_requests` SET `status`=? WHERE `status`=? && TIMESTAMPDIFF(SECOND, `request_sent_timestamp`, NOW()) > ? ";
    connection.query(sql, [constants.splitFareRequestStatus.SPLITFARE_REQUEST_TIMEOUT, constants.splitFareRequestStatus.SPLITFARE_REQUEST_SENDING_SUCCESSFUL, constants.splitFareRequestTimer.TIMEINSECONDS], function(err, result){

        if(err){
            return callback(err,null);
        }

        // Get updated split fare screen data
        var sql = "SELECT a.id AS split_id, b.user_name, b.phone_no, a.status FROM tb_split_fare_requests a, tb_users b WHERE a.sent_to != ? && a.sent_to = b.user_id && a.split_fare_key=?";
        connection.query(sql, [userId, splitFareKey], function(err,resultSplitFareScreenData){

            logging.logDatabaseQueryError("Get split fare screen data", err, resultSplitFareScreenData);

            if (err) {

                return callback(err,null);
            }

            callback(null,resultSplitFareScreenData);

        });

    });

}

function getEngagementInfoBySplitId(splitId,callback){

    var sql = "SELECT `split_fare_key` FROM `tb_split_fare_requests` WHERE `id`=? LIMIT 1";
    connection.query(sql, [splitId], function(err,resultSplitFareKey){
        logging.logDatabaseQuery("Get split_fare_key by split_id", err, resultSplitFareKey, null);

        if(err){
            return callback(err,null);
        }

        var SplitFareKey = resultSplitFareKey[0].split_fare_key;

        var sql = "SELECT * FROM `tb_engagements` WHERE `split_fare_key`=? LIMIT 1";
        connection.query(sql, [SplitFareKey], function(err,resultEngagementInfo){
            logging.logDatabaseQuery("Get recent engagement's info by split_fare_key", err, resultEngagementInfo, null);

            if(err){
                return callback(err,null);
            }
            callback(null,resultEngagementInfo[0]);
        });

    });
}

function copyEngagement(customerId, resultEngagement, callback) {

    var create_engagement = "INSERT INTO `tb_engagements` " +
        "(`user_id`, `driver_id`, `pickup_latitude`, `pickup_location_address`, `pickup_longitude`, `status`, `driver_accept_latitude`, `driver_accept_longitude`, `session_id`, `accept_time`," +
        "`manual_destination_latitude`,`manual_destination_longitude`,`manual_destination_address`,`split_fare_key`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    var values = [customerId, resultEngagement.driver_id, resultEngagement.pickup_latitude, resultEngagement.pickup_location_address, resultEngagement.pickup_longitude, resultEngagement.status,
        resultEngagement.driver_accept_latitude, resultEngagement.driver_accept_longitude, resultEngagement.session_id, resultEngagement.accept_time,
        resultEngagement.manual_destination_latitude, resultEngagement.manual_destination_longitude, resultEngagement.manual_destination_address, resultEngagement.split_fare_key];

    connection.query(create_engagement, values, function (err, engagement) {
        logging.logDatabaseQuery("Adding a active engagement to the session", err, engagement, null);

        if (err) {
            return callback(err, null);
        }

        callback(null, engagement.insertId);

    });
}

function updateSplitFareRequestStatus(splitId, status){
    var sql = "UPDATE `tb_split_fare_requests` SET `status`=? WHERE `id`=? LIMIT 1";
    connection.query(sql, [status, splitId], function(err,result){
        logging.logDatabaseQuery("Update Split Fare Request Status", err, result, null);

    });
}

function userResponseToSplitFareRequestResponse(message, res){
    var response = {
        "error": message,
        "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
    }
    logging.logResponse(response);
    res.send(JSON.stringify(response));
}

function getSplitFareRequestStatus(splitId, callback){
    var sql = "SELECT `status` FROM `tb_split_fare_requests` WHERE `id`=? LIMIT 1";
    connection.query(sql, [splitId], function(err,resultSplitFareRequestStatus){
        logging.logDatabaseQuery("Get Split Fare Request Status", err, resultSplitFareRequestStatus, null);
        if(err){
            return callback(err, null);
        }
        callback(err, resultSplitFareRequestStatus[0].status);
    });
}

function getUserInfoBySplitId(splitId,callback){
    var sql = "SELECT b.user_id,b.current_user_status,b.is_blocked,b.user_device_token,a.split_fare_key FROM tb_split_fare_requests a,tb_users b WHERE a.id = ? && a.sent_to = b.user_id LIMIT 1";
    connection.query(sql,[splitId],function(err,result){
        logging.logDatabaseQueryError("Get user from split id", err, result);
        if(err){
            return callback(err,null);
        }
        callback(null,result);

    });
}

function isSplitFarePopup(userId, callback){
    // Set all timed out request status
    var sql = "UPDATE `tb_split_fare_requests` SET `status`=? WHERE `status`=? && TIMESTAMPDIFF(SECOND, `request_sent_timestamp`, NOW()) > ? ";
    connection.query(sql, [constants.splitFareRequestStatus.SPLITFARE_REQUEST_TIMEOUT, constants.splitFareRequestStatus.SPLITFARE_REQUEST_SENDING_SUCCESSFUL, constants.splitFareRequestTimer.TIMEINSECONDS], function(err, result){

        if(err){
            return callback(err,null);
        }

        // Get updated split fare screen data
        var sql = "SELECT a.id AS split_id, TIMESTAMPDIFF(SECOND, a.request_sent_timestamp, NOW()) AS time, b.user_name, b.user_image FROM tb_split_fare_requests a, tb_users b WHERE a.sent_to = ? && a.sent_by = b.user_id && a.status=? ORDER BY a.id DESC LIMIT 1";
        connection.query(sql, [userId, constants.splitFareRequestStatus.SPLITFARE_REQUEST_SENDING_SUCCESSFUL], function(err,resultSplitFareData){

            logging.logDatabaseQueryError("Get split fare request if not timeout", err, resultSplitFareData);

            //return process.nextTick(callback.bind(null, err, resultSplitFareData));

            //if (err) {
            //    return callback(err,null);
            //}
            //
            callback(resultSplitFareData);

        });

    });
}