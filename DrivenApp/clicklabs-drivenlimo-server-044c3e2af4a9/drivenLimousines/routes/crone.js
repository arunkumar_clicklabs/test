var request = require('request');
var aysnc = require('async');
var fs = require('fs');
var utils = require('./commonfunction');
var logging = require('./logging');
var constants = require('./constants');
var mailer = require('./mailer');
var messenger = require('./messenger');
var login = require('./user_login');
var userTriggers    = require('./user_triggers');



exports.sendNotificationToDormantUsers      = userTriggers.sendNotificationToDormantUsers;
exports.sendNotificationForReferrals        = userTriggers.sendNotificationForReferrals;
exports.sendNotificationForExpiringCoupons  = userTriggers.sendNotificationForExpiringCoupons;

exports.expireCouponsForYesterday           = expireCouponsForYesterday;



exports.crone_to_calculate_drivers_online_each_half_hour = function(req, res) {

    var sql = "SELECT `user_id` FROM `tb_users` WHERE `current_user_status`=? && `reg_as`=? && `timestamp` >= (NOW() - INTERVAL 15 MINUTE)";
    connection.query(sql, [1, 1], function(err, result) {
        if (!err) {
            if (result.length > 0) {
                var result_length = result.length;
                for (var i = 0; i < result_length; i++) {
                    (function(i) {
                        var sql1 = "INSERT INTO `tb_list_drivers_online`(`driver_id`) VALUES(?)";
                        connection.query(sql1, [result[i].user_id], function(err, result1) {
                            var id = result1.insertId;
                            var sql11 = "UPDATE `tb_list_drivers_online` SET `slot`= NOW() + INTERVAL 15 MINUTE WHERE `id`=? LIMIT 1";
                            connection.query(sql11, [id], function(err, result11) {

                            });
                        });
                    })(i);
                }

                var sql2 = "INSERT INTO `tb_drivers_online`(`drivers_online_count`) VALUES(?)";
                connection.query(sql2, [result.length], function(err, result2) {
                    var id = result2.insertId;
                    var sql11 = "UPDATE `tb_drivers_online` SET `slot`= NOW() + INTERVAL 15 MINUTE WHERE `id`=? LIMIT 1";
                    connection.query(sql11, [id], function(err, result11) {

                    });
                });
            }            
            var response = {"log": 'crone successful'};
            res.send(JSON.stringify(response));
        }
    });
};

exports.removeInactiveEngagements = function(req, res) {

    var sql = "SELECT `engagement_id`,`current_time`,`driver_id` FROM `tb_engagements` WHERE `status` IN (0,1,2)";
    connection.query(sql, function(err, result) {
        if (!err) {
            var result_length = result.length;
            if (result_length > 0) {

                var engage = [];
                var technician = [];
                for (var i = 0; i < result_length; i++) {

                    var diff = getTimeDifferenceInMinutes(result[i].current_time, 2);
                    if (diff >= 120)
                    {
                        engage.push(result[i].engagement_id);
                        technician.push(result[i].driver_id);
                    }

                }
                if (engage.length > 0)
                {
                    var engageStr = engage.toString(",");
                    var technicianStr = technician.toString(",");

                    var sql11 = "UPDATE `tb_users` SET `status`= ? WHERE user_id IN (" + technicianStr + ")";
                    connection.query(sql11, [0], function(err, result11) {

                    });
                    var sql11 = "UPDATE `tb_engagements` SET `status`= ? WHERE engagement_id IN (" + engageStr + ")";
                    connection.query(sql11, [7], function(err, result1) {

                    });

                    var response = {"log": 'crone successful'};
                    res.send(JSON.stringify(response));
                }
                else
                {
                    var response = {"log": 'crone successful'};
                    res.send(JSON.stringify(response));
                }

            }
            else
            {
                var response = {"log": 'crone successful'};
                res.send(JSON.stringify(response));
            }

        }
        else
        {
            var response = {"log": 'crone successful'};
            res.send(JSON.stringify(response));
        }
    });
};

function getTimeDifferenceInMinutes(time, flag)
{
    var today = new Date();

    if (flag == 1){
        var diffMs = (time - today); // milliseconds between post date & now
    }
    else{
        var diffMs = (today - time); // milliseconds between now & post date
    }

    var minutes = Math.floor(0.000016667 * diffMs);

    return minutes;
}


function getOfflineDrivers(callback) {
    var get_offline_drivers =
        "SELECT `user_id`, `timestamp`, `user_name`, `phone_no`, `reg_as` " +
        "FROM `tb_users` " +
        "WHERE `current_user_status` = ? && `reg_as` IN (?, ?) && `status` = ? && `driver_suspended` = 0 && `timestamp` < (NOW() - INTERVAL 10 MINUTE)";
    var values = [constants.userCurrentStatus.DRIVER_ONLINE,
        constants.userRegistrationStatus.DRIVER, constants.userRegistrationStatus.DEDICATED_DRIVER,
        constants.userFreeStatus.FREE];
    connection.query(get_offline_drivers, values, function (err, inactive_drivers) {
        logging.logDatabaseQuery("Getting inactive drivers", err, inactive_drivers, null);
        callback(err, inactive_drivers);
    });
}


exports.update_lat_long_of_driver_to_zero = function(req, res){
    logging.startSection("update_lat_long_of_driver_to_zero");
    logging.logRequest(req);

    getOfflineDrivers(function(err, inactive_drivers){
        if (!err) {
            if (inactive_drivers.length > 0) {
                var inactive_drivers_length = inactive_drivers.length;
                var arr_users = [];
                for (var i = 0; i < inactive_drivers_length; i++) {
                    arr_users.push(inactive_drivers[i].user_id);
                }
                var str_users = arr_users.toString();
                var sql4 = "UPDATE `tb_users` SET `current_location_latitude`=?,`current_location_longitude`=? WHERE `user_id` IN (" + str_users + ")";
                connection.query(sql4, [0,0], function(err, result4) {
                    if (!err) {
                        var response = {"log": 'crone successful'};
                        res.send(JSON.stringify(response));
                    }
                });
            }
            else{
                var response = {"log": 'crone successful'};
                res.send(JSON.stringify(response));
            }
        }
    });
};


exports.send_mail_for_offline_drivers = function(req, res){
    logging.startSection("send_mail_for_offline_drivers");
    logging.logRequest(req);

    getOfflineDrivers(function(err, inactive_drivers) {
        if (!err && inactive_drivers.length > 0) {
            mailer.sendMailForOfflineDrivers(inactive_drivers);

            var response = {"log": 'crone successful'};
            res.send(JSON.stringify(response));
        }
    });
};


function getAllDrivers(callback) {
    var get_all_drivers = "SELECT `user_id`, `timestamp`, `user_name`, `phone_no`, `reg_as`, " +
        "`current_location_latitude`, `current_location_longitude`, `current_user_status`, `status` " +
        "FROM `tb_users` " +
        "WHERE `reg_as` IN (?, ?) && `current_user_status` = ? && `user_id` NOT IN (" + constants.rogueDrivers.toString() + ")";
    var values = [constants.userRegistrationStatus.DRIVER, constants.userRegistrationStatus.DEDICATED_DRIVER,
        constants.userCurrentStatus.DRIVER_ONLINE];
    connection.query(get_all_drivers, values, function (err, all_drivers) {
        //logging.logDatabaseQuery("Getting all drivers", err, all_drivers, null);
        logging.logDatabaseQuery("Getting all drivers", err, [], null);
        callback(err, all_drivers);
    });
}


exports.send_mail_for_all_drivers = function(req, res){
    logging.startSection("send_mail_for_all_drivers");

    getAllDrivers(function(err, drivers){
        var online_drivers = [];
        var offline_drivers = [];
        var busy_drivers = [];

        for(var i=0; i<drivers.length; i++){
            if(drivers[i].status == constants.userFreeStatus.BUSY)
                busy_drivers.push(drivers[i]);
            else{
                if(drivers[i].current_location_latitude == 0){
                    var timestamp = drivers[i].timestamp;
                    timestamp = timestamp.toISOString().replace(/T/, ' ').replace(/\..+/, '')
                    var t = timestamp.split(/[- :]/);
                    // Apply each element to the Date function
                    var date = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
                    var now = new Date();
                    var since = now.getTime() - date.getTime();
                    since = Math.floor(since/60000);

                    // Change the timezone to IST
                    var utc = date.getTime() + (date.getTimezoneOffset() * 60000);
                    var ist = new Date(utc + (3600000 * 5.5)); // adding 5.5 hours

                    if(since > 20){
                        drivers[i].ist = ist;
                        drivers[i].since = since;
                        offline_drivers.push(drivers[i]);
                    }
                    else{
                        online_drivers.push(drivers[i]);
                    }
                }
                else{
                    online_drivers.push(drivers[i]);
                }
            }
        }

        mailer.sendMailForAllDrivers(busy_drivers, online_drivers, offline_drivers);
        var response = {"log": 'crone successful'};
        res.send(JSON.stringify(response));
    });
};


var driverProcessingData = {};

exports.send_mail_for_non_processing_drivers = function(req, res){
    logging.startSection("send_mail_for_non_processing_drivers");


};


exports.send_mail_for_unavailable_drivers = function(req, res){
    logging.startSection("send_mail_for_unavailable_drivers");

    var getUnavailableDrivers =
        "SELECT * FROM " +
        "(SELECT `user_id`, `user_name`, `phone_no` FROM `tb_users` WHERE `reg_as` = 2 AND `is_available` = 0) as `drivers` " +
        "JOIN " +
        "tb_timings " +
        "ON drivers.user_id = tb_timings.driver_id " +
        "WHERE TIMESTAMPDIFF(MINUTE, tb_timings.start_time, NOW()) <= 10 AND TIMESTAMPDIFF(MINUTE, tb_timings.start_time, NOW()) > 0";
    connection.query(getUnavailableDrivers, [], function(err, drivers){
        if(err){
            logging.logDatabaseQuery("Getting the drivers still unavailable at duty start time", err, drivers);
        }

        if(drivers.length > 0){
            mailer.sendMailForUnavailableDrivers(drivers);
        }
    });

    res.send('Crone successful.');
};


function expireCouponsForYesterday(req, res){
    var time = new Date((new Date()).getTime() - 86400000);
    var yesterday = utils.getMysqlStyleDateString(time);

    var expireCoupons =
        'UPDATE `tb_accounts` ' +
        'SET `status` = ? ' +
        'WHERE `status` = 1 && DATE(`expiry_date`) = ?';
    connection.query(expireCoupons, [constants.couponStatus.EXPIRED, yesterday], function(err, result){
        logging.logDatabaseQuery('Expiring coupons for ' + yesterday, err, result);
        res.send(result.affectedRows + ' coupons were expired');
    });
}




// Crone to define the undefined pickup locations
// The locations are undefined because of the previous logic of populating the field
// only when the ride has started
// We have made changes to the logic where the address gets populated now to get the
// when the customer sends a request and hence, is the logic for retreiving the calls
// missed by the driver becomes easy
exports.define_undefined_pickup_location_addresses = function(req, res)
{
    var intervalID = setInterval(callBatch, 2000);

    function callBatch()
    {
        var changed = define_undefined_pickup_location_addresses_for_batch(function(changed){
            if(!changed)
            {
                clearInterval(intervalID);
                res.send({"log": "All addresses has been defined."});
            }
        });
    }
}


function define_undefined_pickup_location_addresses_for_batch(callback)
{
    var get_addresses = "SELECT `engagement_id`, `pickup_latitude`, `pickup_longitude`, `pickup_location_address` " +
        "FROM `tb_engagements`";
    connection.query(get_addresses, function(err, result_engagements) {
        logging.logDatabaseQuery("Fetching addresses for engagements with status 4 and 6.", err, result_engagements, null);

        var num_engagements = result_engagements.length;

        // Get all the engagements for which the addresses are undefined
        var engagements = {};
        var requests_made = 0;
        var responses_rec = 0;
        var counter = 0;
        for (var i = 0; i < num_engagements && requests_made<10; i++) {
            var existing_address = result_engagements[i].pickup_location_address;
            if (!existing_address || existing_address==="Unnamed") {
                var engagement_id = result_engagements[i].engagement_id;
                var latitude = result_engagements[i].pickup_latitude;
                var longitude = result_engagements[i].pickup_longitude;
                fetchAndUpdatePickupAddress(engagement_id, latitude, longitude);
                requests_made++;
            }
        }
        callback(requests_made != 0);

        function fetchAndUpdatePickupAddress(engagement_id, latitude, longitude)
        {
            request('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude, function (error, response, body) {
                responses_rec++;
                if (!error && response.statusCode == 200) {
                    body = JSON.parse(body);
                    if (body.results.length > 0) {
                        var raw_address = body.results[0].formatted_address;
                        var pickup_address = utils.formatLocationAddress(raw_address);

                        var update_address = "UPDATE `tb_engagements` SET `pickup_location_address`=? WHERE `engagement_id`=?";
                        connection.query(update_address, [pickup_address, engagement_id], function (err, result) {
                            logging.logDatabaseQuery("Updating undefined pickup location address.", err, result, null);
                        });
                    }
                }
            });
        }
    });
};



exports.give_coupons = function(req, res){
    logging.startSection("crone_give_coupons");
    logging.logRequest(req);

    //var expire_coupons = "UPDATE `tb_accounts` " +
    //    "SET `status`=? " +
    //    "WHERE `expiry_date` < NOW() && `status`=?";
    //var values = [constants.couponStatus.EXPIRED, constants.couponStatus.ACTIVE];
    //connection.query(expire_coupons, values, function(err, result){
    //    logging.logDatabaseQuery("Expiring the coupons", err, result, null);
    //});


    var get_all_users = "SELECT `user_id` FROM `tb_users`";
    connection.query(get_all_users, [], function(err, users){
        for(var i=0; i<users.length; i++){
            var insert_coupon = "INSERT INTO `tb_accounts` (`user_id`, `coupon_id`, `added_on`, `expiry_date`) VALUES (?, ?, NOW(), concat_ws(' ',DATE(DATE_ADD(NOW(), INTERVAL ? DAY)), '18:29:59'))";
            connection.query(insert_coupon, [users[i].user_id, 1, 30], function(err, result){
                logging.logDatabaseQuery("Adding coupon to account", err, result);
            });
        }
    });
    res.send(JSON.stringify({log: 'added coupons for existing users'}));
};


exports.change_coupon_to_account = function(req, res){
    var get_engagements = "SELECT `user_id`, `drop_time`, `engagement_id` FROM `tb_engagements` WHERE `coupon_id`=1";
    connection.query(get_engagements, [], function (err, engagements){

        for(var i=0; i<engagements.length; i++){
            function changeToAccount(engagement){
                var get_account = "SELECT `account_id` FROM `tb_accounts` WHERE `user_id`=? ORDER BY ABS(`redeemed_on` - ?) LIMIT 1";
                connection.query(get_account, [engagement.user_id, engagement.drop_time], function(err, account){
                    logging.log("Getting account closest to " + engagement.drop_time, err, account, null);

                    //var update = "UPDATE `tb_engagements` SET `coupon_id`=? WHERE `engagement_id`=?";
                    //connection.query(update, [account[0].account_id, engagement.engagement_id], function(err, result){
                    //
                    //});
                })
            }
        }
    });
};


exports.make_verification_calls = function(req, res){
    logging.startSection("making verification calls for unverified users");

    var get_unverified_users =
        "SELECT `user_id`, `phone_no`, `verification_calls` " +
        "FROM `tb_users` WHERE `verification_status` = 0 && `verification_calls` = 0 " +
            "&& `date_registered` > DATE_SUB(NOW(), INTERVAL 7 MINUTE) && `date_registered` < DATE_SUB(NOW(), INTERVAL 5 MINUTE)";
    connection.query(get_unverified_users, [], function(err, users){
        logging.logDatabaseQuery("Getting the unverified users registered within last 3 minutes", err, users);

        var i=0;
        //var update_num_calls = "UPDATE `tb_users` SET `verification_calls`=`verification_calls`+1 WHERE `user_id`=?";
        for(i = 0; i < users.length; i++){
            var req = {body: {phone_no: users[i].phone_no}};
            var res_dummy = {
                send: function(message){
                    console.log(message);
            }};
            login.send_otp_via_call(req, res_dummy);

            //connection.query(update_num_calls, [users[i].user_id], function(err, result){
            //    logging.logDatabaseQuery("Incrementing the number of verification calls", err, result);
            //});
        }
        var response = "Making verification calls to " + users.length + " users";
        logging.logResponse(response);
        res.send(response);
    });
};


function emptyFunction(){
    console.log("Running the empty function");
}

function generateReferralCodeForExistingUsers(){
    logging.startSection("generate_referral_code");

    var get_users = "SELECT `user_id`, `user_name` FROM `tb_users`";
    connection.query(get_users, [], function(err, users){
        var i=0;
        for(i=0; i<users.length; i++){
            (function insertReferralCode(user_id, user_name){
                utils.generateUniqueReferralCode(user_name, function(referral_code){
                    var insert_ref_code = "UPDATE `tb_users` SET `referral_code`=? WHERE `user_id`=?";
                    connection.query(insert_ref_code, [referral_code, user_id], function(err, result){
                    });
                });
            })(users[i].user_id, users[i].user_name);
        }
        console.log("Added referral code for " + users.length + " users");
    });
};

function giveMoreCouponsToInactiveUsers(){
    logging.startSection("giving_more_coupons");

    var users_with_coupons = [];
    var users_with_rides = [];
    var required_users = [];

    function getUsersWithActiveCoupons(callback){
        var get_users_with_coupons = "SELECT DISTINCT(`user_id`) FROM `tb_accounts` WHERE `status`=?";
        connection.query(get_users_with_coupons, [constants.couponStatus.ACTIVE], function(err, users){
            logging.logDatabaseQuery("Getting all users with active coupons", err, users);

            var i=0;
            for(i=0; i<users.length; i++){
                users_with_coupons.push(users[i].user_id);
            }

            callback();
        });
    }

    function getUsersWithRides(callback){
        var get_users_with_rides = "SELECT DISTINCT(`user_id`) FROM `tb_engagements` WHERE `status`=? && `current_time` > DATE_SUB(NOW(), INTERVAL 3 DAY)";
        connection.query(get_users_with_rides, [constants.engagementStatus.ENDED], function(err, users){
            logging.logDatabaseQuery("Getting users with a ride in last 3 days", err, users);

            var i=0;
            for(i=0; i<users.length; i++){
                users_with_rides.push(users[i].user_id);
            }

            callback();
        });
    }

    function filterAndGiveCouponsToUsers(){
        var filtered_out_users = users_with_coupons.concat(users_with_rides);
        //var get_required_users = "" +
        //    "SELECT `user_id`, `user_name`, `user_email`, `phone_no` " +
        //    "FROM (SELECT * FROM `tb_users` WHERE `user_id` IN (" + users_with_rides.toString() + ")) as users_with_rides " +
        //    "WHERE users_with_rides.user_id NOT IN (" + users_with_coupons.toString() + ")";
        var get_required_users = "" +
            "SELECT `user_id`, `user_name`, `user_email`, `phone_no` " +
            "FROM `tb_users` " +
            "WHERE `user_id` NOT IN (" + filtered_out_users.toString() + ")";
        connection.query(get_required_users, [], function(err, users){
            logging.logDatabaseQuery("Getting all users to give coupon to", err, users);

            var i=0;
            for(i=0; i<users.length; i++){
                (function(j){
                    var insert_coupon = "INSERT INTO `tb_accounts` (`user_id`, `coupon_id`, `added_on`, `expiry_date`) VALUES (?, ?, NOW(), concat_ws(' ',DATE(DATE_ADD(NOW(), INTERVAL ? DAY)), '18:29:59'))";
                    connection.query(insert_coupon, [users[i].user_id, 1, 30], function(err, result){
                        if (err) {
                            logging.logDatabaseQuery("Inserting coupon for: " + users[j].user_id + "failed.", err, result);
                        }
                        else {
                            mailer.sendMailForCouponsToInactiveUsers(users[j].user_name, users[j].user_email);
                            messenger.sendMessageForCouponsToInactiveUsers(users[j].phone_no);
                        }
                    });
                })(i);
            }
        });
    }

    var informationTasks = [];
    informationTasks.push(getUsersWithActiveCoupons);
    informationTasks.push(getUsersWithRides);

    aysnc.parallel(informationTasks, filterAndGiveCouponsToUsers);
}

function sendMailForFreeCouponsToInactiveUsers(){
    logging.startSection("sending mail to inactive users to whom coupons were given");

    var get_users = "SELECT `user_name`, `user_email` FROM `tb_users` WHERE `user_id` IN (SELECT `user_id` FROM `tb_accounts` WHERE `account_id`>=5162 && `account_id`<=5331)";
    connection.query(get_users, [], function(err, users){
        logging.logDatabaseQuery("Getting the users", err, users);
        var i=0;
        for(i=0; i<users.length; i++){
            mailer.sendMailForCouponsToInactiveUsers(users[i].user_name, users[i].user_email);
        }
    });
}

function sendPromotionalMailToAllUsers(){
    logging.startSection("Sending promotional mail to all users");

    var promotional_mail = "/home/jugnoo/jugnoo-v12/data/rates_slash_email.html";

    var get_users = "SELECT `user_id`, `user_name`, `user_email` FROM `tb_users` WHERE `user_email` NOT LIKE 'disabled@jugnoo.in' && DATE(`date_registered`) >= '2014-10-01'";
    connection.query(get_users, [], function(err, users){
        logging.logDatabaseQuery("Getting all the users to send the promotional mail", err, []);
        console.log("A total of " + users.length + " will be sent this mail");

        var subject = config.get('projectName')+" is now CHEAPER by 15%";
        fs.readFile(promotional_mail, {encoding: 'utf-8'}, function(err, data){
            console.log("Error in reading the file: " + JSON.stringify(err));

            if(!err){
                var to = "";
                for(var i=0; i<users.length; i++){
                    to = users[i].user_email;
                    sendMail(to, i);
                }

                function sendMail(to, i) {
                    var sending_to = to.slice(0);
                    setTimeout(function() {
                        utils.sendHtmlContent(sending_to, data, subject, function(result){
                        });
                    }, 2000*i);
                }
                //to = 'harsh@jugnoo.in';
                //to.push('chinmay@jugnoo.in');
                //utils.sendHtmlContent(to, data, subject, function(result){
                //});
            }
        });
    });
}


function generateTridealCoupons(){
    var num_coupons = 1000;

    for(var i=0; i<100; i++){
        utils.generateUniqueCode(function(err, code){
            if(err){
                console.log("An error has occurred when generating coupons for trideals");
            }
            else{
                var add_code = "INSERT INTO `tb_promotions` " +
                    "(`promotion_code`, `master_id`, `num_coupons`, `validity_window`, `coupon_id`, `start_date`, " +
                    "`end_date`, `type`, `device_specific`, `max_number`, `num_redeemed`, `email_template`) " +
                    "VALUES (?, 1, 1, 30, 1, '2014-11-28 00:00:00', " +
                    "'2014-12-30 18:29:59', 2, 1, 1, 0, 1)";
                connection.query(add_code, [code], function(err, result){
                    if(err){
                        logging.logDatabaseQuery("Adding the promotional codes for trideal", err, result);
                    }
                });
            }
        });
    }
}

function generateGrouponCoupons(){
    var num_coupons = 1000;

    for(var i = 0; i < num_coupons; i++){
        utils.generateUniqueCode(function(err, code){
            if(err){
                console.log("An error has occurred when generating coupons for groupon");
            }
            else{
                var add_code = "INSERT INTO `tb_promotions` " +
                    "(`promotion_code`, `master_id`, `num_coupons`, `validity_window`, `coupon_id`, `start_date`, " +
                    "`end_date`, `type`, `device_specific`, `max_number`, `num_redeemed`, `email_template`) " +
                    "VALUES (?, 1, 2, 30, 1, '2014-12-10 00:00:00', " +
                    "'2015-01-31 18:29:59', 2, 1, 1, 0, 1)";
                connection.query(add_code, [code], function(err, result){
                    if(err){
                        logging.logDatabaseQuery("Adding the promotional codes for GroupOn", err, result);
                    }
                });
            }
        });
    }
}


var notifications = require('./notifications');
var emails = require('./emails');

function giveCouponsToInactiveUsersAndNotify(){
    logging.startSection("Giving coupons to inactive users");

    function filterAndGiveCouponsToUsers(){
        var getRequiredUsers =
            "SELECT no_rides_users.user_id FROM " +
            "(SELECT `user_id` FROM `tb_users` WHERE `user_id` NOT IN (SELECT `user_id` FROM `tb_engagements` WHERE `status` = 3 AND DATE(`pickup_time`) > DATE_SUB(NOW(), INTERVAL 7 DAY))) AS no_rides_users " +
            "LEFT JOIN " +
            "(SELECT `user_id`, count(*) AS `num_coupons` FROM `tb_accounts` WHERE `status` = 1 GROUP BY `user_id`) AS `users_with_coupons` " +
            "ON users_with_coupons.user_id = no_rides_users.user_id " +
            "WHERE ISNULL(users_with_coupons.num_coupons)";
        connection.query(getRequiredUsers, [], function(err, users){
            logging.logDatabaseQuery("Getting all users to give coupon to", err, users);

            var i=0;
            for(i=0; i < users.length; i++){
                (function(j){
                    var insert_coupon = "INSERT INTO `tb_accounts` (`user_id`, `coupon_id`) VALUES (?, 1)";
                    connection.query(insert_coupon, [users[j].user_id], function(err, result) {
                        if (err) {
                            logging.logDatabaseQuery("Inserting coupon for: " + users[j].user_id + "failed.", err, result);
                        }
                        else {
                            //mailer.sendMailForCouponsToInactiveUsers(users[j].user_name, users[j].user_email);
                            //messenger.sendMessageForCouponsToInactiveUsers(users[j].phone_no);
                        }
                    });
                })(i);
            }
        });
    }

    function sendNotifications(){
        var req = {
            body: {
                message : "A Free ride (worth $100) has been credited to your account for a happy weekend! See you soon in a "+config.get('projectName')+"!",
                filter: "3",
                password: constants.NOTIFICATION_BLAST_PASSWORD
            }
        };
        var res = {
            send: function(str){
                console.log("Response for sending notifications: " + str)
            }
        };
        notifications.blast_notification_to_users(req, res);
    }

    function sendEmails(){
        var req = {
            body: {
                subject : "Your account has been credited a free "+config.get('projectName')+" ride",
                file_name : "coupon_added.html",
                filter: "3",
                password: constants.EMAIL_BLAST_PASSWORD
            }
        };
        var res = {
            send: function(str){
                console.log("Response for sending notifications: " + str)
            }
        };
        emails.blast_emails_to_users(req, res);
    }

    //sendNotifications();
    sendEmails();
    setTimeout(filterAndGiveCouponsToUsers, 60000);
};


exports.run_once = function(req, res){
    logging.startSection("run_once");
    logging.logRequest(req);

    var function_to_run;

    function_to_run = emptyFunction;

    //function_to_run = addPickupDistanceForEngagements;

    //var user = require('./user');
    //function_to_run = generateReferralCodeForExistingUsers;

    //var service = require('./service');
    //function_to_run = service.giveCouponsForMAD;

    //function_to_run = giveMoreCouponsToInactiveUsers;

    //function_to_run = sendMailForFreeCouponsToInactiveUsers;

    //function_to_run = sendPromotionalMailToAllUsers;

    //function_to_run = generateTridealCoupons;

    //function_to_run = giveCouponsToInactiveUsersAndNotify;

    //function_to_run = generateGrouponCoupons;

    function_to_run();
    res.send("Running the function: " + function_to_run.name);
};
