/**
 * Created by harsh on 12/14/14.
 */


var async       = require('async');
var request    = require('request');

var utils       = require('./commonfunction');
var constants   = require('./constants');
var logging     = require('./logging');
var responses   = require('./responses');
var mailer      = require('./mailer');



////////////////////////////////////////////
// API handlers
////////////////////////////////////////////

// API handler for fetching the nearest available station when the driver ends/cancels the ride
exports.getNearestAvailableStation          = getNearestAvailableStation;
// API handler to acknowledge the stationing
exports.acknowledgeStationingRequest        = acknowledgeStationingRequest;
// API handler to check the sanity of the stationing numbers
exports.checkStationingSanity               = checkAllocationSanity;
// crone to report unacknowledged stationing by drivers
exports.reportUnacknowledgedStationing      = reportUnacknowledgedStationing;
// crone to remove the drivers from the station after given set timeout
exports.removeDriversFromStation            = removeDriversFromStation;

////////////////////////////////////////////
// Operations APIs
////////////////////////////////////////////

// Operations API to change the stationing for the driver dynamically
exports.relocateDriver                      = relocateAndNotifyDriver;
//// Operations API to remove the station assigned to the driver
exports.removeDriverFromStation             = removeDriverFromStation;

////////////////////////////////////////////
// other exposed function
////////////////////////////////////////////

// check if the driver has reached the assigned station when the driver's location update happens
exports.checkDriverAtStation                = checkDriverAtStation;
// mark drivers as leaving the stationing when they accept a request
exports.markDriverAsLeaving                 = markDriverAsLeaving;

exports.sendStationChangeNotification       = sendStationChangeNotification;





function getNearestAvailableStation(req, res){
    logging.startSection('get_nearest_available_station');
    logging.logRequest(req);

    var accessToken     = req.body.access_token;
    var latitude        = req.body.latitude;
    var longitude       = req.body.longitude;

    utils.authenticateUser(accessToken, function(user){
        if(user == 0){
            responses.sendAuthenticationError(res);
            return;
        }

        // no stationing for ad-hoc drivers
        if(user[0].reg_as === constants.userRegistrationStatus.DRIVER){
            var response = {
                flag    : constants.responseFlags.NO_STATION_ASSIGNED,
                log     : 'No stationing for ad-hoc drivers'};
            logging.logResponse(response);
            res.send(response);
            return;
        }

        var driverId = user[0].user_id;

        /*
         * Get the station already assigned to the driver,
         * This will be the case when the driver cancels the request he accepted within the STATION_HOLDING_WINDOW
         */
        fetchAlreadyAssignedStation(driverId, function(err, station){
            // If a station has already been assigned to the driver, send him the information for the same station
            if(station.station_id !== constants.STATION_NULL){
                // Get the arrival time in IST
                var arrivalTime = new Date(station.should_arrive_at);
                arrivalTime.setTime(arrivalTime.getTime() + 5.5 * 3600000);
                // Get the time of arrival in readable string format
                var timeString = arrivalTime.getMinutes().toString();
                timeString = timeString.length == 1 ? '0' + timeString : timeString;
                timeString = arrivalTime.getHours() + ':' + timeString;

                var response = {
                    flag        : constants.responseFlags.STATION_ASSIGNED,
                    station_id  : station.station_id,
                    address     : station.address,
                    latitude    : station.latitude,
                    longitude   : station.longitude,
                    arrival_time: station.should_arrive_at,
                    radius      : constants.STATION_PROXIMITY,
                    message     : 'Please reach ' + station.address + ' by ' + timeString
                };
                logging.logResponse(response);
                res.send(response);

                // If the driver was marked as leaving , then
                // mark the last stationing log as left to insert a new logging row
                // This is the case when the driver accepts the request and then cancels it
                if(station.status === constants.driverStationingStatus.LEAVING){
                    changeDriverStatus(driverId, station.station_id, constants.driverStationingStatus.LEFT);
                    logDriverStatusChange(driverId, station.station_id, constants.driverStationingStatus.LEFT);

                    changeDriverStatus(driverId, station.station_id, constants.driverStationingStatus.ASSIGNED);
                    logDriverStatusChange(driverId, station.station_id, constants.driverStationingStatus.ASSIGNED);
                }

                return;
            }

            /*
             * No station is currently assigned to the driver
             * Assign a new station to the driver
             */

            // Get all the available stations at the moment
            fetchAvailableStations(latitude, longitude, function(err, availableStations){
                var noStationResponse = {
                    flag    : constants.responseFlags.NO_STATION_AVAILABLE,
                    log     : 'No station is available at this point of time.'
                };

                if(err){
                    process.stderr.write('An error occurred while fetching available stations: %s', err);
                    logging.logResponse(noStationResponse);
                    res.send(noStationResponse);
                    return;
                }

                // TODO discuss this point, the chances that the stationing numbers are incorrect is high
                if(availableStations.length === 0){
                    process.stderr.write('No stations available at this point of time\n');
                    logging.logResponse(noStationResponse);
                    res.send(noStationResponse);
                    return;
                }

                // Assign the nearest available station to the driver,
                // can't use the first station in the array fetched earlier to handle the race conditions between drivers,
                // get the station id of the station assigned in the callback
                assignStation(availableStations, driverId, function(err, station){
                    if(err){
                        process.stderr.write('Some error occurred when assigning station to the driver');
                        logging.logResponse(noStationResponse);
                        res.send(noStationResponse);
                        return;
                    }

                    console.log('ASSIGNED STATION: ');
                    console.log(JSON.stringify(station));

                    // Assign the station to the driver and change the status to assigned
                    changeDriverStatus(driverId, station.station_id, constants.driverStationingStatus.ASSIGNED);
                    // Log the assigning of the station for the driver
                    logDriverStatusChange(driverId, station.station_id, constants.driverStationingStatus.ASSIGNED);

                    calculateAndUpdateArrivalTime(driverId, latitude, longitude, station.station_id, station.latitude, station.longitude, function(err, secondsToArrival){
                        var arrivalTime = new Date();
                        arrivalTime.setTime(arrivalTime.getTime() + secondsToArrival * 1000);
                        arrivalTime.setTime(arrivalTime.getTime() + 5.5 * 3600000);
                        var timeString = arrivalTime.getMinutes().toString();
                        timeString = timeString.length == 1 ? '0' + timeString : timeString;
                        timeString = arrivalTime.getHours() + ':' + timeString;
                        var response = {
                            flag        : constants.responseFlags.STATION_ASSIGNED,
                            station_id  : station.station_id,
                            address     : station.address,
                            latitude    : station.latitude,
                            longitude   : station.longitude,
                            arrival_time: arrivalTime.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                            radius      : constants.STATION_PROXIMITY,
                            message     : 'Please reach ' + station.address + ' by ' + timeString
                        };
                        logging.logResponse(response);
                        res.send(response);
                    });
                });
            });
        });
    });
};

function fetchAlreadyAssignedStation(driverId, callback){
    console.log('Fetching already assigned station for ' + driverId);
    var getExistingStation =
        'SELECT tb_stations.station_id, tb_stations.address, tb_stations.latitude, tb_stations.longitude FROM ' +
        '(SELECT driver_id, station_id, status FROM tb_driver_stationing WHERE driver_id = ?) as assigned_station ' +
        'JOIN ' +
        'tb_stations ' +
        'ON assigned_station.station_id = tb_stations.station_id';
    connection.query(getExistingStation, [driverId], function(err, stationingInfo){
        logging.logDatabaseQuery('Getting the existing stationing information for the driver', err, stationingInfo);
        if(err || stationingInfo.length === 0){
            return process.nextTick(callback.bind(null, err, { station_id : constants.STATION_NULL }));
        }
        else{
            getExistingEtaForStationing(driverId, stationingInfo[0].station_id, function(err, expectedArrivalTime){
                stationingInfo[0].should_arrive_at = expectedArrivalTime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                process.nextTick(callback.bind(null, err, stationingInfo[0]));
            });
        }
    });
}

function getExistingEtaForStationing(driverId, stationId, callback){
    var getStationingLogs =
        'SELECT should_arrive_on ' +
        'FROM tb_stationing_logs ' +
        'WHERE driver_id = ? AND station_id = ? ' +
        'ORDER BY `log_id` DESC ' +
        'LIMIT 1';
    connection.query(getStationingLogs, [driverId, stationId], function(err, stationingLog){
        logging.logDatabaseQuery('Getting the recent stationing log for the driver', err, stationingLog);

        if(err){
            return process.nextTick(callback.bind(null, err, null));
        }

        return process.nextTick(callback.bind(null, err, stationingLog[0].should_arrive_on));
    });
}

function fetchAvailableStations(driverLatitude, driverLongitude, callback){
    var getAvailableStations =
        'SELECT `station_id`, `station_number`, `latitude`, `longitude`, `address` ' +
        'FROM `tb_stations` ' +
        'WHERE `available` < `required` AND `start_time` <= CURTIME() AND `end_time` > CURTIME()';
    connection.query(getAvailableStations, [], function(err, allAvailableStations){
        logging.logDatabaseQuery('Getting all the available stations for the current time', err, allAvailableStations);
        if(err){
            return process.nextTick(callback.bind(null, err, null));
        }

        var i = 0;
        for(i = 0; i < allAvailableStations.length; i++){
            allAvailableStations[i].distance = utils.calculateDistance(driverLatitude, driverLongitude, allAvailableStations[i].latitude, allAvailableStations[i].longitude);
        }

        allAvailableStations = utils.sortByKeyAsc(allAvailableStations, 'distance');

        return process.nextTick(callback.bind(null, err, allAvailableStations));
    });
}

function assignStation(availableStations, driverId, callback_main){
    var index = 0;
    var assignedStation = null;

    async.whilst(
        // run while we have not exhausted all stations and no station has been assigned yet
        function(){
            return index < availableStations.length && assignedStation === null;
        },
        // function to be run
        function(callback){
            fillSlotAtStation(availableStations[index].station_id, function(err, assigned){
                if(err){
                    process.stderr.write('An error occurred while assigning a station to driver: ' + driverId + ', ' + new Date().toISOString());
                    process.stderr.write('Available stations:\n' + JSON.stringify(availableStations));
                    return process.nextTick(callback.bind(null, err));
                }

                // return the station assigned to the driver
                if(assigned === true){
                    assignedStation = availableStations[index];
                    return process.nextTick(callback.bind(null, err));
                }

                // increment the index to test the condition for running the function again
                index++;
            });
        },
        // function to be run in case of error or when the loop terminates
        function(err){
            if(err){
                return process.nextTick(callback_main.bind(null, err));
            }

            if(assignedStation !== null){
                return process.nextTick(callback_main.bind(null, err, assignedStation));
            }

            if(index === availableStations.length){
                err = new Error('All stations exhausted but no free station was found');
                return process.nextTick(callback_main.bind(null, err));
            }

            process.stderr.write('No such condition should ever arise when assigning a new station to the driver');
            err = new Error('No such condition should ever arise when assigning a new station to the driver');
            return process.nextTick(callback_main.bind(null, err));
        }
    );
}

function fillSlotAtStation(stationId, callback){
    var increaseNumDrivers =
        'UPDATE `tb_stations` ' +
        'SET `available` = `available` + 1 ' +
        'WHERE `station_id` = ? AND `available` < `required`';
    connection.query(increaseNumDrivers, [stationId], function(err, result){
        if(err){
            process.stderr.write('An error occurred while increasing the number of drivers available at the station: %s', stationId);
            return process.nextTick(callback.bind(null, err));
        }

        // If the number was increased, return true
        if(result.affectedRows > 0){
            return process.nextTick(callback.bind(null, err, true));
        }
        else{
            return process.nextTick(callback.bind(null, err, false));
        }
    });
}

function calculateAndUpdateArrivalTime(driverId, latitude, longitude, stationId, stationLatitude, stationLongitude, callback){
    var getArrivalTime =
        'https://maps.googleapis.com/maps/api/directions/json?' +
        'origin=' + latitude + ',' + longitude + '&' +
        'destination=' + stationLatitude + ',' + stationLongitude;
    //console.log('GOOGLE QUERY ' + getArrivalTime);
    request(getArrivalTime, function (error, response, body)
    {
        var timeForArrival = 0;
        if (!error && response.statusCode == 200){
            body = JSON.parse(body);
            //console.log(JSON.stringify(body, undefined, 2));

            timeForArrival = body.routes[0].legs[0].duration.value;
            var buffer = timeForArrival * constants.STATIONING_ARRIVAL_BUFFER;
            buffer = buffer < 180 ? 180 : buffer;
            timeForArrival += buffer;
        }
        else{
            timeForArrival = constants.STATIONING_ARRIVAL_DEFAULT;
        }

        console.log('SECONDS TO ARRIVAL: ' + timeForArrival);
        var updateArrivalTime =
            'UPDATE `tb_stationing_logs` ' +
            'SET `should_arrive_on` = TIMESTAMPADD(SECOND, ?, `assigned_on`) ' +
            'WHERE `driver_id` = ? AND `station_id` = ? ' +
            'ORDER BY `log_id` DESC LIMIT 1';
        connection.query(updateArrivalTime, [timeForArrival, driverId, stationId], function(err, result){
            logging.logDatabaseQuery('Updating the arrival time for the stationing', err, result);
        });

        return process.nextTick(callback.bind(null, null, timeForArrival));
    });
}




function changeDriverStatus(driverId, stationId, status){
    console.log(driverId + ', ' + stationId + ', ' + status);
    var changeStatus =
        'INSERT INTO `tb_driver_stationing` ' +
        '(`driver_id`, `station_id`, `status`) ' +
        'VALUES (?, ?, ?) ' +
        'ON DUPLICATE KEY ' +
        'UPDATE `station_id` = VALUES(`station_id`), `status` = VALUES(`status`)';
    connection.query(changeStatus, [driverId, stationId, status], function(err, result){
        logging.logDatabaseQuery('Changing the status of the driver when stationing', err, result);
        if(err){
            process.stderr.write('An error occurred when updating the status of the driver when stationing');
        }
    });
}

function logDriverStatusChange(driverId, stationId, status, reason){
    var logStatus = '';
    var values = [];

    switch(status){
        case constants.driverStationingStatus.ASSIGNED:
            logStatus =
                'INSERT INTO `tb_stationing_logs` ' +
                '(`driver_id`, `station_id`, `assigned_on`) ' +
                'VALUES (?, ?, NOW())';
            values = [driverId, stationId];
            break;
        case constants.driverStationingStatus.ACKNOWLEDGED:
            logStatus =
                'UPDATE `tb_stationing_logs` ' +
                'SET `acknowledged_on` = NOW() ' +
                'WHERE `driver_id` = ? AND `station_id` = ? ' +
                'ORDER BY `log_id` DESC LIMIT 1';
            values = [driverId, stationId];
            break;
        case constants.driverStationingStatus.REACHED:
            logStatus =
                'UPDATE `tb_stationing_logs` ' +
                'SET `reached_on` = NOW() ' +
                'WHERE `driver_id` = ? AND `station_id` = ? ' +
                'ORDER BY `log_id` DESC LIMIT 1';
            values = [driverId, stationId];
            break;
        case constants.driverStationingStatus.LEAVING:
            logStatus =
                'UPDATE `tb_stationing_logs` ' +
                'SET `leaving_on` = NOW(), `reason_for_leaving` = ? ' +
                'WHERE `driver_id` = ? AND `station_id` = ? ' +
                'ORDER BY `log_id` DESC LIMIT 1';
            values = [reason, driverId, stationId];
            break;
        case constants.driverStationingStatus.LEFT:
            logStatus =
                'UPDATE `tb_stationing_logs` ' +
                'SET `left_on` = NOW() ' +
                'WHERE `driver_id` = ? AND `station_id` = ? ' +
                'ORDER BY `log_id` DESC LIMIT 1';
            values = [driverId, stationId];
            break;
    }

    connection.query(logStatus, values, function(err, result){
        logging.logDatabaseQuery('Adding logs for changing stationing status for the driver', err, result);
        if(err){
            process.stderr.write('An error occurred when logging the status change for stationing the driver');
            return;
        }
    });
}





function reportUnacknowledgedStationing(req, res){
    logging.startSection('Reporting unacknowledged stationings');
    logging.logRequest(req);

    var response = {};

    var getUnacknowledgedStationing =
        'SELECT * FROM ' +
            '(SELECT `log_id`, `driver_id`, `station_id`, `assigned_on` ' +
            'FROM `tb_stationing_logs` ' +
	        'WHERE `acknowledged_on` IS NULL AND `unacknowledged_reported` = 0 AND TIMESTAMPDIFF(SECOND, `assigned_on`, NOW()) > ?) AS `unacknowledged_requests` ' +
        'JOIN ' +
            '`tb_stations` ' +
        'JOIN ' +
            '(SELECT `user_id`, `user_name`, `phone_no`, `user_email` FROM `tb_users` WHERE `reg_as` IN (1,2)) as `drivers` ' +
        'ON unacknowledged_requests.station_id = tb_stations.station_id AND unacknowledged_requests.driver_id = drivers.user_id' ;
    connection.query(getUnacknowledgedStationing, [constants.STATIONING_ACK_WINDOW], function(err, unacknowledgedRequests){
        logging.logDatabaseQuery('Getting the unacknowledged stationing requests', err, unacknowledgedRequests);
        if(err){
            response = {
                flag    : constants.responseFlags.ACTION_FAILURE,
                error   : 'The unacknowledged stationings couldn\'t be reported now'
            };
            logging.logResponse(response);
            res.send(response);
            return;
        }

        markUnacknowledgedAsReported(unacknowledgedRequests);

        if(unacknowledgedRequests.length > 0){
            mailer.sendMailForUnacknowledgedStationing(unacknowledgedRequests);
        }
    });

    response = {
        flag    : constants.responseFlags.ACTION_COMPLETE,
        message : 'The unacknowledged stationings have been reported'
    };
    res.send(response);
}

function markUnacknowledgedAsReported(unacknowledgedRequests){
    var logIds = [];
    var i = 0;
    for(i = 0; i < unacknowledgedRequests.length; i++){
        logIds.push(unacknowledgedRequests[i].log_id);
    }

    if(logIds.length > 0){
        var reportUnacknowledged =
            'UPDATE `tb_stationing_logs` ' +
            'SET `unacknowledged_reported` = 1 ' +
            'WHERE `log_id` IN (' + logIds.join(',') + ')';
        connection.query(reportUnacknowledged, [], function(err, result){
            logging.logDatabaseQuery('Changing the flag that unacknowledged stationing has been reported', err, result);
        });
    }
}




function acknowledgeStationingRequest(req, res){
    logging.startSection('Acknowledging stationing request');
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var stationId   = req.body.station_id;

    utils.authenticateUser(accessToken, function(user){
        if(user == 0){
            responses.sendAuthenticationError(res);
            return;
        }

        var driverId = user[0].user_id;
        changeDriverStatus(driverId, stationId, constants.driverStationingStatus.ACKNOWLEDGED);
        logDriverStatusChange(driverId, stationId, constants.driverStationingStatus.ACKNOWLEDGED);

        var response = {
            flag    : constants.responseFlags.ACTION_COMPLETE,
            log     : 'logged successfully'};
        logging.logResponse(response);
        res.send(response);
    });
}




function checkDriverAtStation(driverId, registrationStatus, latitude, longitude){
    // Stationing logic will only be there for dedicated drivers in the beginning,
    // so return before making any computations
    if(registrationStatus !== constants.userRegistrationStatus.DEDICATED_DRIVER){
        return;
    }

    var getStationInformation =
        'SELECT * FROM ' +
        '(SELECT `driver_id`, `station_id`, `status` FROM `tb_driver_stationing` WHERE `driver_id` = ?) as `assigned_station` ' +
        'JOIN ' +
        '(SELECT `station_id`, `latitude`, `longitude` FROM `tb_stations`) as `stations` ' +
        'ON assigned_station.station_id = stations.station_id';
    connection.query(getStationInformation, [driverId], function(err, station){
        logging.logDatabaseQuery('GETTING THE INFORMATION FOR THE STATION ASSIGNED TO THE DRIVER', err, station);

        if(err){
            return;
        }

        if(station.length === 0){
            return;
        }

        var inRange = checkDriverInRange(latitude, longitude, station[0].latitude, station[0].longitude);
        if(inRange){
            switch(station[0].status){
                case constants.driverStationingStatus.ACKNOWLEDGED:
                    changeDriverStatus(driverId, station[0].station_id, constants.driverStationingStatus.REACHED);
                    logDriverStatusChange(driverId, station[0].station_id, constants.driverStationingStatus.REACHED);
                    break;
            }
        }
        else{
            switch(station[0].status){
                case constants.driverStationingStatus.ACKNOWLEDGED:
                    checkArrivalComplianceAndReport(driverId, station[0].station_id);
                    break;
                // TODO implement this
                //case constants.driverStationingStatus.REACHED:
                //    reportLeavingWithoutNotice(driverId, station[0].station_id);
                //    break;
            }
        }
    });
}

function checkDriverInRange(latitude, longitude, stationLatitude, stationLongitude){
    var distance = utils.calculateDistance(latitude, longitude, stationLatitude, stationLongitude);
    return distance < constants.STATION_PROXIMITY;
}

function checkArrivalComplianceAndReport(driverId, stationId){
    checkArrivalCompliance(driverId, stationId, function(err, is_compliant, stationingLog){
        if(is_compliant === false && !stationingLog.not_arrived_reported){
            mailer.sendMailForArrivalNoncompliance(stationingLog);
            markNotArrivedAsReported(stationingLog.log_id);
        }
    });
}

function checkArrivalCompliance(driverId, stationId, callback){
    var getLastStationing =
        'SELECT `log_id`, `driver_id`, `should_arrive_on`, `not_arrived_reported` ' +
        'FROM `tb_stationing_logs` ' +
        'WHERE `driver_id` = ? AND `station_id` = ? ' +
        'ORDER BY `log_id` DESC LIMIT 1';
    connection.query(getLastStationing, [driverId, stationId], function(err, stationingLog){
        logging.logDatabaseQuery('GETTING THE LAST STATIONING LOG FOR THE DRIVER', err, stationingLog);

        if(err){
            process.stderr.write('Couldn\'t get the last stationing logs for the driver\n');
            return process.nextTick(callback.bind(null, err, true, null));
        }

        // If the case has not been reported earlier and the driver has not arrived within the stipulated time
        if(!stationingLog[0].non_arrived_reported && utils.timeDifferenceInMinutes(stationingLog[0].should_arrive_on, new Date()) > 0){
            var getCompleteInfo =
                'SELECT * FROM ' +
                    '(SELECT `log_id`, `driver_id`, `station_id`, `assigned_on`, `should_arrive_on`, `not_arrived_reported` ' +
                    'FROM `tb_stationing_logs` ' +
                    'WHERE `log_id` = ?) AS `non_compliant_stationing` ' +
                'JOIN ' +
                    '`tb_stations` ' +
                'JOIN ' +
                    '(SELECT `user_id`, `user_name`, `phone_no`, `user_email` FROM `tb_users` WHERE `user_id` = ?) as `driver` ' +
                'ON non_compliant_stationing.station_id = tb_stations.station_id AND non_compliant_stationing.driver_id = driver.user_id';
            connection.query(getCompleteInfo, [stationingLog[0].log_id, stationingLog[0].driver_id], function(err, stationingInfo){
                logging.logDatabaseQuery('GETTING THE COMPLETE INFORMATION FOR THE STATIONING', err, stationingInfo);
                return process.nextTick(callback.bind(null, err, false, stationingInfo[0]));
            });
        }
        else{
            return process.nextTick(callback.bind(null, err, true, stationingLog[0]));
        }
    });
}

function markNotArrivedAsReported(logId){
    var markReported =
        'UPDATE `tb_stationing_logs` ' +
        'SET `not_arrived_reported` = 1 ' +
        'WHERE `log_id` = ?';
    connection.query(markReported, [logId], function(err, result){
        logging.logDatabaseQuery('Marking driver not arrived within time as reported', err, result);
    });
}




function markDriverAsLeaving(driverId, reason){
    console.log('MARKING THE DRIVER AS LEAVING WHEN THE DRIVER ACCEPTS A REQUEST');
    var getAssignedStation =
        'SELECT `driver_id`, `station_id`, `status` ' +
        'FROM `tb_driver_stationing` ' +
        'WHERE `driver_id` = ?';
    connection.query(getAssignedStation, [driverId], function(err, assignedStation){
        logging.logDatabaseQuery('GETTING THE STATION ASSIGNED TO THE DRIVER', err, assignedStation);

        if(err){
            return;
        }

        // For ad-hoc drivers or for drivers with outdated build
        if(assignedStation.length === 0){
            process.stdout.write('NO STATION ASSIGNED TO THE DRIVER: ' + driverId);
            return;
        }

        var stationId = assignedStation[0].station_id;

        if(stationId !== constants.STATION_NULL){
            changeDriverStatus(driverId, stationId, constants.driverStationingStatus.LEAVING);
            logDriverStatusChange(driverId, stationId, constants.driverStationingStatus.LEAVING, reason);
        }
    });
}




function sendStationChangeNotification(driverId){

}




function removeDriverFromStation(req, res){
    logging.startSection('Removing driver from the assigned station');
    logging.logRequest(req);

    var password = req.body.password;
    var driverId = req.body.driver_id;

    if(password === constants.SUPER_ADMIN_PASSWORD){
        res.send('You are not authorized to perform this operation');
        return;
    }

    fetchAlreadyAssignedStation(driverId, function(err, station){
        if(err){
            res.send('An error occurred when fetching the existing station for the driver.');
            return;
        }

        if(station.station_id === constants.STATION_NULL){
            res.send('No station assigned to the driver');
            return;
        }

        removeStationForDriver(driverId);
        freeSlotAtStation(station.station_id);
        logDriverStatusChange(driverId, station.station_id, constants.driverStationingStatus.LEFT);

        res.send('The driver has been successfully removed from the station');
        return;
    });
}




function removeDriversFromStation(req, res){
    var getStationingLogs =
        'SELECT `log_id`, `driver_id`, `station_id` FROM ' +
        '`tb_stationing_logs` ' +
        'WHERE `leaving_on` IS NOT NULL AND `left_on` IS NULL AND TIMESTAMPDIFF(SECOND, `leaving_on`, NOW()) > ?';
    connection.query(getStationingLogs, [constants.STATION_LEAVING_WINDOW], function(err, stationingLogs){
        logging.logDatabaseQuery('Getting the stationing requests to be marked as left', err, stationingLogs);

        if(err){
            return;
        }

        var i = 0;
        for(i = 0; i < stationingLogs.length; i++){
            removeStationForDriver(stationingLogs[i].driver_id);
            freeSlotAtStation(stationingLogs[i].station_id);
            logDriverStatusChange(stationingLogs[i].driver_id, stationingLogs[i].station_id, constants.driverStationingStatus.LEFT);
        }
    });
}

function removeStationForDriver(driverId){
    var removeStation =
        'UPDATE `tb_driver_stationing` ' +
        'SET `station_id` = ?, `status` = ? ' +
        'WHERE `driver_id` = ?';
    connection.query(removeStation, [constants.STATION_NULL, constants.driverStationingStatus.NO_STATION, driverId], function(err, result){
        logging.logDatabaseQuery('Removing the station assigned to the driver', err, result);
    });
}

function freeSlotAtStation(stationId){
    var changeAvailability =
        'UPDATE `tb_stations` ' +
        'SET `available` = `available` - 1 ' +
        'WHERE `station_id` = ?';
    connection.query(changeAvailability, [stationId], function(err, result){
        logging.logDatabaseQuery('Increasing the availability for the station', err, result);
    });
}




function relocateAndNotifyDriver(req, res){
    logging.startSection('Sending notification to the driver app to get a new station for the driver');
    logging.logRequest(req);

    var password      = req.body.password;
    var driverId      = req.body.driver_id;
    var stationNumber = req.body.station_number;

    if(password !== constants.SUPER_ADMIN_PASSWORD){
        res.send('You are not authorized to perform this operation');
        return;
    }

    var driverLocation = {};
    var assignedStation = null;
    var stationBeingAssigned = null;

    var assignedStationWrapper = {station : null};
    var stationBeingAssignedWrapper = {station : null};

    async.parallel(
        [
            getDriverCurrentLocation.bind(null, driverId, driverLocation),
            getAlreadyAssignedStation.bind(null, driverId, assignedStationWrapper),
            getStationFromStationNumber.bind(null, stationNumber, stationBeingAssignedWrapper)
        ],
        function(err) {
            if (err) {
                res.send('This assignment can\'t be completed right now.\nError:\n' + JSON.stringify(err, undefined, 2));
                return;
            }

            assignedStation = assignedStationWrapper.station;
            stationBeingAssigned = stationBeingAssignedWrapper.station;

            console.log('Driver location');
            console.log(JSON.stringify(driverLocation, undefined, 2));
            console.log('Already assigned station');
            console.log(JSON.stringify(assignedStation, undefined, 2));
            console.log('Station being assigned');
            console.log(JSON.stringify(stationBeingAssigned, undefined, 2));

            if (assignedStation !== null) {
                res.send('A station has already been assigned to the driver. Please remove the driver from the station before assigning him a new station');
                return;
            }

            if (stationBeingAssigned === null) {
                res.send('No entry for this station number at this point of time. This station can\'t be assigned to the driver right now');
                return;
            }

            // Try assigning the station to the driver
            var station = stationBeingAssigned;

            fillSlotAtStation(station.station_id, function (err, assigned) {
                if (err) {
                    res.send('Some error occurred when assigning this station to the driver');
                    return;
                }

                if (!assigned) {
                    res.send('This station can\'t be assigned to the driver at this point of time. Please check the availability at the station');
                    return;
                }

                // Send a push to the driver to change the station
                var message = 'A new Station has been assigned to you.'
                var flag    = 1;
                var payload = {
                    flag    : constants.notificationFlags.STATION_CHANGED,
                    message : message};
                utils.sendNotification(driverId, message, flag, payload);

                // Assign the station to the driver and change the status to assigned
                changeDriverStatus(driverId, station.station_id, constants.driverStationingStatus.ASSIGNED);
                // Log the assigning of the station for the driver
                logDriverStatusChange(driverId, station.station_id, constants.driverStationingStatus.ASSIGNED);

                calculateAndUpdateArrivalTime(driverId, driverLocation.latitude, driverLocation.longitude, station.station_id, station.latitude, station.longitude, function (err, secondsToArrival) {
                    var arrivalTime = new Date();
                    arrivalTime.setTime(arrivalTime.getTime() + secondsToArrival * 1000);
                    arrivalTime.setTime(arrivalTime.getTime() + 5.5 * 3600000);
                    var timeString = arrivalTime.getMinutes().toString();
                    timeString = timeString.length == 1 ? '0' + timeString : timeString;
                    timeString = arrivalTime.getHours() + ':' + timeString;
                    var stationInfo = {
                        flag        : constants.responseFlags.STATION_ASSIGNED,
                        station_id  : station.station_id,
                        address     : station.address,
                        latitude    : station.latitude,
                        longitude   : station.longitude,
                        arrival_time: arrivalTime.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                        radius      : constants.STATION_PROXIMITY,
                        message     : 'Please reach ' + station.address + ' by ' + timeString
                    };
                    logging.logResponse(stationInfo);
                    res.send('The station has been assigned to the driver and expected to arrive by ' + timeString + '\nDetails:\n' + JSON.stringify(stationInfo, undefined, 2));
                });
            });
        }
    );

    function getDriverCurrentLocation(driverId, location, callback){
        var getLocation =
            'SELECT current_location_latitude AS latitude, current_location_longitude AS longitude ' +
            'FROM tb_users ' +
            'WHERE user_id = ?';
        connection.query(getLocation, [driverId], function(err, driver){
            logging.logDatabaseQuery('Getting the current location of the driver', err, driver);

            if(err){
                err = new Error('Error when fetching the location of the driver');
                return process.nextTick(callback.bind(null, err));
            }

            location.latitude  = driver[0].latitude;
            location.longitude = driver[0].longitude;

            return process.nextTick(callback.bind(null, err));
        });
    }

    function getAlreadyAssignedStation(driverId, resultWrapper, callback){
        fetchAlreadyAssignedStation(driverId, function(err, station) {
            if (err) {
                err = new Error('Error when fetching the station already assigned to the driver');
                return process.nextTick(callback.bind(null, err));
            }

            if(station.station_id === constants.STATION_NULL) {
                resultWrapper.station = null;
                return process.nextTick(callback.bind(null, err));
            }

            resultWrapper.station = station;
            return process.nextTick(callback.bind(null, err));
        });
    }

    function getStationFromStationNumber(stationNumber, resultWrapper, callback){
        var getStationId =
            'SELECT station_id, latitude, longitude ' +
            'FROM tb_stations ' +
            'WHERE station_number = ? AND start_time < NOW() AND end_time >= NOW()';
        connection.query(getStationId, [stationNumber], function(err, station){
            logging.logDatabaseQuery('Getting the station id for the station number at this point of time', err, station);

            if(err){
                err = new Error('Error when getting the station from the station number provided.');
                return process.nextTick(callback.bind(null, err));
            }

            if(station.length === 0){
                resultWrapper.station = null;
                return process.nextTick(callback.bind(null, err));
            }

            resultWrapper.station = station[0];
            return process.nextTick(callback.bind(null, err));
        });
    }
}




function checkAllocationSanity(req , res) {
    var stationReports = {};
    var dayReport = [];

    async.parallel(
        [
            checkAllocationForStations.bind(null, stationReports),
            checkAllocationForDay.bind(null, dayReport)
        ],
        function (err) {
            if(err){
                process.stderr.write('An error occurred when checking the daily allocation strategy\n');
                process.stderr.write('Error: ' + JSON.stringify(err) + '\n');
                return;
            }

            //var response = {
            //    station_report  : reportStations(stationReports),
            //    daily_report    : reportAllocationIntervals(dayReport)
            //};
            var response = {
                station_report  : stationReports,
                daily_report    : dayReport
            };
            logging.logResponse(response);
            res.send(JSON.stringify(response, undefined, 2));
        }
    );
}

// Get allocations for all the stations and see if there are intervals defined that are intersecting
function checkAllocationForStations(stationReports, callback){
    var getAllStations =
        'SELECT * FROM ' +
        '`tb_stations` ' +
        'ORDER BY `station_number`, `start_time`';
    connection.query(getAllStations, [], function(err, stationingInfo){
        logging.logDatabaseQuery('Fetching information for all the stations', err, stationingInfo);
        if(err){
            return process.nextTick(callback.bind(null, err));
        }

        if(stationingInfo.length == 0){
            process.stdout.write('No stations defined yet\n');
            return process.nextTick(callback.bind(null, err));
        }

        var lastStation = constants.STATION_NULL;

        for(var i = 0; i < stationingInfo.length; i++){
            if(stationingInfo[i].station_id !== lastStation){
                stationReports[stationingInfo[i].station_id.toString()] = {
                    station_id  : stationingInfo[i].station_id,
                    latitude    : stationingInfo[i].latitude,
                    longitude   : stationingInfo[i].longitude,
                    address     : stationingInfo[i].address,
                    sane        : true,
                    allocations : [],
                    failures    : []
                };

                lastStation = stationingInfo[i].station_id;
            }

            var allocation = {
                start_time  : stationingInfo[i].start_time,
                end_time    : stationingInfo[i].end_time,
                required    : stationingInfo[i].required
            };

            stationReports[lastStation.toString()].allocations.push(allocation);
        }

        var j = 0;
        for(var stationId in stationReports){
            for(j = 1; j < stationReports[stationId.toString()].allocations.length; j++){
                var sane =
                    stationReports[stationId.toString()].allocations[j-1].end_time.localeCompare(
                        stationReports[stationId.toString()].allocations[j].start_time) < 0;
                if(!sane){
                    stationReports[stationId.toString()].sane = false;
                    stationReports[stationId.toString()].failures.push({
                        reason          : 'The two time intervals intersect',
                        prev_start_time : stationReports[stationId.toString()].allocations[j-1].start_time,
                        prev_end_time   : stationReports[stationId.toString()].allocations[j-1].end_time,
                        start_time      : stationReports[stationId.toString()].allocations[j].start_time,
                        end_time        : stationReports[stationId.toString()].allocations[j].end_time
                    });
                }
            }
        }

        reportStations(stationReports);

        return process.nextTick(callback.bind(null, null));
    });
}

// Check if throughout the day there is an interval where number of allocations at all stations
// are less than the number of dedicated drivers available at that point of time
function checkAllocationForDay(dayReport, callback){
    var stationingReport = [];
    var availabilityReport = [];

    async.parallel(
        [
            getDriversStationed.bind(null, stationingReport),
            getDedicatedDriverAvailability.bind(null, availabilityReport)
        ],
        function(err){
            if(err) {
                process.stderr.write('Couldn\'t complete the sanity check for allocation for the whole day\n');
                process.stderr.write('Error: ' + JSON.stringify(err) + '\n');
                return process.nextTick(callback.bind(null, err));
            }

            var incorrectIntervals = [];

            fetchIncorrectAllocationIntervals(stationingReport, availabilityReport, dayReport);
            reportAllocationIntervals(dayReport);

            return process.nextTick(callback.bind(null, err));
        }
    )
};

function getDriversStationed(stationingReport, callback){
    var getStations =
        'SELECT * FROM ' +
        '`tb_stations`';
    connection.query(getStations, [], function(err, stations){
        logging.logDatabaseQuery('Getting the information for all the stations', err, stations);

        if(err){
            return process.nextTick(callback.bind(null, err));
        }

        stationingReport.push({
            start_time  : '00:00:00',
            end_time    : '23:59:59',
            required    : 0
        });

        var index = 0;
        var j = 0;
        for(var i = 0; i < stations.length; i++){
            index = getIntervalIncludingTimestamp(stationingReport, stations[i].start_time);
            var interval1 = {
                start_time  : stationingReport[index].start_time,
                end_time    : stations[i].start_time,
                required    : stationingReport[index].required
            };
            var interval2 = {
                start_time  : stations[i].start_time,
                end_time    : stationingReport[index].end_time,
                required    : stationingReport[index].required
            };
            stationingReport.splice(index, 1, interval1, interval2);
            for(j = index+1; j < stationingReport.length; j++){
                stationingReport[j].required += stations[i].required;
            }

            index = getIntervalIncludingTimestamp(stationingReport, stations[i].end_time);
            var interval3 = {
                start_time  : stationingReport[index].start_time,
                end_time    : stations[i].end_time,
                required    : stationingReport[index].required
            };
            var interval4 = {
                start_time  : stations[i].end_time,
                end_time    : stationingReport[index].end_time,
                required    : stationingReport[index].required
            };
            stationingReport.splice(index, 1, interval3, interval4);
            for(j = index+1; j < stationingReport.length; j++){
                stationingReport[j].available -= stations[i].required;
            }
        }

        collapseIntervals(stationingReport);

        return process.nextTick(callback.bind(null, err));
    });
}

function getDedicatedDriverAvailability(availabilityReport, callback){
    var getTimings =
        'SELECT * FROM ' +
        '(SELECT `user_id` FROM `tb_users` WHERE `reg_as` = 2) AS `dedicated_drivers` ' +
        'JOIN ' +
        '`tb_timings` ' +
        'ON dedicated_drivers.user_id = tb_timings.driver_id';
    connection.query(getTimings, [], function(err, driverTimings){
        logging.logDatabaseQuery('Getting the timings for all dedicated drivers', err, driverTimings);

        if(err){
            return process.nextTick(callback.bind(null, err));
        }

        availabilityReport.push({
            start_time  : '00:00:00',
            end_time    : '23:59:59',
            available   : 0
        });

        var index = 0;
        var j = 0;
        for(var i = 0; i < driverTimings.length; i++){
            index = getIntervalIncludingTimestamp(availabilityReport, driverTimings[i].start_time);
            var interval1 = {
                start_time  : availabilityReport[index].start_time,
                end_time    : driverTimings[i].start_time,
                available   : availabilityReport[index].available
            };
            var interval2 = {
                start_time  : driverTimings[i].start_time,
                end_time    : availabilityReport[index].end_time,
                available   : availabilityReport[index].available
            };
            availabilityReport.splice(index, 1, interval1, interval2);
            for(j = index+1; j < availabilityReport.length; j++){
                availabilityReport[j].available++;
            }

            index = getIntervalIncludingTimestamp(availabilityReport, driverTimings[i].end_time);
            var interval3 = {
                start_time  : availabilityReport[index].start_time,
                end_time    : driverTimings[i].end_time,
                available   : availabilityReport[index].available
            };
            var interval4 = {
                start_time  : driverTimings[i].end_time,
                end_time    : availabilityReport[index].end_time,
                available   : availabilityReport[index].available
            };
            availabilityReport.splice(index, 1, interval3, interval4);
            for(j = index+1; j < availabilityReport.length; j++){
                availabilityReport[j].available--;
            }
        }

        collapseIntervals(availabilityReport);

        return process.nextTick(callback.bind(null, err));
    });
}

function getIntervalIncludingTimestamp(intervals, time){
    for(var i = 1; i < intervals.length; i++){
        // if the time is more than the
        if(intervals[i-1].end_time.localeCompare(time) <= 0     // intervals[i-1].end_time <= time
            && intervals[i].end_time.localeCompare(time) > 0) {  // intervals[i].end_time > time
            return i;
        }
    }
    return intervals.length - 1;
}

function fetchIncorrectAllocationIntervals(stationingReport, availabilityReport, incorrectIntervals){
    console.log('GETTING INCORRECT ALLOCATION INTERVALS');
    console.log('Stationing reports');
    console.log(JSON.stringify(stationingReport, undefined, 2));
    console.log('availability reports');
    console.log(JSON.stringify(availabilityReport, undefined, 2));

    var i = 0;
    var j = 0;
    while(i < stationingReport.length && j < availabilityReport.length){
        if(stationingReport[i].required < availabilityReport[j].available){
            incorrectIntervals.push({
                stationing_start_time   : stationingReport[i].start_time,
                stationing_end_time     : stationingReport[i].end_time,
                required                : stationingReport[i].required,
                availability_start_time : availabilityReport[j].start_time,
                availability_end_time   : availabilityReport[j].end_time,
                available               : availabilityReport[j].available
            });
        }
        if(stationingReport[i].end_time.localeCompare(availabilityReport[i].end_time) < 0){
            i++;
        }
        else{
            j++;
        }
    }
}

function collapseIntervals(report){
    var i = 0;
    for(i = 0; i < report.length; i++){
        if(report[i].start_time.localeCompare(report[i].end_time) == 0){
            report.splice(i, 1);
            i--;
        }
    }
}

function reportStations(stationReports){
    console.log('Station reports: ');
    console.log(JSON.stringify(stationReports));
}

function reportAllocationIntervals(incorrectIntervals){
    console.log('Incorrect allocation intervals: ');
    console.log(JSON.stringify(incorrectIntervals));
}