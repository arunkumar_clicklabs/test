//
// Use this module to implement functions for fetching data for the user
//

var request = require('request');
var async = require('async');

var logging = require('./logging');
var utils = require('./commonfunction');
var constants = require('./constants');
var mailer = require('./mailer');
var messenger = require('./messenger');
var responses = require('./responses');

g_enum_responseFlags = {
    PARAMETER_MISSING: 100,
    INVALID_ACCESS_TOKEN: 101,
    ERROR_IN_EXECUTION: 102
}

// TODO move these function to some common file during refactoring
function sendErrorResponse(res) {
    var response = {"error": 'something went wrong', "flag": g_enum_responseFlags.ERROR_IN_EXECUTION};
    logging.logResponse(response);
    res.send(JSON.stringify(response));
}

function sendParameterMissingResponse(res) {
    var response = {"error": "some parameter missing", "flag": g_enum_responseFlags.PARAMETER_MISSING};
    logging.logResponse(response);
    res.send(response);
}

function sendAuthenticationError(res){
    var response = {"error": 'invalid access token', "flag": g_enum_responseFlags.INVALID_ACCESS_TOKEN};
    logging.logResponse(response);
    res.send(JSON.stringify(response));
}


exports.blacklist_email         = blacklistEmail;
exports.block_email_category    = blockEmailCategoriesForUser;
exports.blockRatingPopupForUser = blockRatingPopupForUser;


// Fetch all the calls missed by the driver
exports.get_missed_rides = function(req, res)
{
	var access_token = req.body.access_token;

    var manValues = [access_token];
    var checkData = utils.checkBlank(manValues);
    if (checkData == 1)
    {
        sendParameterMissingResponse(res);
    }
    else
    {
        utils.authenticateUser(access_token, function(authentication_result)
        {
            if (authentication_result == 0)
            {
                sendAuthenticationError(res);
            }
            else
            {
            	var driver_id = authentication_result[0].user_id;
                var status_required = [constants.engagementStatus.REJECTED_BY_DRIVER, constants.engagementStatus.TIMEOUT, constants.engagementStatus.ACCEPTED_THEN_REJECTED];
                status_required = status_required.toString();

                // Fetch the required data for the engagements that the driver missed
            	var fetch_missed_requests = "SELECT `user_id`, `engagement_id`, `pickup_latitude`, `pickup_longitude`, `pickup_location_address`, `current_time`, `session_id`, `status` " +
                    "FROM `tb_engagements` " + 
                    "WHERE `driver_id`=? && `status` IN (" + status_required + ") ORDER BY `current_time` DESC LIMIT 10";
            	connection.query(fetch_missed_requests, [driver_id], function(err, result_engagements)
                {
                	//logging.logDatabaseQuery("Fetching engagements missed by the driver.", err, result_engagements, req.body);

                	var num_engagements = result_engagements.length;

                	// Using engagements as a map to hold the data (engagement Object) for engagements
                	// session_id is the key for this map
                	var engagements = {};
                    var userIDs = [];
                	for(var i=0; i<num_engagements; i++)
                	{
                        var engagement = {};
                        engagement.engagement_id 	= result_engagements[i].engagement_id;
                        engagement.latitude 		= result_engagements[i].pickup_latitude;
                        engagement.longitude  		= result_engagements[i].pickup_longitude;
                        engagement.address          = result_engagements[i].pickup_location_address;
                        engagement.timestamp        = result_engagements[i].current_time;
                        engagement.customerID       = result_engagements[i].user_id;
                        engagement.status           = result_engagements[i].status;

                		var session_id = result_engagements[i].session_id;
                		engagements[session_id] = engagement;

                        userIDs.push(result_engagements[i].user_id);
                	}

                    // Get the name of all the users with the user IDs returned
                    var got_usernames = false;
                    userIDs = userIDs.toString();
                    var user_names_query = "SELECT `user_name`, `user_id` FROM `tb_users` WHERE `user_id` IN (" + userIDs + ")";
                    connection.query(user_names_query, [], function(err, result_usernames)
                    {
                        logging.logDatabaseQuery("Fetching user names for the customer.", err, result_usernames, req.body);

                        var users = {};
                        if(result_usernames.length > 0)
                        {
                            for(var i=0; i<result_usernames.length; i++)
                            {
                                users[result_usernames[i].user_id] = result_usernames[i].user_name;
                            }

                            // add this to the engagements
                            for(session_id in engagements)
                            {
                                var customerID = engagements[session_id].customerID;
                                engagements[session_id].user_name = users[customerID];
                            }
                        }
                        got_usernames = true;

                        // Have we received all results?
                        // Send the response when you have all the results
                        if (got_usernames == true && locations_received == num_engagements)
                        {
                            sendMissedRequests(engagements);
                        }
                    });


                    // Get the displacement of the driver from the customer when the request is missed
                    var got_driver_location = false;
                    var locations_received = 0;
                    for(session_id in engagements)
                    {
                        getDisplacementForPickup(session_id);
                    }

                    function getDisplacementForPickup(session_id)
                    {
                        engagements[session_id].distance = -1;

                        var pickup_time = engagements[session_id].timestamp;
                        var get_location_query = "SELECT `latitude`, `longitude`, `location_updated_timestamp` " +
                            "FROM `tb_updated_user_location` " +
                            "WHERE `user_id`=? && `location_updated_timestamp` <=? " +
                            "ORDER BY `location_updated_timestamp` ASC " +
                            "LIMIT 1";
                        connection.query(get_location_query, [driver_id, pickup_time], function(err, result_location)
                        {
                            //logging.logDatabaseQuery("Fetching location of driver when request missed.", err, result_location, req.body);

                            if(!err && result_location.length > 0)
                            {
                                // Calculate the displacement between the two locations (returned in meters)
                                var distance = utils.calculateDistance(engagements[session_id].latitude, engagements[session_id].longitude,
                                    result_location[0].latitude, result_location[0].longitude);

                                console.log("================================:::::::::::::::::::::=================================")
                                console.log(distance)
                                console.log("================================:::::::::::::::::::::=================================")

                                distance = distance/1000;

                                engagements[session_id].distance = distance;
                            }

                            locations_received++;

                            // Have we received all results?
                            // Send the response when you have all the results
                            if (got_usernames == true && locations_received == num_engagements)
                            {
                                sendMissedRequests(engagements);
                            }
                        });
                   }

	                // This call should occur after getting the results for both the
	                // timestamps and the user names
	                function sendMissedRequests(engagements)
	                {
	                	var response = [];
	                	for (session_id in engagements)
	                	{
                            var date = engagements[session_id].timestamp;
                            if (date != '0000-00-00 00:00:00') {
                                date = date.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                            }

	                		response.push({
                                "engagement_id": engagements[session_id].engagement_id,
	                			"pickup_location_address": engagements[session_id].address,
	                			"timestamp": date,
                                "user_name": engagements[session_id].user_name,
                                "distance": engagements[session_id].distance,
                                "status": engagements[session_id].status});
	                	}
                        response = {"missed_rides": response};
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
	                }
                });
            }
        });
    }
};


exports.verifyReferredByCode = function(referral_code, callback){
    var get_user = "SELECT `user_id` FROM `tb_users` WHERE `referral_code`=?";
    connection.query(get_user, [referral_code], function(err, user){
        if(user.length>0){
            return callback(user[0].user_id);
        }
        else{
            return callback(0);
        }
    });
};



function giveCoupon(user_id, coupon_id, rides_validity, reason, callback){
    var insert_coupon =
        "INSERT INTO `tb_accounts` " +
        "(`user_id`, `coupon_id`, `added_on`, `expiry_date`, `reason`) " +
        "VALUES (?, ?, NOW(), concat_ws(' ',DATE(DATE_ADD(NOW(), INTERVAL ? DAY)), '18:29:59'), ?)";
    connection.query(insert_coupon, [user_id, coupon_id, rides_validity, reason], function(err, result){
        logging.logDatabaseQuery("Adding coupon to account", err, result);
        if(typeof callback !== 'undefined'){
            return callback();
        }
    });
}

function addPromoTransaction(promotion_id, master_id, user_id, callback_main){
    async.parallel(
        [
            function(callback){
                var insert_transaction = "";
                var values = [];
                if(promotion_id != master_id){
                    insert_transaction = "INSERT INTO `tb_promo_transactions` (`user_id`, `promo_id`, `redeem_date`) VALUES (?, ?, NOW()), (?, ?, NOW())";
                    values = [user_id, promotion_id, user_id, master_id];
                }
                else{
                    insert_transaction = "INSERT INTO `tb_promo_transactions` (`user_id`, `promo_id`, `redeem_date`) VALUES (?, ?, NOW())";
                    values = [user_id, promotion_id];
                }
                connection.query(insert_transaction, values, function(err, result){
                    logging.logDatabaseQuery("Adding promotional transaction", err, result);
                    return callback(err);
                });
            },
            function(callback){
                var update_promo = "";
                var values = [];
                if(promotion_id == master_id){
                    update_promo = "UPDATE `tb_promotions` SET `num_redeemed`=`num_redeemed`+1 WHERE `id` IN (?)";
                    values = [promotion_id];
                }
                else{
                    update_promo = "UPDATE `tb_promotions` SET `num_redeemed`=`num_redeemed`+1 WHERE `id` IN (?, ?)";
                    values = [promotion_id, master_id];
                }
                connection.query(update_promo, values, function(err, result){
                    logging.logDatabaseQuery("Updating the number of coupons that were redeemed", err, result);
                    return callback(err);
                });
            }
        ],
        function(err){
            return callback_main(err);
        }
    );
}

function checkValidity(promotion, date_registered, callback){
    var time_now = new Date();

    // Check if the promotion code is valid right now
    if (time_now - promotion.start_date < 0 || time_now - promotion.end_date > 0){
        return callback(false);
    }

    // Check the validity window of the promotion code using the registration date
    var diff = (time_now - date_registered);    // milliseconds
    diff = diff/60000;                          // minutes
    if(promotion.validity_window < 0 || diff > promotion.validity_window){
        return callback(true);
    }
    else{
        return callback(false);
    }
}

function fetchAnyPromoTransaction(user, callback){
    var check_transaction_level1 = "SELECT * FROM `tb_promo_transactions` WHERE `user_id` = ?";
    connection.query(check_transaction_level1, [user.user_id], function(err, transactions_level1){
        logging.logDatabaseQuery("Getting any existing promotion transaction (level 1)", err, transactions_level1);
        if(transactions_level1.length > 0){
            return callback(true);
        }
        return callback(false);
    });
}

function fetchExistingPromoTransaction(user, promotion, callback){
    // Check if a previous transaction using the same promotion code exists
    var check_transaction_level1 = "SELECT * FROM `tb_promo_transactions` WHERE `promo_id`=? && `user_id` = ?";
    connection.query(check_transaction_level1, [promotion.master_id, user.user_id], function(err, transactions_level1){
        logging.logDatabaseQuery("Getting an existing promotion transaction (level 1)", err, transactions_level1);

        var response = {};
        if(transactions_level1.length > 0){
            return callback(true);
        }
        return callback(false);

        //if(promotion.device_specific == 1){
        //    var check_transaction_level2 = "SELECT * FROM `tb_promo_transactions` WHERE `promo_id`=? " +
        //        "&& `user_id` IN (SELECT `user_id` FROM `tb_users` WHERE `reg_device_token` != '' &&  `reg_device_token` IN (SELECT `reg_device_token` FROM `tb_users` WHERE `user_id`=?))";
        //
        //    connection.query(check_transaction_level2, [promotion.master_id, user.user_id], function(err, transactions_level2){
        //        logging.logDatabaseQuery("Getting an existing promotion transaction (level 2)", err, transactions_level2);
        //
        //        var response = {};
        //        if(transactions_level2.length > 0){
        //            return callback(true);
        //        }
        //        else {
        //            return callback(false);
        //        }
        //    });
        //}
        //else{
        //    return callback(false);
        //}
    });
}

function usePromotionCodeIfValid(user, promotion, res){
    fetchExistingPromoTransaction(user, promotion, function(alreadyExists) {
        console.log("Promo transaction already exists? " + alreadyExists);
        if(alreadyExists === true){
            response = {
                flag: constants.responseFlags.SHOW_MESSAGE,
                message: "This promotional code has already been used."};
            logging.logResponse(response);
            if(typeof res != 'undefined'){
                res.send(response);
            }
            return;
        }

        checkValidity(promotion, user.date_registered, function(isValid){
            if(isValid === true){
                /* if no transaction is there, give coupons and add transactions */
                // check if we have exceeded the limit
                if(promotion.num_redeemed < promotion.max_number){
                    var i = 0;
                    var tasks = [];
                    tasks.push(addPromoTransaction.bind(null, promotion.id, promotion.master_id, user.user_id));
                    for (i = 0; i < promotion.num_coupons; i++){
                        tasks.push(giveCoupon.bind(null, user.user_id, promotion.coupon_id, promotion.rides_validity, 'promo ' + promotion.promotion_code));
                    }

                    async.parallel(tasks, function(err){
                        if(!err){
                            var message = "";
                            if(promotion.num_coupons === 1){
                                message = "A FREE ride coupon has been added to your account";
                            }
                            else{
                                message = promotion.num_coupons + " FREE ride coupons have been added to your account";
                            }

                            var response = {
                                flag: constants.responseFlags.SHOW_MESSAGE,
                                message: message};
                            logging.logResponse(response);
                            if(typeof res != 'undefined'){
                                res.send(response);
                            }

                            if(promotion.notify_user){
                                mailer.sendMailForUsingPromotionalCode(user.user_email, user.user_name, promotion.num_coupons, promotion.promotion_code);
                            }

                            if(promotion.notify_sales){
                                mailer.notifySalesPersonAboutUsage(promotion.sales_email, user.user_name, promotion.promotion_code);
                            }
                        }
                    });
                }
                else{
                    response = {
                        flag: constants.responseFlags.SHOW_MESSAGE,
                        message: "Sorry. These coupons have already been redeemed."};
                    logging.logResponse(response);
                    if(typeof res != 'undefined'){
                        res.send(response);
                    }
                }
            }
            else{
                response = {
                    flag: constants.responseFlags.SHOW_MESSAGE,
                    message: "This promotional code is not valid anymore."};
                logging.logResponse(response);
                if(typeof res != 'undefined'){
                    res.send(response);
                }
            }
        });
    });
}

function useReferralCodeIfValid(user, referral_code, res){

    var existing_referred_by = 0;
    var new_referring_user = undefined;

    /* Fetching the existing referred by for the user */
    function getExistingReferral(callback){
        var get_referral = "SELECT `user_id`, `referred_by` FROM `tb_users` WHERE `user_id`=?";
        connection.query(get_referral, [user.user_id], function(err, result){
            logging.logDatabaseQuery("Getting the existing referral", err, result);

            if(result.length > 0){
                existing_referred_by = result[0].referred_by;
            }
            return callback();
        });
    }

    /* Simultaneously fetching the new referred by user */
    function getNewReferredByUser(callback){
        var get_user = "SELECT `user_id`, `user_name`, `referred_by` FROM `tb_users` WHERE `referral_code`=?";
        connection.query(get_user, [referral_code], function(err, user){
            logging.logDatabaseQuery("Getting the referred by user", err, user);

            if(user.length>0){
                new_referring_user = user[0];
            }
            else{
                new_referring_user = null;
            }
            return callback();
        });
    }

    var tasks = [];
    tasks.push(getExistingReferral);
    tasks.push(getNewReferredByUser);
    async.parallel( tasks, function(err) {
        var response = {};
        fetchAnyPromoTransaction(user, function(alreadyExists) {
            if(alreadyExists === true){
                response = {
                    flag: constants.responseFlags.SHOW_MESSAGE,
                    message: "You have already used a promotion code with this account."};
            }
            else{
                if (existing_referred_by != 0) {
                    response = {
                        flag: constants.responseFlags.SHOW_MESSAGE,
                        message: "We already have a valid \"referred by\" code for you and it can't be changed."};
                }
                else if (new_referring_user == null) {
                    response = {
                        flag: constants.responseFlags.SHOW_MESSAGE,
                        message: referral_code + " is not a valid referral code."};
                }
                else if (user.user_id == new_referring_user.user_id) {
                    response = {
                        flag: constants.responseFlags.SHOW_MESSAGE,
                        message: "Hey! You can't refer yourself. :)"};
                }
                else if (user.user_id == new_referring_user.referred_by) {
                    response = {
                        flag: constants.responseFlags.SHOW_MESSAGE,
                        message: "Hey! " + user.user_name + " was already referred by you and you can't use this referral code."};
                }
                else {
                    // Check if registered within last 24 hours
                    var now = new Date();
                    if(now.getTime() - user.date_registered.getTime() > 86400000 ){
                        var response = {
                            flag: constants.responseFlags.SHOW_MESSAGE,
                            message: "Using a referral code is valid only during registration or within 24 hours of registration."};
                    }
                    else {
                        response = {
                            flag: constants.responseFlags.SHOW_MESSAGE,
                            message: "Awesome. You have been referred to "+config.get('projectName')+" by " + new_referring_user.user_name + ". An additional free ride coupon has been credited to your account."};
                        mailer.sendMailToReferringUser(user.user_name, new_referring_user.user_id);
                        mailer.sendMailToReferredUser(user.user_name, user.user_email, referral_code);

                        /* Give a coupon to the user */
                        giveCoupon(user.user_id, constants.couponType.FREE_RIDE, 30 /* ride validity */, 'was referred', undefined);

                        /* Update the referred by for the user */
                        var update_referred_by = "UPDATE `tb_users` SET `referred_by`=? WHERE `user_id`=?";
                        connection.query(update_referred_by, [new_referring_user.user_id, user.user_id], function (err, result) {
                            logging.logDatabaseQuery("Updating the referred by for the user", err, result);
                        });

                        /* Update last referred time stamp for the referring user */
                        var update_last_referred_on = "UPDATE `tb_users` SET `last_referred_on` = NOW() WHERE `user_id` = ?";
                        connection.query(update_last_referred_on, [new_referring_user.user_id], function(err, result){
                            logging.logDatabaseQuery("Updating the last referred timestamp for the referring user", err, result);
                        });
                    }
                }
            }

            logging.logResponse(response);
            if(typeof res != 'undefined'){
                res.send(response);
            }
        });
    });
}

function checkPromoCode(code, types, callback){
    console.log("Looking for " + code + " IN " + types.toString());
    var check_promo = "SELECT * FROM `tb_promotions` WHERE `promotion_code` = ? && `type` IN (" + types.toString() + ")";
    connection.query(check_promo, [code], function (err, promotion) {
        logging.logDatabaseQuery("Getting any promotion with the supplied code", err, promotion);
        if (promotion.length > 0) {
            return callback(promotion[0]);
        }
        else {
            return callback(null);
        }
    });
}

exports.use_code_during_login = function(user, code){
    code = code.toUpperCase();

    checkPromoCode(code, [constants.promoType.REGISTRATION, constants.promoType.ALL_TIME], function(promotion){
        if(promotion === null){
            useReferralCodeIfValid(user, code);
        }
        else{
            usePromotionCodeIfValid(user, promotion);
        }
    });
};

exports.enter_code = function(req, res){
    logging.startSection("enter_code");
    logging.logRequest(req);

    var access_token = req.body.access_token;
    var code = req.body.code;
    if (code !== undefined){
        code = code.toUpperCase();
    }

    var isBlank = utils.checkBlank([access_token, code]);
    if(isBlank){
        var response = {"error": "some parameter missing", "flag": constants.responseFlags.PARAMETER_MISSING};
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    }
    else{
        utils.authenticateUser(access_token, function(user) {
            if (user == 0) {
                var response = {"error": 'invalid access token', "flag": constants.responseFlags.INVALID_ACCESS_TOKEN};
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            }
            else {
                checkPromoCode(code, [constants.promoType.IN_APP, constants.promoType.ALL_TIME], function(promotion){
                    if(promotion === null){
                        checkPromoCode(code, [constants.promoType.REGISTRATION], function(login_promo){
                            if(login_promo === null){
                                useReferralCodeIfValid(user[0], code, res);
                            }
                            else{
                                var response = {
                                    flag: constants.responseFlags.SHOW_MESSAGE,
                                    message: "This promotion code can only be used while registration."
                                };
                                res.send(response);
                            }
                        });
                    }
                    else{
                        usePromotionCodeIfValid(user[0], promotion, res);
                    }
                });
            }
        });
    }
};

// Get the booking history for the user
exports.booking_history = function(req, res)
{
    var flag = req.body.current_mode;
    var access_token = req.body.access_token;
    var manvalues = [access_token, flag];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1)
    {
        sendParameterMissingResponse(res);
    }
    else
    {
        utils.authenticateUser(access_token, function(result)
        {
            if (result == 0)
            {
                sendAuthenticationError(res);
            }
            else
            {
                var user_id = result[0].user_id;
                var query = "";
                if (flag == 0)
                {
                    query = "SELECT `pickup_location_address`,`drop_location_address`,`money_transacted`,`actual_fare`,`distance_travelled`,`pickup_time`,`account_id` FROM `tb_engagements` WHERE `user_id`=? && `status`=? ORDER BY `pickup_time` DESC";
                }
                else
                {
                    query = "SELECT `pickup_location_address`,`drop_location_address`,`money_transacted`,`actual_fare`,`distance_travelled`,`pickup_time`,`account_id` FROM `tb_engagements` WHERE `driver_id`=? && `status`=? ORDER BY `pickup_time` DESC";
                }
                connection.query(query, [user_id, constants.engagementStatus.ENDED], function(err, result_data)
                {
                    var result_data_length = result_data.length;
                    var final = [];
                    for (var i = 0; i < result_data_length; i++)
                    {
                        var date = result_data[i].pickup_time;
                        if (date != '0000-00-00 00:00:00') {
                            date = result_data[i].pickup_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                        }
                        var coupon_used = result_data[i].account_id === 0 ? 0 : 1;
                        //var balance = 0;
                        //if(coupon_used === 0){
                        //    balance = result_data[i].actual_fare - result_data[i].money_transacted;
                        //}
                        //else{
                        //    balance = result_data[i].actual_fare - ((result_data[i].money_transacted <= 100) ? 0 : (result_data[i].money_transacted - 100));
                        //}
                        final.push({
                            "id": i,
                            "from": result_data[i].pickup_location_address,
                            "to": result_data[i].drop_location_address,
                            "fare": result_data[i].money_transacted,
                            "distance": result_data[i].distance_travelled,
                            "time": date,
                            "coupon_used": coupon_used
                            //"balance": balance
                        });
                    }
                    var finals = {"booking_data": final};
                    //logging.logResponse(finals);
                    res.send(JSON.stringify(finals));
                });
            }
        });
    }
};



exports.get_coupons = function(req, res){
    //logging.logDatabaseQuery("Getting the coupons valid for the user");
    //logging.logRequest(req);

    var access_token = req.body.access_token;
    var isBlank = utils.checkBlank([access_token]);
    if(isBlank){
        var response = {"error": "some parameter missing", "flag": constants.responseFlags.PARAMETER_MISSING};
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    }
    else{
        utils.authenticateUser(access_token, function(result_user){
            if(result_user == 0){
                var response = {"error": 'invalid access token', "flag": constants.responseFlags.INVALID_ACCESS_TOKEN};
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            }
            else{
                var user_id = result_user[0].user_id;

                var get_coupons =
                    "SELECT coupons.title, coupons.subtitle, coupons.description, coupons.discount, coupons.maximum, coupons.image, coupons.type, " +
                    "account.redeemed_on, account.status, account.expiry_date " +
                    "FROM " +
                        "(SELECT * FROM `tb_accounts` WHERE `user_id`=? && `status`=? && `expiry_date` > NOW()) as `account` " +
                    "JOIN " +
                        "`tb_coupons` as `coupons` " +
                    "WHERE account.coupon_id = coupons.coupon_id";
                connection.query(get_coupons, [user_id, constants.couponStatus.ACTIVE], function(err, coupons){
                    //logging.logDatabaseQuery("Getting the coupons for the user", err, coupons, null);

                    if(coupons.length > 0){
                        for(var i=0; i<coupons.length; i++){
                            coupons[i].expiry_date = coupons[i].expiry_date.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                            if(coupons[i].redeemed_on != '0000-00-00 00:00:00')
                                coupons[i].redeemed_on = coupons[i].redeemed_on.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                        }
                        var response = {
                            flag: constants.responseFlags.COUPON,
                            coupons: coupons
                        };
                        //logging.logResponse(response);
                        res.send(JSON.stringify(response));
                    }
                    else{
                        var response = {
                            flag: constants.responseFlags.COUPON,
                            log: 'No coupons available for the user'
                        };
                        //logging.logResponse(response);
                        res.send(JSON.stringify(response));
                    }
                });
            }
        });
    }
};



exports.invite_friends = function(req, res)
{
    var fbId = req.body.fb_id;
    var fbAccessToken = req.body.fb_access_token;
    var users = [];
    var sql = "SELECT `user_fb_id` from `tb_users` where `user_fb_id`!=''";
    connection.query(sql, function(err, result_fb_id) {
        var dbFbIds = [];

        var urls = "https://graph.facebook.com/" + fbId + "/friends?access_token=" + fbAccessToken;

        request(urls, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var output = JSON.parse(body);


                var fbFriends = [];
                var fb_unreg = [];
                var fbFriendsName = [];

                var dbFbLength = result_fb_id.length;
                var fbfriendLength = (output['data']).length;
                for (var i = 0; i < dbFbLength; i++) {
                    dbFbIds.push(parseInt(result_fb_id[i].user_fb_id));
                }

                for (var j = 0; j < fbfriendLength; j++) {
                    fbFriends.push(parseInt(output['data'][j].id));

                    fbFriendsName.push((output['data'][j].name));

                }

                var final = [];
                for (var i = 0; i < fbfriendLength; i++) {
                    if (dbFbIds.indexOf(fbFriends[i]) != -1) {
                    }
                    else
                    {
                        final.push({"fb_id": fbFriends[i], "fb_name": fbFriendsName[i], "flag": 0});
                    }
                }

                if (final.length > 0) {
                    var final_data = {"friends_data": final};
                    logging.logResponse(final_data);
                    res.send(final_data);
                }
                else if (final.length == 0)
                {
                    var response = {"friends_data": []};
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));
                }
                else
                {
                    var response = {"error": "Something went wrong"};
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));
                }
            }
            else {
                var response = {"error": "Error fetching Facebook friends"};
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            }
        });
    });
};



exports.get_driver_current_location = function(req, res)
{
    var access_token = req.body.access_token;
    var driver_id = req.body.driver_id;
    var manvalues = [access_token, driver_id];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1)
    {
        var response = {"error": "some parameter missing", "flag": 0};
        res.send(JSON.stringify(response));
    }
    else
    {
        utils.authenticateUser(access_token, function(result)
        {
            if (result == 0)
            {
                var response = {"error": "Invalid access token", "flag": 1};
                res.send(JSON.stringify(response));
            }
            else
            {
                var sql = "SELECT `user_id`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
                connection.query(sql, [driver_id], function(err, result) {
                    var response = {"data": result};
                    res.send(JSON.stringify(response));

                });
            }
        });
    }
};


exports.disable_user = function(req, res){
    logging.startSection("disable_user");

    var user_id = req.body.user_id;
    var password = req.body.password;

    if(password === "jugnooclicklabs123!@#"){
        var change_parameters = "UPDATE `tb_users` " +
            "SET `phone_no`=?, `user_fb_id`=?, `user_email`=?, `access_token`=?, `password`=?, `verification_status`=?, `verification_token`=?, `status`=?, `reg_as`=? " +
            "WHERE `user_id`=?";
        var change_values = ["+910000000000", "0", "disabled@jugnoo.in", "disabled", "disabled"+Math.floor(Math.random()*999999),
            0, Math.floor(Math.random()*9999), constants.userCurrentStatus.OFFLINE, constants.userRegistrationStatus.CUSTOMER, user_id];
        connection.query(change_parameters, change_values, function(err, result){
            logging.logDatabaseQuery("Disabling user with id: " + user_id, err, result);
        });
        res.send("Disabled user with id: " + user_id);
    }
    else{
        res.send("The password is wrong");
    }
};


exports.block_user = function(req, res){
    logging.startSection("block_user");

    var userId = req.body.user_id;
    var reason = parseInt(req.body.reason);
    var password = req.body.password;

    if(password !== "jugnooclicklabs123!@#"){
        res.send("The password is wrong");
        return;
    }

    // block the user
    var blockUser = "UPDATE `tb_users` SET `is_blocked`=1 WHERE `user_id`=?";
    connection.query(blockUser, [userId], function(err, result){
        logging.logDatabaseQuery("Blocking user with id: " + userId, err, result);
        res.send("Blocked user with id: " + userId);
    });

    // send message to the customer stating the reason
    var getContactInfo = "SELECT `phone_no` FROM `tb_users` WHERE `user_id`=?";
    connection.query(getContactInfo, [userId], function(err, users){
        if(err){
            logging.logDatabaseQuery("Getting the phone number of the user with id: " + userId, err, users);
        }
        else{
            console.log("Sending the message after blocking the user");
            messenger.sendReasonForBlocking(users[0].phone_no, reason);
        }
    });
};


function toggleLocationUpdates(driver_id, flag){
    var message = "Toggling the location update service for the user";
    var notificationFlag = 2;
    var payload = {
        flag: constants.notificationFlags.TOGGLE_LOCATION_UPDATES,
        toggle_location: flag
    };
    utils.sendNotification(driver_id, message, notificationFlag, payload);
}

exports.toggle_location_updates = function(req, res){
    logging.startSection("toggle_location_updates");
    logging.logRequest(req);

    var driver_id = req.body.driver_id;
    var flag = req.body.flag;

    toggleLocationUpdates(driver_id, flag);
};


function blacklistEmail(req, res){
    logging.startSection("Mandrill webhook : blacklist email ID");
    logging.logRequest(req);

    console.log("Event type: " + req.body.event);

    var email = req.body.msg.email;

    var blacklistEmail =
        "UPDATE `tb_users` " +
        "SET `email_blacklisted` = 1 " +
        "WHERE `user_email` = ?";
    connection.query(blacklistEmail, [email], function(err, result){
        if(err){
            logging.logDatabaseQueryError("Blacklisting email using mandrill webhook", err, result);
        }
    });

    res.send('Successful');
}


function blockEmailCategoriesForUser(req, res){
    logging.startSection("block prticular email category for the user");
    logging.logRequest(req);

    var email               = req.body.email;
    var newsletterFlag      = req.body.newsletter_flag;
    var promotionalFlag     = req.body.promotional_flag;
    var transactionalFlag   = req.body.transactionl_flag;

    var checkValues = [email, newsletterFlag, promotionalFlag, transactionalFlag];
    var isBlank = utils.checkBlank(checkValues);
    if(isBlank){
        responses.sendParameterMissingError(res);
        return;
    }

    var subscription = promotionalFlag << 2 + newsletterFlag << 1 + transactionalFlag;
    var updateSubscription =
        "UPDATE `tb_users` " +
        "SET `subscription` = ? " +
        "WHERE `user_email` = ?";
    connection.query(updateSubscription, [subscription, email], function(err, result){
        if(err){
            logging.logDatabaseQueryError("Updating the subscription for user with email " + email, err, result);
        }
    });

    res.send({
        flag: constants.responseFlags.ACTION_COMPLETE
    });
}

function isTransactionalEmailAllowed(userId, subscription){
    return subscription && constants.subscriptionStatus.TRANSACTIONAL;
}

function isNewsletterAllowed(userId, subscription){
    return subscription && constants.subscriptionStatus.NEWSLETTER;
}

function isPromotionalEmailAllowed(userId, subscription){
    return subscription && constants.subscriptionStatus.PROMOTIONAL;
}





function blockRatingPopupForUser(req, res){
    var accessToken = req.body.access_token;

    utils.authenticateUser(accessToken, function(user){
        if(user == 0){
            responses.sendAuthenticationError(res);
            return;
        }

        var userId = user[0].user_id;

        var blockRating =
            'UPDATE `tb_users` ' +
            'SET `block_rating` = 1 ' +
            'WHERE `user_id` = ?';
        connection.query(blockRating, [userId], function(err, result){
            logging.logDatabaseQuery('Changing the rating popup flag for the user', err, result);
        });
    });
}