

var utils       = require('./commonfunction');
var logging     = require('./logging');
var constants   = require('./constants');



exports.sendNotificationToDormantUsers      = sendNotificationToDormantUsers;
exports.sendNotificationForReferrals        = sendNotificationForReferrals;
exports.sendNotificationForExpiringCoupons  = sendNotificationForExpiringCoupons;



function sendNotificationToDormantUsers(req, res){
    logging.startSection('Sending notification to dormant users');
    logging.logRequest(req);

    //var password    = req.body.password;
    //if(password !== constants.SUPER_ADMIN_PASSWORD){
    //    res.send('You are not authorized to perform this action.');
    //    return;
    //}

    var triggerDays = [7, 14, 28, 56];

    var messageWithFreeRides = 'You have a Free Ride with '+config.get('projectName')+' waiting in your account. Try us out.';
    var messageWithoutFreeRides = 'We are missing you at '+config.get('projectName')+'. Try us out.';

    var payloadWithFreeRides = {
        flag    : constants.notificationFlags.DISPLAY_MESSAGE,
        persist : true,
        message : messageWithFreeRides};

    var payloadWithoutFreeRides = {
        flag    : constants.notificationFlags.DISPLAY_MESSAGE,
        persist : true,
        message : messageWithoutFreeRides};

    var notificationFlag = 1;

    var i = 0;
    for(i = 0; i < triggerDays.length; i++){
        (function(i){
            var daysSinceRide = triggerDays[i];
            getDormantUsersWithCoupons(daysSinceRide, function(err, users){
                if(err){
                    return;
                }

                logging.addNotificationBlast('Sending notification to users dormant for past ' + daysSinceRide + ' days', users);

                var j = 0;
                for(j = 0; j < users.length; j++){
                    if(users[j].num_coupons){
                        utils.sendNotificationToDevice(users[j].device_type, users[j].user_device_token, messageWithFreeRides, notificationFlag, payloadWithFreeRides);
                    }
                    else{
                        utils.sendNotificationToDevice(users[j].device_type, users[j].user_device_token, messageWithoutFreeRides, notificationFlag, payloadWithoutFreeRides);
                    }
                }
            });
        })(i);
    }

    res.send('Notification sent to dormant users.');
}

function getDormantUsersWithCoupons(daysSinceRide, callback){
    var getDormantUsers =
        'SELECT * FROM ' +
            '(SELECT `user_id`, `device_type`, `user_device_token`, `date_registered`, `last_ride_on` ' +
            'FROM `tb_users` ' +
            'WHERE `is_blocked` = 0 AND `user_device_token` NOT LIKE \'\' AND ((`last_ride_on` IS NULL AND DATEDIFF(CURDATE(), `date_registered`) = ?) OR (DATEDIFF(CURDATE(), `last_ride_on`) = ?))) as `dormant_users` ' +
        'LEFT JOIN ' +
            '(SELECT `user_id`, COUNT(*) AS `num_coupons` ' +
            'FROM `tb_accounts` ' +
            'WHERE status = 1 and expiry_date > NOW() GROUP BY `user_id`) as `users_with_coupons` ' +
        'ON dormant_users.user_id = users_with_coupons.user_id';
    connection.query(getDormantUsers, [daysSinceRide, daysSinceRide], function(err, users){
        logging.logDatabaseQuery('Getting the users with dormant since ' + daysSinceRide + ' days.', err, users.length);

        return process.nextTick(callback.bind(null, err, users));
    });
}




function sendNotificationForReferrals(req, res){
    logging.startSection('Sending notification to dormant users');
    logging.logRequest(req);

    //var password    = req.body.password;
    //if(password !== constants.SUPER_ADMIN_PASSWORD){
    //    res.send('You are not authorized to perform this action.');
    //    return;
    //}

    var triggerDays = [3, 10, 24, 48];

    var i = 0;
    var j = 0;
    for(i = 0; i < triggerDays.length; i++){
        (function(i){
            var daysSinceReferral = triggerDays[i];
            getNonReferringUsers(daysSinceReferral, function(err, users){

                logging.addNotificationBlast('Sending notification for referral for users who have not referred us for past ' + daysSinceReferral + ' days', users);

                for(j = 0; j < users.length; j++){
                    sendReferralNotification(users[j]);
                }
            });
        })(i);
    }

    res.send('Notification sent to remind users about referrals.');
}

function getNonReferringUsers(daysSinceReferral, callback){
    var getNonReferringUsers =
        'SELECT `user_id`, `referral_code`, `device_type`, `user_device_token`, `last_referred_on` ' +
        'FROM `tb_users` ' +
        'WHERE `is_blocked` = 0 AND `user_device_token` NOT LIKE \'\' AND ((`last_referred_on` IS NULL AND DATEDIFF(CURDATE(), `date_registered`) = ?) OR (DATEDIFF(CURDATE(), `last_referred_on`) = ?))';
    connection.query(getNonReferringUsers, [daysSinceReferral, daysSinceReferral], function(err, users){
        logging.logDatabaseQuery('Getting the users who have not referred us since ' + daysSinceReferral + ' days.', err, users.length);

        return process.nextTick(callback.bind(null, err, users));
    });
}

function sendReferralNotification(user){
    var message = 'Refer your friends to '+config.get('projectName')+' using your code ' + user.referral_code + ' and get free rides.';
    var flag    = 1;
    var payload = {
        flag    : constants.notificationFlags.DISPLAY_MESSAGE,
        persist : true,
        message : message};

    utils.sendNotificationToDevice(user.device_type, user.user_device_token, message, flag, payload);
}




function sendNotificationForExpiringCoupons(req, res){
    logging.startSection('Sending notification for coupons that are expiring');
    logging.logRequest(req);

    //var password    = req.body.password;
    //if(password !== constants.SUPER_ADMIN_PASSWORD){
    //    res.send('You are not authorized to perform this action.');
    //    return;
    //}

    // Get today's date to send notification for the coupons expiring today
    var todaysDate  = utils.getMysqlStyleDateString(new Date());

    getUsersWithCoupons(todaysDate, function(err, users){

        logging.addNotificationBlast('Sending notification to users with coupon expiring on ' + todaysDate, users);

        var message = 'Your FREE ride with '+config.get('projectName')+' is going to expire today. Please take a ride with us soon.';
        var flag    = 1;
        var payload = {
            flag    : constants.notificationFlags.DISPLAY_MESSAGE,
            persist : true,
            message : message};

        var i = 0;
        for(i = 0; i < users.length; i++){
            utils.sendNotificationToDevice(users[i].device_type, users[i].user_device_token, message, flag, payload);
        }
    });


    // Add three days to send notifications for users with coupons
    var nextDate    = new Date((new Date).getTime() + 3 * 86400000);
    var nextDate    = utils.getMysqlStyleDateString(nextDate);

    getUsersWithCoupons(nextDate, function(err, users){

        logging.addNotificationBlast('Sending notification to users with coupon expiring on ' + nextDate, users);

        var message = 'Your FREE ride with '+config.get('projectName')+' is going to expire on ' + nextDate + '. Please take a ride with us soon.';
        var flag    = 1;
        var payload = {
            flag    : constants.notificationFlags.DISPLAY_MESSAGE,
            persist : true,
            message : message};

        var i = 0;
        for(i = 0; i < users.length; i++){
            utils.sendNotificationToDevice(users[i].device_type, users[i].user_device_token, message, flag, payload);
        }
    });

    res.send('Notification sent to remind about coupons expiring.');
}

function getUsersWithCoupons(expiryDate, callback){
    console.log('Getting users with coupons expiring on ' + expiryDate);
    var getUsers =
        'SELECT tb_users.user_id, tb_users.device_type, tb_users.user_device_token ' +
        'FROM ' +
            '(SELECT DISTINCT(user_id) FROM tb_accounts where status = 1 and DATE(expiry_date) = ?) AS users_with_coupons ' +
        'JOIN ' +
            'tb_users ' +
        'ON users_with_coupons.user_id = tb_users.user_id ' +
        'WHERE tb_users.user_device_token NOT LIKE \'\'';
    connection.query(getUsers, [expiryDate], function(err, users){
        logging.logDatabaseQuery('Getting the users with coupons expiring on ' + expiryDate, err, users.length);

        return process.nextTick(callback.bind(null, err, users));
    });
}