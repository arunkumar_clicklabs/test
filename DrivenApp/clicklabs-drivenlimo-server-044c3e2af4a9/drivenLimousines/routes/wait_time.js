var utils = require('./commonfunction');
var logging = require('./logging');
var constants = require('./constants');
var responses = require('./responses');
var messenger = require('./messenger');
var ride_parallel_dispatcher = require('./ride_parallel_dispatcher');

/**
 * [Send notification to the customer that the waiting state has started or ended]
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
exports.startEndWait = function(req, res) {
    logging.startSection("startEndWait");
    logging.logRequest(req);


    var access_token = req.body.access_token;
    var flag = req.body.flag;
    var engagement_id = req.body.engagement_id;


    var manvalues = [access_token, engagement_id, flag];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        ride_parallel_dispatcher.sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                ride_parallel_dispatcher.sendAuthenticationError(res);
            } else {
                var driver_id = result[0].user_id;



                var sql = "UPDATE `tb_engagements` SET `wait_time_started` = 0 ,`wait_time_seconds` = `wait_time_seconds` + TIMESTAMPDIFF(SECOND,`last_wait_time_started`,NOW())  WHERE engagement_id= ? LIMIT 1";

               var waitMessage = "wait ended";

               console.log(constants.notificationFlags)
               console.log(constants.responseFlag)

               var pushFlag = constants.notificationFlags.WAITING_ENDED;
               var responseFlag = constants.responseFlags.WAITING_ENDED;
                if (flag == 1) {
                    sql = "UPDATE `tb_engagements` SET `last_wait_time_started` = NOW() , `wait_time_started` = 1 WHERE engagement_id= ? LIMIT 1";
                    waitMessage = "Wait started";
                    pushFlag = constants.notificationFlags.WAITING_STARTED;
                    responseFlag = constants.responseFlags.WAITING_STARTED;
                }

                connection.query(sql, [engagement_id], function(err, start) {
                    console.log(err);
                    console.log(start);
                    getWaitTime(engagement_id, function(waitInfo) {
                        var customerId = waitInfo.customer_id;


                        var response = {
                            "log": waitMessage,
                            "flag": responseFlag
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));



                        var payload = {
                            "flag": pushFlag,
                            "wait_time": waitInfo.totalWaitTime,
                            "wait_time_started" : waitInfo.wait_time_started
                        };
                        utils.sendNotification(customerId, waitMessage, pushFlag, payload);




                    });
                });




            }
        });
    }
};

function getWaitTime(engagement_id, callback) {
    var wait_time = "select wait_time_seconds,last_wait_time_started,wait_time_started , user_id  from tb_engagements where engagement_id = ? limit 1"
    connection.query(wait_time, [engagement_id], function(err, result) {
        var totalWaitTime;
        var time = new Date(result[0].last_wait_time_started);
        if (result[0].wait_time_started == 1) {
            totalWaitTime = result[0].wait_time_seconds + (new Date() - time) / 1000;
        } else {
            totalWaitTime = result[0].wait_time_seconds;
        }

        var waitInfo = {

            totalWaitTime: parseInt(totalWaitTime),
            customer_id: result[0].user_id,
            wait_time_started:result[0].wait_time_started



        };
        callback(waitInfo);
    });
}
