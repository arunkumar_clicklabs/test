/**
 * This javascript file will be used for sending mails for Jugnoo
 */

var logging = require('./logging');
var utils = require('./commonfunction');
var constants = require('./constants');
var async = require('async');
var fs = require('fs');

exports.sendMailForRideRequest = function(customer_id, latitude, longitude) {
    var customer_info="SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
    connection.query(customer_info,[customer_id],function(err,result_customer_info){
        logging.logDatabaseQuery("Getting information for the customer", err, result_customer_info, null);

        // No request emails outside of 20 kms from chandigarh
        if(utils.calculateDistance(30.708001, 76.7505781, latitude, longitude)/1000 < 20){
            utils.getLocationAddress(latitude, longitude, function(pickup_address){
                if(result_customer_info.length > 0){
                    var game_name = config.get('projectName');
                    var to = config.get('emailCredentials.senderEmail');
                    var sub = "Ride request by "+result_customer_info[0].user_name;
                    var msg = "Customer summary ->\n\n";
                    msg += "" +
                    "\tUser Id : "+result_customer_info[0].user_id+"\n" +
                    "\tName : "+result_customer_info[0].user_name+"\n" +
                    "\tPhone : "+result_customer_info[0].phone_no+"\n" +
                    "\tEmail : "+result_customer_info[0].user_email+"\n" +
                    "\tCurrent location: https://www.google.co.in/maps/preview?q="+latitude+","+longitude+"\n" +
                    "\tFor iOS: comgooglemaps://?q="+latitude+","+longitude+ "\n\n";
                    msg += config.get('emailCredentials.signature');
                    utils.sendEmailForPassword(to, msg, sub, function(resultEmail)
                    {
                    });
                }
            });
        }
    });
};

exports.sendOTP = function(name, to, otp){
    var sub = "Your one time password  ";
    var message = "Dear " + name.split(' ')[0] + ", Your One Time Password is " + otp + ".";

                        utils.sendEmailForPassword(to, message, sub, function(resultEmail)
                    {
                    });

};


exports.sendMailForAcceptedRide = function(customer_id, driver_id) {
    var customer_info = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude` as latitude,`current_location_longitude` as longitude " +
        "FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customer_id], function(err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        var driver_info_query = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude` as latitude,`current_location_longitude` as longitude " +
            "FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
        connection.query(driver_info_query, [driver_id], function (err, driver) {
            logging.logDatabaseQuery("Fetch information for driver", err, driver, null);
            // Send e-mail to the support team
            var game_name = config.get('projectName');
            var to = config.get('emailCredentials.senderEmail');
            var sub = "Ride accepted by " + driver[0].user_name + " for " + customer[0].user_name;
            var msg = "Customer summary ->\n\n";
            msg +=
                "\tUser Id : " + customer[0].user_id + "\n" +
                "\tName : " + customer[0].user_name + "\n" +
                "\tPhone : " + customer[0].phone_no + "\n" +
                "\tEmail : " + customer[0].user_email + "\n" +
                "\tCurrent location: https://www.google.co.in/maps/preview?q=" + customer[0].latitude + "," + customer[0].longitude + "\n" +
                "\tFor iOS: comgooglemaps://?q=" + customer[0].latitude + "," + customer[0].longitude + "\n\n";
            msg += "Driver summary ->\n\n";
            msg +=
                "\tUser Id : " + driver[0].user_id + "\n" +
                "\tName : " + driver[0].user_name + "\n" +
                "\tPhone : " + driver[0].phone_no + "\n" +
                "\tEmail : " + driver[0].user_email + "\n" +
                "\tCurrent location: https://www.google.co.in/maps/preview?q=" + driver[0].latitude + "," + driver[0].longitude + "\n" +
                "\tFor iOS: comgooglemaps://?q=" + driver[0].latitude + "," + driver[0].longitude + "\n" +
                "\tDistance : " + utils.calculateDistance(customer[0].latitude, customer[0].longitude, driver[0].latitude, driver[0].longitude) + " meters\n\n";
            msg += config.get('emailCredentials.signature');
            utils.sendEmailForPassword(to, msg, sub, function (resultEmail) {
            });
        });
    });
};


exports.sendMailForStartedRide = function(customer_id, driver_id){
    var customer_info = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customer_id], function(err, result_customer)
    {
        logging.logDatabaseQuery("Fetch information for customer", err, result_customer, null);

        var driver_info_query = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ?";
        connection.query(driver_info_query, [driver_id], function(err, result_driver)
        {
            logging.logDatabaseQuery("Fetch information for driver", err, result_driver, null);

            var game_name = config.get('projectName');
            var to = config.get('emailCredentials.senderEmail');
            var sub = "Ride started by "+result_driver[0].user_name + " for " + result_customer[0].user_name;

            var msg = "Customer summary:\n\n";
            msg +=
                "\tUser Id : "+result_customer[0].user_id+"\n" +
                "\tName : "+result_customer[0].user_name+"\n" +
                "\tPhone : "+result_customer[0].phone_no+"\n" +
                "\tEmail : "+result_customer[0].user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+result_customer[0].current_location_latitude+","+result_customer[0].current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+result_customer[0].current_location_latitude+","+result_customer[0].current_location_longitude+ "\n\n";

            msg += "Driver summary:\n\n";
            msg +=
                "\tUser Id : "+result_driver[0].user_id+"\n" +
                "\tName : "+result_driver[0].user_name+"\n" +
                "\tPhone : "+result_driver[0].phone_no+"\n" +
                "\tEmail : "+result_driver[0].user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+result_driver[0].current_location_latitude+","+result_driver[0].current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+result_driver[0].current_location_latitude+","+result_driver[0].current_location_longitude+ "\n\n";

            msg += config.get('emailCredentials.signature');
            utils.sendEmailForPassword(to, msg, sub, function(resultEmail)
            {
            });
        });
    });
};


exports.sendMailForCompletedRide = function(engagement_id, customer_id, driver_id, drop_info, fare_info){
    var customer_info = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude` as latitude,`current_location_longitude` as longitude " +
        "FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customer_id], function(err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        var driver_info_query = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude` as latitude,`current_location_longitude` as longitude " +
            "FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
        connection.query(driver_info_query, [driver_id], function (err, driver) {
            logging.logDatabaseQuery("Fetch information for driver", err, driver, null);

            // Send e-mail to the customer
            {
                var name = [];
                var username = customer[0].user_name;
                var name = username.split(" ");
                if (customer[0].user_email != "") {
                    var sub = "Your "+config.get('projectName')+" ride summary";
                    var msg = "Hi " + name[0] + ", \n\n";

                    msg += "Hope you had a good time in your "+config.get('projectName')+" ride and made a new friend. Here's a summary of your ride\n\n";
                    msg += "Distance: " + fare_info.distance + " Kms\n";
                    msg += "Ride time: " + fare_info.ride_time + " mins\n\n";
                    if(fare_info.coupon_used === true){
                        msg += "And since, you had a free ride available with you, your fare details are as following\n";
                        msg += "Total fare: $" + fare_info.total_fare + "\n";
                        msg += "Discount: $" + fare_info.discount + "\n";
                        msg += "Fare to be paid: $" + fare_info.to_pay + "\n\n";
                    }
                    else{
                        msg += "Total fare: $" + fare_info.total_fare + "\n\n";
                    }
                    msg += "Thank you for using "+config.get('projectName')+" to find a ride. Keep riding along! \n\n";
                    msg += config.get('emailCredentials.signature');
                    utils.sendEmailForPassword(customer[0].user_email, msg, sub, function (resultEmail) {
                    });
                }
            }
            {
                var get_engagement = "SELECT `pickup_latitude`, `pickup_longitude`, `pickup_location_address` " +
                    "FROM `tb_engagements` " +
                    "WHERE `engagement_id`=?";
                connection.query(get_engagement, [engagement_id], function(err, engagements) {
                    var sub = config.get('projectName')+" ride summary for " + customer[0].user_name;

                    var msg = config.get('projectName')+" ride summary: \n\n";
                    msg += "Customer summary:\n\n";
                    msg +=
                        "\tUser Id : " + customer[0].user_id + "\n" +
                        "\tName : " + customer[0].user_name + "\n" +
                        "\tPhone : " + customer[0].phone_no + "\n" +
                        "\tEmail : " + customer[0].user_email + "\n\n";

                    msg += "Driver summary:\n\n";
                    msg +=
                        "\tUser Id : " + driver[0].user_id + "\n" +
                        "\tName : " + driver[0].user_name + "\n" +
                        "\tPhone : " + driver[0].phone_no + "\n" +
                        "\tEmail : " + driver[0].user_email + "\n\n";

                    msg += "Ride summary:\n\n";
                    msg +=
                        "\tPickup location: https://www.google.co.in/maps/preview?q="+engagements[0].pickup_latitude+","+engagements[0].pickup_longitude+"\n" +
                        "\tPickup address: " + engagements[0].pickup_location_address + "\n" +
                        "\tDrop location: https://www.google.co.in/maps/preview?q="+drop_info.latitude+","+drop_info.longitude+"\n" +
                        "\tDrop address: " + drop_info.address + "\n" +
                        "\tRoute on google map: " + "https://www.google.co.in/maps/dir/" + engagements[0].pickup_latitude + "," + engagements[0].pickup_longitude +
                            "/'" + drop_info.latitude + "," + drop_info.longitude + "'\n" +
                        "\tDistance: " + fare_info.distance + " Kms\n" +
                        "\tRide time: " + fare_info.ride_time + " mins\n" +
                        "\tTotal fare: $" + fare_info.total_fare + "\n";
                    if(fare_info.coupon_used === true){
                        msg += "\tDiscount: $" + fare_info.discount + "\n";
                        msg += "\tFare to be paid: $" + fare_info.to_pay + "\n";
                    }
                    msg += "\n";

                    msg += config.get('emailCredentials.signature');
                    utils.sendEmailForPassword(config.get('emailCredentials.senderEmail'), msg, sub, function (resultEmail) {
                    });
                });
            }
        });
    });
};


exports.sendMailForCancelledRequest = function(customer_id){
    // Get the information for the customer
    var customer_info_query = "SELECT `user_id`, `user_email`, `user_name`, `phone_no` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info_query, [customer_id], function(err, result_customer) {
        logging.logDatabaseQuery("Getting the information for the customer when cancelling the ride", err, result_customer, null);

        var to = config.get('emailCredentials.senderEmail');
        var sub = "Request cancelled by " + result_customer[0].user_name;
        var msg = "Customer summary:\n\n";
        msg +=
            "\tUser Id : " + result_customer[0].user_id + "\n" +
            "\tName : " + result_customer[0].user_name + "\n" +
            "\tPhone : " + result_customer[0].phone_no + "\n" +
            "\tEmail : " + result_customer[0].user_email + "\n\n";

        msg += config.get('emailCredentials.signature');
        utils.sendEmailForPassword(to, msg, sub, function (resultEmail) {
        });
    });
};


exports.sendMailForCancelledRide = function(driver_id, customer_id){
    var customer_info = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customer_id], function(err, result_customer)
    {
        logging.logDatabaseQuery("Fetch information for customer", err, result_customer, null);

        var driver_info_query = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ?";
        connection.query(driver_info_query, [driver_id], function(err, result_driver)
        {
            logging.logDatabaseQuery("Fetch information for driver", err, result_driver, null);

            var game_name = config.get('projectName');
            var to = config.get('emailCredentials.senderEmail');
            var sub = "Ride cancelled by " + result_driver[0].user_name + " for " + result_customer[0].user_name;

            var msg = "Customer summary:\n\n";
            msg +=
                "\tUser Id : "+result_customer[0].user_id+"\n" +
                "\tName : "+result_customer[0].user_name+"\n" +
                "\tPhone : "+result_customer[0].phone_no+"\n" +
                "\tEmail : "+result_customer[0].user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+result_customer[0].current_location_latitude+","+result_customer[0].current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+result_customer[0].current_location_latitude+","+result_customer[0].current_location_longitude+ "\n\n";

            msg += "Driver summary:\n\n";
            msg +=
                "\tUser Id : "+result_driver[0].user_id+"\n" +
                "\tName : "+result_driver[0].user_name+"\n" +
                "\tPhone : "+result_driver[0].phone_no+"\n" +
                "\tEmail : "+result_driver[0].user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+result_driver[0].current_location_latitude+","+result_driver[0].current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+result_driver[0].current_location_latitude+","+result_driver[0].current_location_longitude+ "\n\n";

            msg += config.get('emailCredentials.signature');
            utils.sendEmailForPassword(to, msg, sub, function(resultEmail) {});
        });
    });
};


exports.sendMailForManualEngagementFailure = function(customerId){
    var customerInfo = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customerInfo, [customerId], function(err, customer)
    {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);
        customer = customer[0];

        var game_name = config.get('projectName');
            var to = config.get('emailCredentials.senderEmail');
        var sub = "MANUAL engagement creation FAILED for " + customer.user_name;

        var msg = "Customer summary:\n\n";
        msg +=
            "\tUser Id : "+customer.user_id+"\n" +
            "\tName : "+customer.user_name+"\n" +
            "\tPhone : "+customer.phone_no+"\n" +
            "\tEmail : "+customer.user_email+"\n"+
            "\tCurrent location: https://www.google.co.in/maps/preview?q="+customer.current_location_latitude+","+customer.current_location_longitude+"\n" +
            "\tFor iOS: comgooglemaps://?q="+customer.current_location_latitude+","+customer.current_location_longitude+ "\n\n";

        msg += config.get('emailCredentials.signature');
        utils.sendEmailForPassword(to, msg, sub, function(resultEmail) {});
    });
};


exports.sendMailForManualEngagementSuccess = function(customerId, driverId){
    var customerInfo = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customerInfo, [customerId], function(err, customer)
    {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);
        customer = customer[0];

        var driver_info_query = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ?";
        connection.query(driver_info_query, [driverId], function(err, driver)
        {
            logging.logDatabaseQuery("Fetch information for driver", err, driver, null);
            driver = driver[0];

            var game_name = config.get('projectName');
            var to = config.get('emailCredentials.senderEmail');
            var sub = "MANUAL engagement creation SUCCEEDED between " + driver.user_name + " and " + customer.user_name;

            var msg = "Customer summary:\n\n";
            msg +=
                "\tUser Id : "+customer.user_id+"\n" +
                "\tName : "+customer.user_name+"\n" +
                "\tPhone : "+customer.phone_no+"\n" +
                "\tEmail : "+customer.user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+customer.current_location_latitude+","+customer.current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+customer.current_location_latitude+","+customer.current_location_longitude+ "\n\n";

            msg += "Driver summary:\n\n";
            msg +=
                "\tUser Id : "+driver.user_id+"\n" +
                "\tName : "+driver.user_name+"\n" +
                "\tPhone : "+driver.phone_no+"\n" +
                "\tEmail : "+driver.user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+driver.current_location_latitude+","+driver.current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+driver.current_location_latitude+","+driver.current_location_longitude+ "\n\n";

            msg += config.get('emailCredentials.signature');
            utils.sendEmailForPassword(to, msg, sub, function(resultEmail) {});
        });
    });
};


exports.sendMailForManualEngagementAck = function(customerId, driverId){
    var customerInfo = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customerInfo, [customerId], function(err, customer)
    {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);
        customer = customer[0];

        var driver_info_query = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ?";
        connection.query(driver_info_query, [driverId], function(err, driver)
        {
            logging.logDatabaseQuery("Fetch information for driver", err, driver, null);
            driver = driver[0];

            var game_name = config.get('projectName');
            var to = config.get('emailCredentials.senderEmail');
            var sub = "MANUAL engagement ACKNOWLEDGED by " + driver.user_name + " for " + customer.user_name;

            var msg = "Customer summary:\n\n";
            msg +=
                "\tUser Id : "+customer.user_id+"\n" +
                "\tName : "+customer.user_name+"\n" +
                "\tPhone : "+customer.phone_no+"\n" +
                "\tEmail : "+customer.user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+customer.current_location_latitude+","+customer.current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+customer.current_location_latitude+","+customer.current_location_longitude+ "\n\n";

            msg += "Driver summary:\n\n";
            msg +=
                "\tUser Id : "+driver.user_id+"\n" +
                "\tName : "+driver.user_name+"\n" +
                "\tPhone : "+driver.phone_no+"\n" +
                "\tEmail : "+driver.user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+driver.current_location_latitude+","+driver.current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+driver.current_location_latitude+","+driver.current_location_longitude+ "\n\n";

            msg += config.get('emailCredentials.signature');
            utils.sendEmailForPassword(to, msg, sub, function(resultEmail) {});
        });
    });
};


exports.sendMailForManualEngageFollowup = function(customerId, driverId){
    var customerInfo = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customerInfo, [customerId], function(err, customer)
    {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);
        customer = customer[0];

        var driver_info_query = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ?";
        connection.query(driver_info_query, [driverId], function(err, driver)
        {
            logging.logDatabaseQuery("Fetch information for driver", err, driver, null);
            driver = driver[0];

            var game_name = config.get('projectName');
            var to = config.get('emailCredentials.senderEmail');
            var sub = "FOLLOW UP REQUIRED for manual engagement between  " + driver.user_name + " and " + customer.user_name;

            var msg = "Customer summary:\n\n";
            msg +=
                "\tUser Id : "+customer.user_id+"\n" +
                "\tName : "+customer.user_name+"\n" +
                "\tPhone : "+customer.phone_no+"\n" +
                "\tEmail : "+customer.user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+customer.current_location_latitude+","+customer.current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+customer.current_location_latitude+","+customer.current_location_longitude+ "\n\n";

            msg += "Driver summary:\n\n";
            msg +=
                "\tUser Id : "+driver.user_id+"\n" +
                "\tName : "+driver.user_name+"\n" +
                "\tPhone : "+driver.phone_no+"\n" +
                "\tEmail : "+driver.user_email+"\n"+
                "\tCurrent location: https://www.google.co.in/maps/preview?q="+driver.current_location_latitude+","+driver.current_location_longitude+"\n" +
                "\tFor iOS: comgooglemaps://?q="+driver.current_location_latitude+","+driver.current_location_longitude+ "\n\n";

            msg += config.get('emailCredentials.signature');
            utils.sendEmailForPassword(to, msg, sub, function(resultEmail) {});
        });
    });
};


exports.sendMailForSessionTimeout = function(customer_id, pickup_latitude, pickup_longitude, session_id){
    var customer_info = "SELECT `user_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customer_id], function(err, result_customer){

        var driver_type = [constants.userRegistrationStatus.DRIVER, constants.userRegistrationStatus.DEDICATED_DRIVER];
        var all_engagements = "SELECT a.user_id, a.user_name, a.phone_no, a.reg_as, " +
            "b.pickup_location_address, b.status, b.driver_accept_longitude, b.driver_accept_latitude " +
            "FROM `tb_users` a, `tb_engagements` b " +
            "WHERE a.user_id = b.driver_id AND b.session_id = ? AND a.reg_as IN (" + driver_type.toString() + ")";
        connection.query(all_engagements, [session_id], function(err, result_engagements){
            logging.logDatabaseQuery("Getting all engagements for the session which timed out in last try", err, result_engagements, null);

            if(result_engagements.length > 0){
                var to = [config.get('emailCredentials.multipleSupportEmailIds')];
                var sub = "Request not processed by drivers for " + result_customer[0].user_name;

                var msg = "Customer summary:\n\n";
                msg +=
                    "\tUser Id : "+result_customer[0].user_id+"\n" +
                    "\tName : "+result_customer[0].user_name+"\n" +
                    "\tPhone : "+result_customer[0].phone_no+"\n" +
                    "\tEmail : "+result_customer[0].user_email+"\n"+
                    "\tCurrent location: https://www.google.co.in/maps/preview?q="+pickup_latitude+","+pickup_longitude+"\n" +
                    "\tPickup Address : " + result_engagements[0].pickup_location_address +"\n\n";

                msg += "Engagements:\n";
                for(var i=0; i<result_engagements.length; i++){
                    msg +=
                        "\tDriver Id : "+result_engagements[i].user_id+"\n" +
                        "\tName : "+result_engagements[i].user_name+"\n" +
                        "\tRegistered as : " + utils.registrationStatusToString(result_engagements[i].reg_as) +
                        "\tPhone : "+result_engagements[i].phone_no+"\n" +
                        "\tCurrent location : https://www.google.co.in/maps/preview?q=" + result_engagements[i].driver_accept_latitude+"," + result_engagements[i].driver_accept_longitude +"\n" +
                        "\tDistance : " + utils.calculateDistance(pickup_latitude, pickup_longitude,
                            result_engagements[i].driver_accept_latitude, result_engagements[i].driver_accept_longitude) + " meters\n" +
                        "\tEngagement Status : " + utils.engagementStatusToString(result_engagements[i].status) + "\n\n";
                }

                msg += config.get('emailCredentials.signature');
                utils.sendEmailForPassword(to, msg, sub, function(resultEmail) {});
            }
        });
    });
};


exports.sendWelcomeMail = function(email_ids){
    var sub='Welcome to ' + config.get('projectName');
    var html='<!DOCTYPE html>';
    //html+='<head>';
    //html+='<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    //html+=' <title>Welcome</title>';
    //html+='<style type="text/css">';
    //html+="body{font-family: 'O=pen Sans', 'Lucida Grande', 'Segoe UI', Arial, Verdana, 'Lucida Sans Unicod=e', Tahoma, 'Sans Serif';}";
    //html+='a{ text-decoration:none; color:#0093d4;}';
    //html+='</style>';
    //html+='</head>';
    //html+='<body background="#f1f1f1" style="font-family: "O=pen Sans", "Lucida Grande", "Segoe UI", Arial, Verdana, "Lucida Sans Unicod=e", Tahoma, "Sans Serif";margin:0; padding:0;  background-color:#f1f1f1; font-size:15px; line-height:28px; color:#202020;">';
    //html+='<table width="600px" border="0" cellspacing="0" cellpadding="0" align="center" background="#fff" style="background-color:#fff;">';
    //html+='<tr>';
    //html+='<td>';
    //html+='<table width="600px" border="0" cellspacing="0" cellpadding="0" align="center" style="padding: 5px;">';
    //html+='<tr>';
    //html+='<td colspan="2" style="text-align: justify;"><p align="center" style="font-size: 20px; font-weight:300; line-height: 20px;color:#555; ">Welcome to Jugnoo<br></p>';
    //html+='<img width="100%"  src="http://i59.tinypic.com/2zrf6yo.jpg">';
    //html+='<p style="font-size: 14px; padding: 0px; margin:0px;color:#202020;"><br>We are excited to have you on board with us.<br></p>';
    //html+="<p style='font-size: 14px; padding: 0px; margin:0px; color:#202020;'><br>If you're looking for a comfortable yet affordable ride within the tricity, jugnoo is all you need. Your safety and happiness is our priority. It's a reliable and quick way to travel from one destination to another.<br><br>Enjoy your ride with Jugnoo!</p>";
    //html+='<p style="font-size: 14px; padding: 0px; margin:0px; color:#202020;"><br>Thanks,<br>The Jugnoo Team<br><a href="mailto:support@jugnoo.in">support@jugnoo.in</a></p>';
    //html+='<table width="600px" cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="" style="font-family:Arial,serif;font-size:12px;font-weight:300;border-bottom-width:1px">';
    //html+='<tbody><tr>';
    //html+='<td align="center" valign="center" width="25%">';
    //html+='<a href="https://itunes.apple.com/us/app/jugnoo/id882391317?ls=1&mt=8">';
    //html+='<img width="auto" style="padding : 5px; margin-top: 5%; margin-bottom: 5%; float: right; " src="http://i59.tinypic.com/2zfr0ug.png">';
    //html+='</a>';
    //html+='</td>';
    //html+='<td align="center" valign="center" width="25%" style="padding:15px;">';
    //html+='<a href="https://play.google.com/store/apps/details?id=product.clicklabs.jugnoo">';
    //html+='<img width="auto" style="padding : 5px; margin-top: 5%; margin-bottom: 5%; float: left;" src="http://i60.tinypic.com/30xhi4y.png">';
    //html+='</a>';
    //html+='</td>';
    //html+='</tr>';
    //html+='</tbody></table>';
    //html+='<p style="font-size: 14px; padding: 0px; margin:0px; color:#202020;">You are receiving this email because you signed up for Jugnoo. For support requests, please reach us on <a href="mailto:support@jugnoo.in">support@jugnoo.in</a><br></p>';
    //html+='</td>';
    //html+='</tr>';
    //html+='</table>';
    //html+='</td>';
    //html+='</tr>';
    //html+='</table>';
    //html+='<table width="610px" border="0" cellspacing="0" cellpadding="0" align="center" background="#fff" style="background-color:#272727;">';
    //html+='<tbody><tr>';
    //html+='<td align="center" valign="center" width="400">';
    //html+='<a href="http://jugnoo.in/">';
    //html+='<img src="http://i57.tinypic.com/2z9adrc.png" style="display:table-cell;vertical-align:middle;border:none;color:#818181;font-size:9px; float: left; margin-left: 25px;">';
    //html+='</a>';
    //html+='</td>';
    //html+='<td align="right" valign="top" width="100px" style="padding-top:15px; padding-bottom:15px; padding-left:0px; padding-right:0px;"  width="40px">';
    //html+='<a style="border:0;text-decoration:none;display:table" href="https://www.facebook.com/ridejugnoo?fref=ts" target="_blank">';
    //html+='<img src="http://i62.tinypic.com/2qm0z90.png" style="float: right; margin-right: 25px; ">';
    //html+='</a>';
    //html+='</td>';
    //html+='</tr>';
    //html+='</tbody></table>';
    //html+='</body>';
    //html+='</html>';
    //
    utils.sendHtmlContent(email_ids, html, sub, function(resultEmail)
    {
    });
};


exports.sendMailForDuplicateRegistration = function(users){
    
    var to = config.get('emailCredentials.senderEmail');
    var sub = users.length + " DUPLICATE REGISTRATIONS FROM SAME DEVICE";

    var msg = "";
    var i = 0;
    for (i = 0; i < users.length; i++){
        msg +=
            "\tId: " + users[i].user_id + "\n" +
            "\tName: " + users[i].user_name + "\n" +
            "\tEmail: " + users[i].user_email + "\n" +
            "\tPhone no: " + users[i].phone_no + "\n" +
            "\tDate registered: " + users[i].date_registered + "\n\n";
    }
    
    msg += config.get('emailCredentials.particularSignature');

    utils.sendEmailForPassword(to, msg, sub, function(err, result){});
};


exports.sendMailForUsingPromotionalCode = function(user_email, user_name, num_rides, promo_code){
    //var get_email = "SELECT * FROM `tb_email_templates` WHERE `template_id`=?";
    //connection.query(get_email, [email_template], function(err, email){
    //    logging.logDatabaseQuery("Getting the email template.", err, email);
    //
    //    var subject = email[0].subject;
    //    var body = email[0].template;
    //    utils.sendHtmlContent(to, body, subject, function(err, result){
    //    });
    //});

    var subject = "";
    if(num_rides == 1){
        subject = "You have a FREE ride with "+config.get('projectName');
    }
    else{
        subject = "You have " + num_rides + " FREE rides with "+config.get('projectName');
    }
    var body =
        "Hey " + user_name.split(' ')[0] + ",\n\n" +
        "Awesome. You have just registered on "+config.get('projectName')+" using the promo code " + promo_code + " ";
    if(num_rides == 1)
        body += "and you have got a free ride. ";
    else
        body += "and you have got " + num_rides + " free rides. ";
        body +=
        "Have fun in using "+config.get('projectName')+". Should you need any help, send us an email back or call us at +91-9023-121121.\n\n" ;
        body += config.get('emailCredentials.signature');
    utils.sendEmailForPassword(user_email, body, subject, function(err, result){
    });
};


exports.notifySalesPersonAboutUsage = function(to, user_name, promo_code){
    var subject = user_name + " has just registered to "+config.get('projectName')+" using your code";
    var body =
        "Hi,\n\n" +
        "Awesome. " + user_name.split(' ')[0] + " has just registered on "+config.get('projectName')+" using your promo code " + promo_code + ".\n\n" ;
    body += config.get('emailCredentials.signature');
    utils.sendEmailForPassword(to, body, subject, function(err, result){
    });
};


exports.sendMailForOfflineDrivers = function(inactive_drivers){
    var to = config.get('emailCredentials.senderEmail');

    var num_drivers = 0;

    var msg = "";
    for(var i=0; i<inactive_drivers.length; i++){
        if(constants.rogueDrivers.indexOf(inactive_drivers[i].user_id) == -1){
            num_drivers++;
            // Split timestamp into [ Y, M, D, h, m, s ]
            var timestamp = inactive_drivers[i].timestamp;
            timestamp = timestamp.toISOString().replace(/T/, ' ').replace(/\..+/, '')
            var t = timestamp.split(/[- :]/);
            // Apply each element to the Date function
            var date = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
            var now = new Date();
            var since = now.getTime() - date.getTime();

            // Change the timezone to IST
            var utc = date.getTime() + (date.getTimezoneOffset() * 60000);
            var ist = new Date(utc + (3600000 * 5.5)); // adding 5.5 hours

            msg +=
                "\tDriver Id : "+inactive_drivers[i].user_id+"\n" +
                "\tName : "+inactive_drivers[i].user_name+"\n" +
                "\tRegistered as : " + utils.registrationStatusToString(inactive_drivers[i].reg_as) + "\n" +
                "\tPhone : "+inactive_drivers[i].phone_no+"\n" +
                "\tLast updated timestamp (IST) : " + ist +"\n" +
                "\tSince : " + Math.floor(since/60000) + " minutes\n\n";
        }
    }
    var sub = num_drivers + " drivers going offline";

    utils.sendEmailForPassword(to, msg, sub, function(result){});
};


exports.sendMailForAllDrivers = function(busy_drivers, online_drivers, offline_drivers){
    var to = config.get('emailCredentials.senderEmail');

    var sub = "Summary for all drivers";
    var msg = "";

    msg += "Offline Drivers:\n\n";
    for (var i=0; i<offline_drivers.length; i++){
        msg +=
            "\tDriver Id : "+offline_drivers[i].user_id+"\n" +
            "\tName : "+offline_drivers[i].user_name+"\n" +
            "\tRegistered as : " + utils.registrationStatusToString(offline_drivers[i].reg_as) + "\n" +
            "\tPhone : "+offline_drivers[i].phone_no+"\n" +
            "\tLast updated timestamp (IST) : " + offline_drivers[i].ist +"\n" +
            "\tSince : " + offline_drivers[i].since + " minutes\n\n";
    }

    msg += "Busy Drivers:\n\n";
    for (var i=0; i<busy_drivers.length; i++){
        msg +=
            "\tDriver Id : "+busy_drivers[i].user_id+"\n" +
            "\tName : "+busy_drivers[i].user_name+"\n" +
            "\tRegistered as : " + utils.registrationStatusToString(busy_drivers[i].reg_as) + "\n" +
            "\tPhone : "+busy_drivers[i].phone_no+"\n\n";
    }

    msg += "Online Drivers:\n\n";
    for (var i=0; i<online_drivers.length; i++){
        msg +=
            "\tDriver Id : "+online_drivers[i].user_id+"\n" +
            "\tName : "+online_drivers[i].user_name+"\n" +
            "\tRegistered as : " + utils.registrationStatusToString(online_drivers[i].reg_as) + "\n" +
            "\tPhone : "+online_drivers[i].phone_no+"\n\n";
    }

    utils.sendEmailForPassword(to, msg, sub, function(result_email){});
};


exports.sendMailForUnavailableDrivers = function(drivers){
    var to = config.get('emailCredentials.senderEmail');

    var sub = "Drivers with "+config.get('projectName')+" OFF at duty start time";
    var msg = "";

    var i = 0;
    for(i = 0; i < drivers.length; i++){
        msg +=
            "\tDriver Id : " + drivers[i].user_id + "\n" +
            "\tName : " + drivers[i].user_name + "\n" +
            "\tPhone : " + drivers[i].phone_no + "\n" +
            "\tDuty start time : " + drivers[i].start_time + "\n\n";
    }

    utils.sendEmailForPassword(to, msg, sub, function(result_email){});
};

exports.sendMailForMissedRequests = function(engagements){
    var to = config.get('emailCredentials.senderEmail');
    //var to = ["harsh@jugnoo.in"];
    var sub = "Requests missed by dedicated drivers in last 5 minutes";

    var msg = "";
    var i = 0;
    for(i = 0; i < engagements.length; i++){
        console.log("Engagement:\n" + JSON.stringify(engagements[i]));
        msg +=
            "\tDriver Id : " + engagements[i].driver_id + "\n" +
            "\tName : " + engagements[i].user_name + "\n" +
            "\tPhone no : " + engagements[i].phone_no + "\n" +
            "\tPickup Address : " + engagements[i].pickup_location_address + "\n" +
            "\tDistance : " + engagements[i].accept_distance + " meters\n" +
            "\tRequest reached in : " + (engagements[i].difference < 0 ? 0 : engagements[i].difference) + " seconds\n";
            "\tRequest made at : " + new Date(utils.changeTimezoneFromUtcToIst(engagements[i].accept_distance)).toLocaleString() + " meters\n\n";
    }

    msg += config.get('emailCredentials.particularSignature');

    utils.sendEmailForPassword(to, msg, sub, function(err, result){});
};


exports.sendMailForScheduledRide = function(customer, pickup){
    var to = config.get('emailCredentials.senderEmail');
    var sub = "Searching driver for SCHEDULED PICKUP for " + customer.user_name;

    var msg = "";
    msg += "Customer summary:\n";
    msg +=
        "\tId: " + customer.user_id + "\n" +
        "\tName: " + customer.user_name + "\n" +
        "\tPhone no: " + customer.phone_no + "\n\n";

    msg += "Pickup summary:\n";
    msg +=
        "\tPickup location: https://www.google.co.in/maps/preview?q="+pickup.latitude+","+pickup.longitude+"\n" +
        "\tPickup address: " + pickup.address + "\n" +
        "\tPickup time: " + pickup.pickup_time + "\n\n";

    msg += config.get('emailCredentials.signature');
    utils.sendEmailForPassword(to, msg, sub, function(resultEmail) {});
};


exports.sendMailForScheduledRideFailure = function(pickup){
    var customer_info = "SELECT `phone_no`, `user_name`, `user_id`, `user_email` " +
        "FROM `tb_users` WHERE `user_id`=?";
    connection.query(customer_info, [pickup.user_id], function(err, customer) {
        logging.logDatabaseQuery("Information for the customer", err, customer, null);

        var to = config.get('emailCredentials.senderEmail');
        var sub = "Canceling request of SCHEDULED PICKUP for " + customer[0].user_name;

        var msg = "";
        msg += "Customer summary:";
        msg +=
            "\tId: " + customer[0].user_id + "\n" +
            "\tName: " + customer[0].user_name + "\n" +
            "\tPhone no: " + customer[0].phone_no + "\n\n";

        msg += "Pickup summary:";
        msg +=
            "\tPickup location: https://www.google.co.in/maps/preview?q="+pickup.latitude+","+pickup.longitude+"\n" +
            "\tPickup address: " + pickup.address + "\n" +
            "\tPickup time: " + pickup.pickup_time + "\n\n";

        msg += config.get('emailCredentials.signature');
        utils.sendEmailForPassword(to, msg, sub, function(resultEmail) {});
    });
};


exports.sendScheduleConfirmationMail = function(customer_id, pickup){
    var customer_info = "SELECT `user_id`,`user_name`,`phone_no`,`user_email` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [customer_id], function(err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        if (customer[0].user_email != '') {
            var to = customer[0].user_email;
            var sub = "A pickup scheduled with "+config.get('projectName')+"";

            var pickup_time = utils.changeTimezoneFromUtcToIst(pickup.time).replace(/T/, ' ').replace(/\..+/, '');
            var msg = "Hi " + customer[0].user_name.split(" ")[0] + ", \n\n";
            msg +=
                "You have scheduled a "+config.get('projectName')+" for " + pickup_time + " at " + pickup.address + ". " +
                "We will confirm your booking 20 minutes before the scheduled pickup time. " +
                ""+config.get('projectName')+" app is available on IOS and Android stores.\n\n";

            msg += config.get('emailCredentials.signature');

            utils.sendEmailForPassword(to, msg, sub, function (resultEmail) {
            });
        }
    });
};


exports.sendMail = function(req, res){
    var user_id = req.body.user_id;
    var subject = req.body.subject;
    var message = req.body.message;

    var customer_info = "SELECT `user_email` FROM `tb_users` WHERE `user_id`= ? LIMIT 1";
    connection.query(customer_info, [user_id], function(err, customer) {
        logging.logDatabaseQuery("Fetch information for customer", err, customer, null);

        if (customer.length > 0) {
            customer = customer[0];
            if (customer.user_email != '') {
                var to = customer.user_email;
                utils.sendEmailForPassword(to, subject, message, function(result_email){});
                res.send(JSON.stringify({log: 'e-mail sent'}));
            }
        }
        else{
            res.send(JSON.stringify({log: 'user doesn\'t exist'}));
        }
    });
};


exports.sendMailForFreeRide = function(req, res){
    logging.startSection("send_mail_for_free_ride");

    var sub = "You have a free ride with "+config.get('projectName');
    var html=
        '<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border:0px;color:rgb(0,0,0);font-family:Times;font-size:medium;background-color:rgb(242,242,242)"><tbody><tr><td align="center" valign="top">' +
        '<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-top-width:0px;border-bottom-width:0px;background-color:rgb(255,255,255)"><tbody><tr><td valign="top" style="padding-top:9px">' +
        '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody></tbody>' +
        '</table></td></tr></tbody>' +
        '</table></td></tr><tr><td align="center" valign="top">' +
        '<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-top-width:0px;border-bottom-width:0px;background-color:rgb(255,255,255)"><tbody><tr><td valign="top">' +
        '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr><td valign="top" style="padding:9px">' +
        '<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse"><tbody><tr><td valign="top" style="padding:0px 9px"><img align="left" alt="" src="https://ci5.googleusercontent.com/proxy/INPmEWvpS6WVmrM6UBWEJeWQ03DX62Tg38Npqe9HsW_KcwYVAGAA5hb58yxOKMBchyeRMg8qmFQOaVP1ZQNpu5w5LSPV5By-v7yyTYcO--xFOvOUf8D1kc3zAWhtT2rvVlcK03co0X89gghTwtT_iz-JXvGyZHmP1mStP_Y=s0-d-e1-ft#https://gallery.mailchimp.com/4f0d0bd2cf1c2a9d816925da7/images/75a1d994-4ffe-4c24-89fa-e2cda6ff3a23.png" width="564" style="border:0px;outline:none;vertical-align:bottom;max-width:851px;padding-bottom:0px;display:inline!important" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 534px; top: 193px;"><div id=":19k" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div></td></tr></tbody>' +
        '</table></td></tr></tbody>' +
        '</table></td></tr></tbody>' +
        '</table></td></tr><tr><td align="center" valign="top">' +
        '<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-top-width:0px;border-bottom-width:0px;background-color:rgb(255,255,255)"><tbody><tr><td valign="top">' +
        '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr><td valign="top">' +
        '<table align="left" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse"><tbody><tr><td valign="top" style="color:rgb(96,96,96);font-family:Helvetica;font-size:15px;line-height:22.5px;padding:9px 18px"><h1 style="margin:0px;padding:0px;font-size:40px;line-height:50px;letter-spacing:-1px"><span style="line-height:1.6em">You have a free ride&nbsp;</span></h1><p style="margin:1em 0px;padding:0px">It had been a while since you have downloaded the app and used Jugnoo. As an early adopter we have given you free rides to travel around Chandigarh tri city area. Any fare charged upto $100 would be waived off completely.</p><p style="margin:1em 0px;padding:0px">Over a thousand people in Chandigarh tri city area have used Jugnoo and given us an overwhelming feedback. In case you are unable to see the free ride coupon please update your app from the Google Play store.</p><p style="margin:1em 0px;padding:0px">Whenever you are free try using our services and trust us you will be delighted.</p><p style="margin:1em 0px;padding:0px">Want to know what happens behind the scenes at Jugnoo?&nbsp;<a href="http://chalojugnoose.tumblr.com/post/101785043829/thanks-real-work-begins-now" style="word-wrap:break-word;color:rgb(109,198,221)" target="_blank">Check out this blog post for more details</a></p></td></tr></tbody>' +
        '</table></td></tr></tbody>' +
        '</table></td></tr></tbody>' +
        '</table></td></tr><tr><td align="center" valign="top">' +
        '<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-top-width:0px;border-bottom-width:0px;background-color:rgb(255,255,255)"><tbody><tr><td valign="top" style="padding-bottom:9px">' +
        '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr><td valign="top">' +
        '<table align="left" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse"><tbody><tr><td valign="top" style="color:rgb(96,96,96);font-family:Helvetica;font-size:11px;line-height:13.75px;padding:9px 18px"><em>Copyright ?? 2014 Jugnoo, All rights reserved.</em><br></td></tr></tbody>' +
        '</table></td></tr></tbody>' +
        '</table></td></tr></tbody>' +
        '</table></td></tr></tbody>' +
        '</table>';

    var get_users =
        "SELECT `user_email` FROM `tb_users` WHERE `date_registered` > '2014-10-31 00:00:00' && `user_id` IN " +
        "(" +
            "SELECT DISTINCT(`user_id`) FROM `tb_accounts` WHERE `status`=? && `user_id` NOT IN " +
            "(" +
                "SELECT DISTINCT(`user_id`) FROM `tb_session` WHERE `ride_acceptance_flag`=? && `date` >  timestamp(DATE_SUB(NOW(), INTERVAL 3 DAY))" +
            ")" +
        ")";
    connection.query(get_users, [constants.couponStatus.ACTIVE, constants.rideAcceptanceFlag.ACCEPTED], function(err, users){
        console.log("number of contacts: " + users.length);

        var to = [];
        for(var i=0; i < users.length; i++){
            if(users[i].user_email != ''){
                to.push(users[i].user_email);
            }
        }

        to.push(config.get('emailCredentials.senderEmail'));
        utils.sendHtmlContent_UseBCC(to, html, sub, function(resultEmail)
        {
        });

        res.send("Sent the email to " + users.length + " users");
    });
};


exports.sendMailForCouponsAdded = function(user_name, user_email, number_of_coupons){
    var to = config.get('emailCredentials.senderEmail');
    var sub = "";
    if(number_of_coupons == 1){
        sub = "A free "+config.get('projectName')+" ride credited to your account";
    }
    else{
        sub = number_of_coupons + " free "+config.get('projectName')+" rides credited to your account";
    }
    var msg =
        "Hello " + user_name.split(" ")[0] +",\n\n" +
        "Thank you for downloading "+config.get('projectName')+". ";

    if(number_of_coupons == 1){
        msg += "We have credited a free ride coupon (worth $100) to your account.\n";
    }
    else{
        msg += "We have credited " + number_of_coupons + " free ride coupons (worth $" + number_of_coupons*100+ ") to your account.\n"
    }
    msg += "Now enjoy convenience at just a tap of a button with "+config.get('projectName')+"\n\n" ;
    msg += config.get('emailCredentials.signature');

    utils.sendEmailForPassword(to, msg, sub, function(result){
    });
};


exports.sendMailForCouponsToInactiveUsers = function(user_name, user_email){
    var to = [user_email];
    var sub = "One More FREE Ride";
    fs.readFile('/home/jugnoo/jugnoo-v12/data/free_ride_inactive_user.html', {encoding: 'utf-8'}, function(err, data){
        console.log("Error in reading the file: " + JSON.stringify(err));
        if(!err){
            utils.sendHtmlContent(to, data, sub, function(result){
            });
        }
    });
};


exports.sendMailToReferringUser = function(friends_name, referring_user_id){
    var referring_user = undefined;

    function getReferringUser(callback){
        var get_user = "SELECT `user_name`, `user_email`, `user_name` FROM `tb_users` WHERE `user_id`=?";
        connection.query(get_user, [referring_user_id], function(err, user){
            logging.logDatabaseQuery("Getting the information for referring person", err, user);
            referring_user = user[0];
            callback();
        });
    }

    function sendMail(){
        var first_name = friends_name.split(' ')[0];
        var to = referring_user.user_email;
        var sub = friends_name + " has just registered on "+config.get('projectName')+" using your referral code";
        var msg = "Hey " + referring_user.user_name.split(' ')[0] +",\n\n" +
            "Awesome. " + first_name + " has just registered on "+config.get('projectName')+" as " + first_name + " was referred by you. " +
            "We are awaiting for " + first_name + " to take the first ride with "+config.get('projectName')+" which is actually free. " +
            "Once " + first_name + " takes the first ride, we will credit the referral free ride into your account. " +
            "In the meanwhile if you can help " + first_name + " in using "+config.get('projectName')+", that would be of great help.\n\n" ;
        msg += config.get('emailCredentials.signature');

        utils.sendEmailForPassword(to, msg, sub, function(result){
        });
    }

    // Get the information for the referring user and send the email
    async.series([getReferringUser], sendMail);
};


exports.sendMailToReferredUser = function(name, email, referred_by_code){
    var to = email;
    var sub = "You have just earned a FREE ride on "+config.get('projectName')+"";
    var msg = "Hey " + name.split(' ')[0] +",\n\n" +
        "Great to see you on "+config.get('projectName')+". You have used the referral code \"" + referred_by_code + "\" to use "+config.get('projectName')+" and we are giving you a free ride for it. " +
        "We hope you will use "+config.get('projectName')+" soon and will find it incredibly useful.\n\n" ;
    msg += config.get('emailCredentials.signature');

    utils.sendEmailForPassword(to, msg, sub, function(result){
    });
};


exports.sendMailForFirstReferralRide = function (friends_name, referring_user){
    var first_name = friends_name.split(' ')[0];
    var to = referring_user.user_email;
    var sub = first_name + " has just taken the first ride on "+config.get('projectName')+" and you have been credited a FREE ride";
    var msg = "Hey " + referring_user.user_name.split(' ')[0] + ",\n\n" +
        "Congratulations. " + first_name + " has just taken the first ride with "+config.get('projectName')+". We have credited a free ride in your account. Have fun with "+config.get('projectName')+".\n\n" ;
    msg += config.get('emailCredentials.signature');

    //console.log("Sending message to the REFERRING USER for the first ride taken");
    utils.sendEmailForPassword(to, msg, sub, function(result){
    });
};


exports.sendMailForAdvancedPermissions = function(userId){
    utils.getUserInformation(userId, function(err, user){
        if(err || user === null){
            process.stderr.write('The mail for advanced permissions couldn\' be sent\n');
            return;
        }

        var to = [];
        to.push(user.user_email);
        var cc = [];
        var bcc = [];

        var sub = 'New Features available to you at '+config.get('projectName');

        var fileName = __dirname + '/../data/permissions_granted.html';
        fs.readFile(fileName, {encoding: 'utf-8'}, function(err, msg){
            console.log("Error in reading the file: " + JSON.stringify(err));
            if(!err){
                utils.sendHtmlEmail(to, cc, bcc, sub, msg, function(err, result){});
            }
        });
    });
};


/*
 * All mails for stationing logic for drivers
 */


exports.sendMailForUnacknowledgedStationing = function(unacknowledgedRequests){
    var to = [];
    //to.push('manpreet@jugnoo.in');
    to.push(config.get('emailCredentials.senderEmail'));
    var cc = [];
    var bcc = [];

    var numDrivers = unacknowledgedRequests.length;
    var sub = 'Stationing request unacknowledged by ' + numDrivers + (numDrivers > 1 ? ' drivers' : ' driver');
    var msg = '';

    var i = 0;
    for(i = 0; i < unacknowledgedRequests.length; i++){
        msg +=
            'Driver Id : ' + unacknowledgedRequests[i].user_id + '\n' +
            'Driver Name : ' + unacknowledgedRequests[i].user_name + '\n' +
            'Contact No : ' + unacknowledgedRequests[i].phone_no + '\n' +
            'Station Address : ' + unacknowledgedRequests[i].address + '\n' +
            'Assigned at : ' + new Date(utils.changeTimezoneFromUtcToIst(unacknowledgedRequests[i].assigned_on)).toLocaleTimeString() + '\n\n';
    }

    msg += config.get('emailCredentials.devTeamSignature');

    utils.sendPlainTextEmail(to, cc, bcc, sub, msg, function(err, result){});
};


exports.sendMailForArrivalNoncompliance = function(stationingInfo){
    var to = [];
    //to.push('manpreet@jugnoo.in');
    to.push(config.get('emailCredentials.senderEmail'));
    var cc = [];
    var bcc = [];

    var sub = stationingInfo.user_name + ' not arrived at station within time';
    var msg = '';

    msg +=
        'Driver Id : ' + stationingInfo.user_id + '\n' +
        'Driver Name : ' + stationingInfo.user_name + '\n' +
        'Contact No : ' + stationingInfo.phone_no + '\n' +
        'Station Address : ' + stationingInfo.address + '\n' +
        'Assigned at : ' + new Date(utils.changeTimezoneFromUtcToIst(stationingInfo.assigned_on)).toLocaleTimeString() + '\n' +
        'Should arrive on: ' + new Date(utils.changeTimezoneFromUtcToIst(stationingInfo.should_arrive_on)).toLocaleTimeString() + '\n';

    msg += config.get('emailCredentials.devTeamSignature');

    utils.sendPlainTextEmail(to, cc, bcc, sub, msg, function(err, result){});
}