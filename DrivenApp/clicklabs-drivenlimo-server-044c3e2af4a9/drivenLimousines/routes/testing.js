var utils = require('./commonfunction');
var logging = require('./logging');
var constants = require('./constants');

exports.userData = function(req, res)
{
    var access_token = req.body.access_token;
    var manvalues = [access_token];
//console.log("values"+req.body);
    var checkblank = utils.checkBlank(manvalues);

    if (checkblank == 1)
    {
        response1 = {"error": "some parameter missing", "flag": 0}
        res.send(JSON.stringify(response1));
    }
    else
    {
        utils.authenticateUser(access_token, function(result)
        {
            if (result == 0)
            {
                response1 = {"error": "Invalid access token", "flag": 1}
                res.send(JSON.stringify(response1));
            }
            else
            {
                 var sql = "SELECT `user_id`,`driver_id`,`engagement_id` FROM `tb_engagements` WHERE `status`= ?"
                connection.query(sql, [0], function(err, result)
                {
//                    console.log(err);
                    var customerId= result[0].user_id;
                    var driverId=result[0].driver_id;
                    var engagementId=result[0].engagement_id;
                    
                        response1 = {"favourite_data": result}
                        res.send(JSON.stringify(response1));
                    
                    
//                var sql = "SELECT `reg_as` FROM `tb_users` WHERE `user_id`= ?"
//                connection.query(sql, [customer_user_id], function(err, result_customer)
//                {
//
//                    var result_customer_device = result_customer[0].device_type;
//                    if (result_customer_device == 0)
//                    {
//                        // sendAndroidPushNotifications(result_driver[0].user_device_token, message, brandID, brandName)
//                    }
//                    else if (result_customer_device == 1)
//                    {
//                        //   sendIosPushNotification(result_driver[0].user_device_token, couponTitle);
//                    }
//                    var sql = "UPDATE `tb_users` SET `status`=? WHERE `user_id`=?"
//                    connection.query(sql, [1, DriverUserId], function(err, results)
//                    {
//                        var sql = "UPDATE `tb_engagements` SET `status`=? WHERE `engagement_id`=? "
//                        connection.query(sql, [1, engage_id], function(err, result_accept)
//                        {
//                            response1 = {"user_name": result_customer[0].user_name, "phone_no": result_customer[0].phone_no, "user_image": result_customer[0].user_image}
//                            var final = {"user_data": response1};
//                            res.send(JSON.stringify(final));
//                        });
//                    });
//                });
            });

            }

        });
    }
};


exports.test_sending_sms = function(req, res){
    var contact_number = req.body.contact_number;

    var manvalues = [contact_number];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        sendParameterMissingResponse(res);
    }
    else {
        var accountSid = config.get('twillioCredentials.accountSid');
        var authToken = config.get('twillioCredentials.authToken');

        var client = require('twilio')(accountSid, authToken);
        client.messages.create({
            to: contact_number, // Any number Twilio can deliver to
            from: config.get('twillioCredentials.fromNumber'),
            body: 'Testing SMS service'// body of the SMS message
        }, function(err, message) {
            console.log("Testing sms service: " + "Error: " + err );
            console.log("Testing sms service: " + "Message: " + message );
            res.send({"error":JSON.stringify(err), "message":JSON.stringify(message)});
        });
    }
};


exports.test_email_service = function(req,res){
    var receiver_id = req.body.receiver_id;

    var manvalues = [receiver_id];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        sendParameterMissingResponse(res);
    }
    else {
        var to = receiver_id;
        var sub = "test email";
        var msg = "test email";

        utils.sendEmailForPassword(to, msg, sub, function(resultEmail)
        {
            console.log("The result for the sending email is: " + resultEmail);
        });
    }
};


exports.check_push_notification = function(req, res){
    var user_id = req.body.user_id;
    var num_notifications = req.body.num;

    var message = "This is a test notification";
    var flag = 1;       // silent notification for iOS
    var payload = {"log": "no drivers available", "flag": constants.notificationFlags.NO_DRIVERS_AVAILABLE};

    var get_user_device_info = "SELECT `user_id`,`device_type`,`user_device_token` FROM `tb_users` WHERE `user_id`=?";
    connection.query(get_user_device_info, [user_id], function (err, result_user)
    {
        logging.logDatabaseQuery("Get device information for the driver", err, result_user, null);

        if (result_user[0].device_type == constants.deviceType.ANDROID)
        {
            for(var i=0; i < num_notifications; i++)
                utils.sendAndroidPushNotification(result_user[0].user_device_token, payload);
        }
        else if (result_user[0].device_type == constants.deviceType.iOS) {
            for (var i = 0; i < num_notifications; i++)
                utils.sendIosPushNotification(result_user[0].user_device_token, message, flag, payload);
        }
    });

    res.send(JSON.stringify({log: "sending " + num_notifications + " notifications to " + user_id}));
};