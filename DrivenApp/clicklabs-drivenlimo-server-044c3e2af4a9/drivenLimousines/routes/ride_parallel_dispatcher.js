var request = require('request');
var async = require('async');

var utils = require('./commonfunction');
var logging = require('./logging');
var mailer = require('./mailer');
var messenger = require('./messenger');
var constants = require('./constants');
var responses = require('./responses');
var stationing = require('./stationing');



// global variables
g_drivers_assigned = {};
// keeping the type of batch processed for the session
g_batch_processed = {};
// keeping the timeout IDs for the manual engagements created
g_maunal_engage_timeouts = {};

driver_home_locations = undefined;
home_location_tolerance = 0.000494; // defines a square of approx 50 meters



exports.findDriversInArea = findDriversInArea;
exports.getCurrentUserStatus = getCurrentUserStatus;
exports.getLastRideForRating = getLastRideForRating;
//exports.makeDriverFree       = makeDriverFree;



function sendErrorResponse(res) {
    var response = {
        "error": 'something went wrong',
        "flag": constants.responseFlags.ERROR_IN_EXECUTION
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
}

function sendParameterMissingResponse(res) {
    var response = {
        "error": "some parameter missing",
        "flag": constants.responseFlags.PARAMETER_MISSING
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
}

function sendAuthenticationError(res) {
    var response = {
        "error": 'invalid access token',
        "flag": constants.responseFlags.INVALID_ACCESS_TOKEN
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
}

exports.sendAuthenticationError = sendAuthenticationError;
exports.sendParameterMissingResponse = sendParameterMissingResponse;


// Find drivers available within the vicinity of my current location
exports.find_driver_in_area = function(req, res) {
    //logging.startSection("Find_driver_in_area");
    //logging.logRequest(req);

    var customer_access_token = req.body.access_token;
    var customer_lat = req.body.latitude;
    var customer_long = req.body.longitude;
    var carType = req.body.car_type;

    if (typeof carType === 'undefined') {
        carType = 0;
    }

    var manValues = [customer_access_token, customer_lat, customer_long];
    var checkData = utils.checkBlank(manValues);
    if (checkData == 1) {
        sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(customer_access_token, function(user) {
            if (user == 0) {
                sendAuthenticationError(res);
            } else {
                findDriversInArea(carType, customer_lat, customer_long, function(response) {
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));
                });

                logFindDriversRequest(user[0].user_id, customer_lat, customer_long);
            }
        });
    }
};

function logFindDriversRequest(userId, latitude, longitude) {
    var getLastLog =
        "SELECT * " +
        "FROM `tb_driver_search_logs` " +
        "WHERE `user_id` = ? AND `logged_on` > DATE_SUB(NOW(), INTERVAL 5 MINUTE)";
    connection.query(getLastLog, [userId], function(err, logs) {
        if (err) {
            logging.logDatabaseQueryError("Getting any search driver logs within last 5 minutes", err, logs);
            return;
        }

        if (logs.length > 0) {
            return;
        }

        // Else log this find drivers query
        var insertLog =
            "INSERT INTO `tb_driver_search_logs` " +
            "(`user_id`, `latitude`, `longitude`) " +
            "VALUES (?, ?, ?)";
        var values = [userId, latitude, longitude];
        connection.query(insertLog, values, function(err, result) {
            if (err) {
                logging.logDatabaseQueryError("Inserting logs for search drivers by user", err, result);
                return;
            }
        });
    });
}


// This is not getting called right now as we are using the approach for joining the tables
function getHomeLocationsForDrivers(callback) {
    if (driver_home_locations === undefined) {
        driver_home_locations = {};
        var get_locations = "SELECT `driver_id`, `latitude`, `longitude` FROM `tb_home_locations`";
        connection.query(get_locations, [], function(err, locations) {
            logging.logDatabaseQuery("Getting the home locations for the drivers", err, locations, null);

            var i = 0;

            for (i = 0; i < locations.length; i++) {
                driver_home_locations[locations[i].driver_id] = {
                    latitude: locations[i].latitude,
                    longitude: locations[i].longitude
                };
            }
            callback();
        });
    } else {
        callback();
    }
}


function isDriverAtHome(home_latitude, home_longitude, current_latitude, current_longitude) {
    if (Math.abs(home_latitude) < 0.00001 && Math.abs(home_longitude) < 0.00001) {
        return false;
    } else if (current_latitude > home_latitude + home_location_tolerance || current_latitude < home_latitude - home_location_tolerance || current_longitude > home_longitude + home_location_tolerance || current_longitude < home_longitude - home_location_tolerance) {
        return false;
    } else {
        return true;
    }
}


function findDriversInArea(carType, latitude, longitude, callback) {
    var fetch_drivers_query = "SELECT * " +
        "FROM" +
        "(" +
        "SELECT * FROM" +
        "(SELECT `user_name`,`phone_no`,`user_image`,`driver_car_image`,`current_location_latitude` as latitude, `current_location_longitude` as longitude," +
        "`user_id`,`total_rating_got_driver`,`total_rating_driver` FROM `tb_users` WHERE `current_user_status`=? && `status`=? && `reg_as`!= ? && `is_available` = 1 && `car_type`=?) AS `drivers`" +
        "JOIN" +
        "(SELECT `driver_id`, `latitude` as home_latitude, `longitude` as home_longitude FROM `tb_home_locations`) AS `home_locations`" +
        "WHERE drivers.user_id = home_locations.driver_id) " +
        "AS `drivers_with_locations`" +
        "JOIN " +
        "`tb_timings` " +
        "WHERE drivers_with_locations.user_id = tb_timings.driver_id && TIME(NOW()) > tb_timings.start_time && TIME(NOW()) < tb_timings.end_time";
    var values = [constants.userCurrentStatus.DRIVER_ONLINE, constants.userFreeStatus.FREE, constants.userRegistrationStatus.CUSTOMER, carType];
    connection.query(fetch_drivers_query, values, function(err, resultdrivers) {

        var drivers = [];
        var resultdrivers_length = resultdrivers.length;
        for (var i = 0; i < resultdrivers_length; i++) {
            if (isDriverAtHome(resultdrivers[i].home_latitude, resultdrivers[i].home_longitude, resultdrivers[i].latitude, resultdrivers[i].longitude)) {
                console.log(resultdrivers[i].user_name + " is at home right now");
            } else if (Math.abs(resultdrivers[i].latitude) < 0.001 && Math.abs(resultdrivers[i].longitude) < 0.001) {
                console.log("The current location of " + resultdrivers[i].user_name + " is at 0,0");
            } else {
                resultdrivers[i].distance = utils.calculateDistance(
                    latitude, longitude,
                    resultdrivers[i].latitude, resultdrivers[i].longitude);

                resultdrivers[i].rating = resultdrivers[i].total_rating_driver / resultdrivers[i].total_rating_got_driver;
                delete resultdrivers[i].total_rating_got_driver;
                delete resultdrivers[i].total_rating_driver;

                if (resultdrivers[i].distance / 1000 < 12) {
                    drivers.push(resultdrivers[i]);
                }
            }
        }

        logging.logDatabaseQuery("Fetching all drivers", err, resultdrivers, null);

        drivers = utils.sortByKeyAsc(drivers, "distance");
        drivers = {
            "data": drivers
        };

        callback(drivers);
    });
}


// a customer requests for a ride from Jugnoo
exports.request_ride = function(req, res) {

    if (req.body.admin_panel_request_flag == 1) {
        res.header("Access-Control-Allow-Origin", "*");
    }

    logging.startSection("request_ride");
    logging.logRequest(req);

    var access_token = req.body.access_token;
    var pickup_latitude = req.body.latitude;
    var pickup_longitude = req.body.longitude;
    var current_latitude = req.body.current_latitude;
    var current_longitude = req.body.current_longitude;
    var duplicate_flag = req.body.duplicate_flag;
    var accuracy = req.body.location_accuracy;
    var pickup_id = req.body.pickup_id;
    var carType = req.body.car_type;


    var manualDestinationLatitude = req.body.manual_destination_latitude;
    var manualDestinationLongitude = req.body.manual_destination_longitude;
    var manualDestinationAddress = req.body.manual_destination_address;

    if (typeof accuracy === 'undefined')
        accuracy = constants.EMPTY_ACCURACY;
    if (typeof pickup_id === 'undefined')
        pickup_id = 0;
    if (typeof current_latitude === 'undefined')
        current_latitude = 0;
    if (typeof current_longitude === 'undefined')
        current_longitude = 0;
    if (typeof manualDestinationLatitude === 'undefined')
        manualDestinationLatitude = 0;
    if (typeof manualDestinationLongitude === 'undefined')
        manualDestinationLongitude = 0;
    if (typeof manualDestinationAddress === 'undefined')
        manualDestinationAddress = '';
    if (typeof carType === 'undefined') {
        carType = 0;
    }

    var manvalues = [access_token, pickup_latitude, pickup_longitude, duplicate_flag, carType];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                sendAuthenticationError(res);
            } else {
                var customer_id = result[0].user_id;
                var is_blocked = result[0].is_blocked;
                var customer_image = result[0].user_image;
                var customer_rating = result[0].customer_rating;

                var update_location = "UPDATE `tb_users` " +
                    "SET `current_location_latitude` = ?, `current_location_longitude` = ? " +
                    "WHERE `user_id` = ?";
                var values = [pickup_latitude, pickup_longitude, customer_id];
                connection.query(update_location, values, function(err, result) {
                    logging.logDatabaseQuery("Updating the location of the customer", err, result, null);
                });

                if (duplicate_flag == 0) {
                    // Get the most recent session for the customer
                    var get_session = "SELECT `session_id`, `is_active`, `cancelled_by_user`, TIMESTAMPDIFF(SECOND, `date`, NOW()) as `time_difference` " +
                        "FROM `tb_session` " +
                        "WHERE `user_id` = ? && `date` > timestamp(DATE_SUB(NOW(), INTERVAL 3 HOUR)) ORDER BY `session_id` DESC LIMIT 1";
                    connection.query(get_session, [customer_id], function(err, result_session) {
                        logging.logDatabaseQuery("Getting the previous session for the customer even in case of new request", err, result_session, null);

                        // If no session has been created yet or the previous session has timed out or is inactive
                        // then I have to make a request to the system to create a request for this schedule
                        if (result_session.length > 0 &&
                            result_session[0].cancelled_by_user == 1 && result_session[0].time_difference < 1) {
                            var response = {
                                error: 'Please wait for a while before you can request a ride again.',
                                flag: constants.responseFlags.SHOW_ERROR_MESSAGE
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));

                            logging.addEventToSession(result_session[0].session_id, {
                                event: "The user is requesting again within 60 seconds of cancelling the previous request",
                                response: response
                            });
                        } else if (result_session.length > 0 && result_session[0].is_active == constants.sessionStatus.ACTIVE) {
                            console.log("CHANGING THE DUPLICATE FLAG FOR THE REQUEST");
                            req.body.duplicate_flag = 1;

                            logging.addEventToSession(result_session[0].session_id, {
                                event: "The user already has an active session within last three hours, changing the duplicate flag and making the request again",
                                session: result_session[0],
                                time_difference: Math.floor(result_session[0].time_difference / 60)
                            });

                            module.exports.request_ride(req, res);
                        } else {
                            // Create a new session for the passenger and send request to
                            // constants.NUM_DRIVER_REQUESTS_BATCH drivers simultaneously
                            var create_session_query = "INSERT INTO `tb_session`(`user_id`, `accuracy`, `pickup_id`, `current_latitude`, `current_longitude`) VALUES(?, ?, ?, ?, ?)";
                            var values = [customer_id, accuracy, pickup_id, current_latitude, current_longitude];
                            connection.query(create_session_query, values, function(err, result_create_session) {
                                logging.logDatabaseQuery("Creating a new session", err, result_create_session, req.body);

                                if (err) {
                                    sendErrorResponse(res);
                                } else {
                                    var session_id = result_create_session.insertId;
                                    // Get the start time for the session

                                    logging.addSession({
                                        session_id: session_id,
                                        timestamp: new Date(),
                                        user: result[0]
                                    });

                                    var get_session = "SELECT `date` FROM `tb_session` WHERE `session_id`=?";
                                    connection.query(get_session, [session_id], function(err, result_session) {
                                        // Tell the passenger that we are allocating driver for him
                                        var response = {
                                            "log": 'Assigning driver',
                                            "flag": constants.responseFlags.ASSIGNING_DRIVERS,
                                            "session_id": session_id,
                                            "start_time": result_session[0].date.toISOString().replace(/T/, ' ').replace(/\..+/, '')
                                        };
                                        logging.logResponse(response);
                                        res.send(JSON.stringify(response));

                                        if (is_blocked === 0) {
                                            mailer.sendMailForRideRequest(customer_id, pickup_latitude, pickup_longitude);
                                        }

                                        // Create a function which will be called after a timeout of 60 seconds
                                        // and will get the appropriate thing done
                                        var num_try = 0;
                                        var data = {
                                            customer_id: customer_id,
                                            is_blocked: is_blocked,
                                            pickup_latitude: pickup_latitude,
                                            pickup_longitude: pickup_longitude,
                                            session_id: session_id,
                                            manualDestinationLatitude: manualDestinationLatitude,
                                            manualDestinationLongitude: manualDestinationLongitude,
                                            manualDestinationAddress: manualDestinationAddress,
                                            customer_rating : customer_rating,
                                            customer_image:customer_image,
                                            car_type: carType
                                        };
                                        processRideRequest(data, num_try);
                                        var intervalID = setInterval(function() {
                                            num_try++;
                                            processRideRequest(data, num_try);
                                            if (num_try == constants.NUM_TRIES_ALLOCATE_DRIVER) {
                                                clearInterval(intervalID);
                                            }
                                        }, constants.TRIES_TIMEOUT);
                                    });
                                }
                            }); // create a new session
                        }
                    });
                } else if (duplicate_flag == 1) {
                    // Get the previous session for the customer
                    var session_exists_query = "SELECT `session_id`, `user_id`, `ride_acceptance_flag`, `is_active`, `date` " +
                        "FROM `tb_session` " +
                        "WHERE `user_id`=? " +
                        "ORDER BY `date` DESC LIMIT 1";
                    connection.query(session_exists_query, [result[0].user_id], function(err, result_get_session) {
                        logging.logDatabaseQuery("Getting previous session for the customer", err, result_get_session, null);

                        if (err) {
                            sendErrorResponse(res);
                        } else {
                            var session_id = -1;

                            if (result_get_session.length > 0 && result_get_session[0].is_active == constants.sessionStatus.ACTIVE) {
                                session_id = result_get_session[0].session_id;
                                // We have at least one session for this customer

                                logging.addEventToSession(session_id, {
                                    event: "Getting duplicate hit for request_ride"
                                });

                                if (result_get_session[0].ride_acceptance_flag == constants.rideAcceptanceFlag.NOT_YET_ACCEPTED) {
                                    // Tell the customer that we are looking for a driver
                                    var response = {
                                        "log": 'Assigning driver',
                                        "flag": constants.responseFlags.ASSIGNING_DRIVERS,
                                        "session_id": session_id,
                                        "start_time": result_get_session[0].date.toISOString().replace(/T/, ' ').replace(/\..+/, '')
                                    };
                                    logging.logResponse(response);
                                    res.send(JSON.stringify(response));

                                    logging.addEventToSession(session_id, {
                                        event: "Getting duplicate hit, session is active and not yet accepted",
                                        response: response
                                    });
                                } else if (result_get_session[0].ride_acceptance_flag == constants.rideAcceptanceFlag.ACCEPTED) {
                                    // Get the information for the active engagement
                                    var statues = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED];
                                    var active_engagement = "SELECT `driver_id`, `engagement_id`, `status` " +
                                        "FROM `tb_engagements` " +
                                        "WHERE `session_id`=? && `user_id`=? && `status` IN (" + statues.toString() + ")";
                                    var values = [session_id, customer_id];
                                    connection.query(active_engagement, values, function(err, result_engagement) {
                                        logging.logDatabaseQuery("Getting the active engagement (duplicate request)", err, result_engagement, null);

                                        logging.addEventToSession(session_id, {
                                            event: "Getting duplicate hit, session is active and accepted"
                                        });

                                        if (err) {
                                            sendErrorResponse(res);
                                        } else if (result_engagement.length == 0) {
                                            deactivateSession(session_id);
                                            var response = {
                                                "message": 'Please try again.',
                                                "flag": constants.responseFlags.SHOW_MESSAGE
                                            };
                                            logging.logResponse(response);
                                            res.send(response);

                                            logging.addEventToSession(session_id, {
                                                event: "Getting duplicate hit, THE SESSION IS ACTIVE AND ACCEPTED BUT NO ENGAGEMENT IS ACTIVE",
                                                response: response
                                            });
                                        } else if (result_engagement.length > 0 &&
                                            (result_engagement[0].status == constants.engagementStatus.ACCEPTED ||
                                                result_engagement[0].status == constants.engagementStatus.STARTED)) {
                                            logging.addEventToSession(session_id, {
                                                event: "Getting duplicate hit, Sending the engagement data for the active engagement"
                                            });

                                            // Send response to the customer
                                            sendRideAcceptanceResponseToCustomer(res, customer_id, result_engagement[0].driver_id, session_id, result_engagement[0].engagement_id, result_engagement[0].status);
                                        }
                                        //else if(result_engagement.length > 0 && result_engagement[0].status == constants.engagementStatus.STARTED) {
                                        //    // Send response that the ride has started
                                        //    //sendRideStartedResponseToCustomer();
                                        //    var response = {"log": 'Your ride has started', "flag": constants.responseFlags.RIDE_STARTED};
                                        //    logging.logResponse(response);
                                        //    res.send(response);
                                        //}

                                    });
                                }
                            } else if (result_get_session.length > 0 &&
                                result_get_session[0].is_active == constants.sessionStatus.TIMED_OUT) {
                                response = {
                                    "log": "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.",
                                    "flag": constants.responseFlags.NO_DRIVERS_AVAILABLE
                                };
                                res.send(JSON.stringify(response));
                                logging.addEventToSession(session_id, {
                                    event: "Getting duplicate hit, the session has timed out",
                                    response: response
                                });
                            } else if (result_get_session.length > 0 &&
                                result_get_session[0].is_active == constants.sessionStatus.INACTIVE) {
                                response = {
                                    "log": "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.",
                                    "flag": constants.responseFlags.NO_DRIVERS_AVAILABLE
                                };
                                res.send(JSON.stringify(response));
                                logging.addEventToSession(session_id, {
                                    event: "Getting duplicate hit, the session is inactive now",
                                    response: response
                                });
                            }
                        }
                    }); // if session exists
                } // if duplicate request
            }
        }); // authenticate User
    }
};


// Update the previous engagements and start new ones if the number of tries is less than constants.NUM_TRIES_ALLOCATE_DRIVER
// Update the previous engagements and clear memory if this is being called for one more time
// STEPS:
//      Get the current status for the session to see if the session is active or not
//      If Active,
//          Change the status for previous engagements which has not been accepted and send notifications to the drivers
//          If the ride has been accepted, do nothing
//          If the ride has not been accepted, process the next batch of requests
//              Deactivate the session if no new drivers are present
//          Deactivate the session if maximum number of tries reached
function processRideRequest(data, num_tries) {
    var session_id = data.session_id;

    logging.addEventToSession(session_id, {
        event: "Processing the ride request for this session"
    });

    // Get the current status for the session to see if the session is active or not
    var get_session_info = "SELECT `is_active`, `ride_acceptance_flag` FROM `tb_session` WHERE `session_id`=?";
    var values = [session_id];
    connection.query(get_session_info, values, function(err, result_session) {
        logging.logDatabaseQuery("Getting the information for the session", err, result_session, null);
        if (err) {
            sendErrorResponse(res);
        } else {
            logging.addEventToSession(session_id, {
                event: "Fetching the information for the session",
                session: result_session[0]
            });

            if (result_session[0].is_active == constants.sessionStatus.ACTIVE) {
                // Change the status for previous engagements which has not been accepted and send notifications to the drivers
                var status = [constants.engagementStatus.REQUESTED];
                var get_previous_engagements = "SELECT `engagement_id`, `driver_id` FROM `tb_engagements` WHERE `session_id`=? && `status` IN (" + status + ")";
                connection.query(get_previous_engagements, [session_id], function(err, result_engagements) {
                    logging.logDatabaseQuery("Getting previous engagements for the session", err, result_engagements, null);
                    if (err) {
                        sendErrorResponse(res);
                    } else {
                        for (var i = 0; i < result_engagements.length; i++) {
                            var update_previous_engagement = "UPDATE `tb_engagements` SET `status`=? WHERE `engagement_id`=?";
                            var values = [constants.engagementStatus.TIMEOUT, result_engagements[i].engagement_id];
                            connection.query(update_previous_engagement, values, function(err, result_update) {
                                logging.logDatabaseQuery("Updating status of previous engagement to timeout", err, result_update, null);
                            });

                            var message = "The request has timed out";
                            var flag = 2; // silent notification for iOS
                            var payload = {
                                "engagement_id": result_engagements[i].engagement_id,
                                "flag": constants.notificationFlags.REQUEST_TIMEOUT
                            };
                            utils.sendNotification(result_engagements[i].driver_id, message, flag, payload);

                            logging.addEventToSession(session_id, {
                                event: "Sending notification to driver that the request has timed out",
                                driver: result_engagements[i].driver_id,
                                payload: payload
                            });
                        }
                    }
                });
            }

            // If the session is still active and the ride has not been accepted,
            // process the next batch of requests
            if (result_session[0].is_active == constants.sessionStatus.ACTIVE && !result_session[0].ride_acceptance_flag) {
                if (num_tries == 0) {
                    logging.addEventToSession(session_id, {
                        event: "Processing the first batch of drivers"
                    });

                    processNearestDriverIfAny(data);
                    //processAdhocBatchOfDrivers(data);
                } else if (num_tries == 1) {
                    logging.addEventToSession(session_id, {
                        event: "Processing the second batch of drivers"
                    });

                    if (data.is_blocked === 0) {
                        mailer.sendMailForSessionTimeout(data.customer_id, data.pickup_latitude, data.pickup_longitude, data.session_id);
                    }

                    if (g_batch_processed.hasOwnProperty(data.session_id.toString()) && g_batch_processed[data.session_id.toString()] == constants.batchType.NEAREST) {
                        console.log("PROCESSING ADHOC BATCH OF DRIVER IN THE SECOND BATCH SINCE THERE WERE DRIVERS NEARBY");
                        processAdhocBatchOfDrivers(data);
                    } else {
                        console.log("PROCESSING *DEDICATED* BATCH OF DRIVER IN THE SECOND BATCH SINCE THERE WERE DRIVERS NEARBY");
                        processDedicatedBatchOfDrivers(data);
                    }
                } else if (num_tries > 1 && num_tries < constants.NUM_TRIES_ALLOCATE_DRIVER - 1) {
                    logging.addEventToSession(session_id, {
                        event: "Processing the third batch of drivers"
                    });

                    if (data.is_blocked === 0) {
                        mailer.sendMailForSessionTimeout(data.customer_id, data.pickup_latitude, data.pickup_longitude, data.session_id);
                    }

                    processDedicatedBatchOfDrivers(data);
                } else if (num_tries == constants.NUM_TRIES_ALLOCATE_DRIVER - 1) {
                    logging.addEventToSession(session_id, {
                        event: "Creating a manual engagement of the customer with a dedicated driver."
                    });

                    if (data.is_blocked === 0) {
                        mailer.sendMailForSessionTimeout(data.customer_id, data.pickup_latitude, data.pickup_longitude, data.session_id);
                    }

                    // Making sure that we have processed the dedicated batch of drivers before coming to
                    if (g_batch_processed.hasOwnProperty(data.session_id.toString()) && g_batch_processed[data.session_id.toString()] == constants.batchType.DEDICATED) {
                        console.log("PROCESSING ADHOC BATCH OF DRIVER IN THE SECOND BATCH SINCE THERE WERE DRIVERS NEARBY");
                        createManualEngagement(data);
                    }
                } else if (num_tries == constants.NUM_TRIES_ALLOCATE_DRIVER) {
                    logging.addEventToSession(session_id, {
                        event: "Exhausted the number of tries to be made for this session"
                    });

                    // All the requests sent has timed out,
                    // In this case, we deactivate the session and send the notification that no driver is available
                    timeoutSession(session_id);

                    var message = "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.";
                    var flag = 2; // silent iOS notification
                    var payload = {
                        "log": "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.",
                        "flag": constants.notificationFlags.NO_DRIVERS_AVAILABLE
                    };
                    utils.sendNotification(data.customer_id, message, flag, payload);

                    if (data.is_blocked === 0) {
                        mailer.sendMailForSessionTimeout(data.customer_id, data.pickup_latitude, data.pickup_longitude, data.session_id);
                    }

                    logging.addEventToSession(session_id, {
                        event: "Sending notification to customer that the session has timed out",
                        payload: payload
                    });
                }


                function processNearestDriverIfAny(data) {
                    logging.addEventToSession(data.session_id, {
                        event: "Processing the nearest batch of drivers"
                    });

                    g_batch_processed[data.session_id.toString()] = constants.batchType.NEAREST;
                    fetchNearestDriver(data, function(err, drivers) {
                        if (drivers.length == 0) {
                            logging.addEventToSession(data.session_id, {
                                event: "No drivers found within the proximity radius"
                            });

                            console.log("driverssssssss==============")
                            console.log(drivers);

                            processAdhocBatchOfDrivers(data);
                            //// deactivate the session if no new drivers are present
                            //timeoutSession(session_id);
                            //
                            //var message = "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.";
                            //var flag = 2;       // silent iOS notification
                            //var payload = {
                            // "log": "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.",
                            // "flag": constants.notificationFlags.NO_DRIVERS_AVAILABLE};
                            //utils.sendNotification(data.customer_id, message, flag, payload);
                        } else {
                            console.log("SENDING REQUEST TO THE NEAREST DRIVER");
                            // Get the pickup address for the customer
                            // TODO simplify the logic to get the address from previous engagements if any later
                            utils.getLocationAddress(data.pickup_latitude, data.pickup_longitude, function(pickup_address) {
                                // Send requests to the new batch of drivers
                                var requests_made = 0;
                                for (var i = 0; i < drivers.length && i < constants.NUM_DRIVER_REQUESTS_BATCH; i++) {
                                    createNewEngagement(data, pickup_address, drivers[i], constants.batchType.ADHOC);
                                    requests_made++;
                                    console.log("REQUESTS MADE TILL NOW: " + requests_made);
                                }

                                // Update the number of drivers requested for the session
                                var update_session = "UPDATE `tb_session` SET `requested_drivers`=`requested_drivers`+? WHERE `session_id`=?";
                                var values = [requests_made, session_id];
                                connection.query(update_session, values, function(err, result_update) {
                                    logging.logDatabaseQuery("Updating the requested_drivers for the session", err, result_update, null);
                                });
                            }); // get location address
                        }
                    });
                }


                function processDedicatedBatchOfDrivers(data) {
                    logging.addEventToSession(data.session_id, {
                        event: "Processing the dedicated batch of drivers"
                    });

                    g_batch_processed[data.session_id.toString()] = constants.batchType.DEDICATED;
                    fetchDedicatedDrivers(data, function(err, drivers) {
                        if (drivers.length == 0) {
                            logging.addEventToSession(session_id, {
                                event: "No dedicated drivers found within the pickup radius"
                            });

                            createManualEngagement(data);

                            //// deactivate the session if no new drivers are present
                            //timeoutSession(data.session_id);
                            //
                            //var message = "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.";
                            //var flag = 2;       // silent iOS notification
                            //var payload = {
                            //    "log": "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.",
                            //    "flag": constants.notificationFlags.NO_DRIVERS_AVAILABLE};
                            //utils.sendNotification(data.customer_id, message, flag, payload);
                            //
                            //logging.addEventToSession(session_id, {
                            //    event: "Sending notification to customer that the session has timed out",
                            //    payload: payload
                            //});
                        } else {
                            // Get the pickup address for the customer
                            utils.getLocationAddress(data.pickup_latitude, data.pickup_longitude, function(pickup_address) {
                                // Send requests to the new batch of drivers
                                var requests_made = 0;
                                for (var i = 0; i < drivers.length && i < constants.NUM_DEDICATED_REQUESTS_BATCH; i++) {
                                    createNewEngagement(data, pickup_address, drivers[i], constants.batchType.DEDICATED);
                                    requests_made++;
                                    console.log("REQUESTS MADE TILL NOW: " + requests_made);
                                }

                                // Update the number of drivers requested for the session
                                var update_session = "UPDATE `tb_session` SET `requested_drivers`=`requested_drivers`+? WHERE `session_id`=?";
                                var values = [requests_made, session_id];
                                connection.query(update_session, values, function(err, result_update) {
                                    logging.logDatabaseQuery("Updating the requested_drivers for the session", err, result_update, null);
                                });
                            }); // get location address
                        }
                    });
                }


                function processAdhocBatchOfDrivers(data) {
                    logging.addEventToSession(data.session_id, {
                        event: "Processing the ad-hoc batch of drivers"
                    });

                    g_batch_processed[data.session_id.toString()] = constants.batchType.ADHOC;
                    // Send the requests to a ad-hoc set of drivers
                    fetchNewBatchOfDrivers(data, function(err, drivers) {
                        if (drivers.length == 0) {
                            logging.addEventToSession(session_id, {
                                event: "No ad-hoc drivers found within the pickup radius"
                            });

                            processDedicatedBatchOfDrivers(data);
                            //// deactivate the session if no new drivers are present
                            //timeoutSession(session_id);
                            //
                            //var message = "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.";
                            //var flag = 2;       // silent iOS notification
                            //var payload = {
                            // "log": "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.",
                            // "flag": constants.notificationFlags.NO_DRIVERS_AVAILABLE};
                            //utils.sendNotification(data.customer_id, message, flag, payload);
                        } else {
                            // Get the pickup address for the customer
                            // TODO simplify the logic to get the address from previous engagements if any later
                            utils.getLocationAddress(data.pickup_latitude, data.pickup_longitude, function(pickup_address) {
                                // Send requests to the new batch of drivers
                                var requests_made = 0;
                                for (var i = 0; i < drivers.length && i < constants.NUM_DRIVER_REQUESTS_BATCH; i++) {
                                    createNewEngagement(data, pickup_address, drivers[i], constants.batchType.ADHOC);
                                    requests_made++;
                                    console.log("REQUESTS MADE TILL NOW: " + requests_made);
                                }

                                // Update the number of drivers requested for the session
                                var update_session = "UPDATE `tb_session` SET `requested_drivers`=`requested_drivers`+? WHERE `session_id`=?";
                                var values = [requests_made, session_id];
                                connection.query(update_session, values, function(err, result_update) {
                                    logging.logDatabaseQuery("Updating the requested_drivers for the session", err, result_update, null);
                                });
                            }); // get location address
                        }
                    }); // fetch new drivers
                }


                function createManualEngagement(data) {
                    var numTotal = 3;
                    fetchDrivers(data, {
                            required: 0,
                            maximum: 0
                        } /* adhoc */ , {
                            required: numTotal,
                            maximum: numTotal
                        } /* dedicated */ ,
                        constants.PICKUP_RADIUS_MANUAL, 0 /* don't exclude */ ,
                        function(err, drivers) {
                            drivers = [];
                            if (drivers.length == 0) {
                                logging.addEventToSession(session_id, {
                                    event: "No dedicated drivers found within the manual pickup radius when creating a manual engagement"
                                });

                                // deactivate the session if no new drivers are present
                                timeoutSession(data.session_id);

                                var message = "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.";
                                var flag = 2; // silent iOS notification
                                var payload = {
                                    "log": "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.",
                                    "flag": constants.notificationFlags.NO_DRIVERS_AVAILABLE
                                };
                                utils.sendNotification(data.customer_id, message, flag, payload);

                                logging.addEventToSession(session_id, {
                                    event: "Sending notification to customer that the session has timed out",
                                    payload: payload
                                });
                            } else {
                                var index = 0;

                                (function tryCreatingManualEngagement() {
                                    var driverId = drivers[index].user_id;

                                    // Change the status of the driver to busy to check if the driver was not busy before creating the engagement
                                    var setDriverBusy = "UPDATE `tb_users` SET `status`=? WHERE `user_id`=?";
                                    connection.query(setDriverBusy, [constants.userFreeStatus.BUSY, driverId], function(err, results) {
                                        logging.logDatabaseQuery("Setting driver status to busy", err, results, null);

                                        if (results.affectedRows > 0) {
                                            // Add a manual engagement between the driver and the customer
                                            addAutomaticManualEngagement(data.session_id, data.customer_id, drivers[index].user_id, data.pickup_latitude, data.pickup_longitude, data.manualDestinationLatitude, data.manualDestinationLongitude, data.manualDestinationAddress, function(err, engagementId) {
                                                // Set timeout for 2 minutes for acknowledgement by the driver
                                                var informSupport = mailer.sendMailForManualEngageFollowup.bind(null, data.customer_id, drivers[index].user_id);
                                                var timeoutId = setTimeout(informSupport, 120000);
                                                // Add the timeoutId to the map to clear the timeout if the engagement if acknowledged by the driver
                                                g_maunal_engage_timeouts[engagementId.toString()] = timeoutId;
                                            });
                                            // Relay this to the customer support by mail
                                            mailer.sendMailForManualEngagementSuccess(data.customer_id, drivers[index].user_id);
                                        } else {
                                            index++;
                                            if (index < numTotal && index < drivers.length) {
                                                tryCreatingManualEngagement();
                                            } else {
                                                mailer.sendMailForManualEngagementFailure(data.customer_id);
                                            }
                                        }
                                    });
                                })();
                            }
                        });
                }


                function createNewEngagement(data, pickup_address, driver, batch_type) {

                    console.log("creating a new eng=======================")
                    console.log(data)
                    console.log(driver)
                    var date = new Date();

                    utils.generateSplitFareKey(function(err, splitFareKey) {
                        if (err) {
                            sendErrorResponse(res);
                            return;
                        }
                        var create_engagement_query =
                            "INSERT INTO `tb_engagements` " +
                            "(`user_id`,`driver_id`,`pickup_latitude`,`pickup_longitude`,`pickup_location_address`," +
                            "`driver_accept_latitude`, `driver_accept_longitude`,`status`, `current_time`, `session_id`, `request_made_on`,`manual_destination_latitude`,`manual_destination_longitude`,`manual_destination_address`,`split_fare_key`,`car_type`) " +
                            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        var values = [
                            data.customer_id, driver.user_id,
                            data.pickup_latitude, data.pickup_longitude, pickup_address,
                            driver.latitude, driver.longitude,
                            constants.engagementStatus.REQUESTED,
                            date, session_id, date, data.manualDestinationLatitude, data.manualDestinationLongitude, data.manualDestinationAddress, splitFareKey, data.car_type
                        ];
                        connection.query(create_engagement_query, values, function(err, result) {
                            logging.logDatabaseQuery("Creating a new engagement", err, result, null);

                            date = date.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                            var engagement_id = result.insertId;

                            // INSERT into split fare requests
                            var sql = "INSERT INTO `tb_split_fare_requests`(`split_fare_key`,`sent_by`,`sent_to`,`status`) VALUES(?,?,?,?)";
                            connection.query(sql, [splitFareKey, 0, data.customer_id, constants.splitFareRequestStatus.SPLITFARE_REQUEST_ACCEPTED], function(err, resultInsertSplitFareRequest) {
                                logging.logDatabaseQueryError("Add split fare request to DB", err, resultInsertSplitFareRequest);
                                if (err) {

                                    responses.errorResponse(res);
                                    return;
                                }

                                // Send notification to the driver with the pickup location
                                var message = "You have 1 new ride request";
                                var flag = 0;
                                var payload = {
                                    "engagement_id": engagement_id,
                                    "user_id": data.customer_id,
                                    "flag": constants.notificationFlags.REQUEST,
                                    "latitude": data.pickup_latitude,
                                    "longitude": data.pickup_longitude,
                                    "user_image": data.customer_image,
                                    "rating":data.customer_rating,
                                    "address": pickup_address,
                                    "start_time": date
                                };
                                utils.sendNotification(driver.user_id, message, flag, payload);

                                logging.addEventToSession(session_id, {
                                    event: "Creating a new engagement",
                                    driver: driver.user_id,
                                    payload: payload
                                });

                                // Add the driver to the list of drivers assigned for the session
                                if ((batch_type == constants.batchType.ADHOC && driver.reg_as == constants.userRegistrationStatus.DRIVER) || (batch_type == constants.batchType.DEDICATED && driver.reg_as == constants.userRegistrationStatus.DEDICATED_DRIVER)) {
                                    if (g_drivers_assigned.hasOwnProperty(session_id.toString())) {
                                        g_drivers_assigned[session_id.toString()].push(driver.user_id);
                                    } else {
                                        g_drivers_assigned[session_id.toString()] = [driver.user_id];
                                    }
                                }

                                // Add an entry to get the acknowledgement for the request made
                                var enter_timings = "INSERT INTO `tb_request_acks` " +
                                    "(`engagement_id`, `driver_lat`, `driver_long`, `request_made`) " +
                                    "VALUES (?, ?, ?, ?)";
                                var values = [engagement_id, driver.latitude, driver.longitude, date];
                                connection.query(enter_timings, values, function(err, result) {
                                    if (err) {
                                        logging.logDatabaseQuery("Adding timings for the engagement created", err, result);
                                    }
                                });

                            });
                        });
                    });
                }
            }
            // if the ride has been accepted, we clear the memory and no new ride requests are sent
            else if ((result_session[0].is_active == constants.sessionStatus.ACTIVE && result_session[0].ride_acceptance_flag) || result_session[0].is_active == constants.sessionStatus.TIMED_OUT) {
                // Cleaning the drivers memory for the current session
                if (g_drivers_assigned.hasOwnProperty(session_id.toString())) {
                    delete g_drivers_assigned[session_id.toString()];
                }
                if (g_batch_processed.hasOwnProperty(session_id.toString())) {
                    delete g_batch_processed[session_id.toString()];
                }
            }
            console.log("NUMBER OF TRIES MADE FOR SESSION ID " + session_id + " ARE " + num_tries);

            //logging.flushSession(session_id);
        }
    }); // get the session info
}

// Deactivate the session
function deactivateSession(session_id) {
    logging.addEventToSession(session_id, {
        event: "Changing the status of the session to inactive"
    });

    var deactivate_session = "UPDATE `tb_session` SET `is_active`=? WHERE `session_id`=?";
    var values = [constants.sessionStatus.INACTIVE, session_id];
    connection.query(deactivate_session, values, function(err, result) {
        logging.logDatabaseQuery("Deactivating the session", err, result, null);
    });

    // Wait for 1 minute before flushing the logs to the file and deleting the session
    setTimeout(logging.flushSession.bind(null, session_id), 20000);
    setTimeout(logging.deleteSession.bind(null, session_id), 20000);
}

// Timeout the session
function timeoutSession(session_id) {
    logging.addEventToSession(session_id, {
        event: "Changing the status of the session to timed out"
    });

    var timeout_session = "UPDATE `tb_session` SET `is_active`=? WHERE `session_id`=?";
    var values = [constants.sessionStatus.TIMED_OUT, session_id];
    connection.query(timeout_session, values, function(err, result) {
        logging.logDatabaseQuery("the session has timed out", err, result, null);
    });

    // Wait for 1 minute before flushing the logs to the file and deleting the session
    setTimeout(logging.flushSession.bind(null, session_id), 20000);
    setTimeout(logging.deleteSession.bind(null, session_id), 20000);
}

// Cancel the session
function cancelSession(session_id) {
    logging.addEventToSession(session_id, {
        event: "Changing the status of the session to inactive on being cancelled by the user"
    });

    var timeout_session = "UPDATE `tb_session` SET `is_active`=?, `cancelled_by_user`=? WHERE `session_id`=?";
    var values = [constants.sessionStatus.TIMED_OUT, 1, session_id];
    connection.query(timeout_session, values, function(err, result) {
        logging.logDatabaseQuery("the session has been cancelled by the customer", err, result, null);
    });

    // Wait for 1 minute before flushing the logs to the file and deleting the session
    setTimeout(logging.flushSession.bind(null, session_id), 20000);
    setTimeout(logging.deleteSession.bind(null, session_id), 20000);
}

function fetchDrivers(sessionData, numAdhoc, numDedicated, pickupDistance, excludeFlag, callback) {
    logging.addEventToSession(sessionData.session_id, {
        event: "Fetching a new batch of drivers",
        "ad-hoc": numAdhoc,
        dedicated: numDedicated
    });

    if (sessionData.is_blocked !== 0) {
        console.log("ROGUE!! ROGUE!! ROGUE!! ROGUE!! ROGUE!! ROGUE!! " + " ::::: " + sessionData.customer_id);
        callback(null, []);
    }

    var fetchAllFreeDrivers =
        "SELECT * " +
        "FROM" +
        "(" +
        "SELECT * FROM" +
        "(SELECT `user_name`,`phone_no`,`user_image`,`driver_car_image`,`current_location_latitude` as latitude, `current_location_longitude` as longitude," +
        "`user_id`,`total_rating_got_driver`,`total_rating_driver`, `status`, `reg_as` " +
        "FROM `tb_users` " +
        "WHERE `current_user_status`=? && `status`=? && `reg_as` IN (?, ?) && `user_id`!=? && `is_available` = 1 && `car_type` = ?) AS `drivers` " +
        "JOIN" +
        "(SELECT `driver_id`, `latitude` as home_latitude, `longitude` as home_longitude FROM `tb_home_locations`) AS `home_locations`" +
        "WHERE drivers.user_id = home_locations.driver_id) " +
        "AS `drivers_with_locations`" +
        "JOIN " +
        "`tb_timings` " +
        "WHERE drivers_with_locations.user_id = tb_timings.driver_id && TIME(NOW()) > tb_timings.start_time && TIME(NOW()) < tb_timings.end_time";
    var values = [
        constants.userCurrentStatus.DRIVER_ONLINE,
        constants.userFreeStatus.FREE,
        constants.userRegistrationStatus.DRIVER, constants.userRegistrationStatus.DEDICATED_DRIVER,
        sessionData.customer_id, sessionData.car_type
    ];

    connection.query(fetchAllFreeDrivers, values, function(err, allDrivers) {
        logging.logDatabaseQuery("Getting all the drivers using parameters", err, allDrivers);
        if (err) {
            //logging.logDatabaseQuery("Getting all the drivers using parameters", err, allDrivers);
            return callback(err, []);
        }

        // calculate the distance for all the drivers
        for (var i = 0; i < allDrivers.length; i++) {
            if (isDriverAtHome(allDrivers[i].home_latitude, allDrivers[i].home_longitude, allDrivers[i].latitude, allDrivers[i].longitude)) {
                //console.log(allDrivers[i].user_name + " is at home right now");
            } else {
                var driver_id = allDrivers[i].user_id;
                var distance = utils.calculateDistance(sessionData.pickup_latitude, sessionData.pickup_longitude, allDrivers[i].latitude, allDrivers[i].longitude);
                allDrivers[i].distance = distance;
                allDrivers[i].rating = allDrivers[i].total_rating_driver / allDrivers[i].total_rating_got_driver;
                delete allDrivers[i].total_rating_got_driver;
                delete allDrivers[i].total_rating_driver;
            }
        }

        allDrivers = utils.sortByKeyAsc(allDrivers, "distance");

        var dedicatedDrivers = [];
        var adhocDrivers = [];

        var excludeDriver = false;
        for (var i = 0; i < allDrivers.length; i++) {
            excludeDriver = excludeFlag && alreadyPicked(sessionData.session_id, allDrivers[i].user_id);
            if (allDrivers[i].reg_as == constants.userRegistrationStatus.DEDICATED_DRIVER &&
                allDrivers[i].distance < pickupDistance && !excludeDriver) {
                dedicatedDrivers.push(allDrivers[i]);
            }
        }

        for (var i = 0; i < allDrivers.length; i++) {
            excludeDriver = excludeFlag && alreadyPicked(sessionData.session_id, allDrivers[i].user_id);
            if (allDrivers[i].reg_as == constants.userRegistrationStatus.DRIVER &&
                allDrivers[i].distance < pickupDistance && !excludeDriver) {
                adhocDrivers.push(allDrivers[i]);
            }
        }

        logging.addEventToSession(sessionData.session_id, {
            values: "ad-hoc drivers within the given radius of " + pickupDistance,
            driver: adhocDrivers
        });
        logging.addEventToSession(sessionData.session_id, {
            values: "dedicated drivers within the given radius of " + pickupDistance,
            driver: dedicatedDrivers
        });

        var emptyDedicated = numDedicated.required - dedicatedDrivers.length;
        emptyDedicated = emptyDedicated < 0 ? 0 : emptyDedicated;
        var emptyAdhoc = numAdhoc.required - adhocDrivers.length;
        emptyAdhoc = emptyAdhoc < 0 ? 0 : emptyAdhoc;

        var keepDedicated = numDedicated.required + emptyAdhoc;
        keepDedicated = keepDedicated < numDedicated.maximium ? keepDedicated : numDedicated.maximium;
        console.log("keepDedicated: " + keepDedicated);
        var keepAdhoc = numAdhoc.required + emptyDedicated;
        keepAdhoc = keepAdhoc < numAdhoc.maximium ? keepAdhoc : numAdhoc.maximium;
        console.log("keepAdhoc: " + keepAdhoc);

        dedicatedDrivers = dedicatedDrivers.slice(0, keepDedicated);
        adhocDrivers = adhocDrivers.slice(0, keepAdhoc);

        logging.addEventToSession(sessionData.session_id, {
            values: "truncated ad-hoc drivers within the proximity radius",
            driver: adhocDrivers
        });
        logging.addEventToSession(sessionData.session_id, {
            values: "truncated dedicated drivers within the proximity radius",
            driver: dedicatedDrivers
        });

        drivers = dedicatedDrivers.concat(adhocDrivers);
        drivers = utils.sortByKeyAsc(drivers, "distance");

        logging.addEventToSession(sessionData.session_id, {
            values: "All drivers being returned within the proximity radius",
            driver: drivers
        });

        callback(err, drivers);
    });
}

function fetchNearestDriver(data, callback) {
    logging.addEventToSession(data.session_id, {
        event: "Fetching the nearest batch of drivers"
    });

    if (data.is_blocked !== 0) {
        console.log("ROGUE!! ROGUE!! ROGUE!! ROGUE!! ROGUE!! ROGUE!! " + " ::::: " + data.customer_id);
        return [];
    }

    var fetch_drivers_query =
        "SELECT * " +
        "FROM" +
        "(" +
        "SELECT * FROM" +
        "(SELECT `user_name`,`phone_no`,`user_image`,`driver_car_image`,`current_location_latitude` as latitude, `current_location_longitude` as longitude," +
        "`user_id`,`total_rating_got_driver`,`total_rating_driver`, `status`, `reg_as` " +
        "FROM `tb_users` " +
        "WHERE `current_user_status`=? && `status`=? && `reg_as` IN (?, ?) && `user_id`!=? && `is_available` = 1 && car_type = ?) AS `drivers` " +
        "JOIN " +
        "(SELECT `driver_id`, `latitude` as home_latitude, `longitude` as home_longitude FROM `tb_home_locations`) AS `home_locations`" +
        "WHERE drivers.user_id = home_locations.driver_id) " +
        "AS `drivers_with_locations`" +
        "JOIN " +
        "`tb_timings` " +
        "WHERE drivers_with_locations.user_id = tb_timings.driver_id && TIME(NOW()) > tb_timings.start_time && TIME(NOW()) < tb_timings.end_time  group by drivers_with_locations.user_id ";
    var values = [
        constants.userCurrentStatus.DRIVER_ONLINE,
        constants.userFreeStatus.FREE,
        constants.userRegistrationStatus.DRIVER, constants.userRegistrationStatus.DEDICATED_DRIVER,
        data.customer_id, data.car_type
    ];

    connection.query(fetch_drivers_query, values, function(err, result_drivers) {
        logging.logDatabaseQuery("Fetching all free drivers", err, result_drivers, null);

        if (err) {
            callback(err, null);
        } else {
            var probeDrivers = function(drivers) {
                var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED];
                var get_engaged_drivers = "SELECT `driver_id` FROM `tb_engagements` WHERE `status` IN (" + status.toString() + ")";
                connection.query(get_engaged_drivers, [], function(err, engaged_drivers) {
                    logging.logDatabaseQuery("Getting the list of all engaged drivers", err, engaged_drivers, null);

                    var free_drivers = [];
                    for (var i = 0; i < drivers.length; i++) {
                        free_drivers.push(drivers[i].user_id);
                    }

                    for (var i = 0; i < free_drivers.length; i++) {
                        if (engaged_drivers.indexOf(free_drivers[i]) >= 0) {
                            console.log("ERROR! ERROR! ERROR! " + "Driver: " + free_drivers[i] + " is already in " + engaged_drivers);
                        }
                    }
                });
            };
            probeDrivers(result_drivers);

            var drivers = [];
            var dedicated_drivers = [];
            var adhoc_drivers = [];

            // calculate the distance for all the drivers
            for (var i = 0; i < result_drivers.length; i++) {
                if (isDriverAtHome(result_drivers[i].home_latitude, result_drivers[i].home_longitude, result_drivers[i].latitude, result_drivers[i].longitude)) {
                    //console.log(result_drivers[i].user_name + " is at home right now");
                } else {
                    var driver_id = result_drivers[i].user_id;
                    var distance = utils.calculateDistance(data.pickup_latitude, data.pickup_longitude, result_drivers[i].latitude, result_drivers[i].longitude);
                    result_drivers[i].distance = distance;
                    result_drivers[i].rating = result_drivers[i].total_rating_driver / result_drivers[i].total_rating_got_driver;
                    delete result_drivers[i].total_rating_got_driver;
                    delete result_drivers[i].total_rating_driver;
                }
            }

            result_drivers = utils.sortByKeyAsc(result_drivers, "distance");

            for (var i = 0; i < result_drivers.length; i++) {
                if (result_drivers[i].reg_as == constants.userRegistrationStatus.DEDICATED_DRIVER &&
                    result_drivers[i].distance < constants.PROXIMITY_RADIUS && !alreadyPicked(data.session_id, result_drivers[i].user_id)) {
                    dedicated_drivers.push(result_drivers[i]);
                }
            }

            for (var i = 0; i < result_drivers.length; i++) {
                if (result_drivers[i].reg_as == constants.userRegistrationStatus.DRIVER &&
                    result_drivers[i].distance < constants.PROXIMITY_RADIUS && !alreadyPicked(data.session_id, result_drivers[i].user_id)) {
                    adhoc_drivers.push(result_drivers[i]);
                }
            }

            logging.addEventToSession(data.session_id, {
                values: "ad-hoc drivers within the proximity radius",
                driver: adhoc_drivers
            });
            logging.addEventToSession(data.session_id, {
                values: "ad-hoc drivers within the proximity radius",
                driver: dedicated_drivers
            });

            var num_dedicated = dedicated_drivers.length < constants.NUM_DEDICATED_FIRST_BATCH ? dedicated_drivers.length : constants.NUM_DEDICATED_FIRST_BATCH;
            var num_remaining = constants.NUM_DRIVER_REQUESTS_BATCH - num_dedicated;
            var num_adhoc = adhoc_drivers.length < num_remaining ? adhoc_drivers.length : num_remaining;
            num_remaining = constants.NUM_DRIVER_REQUESTS_BATCH - num_dedicated - num_adhoc;
            num_dedicated += num_dedicated + num_remaining < dedicated_drivers.length ? num_remaining : dedicated_drivers.length - num_dedicated;

            dedicated_drivers = dedicated_drivers.slice(0, num_dedicated);
            adhoc_drivers = adhoc_drivers.slice(0, num_adhoc);

            logging.addEventToSession(data.session_id, {
                values: "truncated ad-hoc drivers within the proximity radius",
                driver: adhoc_drivers
            });
            logging.addEventToSession(data.session_id, {
                values: "truncated dedicated drivers within the proximity radius",
                driver: dedicated_drivers
            });

            drivers = dedicated_drivers.concat(adhoc_drivers);
            drivers = utils.sortByKeyAsc(drivers, "distance");

            logging.addEventToSession(data.session_id, {
                values: "All drivers being returned within the proximity radius",
                driver: drivers
            });

            callback(err, drivers);
        }
    });
}

// Helper function
// Get a new batch of drivers for the given session ID within the pickup radius
function fetchNewBatchOfDrivers(data, callback) {
    logging.addEventToSession(data.session_id, {
        event: "Fetching the nearby ad-hoc + dedicated batch of drivers"
    });

    if (data.is_blocked !== 0) {
        console.log("ROGUE!! ROGUE!! ROGUE!! ROGUE!! ROGUE!! ROGUE!! " + " ::::: " + data.customer_id);
        return [];
    }

    var fetch_drivers_query = "SELECT * " +
        "FROM" +
        "(" +
        "SELECT * FROM" +
        "(SELECT `user_name`,`phone_no`,`user_image`,`driver_car_image`,`current_location_latitude` as latitude, `current_location_longitude` as longitude," +
        "`user_id`,`total_rating_got_driver`,`total_rating_driver`, `status`, `reg_as` " +
        "FROM `tb_users` " +
        "WHERE `current_user_status`=? && `status`=? && `reg_as` IN (?, ?) && `user_id`!=? && `is_available` = 1 && `car_type` = ?) AS `drivers` " +
        "JOIN" +
        "(SELECT `driver_id`, `latitude` as home_latitude, `longitude` as home_longitude FROM `tb_home_locations`) AS `home_locations`" +
        "WHERE drivers.user_id = home_locations.driver_id) " +
        "AS `drivers_with_locations`" +
        "JOIN " +
        "`tb_timings` " +
        "WHERE drivers_with_locations.user_id = tb_timings.driver_id && TIME(NOW()) > tb_timings.start_time && TIME(NOW()) < tb_timings.end_time";
    //var fetch_drivers_query = "SELECT * " +
    //    "FROM" +
    //        "(SELECT `user_name`,`phone_no`,`user_image`,`driver_car_image`,`current_location_latitude` as latitude, `current_location_longitude` as longitude," +
    //        "`user_id`,`total_rating_got_driver`,`total_rating_driver`, `status`, `reg_as` " +
    //        "FROM `tb_users` " +
    //        "WHERE `current_user_status`=? && `status`=? && `reg_as` IN (?, ?) && `user_id`!=?) AS `drivers` " +
    //    "JOIN " +
    //        "`tb_timings` as `timings` " +
    //    "WHERE drivers.user_id = timings.driver_id && TIME(NOW()) > timings.start_time && TIME(NOW()) < timings.end_time";
    var values = [
        constants.userCurrentStatus.DRIVER_ONLINE,
        constants.userFreeStatus.FREE,
        constants.userRegistrationStatus.DRIVER, constants.userRegistrationStatus.DEDICATED_DRIVER,
        data.customer_id, data.car_type
    ];

    connection.query(fetch_drivers_query, values, function(err, result_drivers) {
        //logging.logDatabaseQuery("Fetching all free drivers", err, result_drivers, null);

        if (err) {
            callback(err, null);
        } else {
            var probeDrivers = function(drivers) {
                var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED];
                var get_engaged_drivers = "SELECT `driver_id` FROM `tb_engagements` WHERE `status` IN (" + status.toString() + ")";
                connection.query(get_engaged_drivers, [], function(err, engaged_drivers) {
                    logging.logDatabaseQuery("Getting the list of all engaged drivers", err, engaged_drivers, null);

                    var free_drivers = [];
                    for (var i = 0; i < drivers.length; i++) {
                        free_drivers.push(drivers[i].user_id);
                    }

                    for (var i = 0; i < free_drivers.length; i++) {
                        if (engaged_drivers.indexOf(free_drivers[i]) >= 0) {
                            console.log("ERROR! ERROR! ERROR! " + "Driver: " + free_drivers[i] + " is already in " + engaged_drivers);
                        }
                    }
                });
            };
            probeDrivers(result_drivers);

            var drivers = [];
            var dedicated_drivers = [];
            var adhoc_drivers = [];

            // calculate the distance for all the drivers
            for (var i = 0; i < result_drivers.length; i++) {
                if (isDriverAtHome(result_drivers[i].home_latitude, result_drivers[i].home_longitude, result_drivers[i].latitude, result_drivers[i].longitude)) {
                    console.log(result_drivers[i].user_name + " is at home right now");
                } else {
                    var driver_id = result_drivers[i].user_id;
                    var distance = utils.calculateDistance(data.pickup_latitude, data.pickup_longitude, result_drivers[i].latitude, result_drivers[i].longitude);
                    result_drivers[i].distance = distance;
                    result_drivers[i].rating = result_drivers[i].total_rating_driver / result_drivers[i].total_rating_got_driver;
                    delete result_drivers[i].total_rating_got_driver;
                    delete result_drivers[i].total_rating_driver;
                }
            }

            result_drivers = utils.sortByKeyAsc(result_drivers, "distance");

            for (var i = 0; i < result_drivers.length; i++) {
                if (result_drivers[i].reg_as == constants.userRegistrationStatus.DEDICATED_DRIVER &&
                    result_drivers[i].distance < constants.PICKUP_RADIUS && !alreadyPicked(data.session_id, result_drivers[i].user_id)) {
                    dedicated_drivers.push(result_drivers[i]);
                }
            }

            for (var i = 0; i < result_drivers.length; i++) {
                if (result_drivers[i].reg_as == constants.userRegistrationStatus.DRIVER &&
                    result_drivers[i].distance < constants.PICKUP_RADIUS && !alreadyPicked(data.session_id, result_drivers[i].user_id)) {
                    adhoc_drivers.push(result_drivers[i]);
                }
            }

            logging.addEventToSession(data.session_id, {
                values: "ad-hoc drivers within the pickup radius",
                driver: adhoc_drivers
            });
            logging.addEventToSession(data.session_id, {
                values: "ad-hoc drivers within the pickup radius",
                driver: dedicated_drivers
            });

            var num_dedicated = dedicated_drivers.length < constants.NUM_DEDICATED_FIRST_BATCH ? dedicated_drivers.length : constants.NUM_DEDICATED_FIRST_BATCH;
            var num_remaining = constants.NUM_DRIVER_REQUESTS_BATCH - num_dedicated;
            var num_adhoc = adhoc_drivers.length < num_remaining ? adhoc_drivers.length : num_remaining;
            num_remaining = constants.NUM_DRIVER_REQUESTS_BATCH - num_dedicated - num_adhoc;
            num_dedicated += num_dedicated + num_remaining < dedicated_drivers.length ? num_remaining : dedicated_drivers.length - num_dedicated;

            dedicated_drivers = dedicated_drivers.slice(0, num_dedicated);
            adhoc_drivers = adhoc_drivers.slice(0, num_adhoc);

            logging.addEventToSession(data.session_id, {
                values: "truncated ad-hoc drivers within the pickup radius",
                driver: adhoc_drivers
            });
            logging.addEventToSession(data.session_id, {
                values: "truncated dedicated drivers within the pickup radius",
                driver: dedicated_drivers
            });

            drivers = dedicated_drivers.concat(adhoc_drivers);
            drivers = utils.sortByKeyAsc(drivers, "distance");

            logging.addEventToSession(data.session_id, {
                values: "All drivers being returned within the pickup radius",
                driver: drivers
            });

            callback(err, drivers);
        }
    });
}


// Helper function
// Get a new batch of drivers for the given session ID within the pickup radius
function fetchDedicatedDrivers(data, callback) {
    logging.addEventToSession(data.session_id, {
        event: "Fetching the near-by dedicated batch of drivers"
    });

    if (data.is_blocked !== 0) {
        console.log("ROGUE!! ROGUE!! ROGUE!! ROGUE!! ROGUE!! ROGUE!! " + " ::::: " + data.customer_id);
        return [];
    }

    var fetch_drivers_query = "SELECT * " +
        "FROM" +
        "(" +
        "SELECT * FROM" +
        "(SELECT `user_name`,`phone_no`,`user_image`,`driver_car_image`,`current_location_latitude` as latitude, `current_location_longitude` as longitude," +
        "`user_id`,`total_rating_got_driver`,`total_rating_driver`, `status`, `reg_as` " +
        "FROM `tb_users` " +
        "WHERE `current_user_status`=? && `status`=? && `reg_as`= ? && `user_id`!=? && `is_available` = 1 && `car_type` = ?) AS `drivers` " +
        "JOIN" +
        "(SELECT `driver_id`, `latitude` as home_latitude, `longitude` as home_longitude FROM `tb_home_locations`) AS `home_locations`" +
        "WHERE drivers.user_id = home_locations.driver_id) " +
        "AS `drivers_with_locations`" +
        "JOIN " +
        "`tb_timings` " +
        "WHERE drivers_with_locations.user_id = tb_timings.driver_id && TIME(NOW()) > tb_timings.start_time && TIME(NOW()) < tb_timings.end_time";
    var values = [
        constants.userCurrentStatus.DRIVER_ONLINE,
        constants.userFreeStatus.FREE,
        constants.userRegistrationStatus.DEDICATED_DRIVER,
        data.customer_id, data.car_type
    ];
    connection.query(fetch_drivers_query, values, function(err, result_drivers) {
        logging.logDatabaseQuery("Fetching dedicated drivers", err, result_drivers, null);
        if (err) {
            callback(err, null);
        } else {
            var probeDrivers = function(drivers) {
                var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED];
                var get_engaged_drivers = "SELECT `driver_id` FROM `tb_engagements` WHERE `status` IN (" + status.toString() + ")";
                connection.query(get_engaged_drivers, [], function(err, engaged_drivers) {
                    logging.logDatabaseQuery("Getting the list of all engaged drivers", err, engaged_drivers, null);

                    var free_drivers = [];
                    for (var i = 0; i < drivers.length; i++) {
                        free_drivers.push(drivers[i].user_id);
                    }

                    for (var i = 0; i < free_drivers.length; i++) {
                        if (engaged_drivers.indexOf(free_drivers[i]) >= 0) {
                            console.log("ERROR! ERROR! ERROR! " + "Driver: " + free_drivers[i] + " is already in " + engaged_drivers);
                        }
                    }
                });
            };
            probeDrivers(result_drivers);

            var drivers = [];
            for (var i = 0; i < result_drivers.length; i++) {
                if (isDriverAtHome(result_drivers[i].home_latitude, result_drivers[i].home_longitude, result_drivers[i].latitude, result_drivers[i].longitude)) {
                    console.log(result_drivers[i].user_name + " is at home right now");
                } else {
                    var driver_id = result_drivers[i].user_id;
                    var distance = utils.calculateDistance(data.pickup_latitude, data.pickup_longitude, result_drivers[i].latitude, result_drivers[i].longitude);
                    // Pick new drivers other than me in the constants.PICKUP_RADIUS distance
                    if (distance < constants.DEDICATED_PICKUP_RADIUS && driver_id != data.customer_id && !alreadyPicked(data.session_id, driver_id)) {
                        result_drivers[i].distance = distance;
                        result_drivers[i].rating = result_drivers[i].total_rating_driver / result_drivers[i].total_rating_got_driver;
                        delete result_drivers[i].total_rating_got_driver;
                        delete result_drivers[i].total_rating_driver;

                        drivers.push(result_drivers[i]);
                    }
                }
            }
            drivers = utils.sortByKeyAsc(drivers, "distance");

            logging.addEventToSession(data.session_id, {
                values: "All dedicated drivers being returned within the pickup radius",
                driver: drivers
            });

            callback(err, drivers);
        }
    });
}


// Helper function
// Check if a request has already been sent to the driver for the current session
function alreadyPicked(session_id, driver_id) {
    var exists = false;

    if (g_drivers_assigned.hasOwnProperty(session_id.toString())) {
        console.log("Drivers assigned till now: " + g_drivers_assigned[session_id.toString()]);
        if (g_drivers_assigned[session_id.toString()].indexOf(driver_id) >= 0) {
            exists = true;
        }
    }
    return exists;
}



function acknowledgeRequest(req, res) {
    logging.startSection("acknowledge_request");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var engagementId = req.body.engagement_id;
    var timestamp = req.body.ack_timestamp;
    var network = req.body.network_name;
    if (typeof network === 'undefined') {
        network = 'Not sent';
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }

        // TODO this code is there till the data being gathered doesn't move to tb_engagements
        var updateTimings =
            "UPDATE `tb_request_acks` " +
            "SET `request_received` = ?, `ack_received` = NOW(), `network` = ? " +
            "WHERE `engagement_id` = ?";
        var values = [timestamp, network, engagementId];
        connection.query(updateTimings, values, function(err, result) {
            logging.logDatabaseQuery("Added the timestamp for acknowledgement for engagement " + engagementId, err, result);
        });

        var updateTimings =
            'UPDATE `tb_engagements` ' +
            'SET `request_received_on` = ?, `network_driver` = ? ' +
            'WHERE `engagement_id` = ?';
        connection.query(updateTimings, [timestamp, network, engagementId], function(err, result) {
            logging.logDatabaseQuery("Added the timestamp for acknowledgement for engagement " + engagementId, err, result);

            if (err) {
                responses.errorResponse(res);
                return;
            }

            var response = {
                flag: constants.responseFlags.ACK_RECEIVED,
                log: 'Ack received'
            };
            res.send(response);
        });
    });
};

exports.acknowledge_request = acknowledgeRequest;



// API
// The driver accepts the ride
// LOGIC
//  Check if the session is still active
//  If Active
//      Check if some other driver has already accepted the request
//      If not
//          Send acknowledgement to the driver
//          Get the list of drivers to which the request has been sent for  this batch
//          Send the "Ride accepted by other driver" for these drivers
//      Else
//          Send the "Ride accepted by other driver" for this driver
//  Else
//      Send "Request timed out" to the driver
exports.accept_a_request = function(req, res) {
    logging.startSection("accept_a_request");
    logging.logRequest(req);

    var access_token = req.body.access_token;
    var customer_id = req.body.customer_id;
    var engagement_id = req.body.engagement_id;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;

    var manvalues = [access_token, customer_id, engagement_id];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    }

    utils.authenticateUser(access_token, function(result) {
        if (result == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }

        var driver_id = result[0].user_id;

        // Get the session_id for the engagement
        var get_session = "SELECT `session_id`,`status` FROM `tb_engagements` WHERE `engagement_id`=?";
        connection.query(get_session, [engagement_id], function(err, result_session) {
            logging.logDatabaseQuery("Getting session for the engagement", err, result_session, null);
            if (err) {
                responses.errorResponse(res);
                return;
            }

            // Get the status and ride acceptance flag for the session
            var session_id = result_session[0].session_id;

            logging.addEventToSession(session_id, {
                event: "A driver is trying to accepting the request",
                engagement: engagement_id,
                driver: driver_id
            });

            var get_session_info = "SELECT `is_active`, `ride_acceptance_flag`, `pickup_id` FROM `tb_session` WHERE `session_id`=?";
            connection.query(get_session_info, [session_id], function(err, result_session_info) {
                logging.logDatabaseQuery("Getting the status and ride acceptance flag for the session", err, result_session_info, null);

                logging.addEventToSession(session_id, {
                    event: "Getting the information of the session",
                    session: result_session_info[0]
                });

                if (err) {
                    responses.errorResponse(res);
                    return;
                }

                var is_active = result_session_info[0].is_active;
                var ride_acceptance_flag = result_session_info[0].ride_acceptance_flag;

                if (is_active == constants.sessionStatus.TIMED_OUT) {

                    if (result_session[0].status == constants.engagementStatus.CANCELLED_BY_CUSTOMER) {
                        var response = {
                            "log": 'The user has already cancelled the request',
                            "flag": constants.responseFlags.REQUEST_CANCELLED
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));

                        logging.addEventToSession(session_id, {
                            event: "The user has already cancelled the request and can't be accepted",
                            response: response
                        });
                        return;
                    }
                    // This request session has already timed-out
                    var response = {
                        "log": 'Session already timed out',
                        "flag": constants.responseFlags.SESSION_TIMEOUT
                    };
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));

                    logging.addEventToSession(session_id, {
                        event: "The session has already timed out",
                        response: response
                    });
                } else if (is_active == constants.sessionStatus.INACTIVE) {
                    var response = {
                        "log": 'The user has already cancelled the request',
                        "flag": constants.responseFlags.REQUEST_CANCELLED
                    };
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));

                    logging.addEventToSession(session_id, {
                        event: "The user has already cancelled the request and can't be accepted",
                        response: response
                    });
                } else if (is_active == constants.sessionStatus.ACTIVE && ride_acceptance_flag == constants.rideAcceptanceFlag.ACCEPTED) {
                    // Handling duplicate calls to the API
                    // Check if the ride has been accepted by this driver only
                    logging.addEventToSession(session_id, {
                        event: "The request has already been accepted by some driver, verifying duplicate request by the driver"
                    });

                    var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED];
                    var get_driver_for_ride = "SELECT `driver_id` FROM `tb_engagements` WHERE `session_id`=? && `status` in (" + status.toString() + ")";
                    connection.query(get_driver_for_ride, [session_id], function(err, result_driver_info) {
                        logging.logDatabaseQuery("Checking driver for the engagement", err, result_driver_info, null);

                        if (err) {
                            sendErrorResponse(res);
                        } else {
                            var existing_driver_id = result_driver_info[0].driver_id;

                            logging.addEventToSession(session_id, {
                                event: "Getting the driver that accepted the request",
                                driver: existing_driver_id,
                                this_driver: driver_id
                            });

                            if (driver_id == existing_driver_id) {
                                // Send the same payload for accepting the ride as before
                                // TODO think of cases where the engagement_id maybe needed for distinguishing various engagements
                                logging.addEventToSession(session_id, {
                                    event: "Handling duplicate request for accepting the ride"
                                });

                                sendRideAcceptanceResponseToDriver(session_id, customer_id, res);
                            } else {
                                // The ride has already been accepted by other driver
                                var response = {
                                    "log": 'Ride already accepted by other driver',
                                    "flag": constants.responseFlags.RIDE_ACCEPTED_BY_OTHER_DRIVER
                                };
                                logging.logResponse(response);
                                res.send(JSON.stringify(response));

                                logging.addEventToSession(session_id, {
                                    event: "The request was accepted by some other driver",
                                    response: response
                                });
                            }
                        }
                    });
                } else if (is_active == constants.sessionStatus.ACTIVE && ride_acceptance_flag == constants.rideAcceptanceFlag.NOT_YET_ACCEPTED) {
                    // Although node.js executes code on single thread, still taking care of race
                    // conditions in setting the ride_acceptance_flag
                    // Update the flag only if the ride has not been accepted yet

                    logging.addEventToSession(session_id, {
                        event: "The session is active and not yet accepted, we will try to accept the session for the driver"
                    });

                    // because of changes in driver allocation logic (automatic manual engagement creation)
                    // and hence, when the driver calls accept_a_request, he may already be busy and the logic needs to be
                    // changed to check for the status of the driver before accepting the request.

                    // Set the status of the driver to busy
                    var set_driver_busy_query = "UPDATE `tb_users` SET `status`=? WHERE `user_id`=?";
                    connection.query(set_driver_busy_query, [constants.userFreeStatus.BUSY, driver_id], function(err, result_update_status) {
                        logging.logDatabaseQuery("Setting driver status to busy", err, result_update_status, null);

                        if (result_update_status.affectedRows > 0) {
                            logging.addEventToSession(session_id, {
                                event: "The status of the driver has been set to busy",
                                driver: driver_id
                            });

                            var set_acceptance_flag =
                                "UPDATE " +
                                "`tb_session` session " +
                                "JOIN " +
                                "`tb_engagements` engagement " +
                                "ON session.session_id = engagement.session_id " +
                                "SET session.ride_acceptance_flag = ?, engagement.status = ?, engagement.accept_time = NOW(), " +
                                "engagement.driver_accept_latitude = ?, engagement.driver_accept_longitude = ? " +
                                "WHERE session.ride_acceptance_flag = ? && engagement.status = ? && engagement.engagement_id = ?";
                            values = [constants.rideAcceptanceFlag.ACCEPTED, constants.engagementStatus.ACCEPTED,
                                latitude, longitude,
                                constants.rideAcceptanceFlag.NOT_YET_ACCEPTED, constants.engagementStatus.REQUESTED, engagement_id
                            ];
                            connection.query(set_acceptance_flag, values, function(err, result_acceptance_update) {
                                logging.logDatabaseQuery("Changing the ride acceptance flag for the ride", err, result_acceptance_update, null);

                                // Check if the update happened
                                if (result_acceptance_update.affectedRows > 0) {
                                    logging.addEventToSession(session_id, {
                                        event: "The driver has accepted the request",
                                        driver: driver_id
                                    });

                                    // Update the accepting distance for this notification
                                    updateAcceptDistance(engagement_id);

                                    // Update the accepting distance from the last drop location for the driver
                                    updateLastDropToAcceptLocationDistance(session_id, engagement_id, driver_id, latitude, longitude);

                                    // Send notification to the customer
                                    sendRideAcceptanceNotificationToCustomer(customer_id, driver_id, session_id, engagement_id);

                                    // Update the status for other engagements for this session and send notifications to drivers
                                    updateEngagementsAndSendNotificationsToOtherDrivers(session_id, engagement_id, driver_id);

                                    // Update the status for other engagements for the driver
                                    updateOtherEngagementsForDriver(session_id, engagement_id, driver_id);

                                    // Send RESPONSE to the driver
                                    sendRideAcceptanceResponseToDriver(session_id, customer_id, res);

                                    // Send mail to the support team
                                    mailer.sendMailForAcceptedRide(customer_id, driver_id);

                                    // Mark the driver as leaving the station assigned to him
                                    // Put the reason as accepted a request
                                    stationing.markDriverAsLeaving(driver_id, 'Accepted a request');
                                } else {
                                    logging.addEventToSession(session_id, {
                                        event: "The request was already by some other driver and can't update the session (RACE CONDITION HANDLED)",
                                        driver: driver_id
                                    });

                                    // The ride has already been accepted by other driver
                                    var response = {
                                        "log": 'Ride already accepted by other driver',
                                        "flag": constants.responseFlags.RIDE_ACCEPTED_BY_OTHER_DRIVER
                                    };
                                    logging.logResponse(response);
                                    res.send(JSON.stringify(response));

                                    // Set the status of the driver to free
                                    var set_driver_free_query = "UPDATE `tb_users` SET `status`=? WHERE `user_id`=?";
                                    connection.query(set_driver_free_query, [constants.userFreeStatus.FREE, driver_id], function(err, result) {
                                        logging.logDatabaseQuery("Setting driver status to free", err, result, null);

                                        logging.addEventToSession(session_id, {
                                            event: "The engagement was already accepted by some other driver, changing the status of the driver to free.",
                                            driver: driver_id
                                        });
                                    });
                                }
                            });
                        } else {
                            logging.addEventToSession(session_id, {
                                event: "The status of the driver was already busy (a manual engagement has already been added for the driver)",
                                driver: driver_id
                            });

                            var response = {
                                flag: constants.responseFlags.SHOW_ERROR_MESSAGE,
                                error: "A manual engagement has already been created for you. Please restart the app if required."
                            }
                            res.send(response);
                        }
                    });
                }
            });
        });
    });
};


function updateAcceptDistance(engagement_id) {
    var get_engagement = "SELECT `pickup_latitude`, `pickup_longitude`, `driver_accept_latitude` as accept_latitude, `driver_accept_longitude` as accept_longitude " +
        "FROM `tb_engagements` WHERE `engagement_id`=?";
    connection.query(get_engagement, [engagement_id], function(err, engagement) {
        var google_query = 'https://maps.googleapis.com/maps/api/distancematrix/json?' +
            'origins=' + engagement[0].pickup_latitude + ',' + engagement[0].pickup_longitude +
            '&destinations=' + engagement[0].accept_latitude + ',' + engagement[0].accept_longitude;
        request(google_query, function(error, response, body) {
            var distance = 0;
            if (!error && response.statusCode == 200) {
                body = JSON.parse(body);
                if (body.rows.length > 0) {
                    distance = body.rows[0].elements[0].distance.value;

                    var update_engagement = "UPDATE `tb_engagements` SET `accept_distance`=? WHERE `engagement_id`=?";
                    var values = [distance, engagement_id];
                    connection.query(update_engagement, values, function(err, result) {
                        logging.logDatabaseQuery("Updating the accept distance for the driver using google distance matrix", err, result, null);
                    });
                }
            }
        });
    });
}


// Send notification that the ride has been accepted by some other driver
function updateEngagementsAndSendNotificationsToOtherDrivers(session_id, accepted_engagement_id, driver_id) {
    var other_drivers_query = "SELECT `driver_id`, `engagement_id` FROM `tb_engagements` WHERE `driver_id`!=? && `status`=? && `session_id`=? && `engagement_id`!=?";
    var values = [driver_id, constants.engagementStatus.REQUESTED, session_id, accepted_engagement_id];
    connection.query(other_drivers_query, values, function(err, results_engagements) {
        logging.logDatabaseQuery("Fetching other engagements for a session which was accepted", err, results_engagements, null);

        if (err) {
            sendErrorResponse(res);
        } else {
            if (results_engagements.length > 0) {
                // Update the status of other engagements
                var engagements = [];
                for (var i = 0; i < results_engagements.length; i++) {
                    engagements.push(results_engagements[i].engagement_id);
                }
                engagements = engagements.toString();

                var update_other_engagements = "UPDATE `tb_engagements` SET `status`=? WHERE `engagement_id` IN (" + engagements + ")";
                var values = [constants.engagementStatus.ACCEPTED_BY_OTHER_DRIVER];
                connection.query(update_other_engagements, values, function(err, result_update) {
                    logging.logDatabaseQuery("Updating status for other engagements for a session which was accepted", err, result_update, null);

                    logging.addEventToSession(session_id, {
                        event: "Updating the status of all other engagements to ACCEPTED_BY_OTHER_DRIVER",
                        engagements: engagements
                    });
                });

                for (var i = 0; i < results_engagements.length; i++) {
                    var message = "This request has been accepted by other driver";
                    var flag = constants.notificationFlags.RIDE_ACCEPTED_BY_OTHER_DRIVER; // This flag is for IOS push notification
                    var payload = {
                        "flag": constants.notificationFlags.RIDE_ACCEPTED_BY_OTHER_DRIVER,
                        "engagement_id": results_engagements[i].engagement_id
                    };
                    utils.sendNotification(results_engagements[i].driver_id, message, flag, payload);

                    logging.addEventToSession(session_id, {
                        event: "Sending notification to the driver that the request has been accepted",
                        driver: results_engagements[i].driver_id,
                        payload: payload
                    });
                }
            }
        }
    });
}

exports.formRideAcceptanceMessageForCustomer = formRideAcceptanceMessageForCustomer;


function formRideAcceptanceMessageForCustomer(customerId, driverId, engagementId, sessionId, callback_main) {
    var pickupLatitude = 0;
    var pickupLongitude = 0;
    var driverInfo = null;
    var freeRideData = {
        free_ride: 0
    };

    async.parallel(
        [
            getPickupLocation.bind(null, engagementId),
            getDriverInformation.bind(null, driverId),
            checkFreeRide.bind(null, customerId, freeRideData)
        ],
        function(err) {
            if (err) {
                return process.nextTick(callback_main.bind(null, err, null));
            }

            var message = {
                flag: constants.notificationFlags.RIDE_ACCEPTED,
                driver_id: driverId,
                user_name: driverInfo.user_name,
                phone: driverInfo.phone_no,
                user_pic: driverInfo.user_image,
                car_pic: driverInfo.driver_car_image,
                car_no: driverInfo.driver_car_no,
                current_lat: driverInfo.current_location_latitude,
                current_long: driverInfo.current_location_longitude,
                pickup_lat: pickupLatitude,
                pickup_long: pickupLongitude,
                session_id: sessionId,
                engagement_id: engagementId,
                free_ride: freeRideData.free_ride
            };
            return process.nextTick(callback_main.bind(null, err, message));
        }
    );

    function getPickupLocation(engagementId, callback) {
        var getPickupLocation =
            "SELECT `pickup_latitude`, `pickup_longitude` FROM `tb_engagements` " +
            "WHERE `engagement_id` = ?";
        connection.query(getPickupLocation, [engagementId], function(err, engagement) {
            logging.logDatabaseQuery("Getting the pickup latitude and longitude for the engagement", err, engagement);

            if (err) {
                return process.nextTick(callback.bind(null, err));
            }

            pickupLatitude = engagement[0].pickup_latitude;
            pickupLongitude = engagement[0].pickup_longitude;

            return process.nextTick(callback.bind(null, err));
        });
    }

    function getDriverInformation(driverId, callback) {
        var getDriverInfo =
            "SELECT `user_name`,`phone_no`,`user_image`,`driver_car_image`,`driver_car_no`,`total_rating_driver`,`total_rating_got_driver`,`current_location_latitude`, `current_location_longitude` " +
            "FROM `tb_users` " +
            "WHERE `user_id`= ?";
        connection.query(getDriverInfo, [driverId], function(err, driver) {
            logging.logDatabaseQuery("Fetch information for driver", err, driver, null);

            if (err) {
                return process.nextTick(callback.bind(null, err));
            }

            driver[0].rating = 3;
            if (driver[0].total_rating_got_driver != 0) {
                driver[0].rating = driver[0].total_rating_driver / driver[0].total_rating_got_driver;
            }
            driverInfo = driver[0];

            return process.nextTick(callback.bind(null, err));
        });
    }

    //function checkFreeRide(customerId, callback) {
    //    var getCoupons =
    //        "SELECT * " +
    //        "FROM `tb_accounts` " +
    //        "WHERE `user_id` = ? && `status` = ? && TIMESTAMPADD(MINUTE, 240, `expiry_date`) > NOW() " +
    //        "ORDER BY `expiry_date` ASC LIMIT 1";
    //    connection.query(getCoupons, [customerId, constants.couponStatus.ACTIVE], function (err, coupons) {
    //        logging.logDatabaseQuery('Checking if the coupon exists for the user when the request is accepted', err, coupons);
    //
    //        if(err || coupons.length == 0){
    //            freeRide = 0;
    //        }
    //        else{
    //            freeRide = 1;
    //        }
    //
    //        return process.nextTick(callback.bind(null, err));
    //    });
    //}
}

function checkFreeRide(customerId, returnData, callback) {
    var getCoupons =
        "SELECT * " +
        "FROM `tb_accounts` " +
        "WHERE `user_id` = ? && `status` = ? && TIMESTAMPADD(MINUTE, 240, `expiry_date`) > NOW() " +
        "ORDER BY `expiry_date` ASC LIMIT 1";
    connection.query(getCoupons, [customerId, constants.couponStatus.ACTIVE], function(err, coupons) {
        logging.logDatabaseQuery('Checking if the coupon exists for the user when the request is accepted', err, coupons);

        if (err || coupons.length == 0) {
            returnData.free_ride = 0;
        } else {
            returnData.free_ride = 1;
        }

        return process.nextTick(callback.bind(null, err));
    });
}

// Send notification that the ride has been accepted to the customer
function sendRideAcceptanceNotificationToCustomer(customerId, driverId, sessionId, engagementId) {
    formRideAcceptanceMessageForCustomer(customerId, driverId, engagementId, sessionId, function(err, payload) {
        if (err) {
            process.stderr.write("An error occurred when sending the ride acceptance notification to customer: %s", err);
            return;
        }

        var message = "Your request has been accepted";
        var flag = 2; // This flag is for IOS push notification
        utils.sendNotification(customerId, message, flag, payload);
        logging.addEventToSession(sessionId, {
            event: "Sending notification to the customer with the driver information",
            payload: payload
        });
    });
};

// Send notification that the ride has been accepted to the customer
function sendRideAcceptanceResponseToCustomer(res, customerId, driverId, sessionId, engagementId, engagementStatus) {
    formRideAcceptanceMessageForCustomer(customerId, driverId, engagementId, sessionId, function(err, response) {
        if (err) {
            process.stderr.write("An error occurred when sending the ride acceptance notification to customer: %s", err);
            return;
        }

        response.flag = engagementStatus == constants.engagementStatus.ACCEPTED ? constants.responseFlags.RIDE_ACCEPTED : constants.responseFlags.RIDE_STARTED;
        res.send(response);
        logging.addEventToSession(sessionId, {
            event: "Sending notification to the customer with the driver information",
            response: response
        });
    });
}

// Update the status for other engagements for the driver
function updateOtherEngagementsForDriver(session_id, accepted_engagement_id, driver_id) {
    var other_engagements = "UPDATE `tb_engagements` SET `status`=? WHERE `driver_id`=? && `status`=? && `engagement_id`!=?";
    var values = [constants.engagementStatus.CLOSED, driver_id, constants.engagementStatus.REQUESTED, accepted_engagement_id];
    connection.query(other_engagements, values, function(err, result_engagements) {
        logging.logDatabaseQuery("Get other engagements for the driver and close them", err, result_engagements, null);

        logging.addEventToSession(session_id, {
            event: "Updating the other engagements for the driver as CLOSED",
            number: result_engagements.affectedRows
        });
    });
}


// Send response to the driver that the ride has been accepted by you
function sendRideAcceptanceResponseToDriver(sessionId, customerId, res) {
    var pickupTime = "";
    var pickupId = 0;
    var customer = null;
    var destinationAddress = null;
    var freeRideData = {
        free_ride: 0
    };

    async.parallel(
        [
            getManualDestinationAddress.bind(null, sessionId),
            getPickupTime.bind(null, sessionId),
            getCustomer.bind(null, customerId),
            checkFreeRide.bind(null, customerId, freeRideData)
        ],
        function(err) {
            if (err) {
                console.log("Some error occurred when sending the ride acceptance response to the driver");
                return;
            }

            var rating = 3;
            if (customer.total_rating_got_user != 0) {
                rating = customer.total_rating_user / customer.total_rating_got_user;
            }
            console.log("pickup time : %s", pickupTime);
            var response = {
                user_data: {
                    user_name: customer.user_name,
                    phone_no: customer.phone_no,
                    user_image: customer.user_image,
                    user_rating: rating
                },
                is_scheduled: pickupId == 0 ? 0 : 1,
                pickup_time: pickupTime === "" ? pickupTime : pickupTime.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                free_ride: freeRideData.free_ride,
                manual_destination_latitude: destinationAddress.manual_destination_latitude,
                manual_destination_longitude: destinationAddress.manual_destination_longitude,
                manual_destination_address: destinationAddress.manual_destination_address
            };
            logging.logResponse(response);
            res.send(response);

            logging.addEventToSession(sessionId, {
                event: "Sending the customer information to the driver",
                response: response
            });
        }
    );

    function getManualDestinationAddress(sessionId, callback) {
        var fetchaddress = "SELECT `manual_destination_latitude`,`manual_destination_longitude`,`manual_destination_address` FROM `tb_engagements` WHERE `session_id`=? LIMIT 1";
        connection.query(fetchaddress, [sessionId], function(err, result) {
            logging.logDatabaseQuery("Get the destination address filled by customer for this session", err, result);
            destinationAddress = result[0];
            return process.nextTick(callback.bind(null, err));
        });
    }

    function getPickupTime(sessionId, callback) {
        var fetchPickupTime = "SELECT `pickup_time`, `pickup_id` FROM `tb_schedules` WHERE `pickup_id` IN (SELECT `pickup_id` FROM `tb_session` WHERE `session_id` = ?)";
        connection.query(fetchPickupTime, [sessionId], function(err, pickup) {
            logging.logDatabaseQuery("Get the pickup time for this session, if any.", err, pickup);
            if (pickup.length > 0) {
                pickupId = pickup[0].pickup_id;
                pickupTime = pickup[0].pickup_time;
            }
            return process.nextTick(callback.bind(null, err));
        });
    }

    function getCustomer(customerId, callback) {
        var fetchCustomerInfo =
            "SELECT `user_name`,`phone_no`,`user_image`,`total_rating_user`,`total_rating_got_user` " +
            "FROM `tb_users` " +
            "WHERE `user_id`= ? LIMIT 1";
        connection.query(fetchCustomerInfo, [customerId], function(err, customerInfo) {
            logging.logDatabaseQuery("Fetch information for customer", err, customerInfo);
            customer = customerInfo[0];
            return process.nextTick(callback.bind(null, err));
        });
    }

    //function checkFreeRide(customerId, callback) {
    //    var getCoupons =
    //        "SELECT * " +
    //        "FROM `tb_accounts` " +
    //        "WHERE `user_id` = ? && `status` = ? && TIMESTAMPADD(MINUTE, 240, `expiry_date`) > NOW() " +
    //        "ORDER BY `expiry_date` ASC LIMIT 1";
    //    connection.query(getCoupons, [customerId, constants.couponStatus.ACTIVE], function (err, coupons) {
    //        logging.logDatabaseQuery('Checking if the coupon exists for the user when the request is accepted', err, coupons);
    //
    //        if(err || coupons.length == 0){
    //            freeRide = false;
    //        }
    //        else{
    //            freeRide = true;
    //        }
    //
    //        return process.nextTick(callback.bind(null, err));
    //    });
    //}
}


function updateLastDropToAcceptLocationDistance(sessionId, engagementId, driverId, latitude, longitude) {
    getLastDropOrAcceptLocation(driverId, engagementId, function(err, lastLocation) {
        if (err) {
            process.stderr.write('Error occurred when getting the last engagement information for the driver\n');
            return;
        }
        if (typeof lastLocation.latitude === 'undefined' || typeof lastLocation.longitude === 'undefined') {
            process.stdout.write('There is no last engagement for the driver');
            return;
        }

        var googleQuery = 'https://maps.googleapis.com/maps/api/distancematrix/json?' +
            'origins=' + lastLocation.latitude + ',' + lastLocation.longitude +
            '&destinations=' + latitude + ',' + longitude;
        request(googleQuery, function(error, response, body) {
            var distance = 0;

            if (error || response.statusCode != 200) {
                process.stderr.write('The google query couldn\'t be processed: ' + googleQuery + '\n');
                return;
            }

            body = JSON.parse(body);
            console.log(JSON.stringify(body, undefined, 2));
            if (body.rows.length <= 0) {
                process.stderr.write('The google query\'s response body is empty: ' + googleQuery + '\n');
                return;
            }

            distance = body.rows[0].elements[0].distance.value;

            var updateEngagement =
                "UPDATE `tb_engagements` " +
                "SET `accept_distance_from_last_drop` = ? " +
                "WHERE `engagement_id` = ?";
            connection.query(updateEngagement, [distance, engagementId], function(err, result) {
                logging.logDatabaseQuery("Updating the distance from last drop location to the current accept location for engagement", err, result);
            });

            logging.addEventToSession(sessionId, {
                event: "Updating the distance from current accept location to the last drop location",
                engagement: engagementId,
                distance: distance
            });
        });
    });
}

function getLastDropOrAcceptLocation(driverId, engagementId, callback) {
    var requiredStatus = [constants.engagementStatus.ENDED, constants.engagementStatus.ACCEPTED_THEN_REJECTED];
    var getLastLocation =
        "SELECT `drop_latitude`, `drop_longitude`, `driver_accept_latitude`, `driver_accept_longitude`, `status` " +
        "FROM `tb_engagements` " +
        "WHERE `driver_id` = ? AND `engagement_id` < ? AND `status` IN (" + requiredStatus.join(',') + ") " +
        "ORDER BY `engagement_id` DESC LIMIT 1";
    connection.query(getLastLocation, [driverId, engagementId], function(err, lastEngagementInfo) {
        logging.logDatabaseQuery("Getting the information for last engagement for the driver", err, lastEngagementInfo);
        if (err) {
            return process.nextTick(callback.bind(null, err, null));
        }

        var location = {
            latitude: undefined,
            longitude: undefined
        };

        if (lastEngagementInfo.length > 0) {
            switch (lastEngagementInfo[0].status) {
                case constants.engagementStatus.ENDED:
                    location = {
                        latitude: lastEngagementInfo[0].drop_latitude,
                        longitude: lastEngagementInfo[0].drop_longitude
                    };
                    break;
                case constants.engagementStatus.ACCEPTED_THEN_REJECTED:
                    location = {
                        latitude: lastEngagementInfo[0].driver_accept_latitude,
                        longitude: lastEngagementInfo[0].driver_accept_longitude
                    };
                    break;
            }
        }

        return process.nextTick(callback.bind(null, err, location));
    });
}



// API
// The driver rejects a ride request for the customer
exports.reject_a_request = function(req, res) {
    logging.startSection("reject_a_request");
    logging.logRequest(req);

    var access_token = req.body.access_token;
    var engagement_id = req.body.engagement_id;
    var customer_id = req.body.customer_id;

    var manvalues = [access_token, engagement_id];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                sendAuthenticationError(res);
            } else {
                var driver_id = result[0].user_id;

                // Get the status for this engagement and see if the ride has been accepted by this driver before
                var engagement_status_query = "SELECT `driver_id`,`status`,`session_id` FROM `tb_engagements` WHERE `engagement_id`=? LIMIT 1";
                connection.query(engagement_status_query, [engagement_id], function(err, result_engagement) {
                    logging.logDatabaseQuery("Fetching information for the engagement", err, result_engagement, null);

                    if (err) {
                        sendErrorResponse(res);
                    } else {
                        var engagement_status = result_engagement[0].status;
                        var session_id = result_engagement[0].session_id;
                        var accepting_driver_id = result_engagement[0].driver_id;

                        if (driver_id == accepting_driver_id) {
                            // TODO need to handle this case
                            logging.addEventToSession(session_id, {
                                event: "Rejecting the request for the driver (UNHANDLED CASE)",
                                driver: driver_id,
                                accepting_driver: accepting_driver_id,
                                engagement: engagement_id,
                                status: utils.engagementStatusToString(engagement_status)
                            });
                        }

                        logging.addEventToSession(session_id, {
                            event: "Rejecting the request for the driver (UNHANDLED CASE)",
                            driver: driver_id,
                            accepting_driver: accepting_driver_id
                        });

                        if (result_engagement[0].status == constants.engagementStatus.TIMEOUT) {
                            var response = {
                                "log": "Request timed out",
                                "flag": constants.responseFlags.REQUEST_TIMEOUT
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));

                            logging.addEventToSession(session_id, {
                                event: "Sending response to the driver",
                                driver: driver_id,
                                response: response
                            });
                        }
                        // Will come here for constants.engagementStatus.REQUEST
                        // There is a seperate API for constants.engagementStatus.ACCEPTED
                        else {
                            // Update the total number of requests cancelled by the driver only if not done earlier
                            if (engagement_status != constants.engagementStatus.REJECTED_BY_DRIVER) {
                                var update_cancel_query = "UPDATE `tb_users` SET `total_cancel_by_driver`=`total_cancel_by_driver`+? WHERE `user_id`=? LIMIT 1";
                                connection.query(update_cancel_query, [1, driver_id], function(err, result) {
                                    logging.logDatabaseQuery("Incrementing the total number of requests cancelled by the driver", err, result, req.body);
                                });
                            }

                            // Update the status of engagement to Rejected
                            var update_engage_status_query = "UPDATE `tb_engagements` SET `status`=? WHERE `engagement_id`=? ";
                            connection.query(update_engage_status_query, [constants.engagementStatus.REJECTED_BY_DRIVER, engagement_id], function(err, result) {
                                logging.logDatabaseQuery("Set status of engagement to Rejected", err, result, req.body);
                            });

                            // Send a response saying that the ride has been accepted successfully
                            var response = {
                                "log": "Rejected successfully",
                                "flag": constants.responseFlags.REQUEST_REJECTED
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));

                            logging.addEventToSession(session_id, {
                                event: "Sending response to the driver",
                                driver: driver_id,
                                response: response
                            });
                        }

                        //logging.flushSession(session_id);
                    }
                });
            }
        });
    }
};


// API
// Customer cancels the request made by him
exports.cancel_the_request = function(req, res) {
    logging.startSection("cancel_the_request");
    logging.logRequest(req);

    var access_token = req.body.access_token;
    var session_id = req.body.session_id;

    var manvalues = [access_token];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                sendAuthenticationError(res);
            } else {
                var customer_id = result[0].user_id;
                var blocked = result[0].is_blocked;

                // Get the status of the request
                var session_info = "SELECT `is_active`, `ride_acceptance_flag` FROM `tb_session` WHERE `session_id`=?";
                connection.query(session_info, [session_id], function(err, result_status) {
                    logging.logDatabaseQuery("Get information for the session", err, result_status, null);

                    logging.addEventToSession(session_id, {
                        event: "The customer is cancelling the request",
                        status: result_status[0]
                    });

                    if (err) {
                        sendErrorResponse(res);
                    } else {
                        // If the session is not active and has timed out, send request cancelled response
                        if (result_status[0].is_active == constants.sessionStatus.INACTIVE) {
                            // Send the response to the customer
                            var response = {
                                "log": 'cancelled successfully',
                                "flag": constants.responseFlags.REQUEST_CANCELLED
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));

                            logging.addEventToSession(session_id, {
                                event: "Sending response to customer, the session was already inactive",
                                response: response
                            });

                            if (blocked == 0) {
                                mailer.sendMailForCancelledRequest(customer_id);
                            }
                        } else if (result_status[0].is_active == constants.sessionStatus.ACTIVE) {
                            if (result_status[0].ride_acceptance_flag == constants.rideAcceptanceFlag.NOT_YET_ACCEPTED) {
                                // Change the status of the session
                                cancelSession(session_id);

                                logging.addEventToSession(session_id, {
                                    event: "The session has not been accepted yet, we will cancel all the active engagements"
                                });

                                // Increment the total number of requests cancelled by the customer
                                var update_customer_info = "UPDATE `tb_users` SET `total_cancel_by_user`=`total_cancel_by_user`+? WHERE `user_id`=? LIMIT 1";
                                connection.query(update_customer_info, [1, customer_id], function(err, result_update) {
                                    logging.logDatabaseQuery("Increment the total number of requests cancelled by the customer", err, result_update, req.body);
                                });

                                // Fetch the active engagements for this session
                                // Change the status of all active requests made
                                var get_engagements = "SELECT `engagement_id`, `driver_id` FROM `tb_engagements` WHERE `session_id`=? && `status`=?";
                                var values = [session_id, constants.engagementStatus.REQUESTED];
                                connection.query(get_engagements, values, function(err, result_engagements) {
                                    logging.logDatabaseQuery("Getting the active engagements for the session", err, result_engagements, null);

                                    if (result_engagements.length > 0) {
                                        var engagements = [];
                                        for (var i = 0; i < result_engagements.length; i++) {
                                            engagements.push(result_engagements[i].engagement_id);

                                            // Send notification to the drivers
                                            var message = "Request cancelled by the customer";
                                            var flag = 2;
                                            var payload = {
                                                "flag": constants.notificationFlags.REQUEST_CANCELLED,
                                                "engagement_id": result_engagements[i].engagement_id,
                                                "user_id": customer_id
                                            };
                                            utils.sendNotification(result_engagements[i].driver_id, message, flag, payload);

                                            logging.addEventToSession(session_id, {
                                                event: "Sending notification to the drivers that the request has been cancelled",
                                                driver: result_engagements[i].driver_id,
                                                payload: payload
                                            });
                                        }

                                        engagements = engagements.toString();
                                        var update_engagements = "UPDATE `tb_engagements` SET `status`=? WHERE `engagement_id` IN (" + engagements + ")";
                                        connection.query(update_engagements, [constants.engagementStatus.CANCELLED_BY_CUSTOMER], function(err, result_update) {
                                            logging.logDatabaseQuery("Updating the status for the active engagements", err, result_update, null);

                                            logging.addEventToSession(session_id, {
                                                event: "Updating the status of engagements to CANCELLED_BY_CUSTOMER",
                                                engagements: engagements
                                            });
                                        });
                                    }
                                });

                                // Send the response to the customer
                                var response = {
                                    "log": 'cancelled successfully',
                                    "flag": constants.responseFlags.REQUEST_CANCELLED
                                };
                                logging.logResponse(response);
                                res.send(JSON.stringify(response));

                                logging.addEventToSession(session_id, {
                                    event: "Sending the response to the customer",
                                    response: response
                                });

                                if (blocked == 0) {
                                    mailer.sendMailForCancelledRequest(customer_id);
                                }
                            } else if (result_status[0].ride_acceptance_flag == constants.rideAcceptanceFlag.ACCEPTED) {
                                // TODO this is a temporary measure to get the engagement data to the customer
                                // TODO This will be removed when changes have been made to the client side app
                                var response = {
                                    "error": 'A ' + config.get('projectName') + ' driver has been linked to you. Please kill the app and start it again. Meanwhile, we are working hard to iron out things. :)',
                                    "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
                                };
                                logging.logResponse(response);
                                res.send(JSON.stringify(response));

                                logging.addEventToSession(session_id, {
                                    event: "Sending the response to the customer, the request customer is trying to cancel has already been accepted",
                                    response: response
                                });

                                var get_engagement = "SELECT a.engagement_id, a.status, a.driver_id, b.session_id " +
                                    "FROM `tb_engagements` AS a " +
                                    "JOIN `tb_session` AS b " +
                                    "WHERE a.session_id = b.session_id && a.user_id=? && a.status=? " +
                                    "ORDER BY a.engagement_id DESC";
                                var values = [customer_id, constants.engagementStatus.ACCEPTED];
                                connection.query(get_engagement, values, function(err, engagements) {
                                    logging.logDatabaseQuery("Getting the active engagement for the user", err, engagements, null);
                                    if (engagements.length > 0) {
                                        (function() {
                                            setTimeout(
                                                sendRideAcceptanceNotificationToCustomer(customer_id, engagements[0].driver_id, session_id, engagements[0].engagement_id),
                                                2000);
                                        })();
                                    }
                                });

                                //// Change the status of the session
                                //deactivateSession(session_id);
                                //
                                //// Increment the total number of requests cancelled by the customer
                                //var update_customer_info = "UPDATE `tb_users` SET `total_cancel_by_user`=`total_cancel_by_user`+? WHERE `user_id`=? LIMIT 1";
                                //connection.query(update_customer_info, [1, customer_id], function (err, result_update) {
                                //    logging.logDatabaseQuery("Increment the total number of requests cancelled by the customer", err, result_update, req.body);
                                //});
                                //
                                //// Fetch this particular engagement for this session
                                //var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED];
                                //var get_engagement = "SELECT `engagement_id`, `driver_id` FROM `tb_engagements` WHERE `session_id`=? && `status` IN (" + status.toString() + ")";
                                //var values = [session_id];
                                //connection.query(get_engagement, values, function (err, result_engagements) {
                                //    logging.logDatabaseQuery("Getting the active engagements for the session", err, result_engagements, null);
                                //
                                //    var update_engagement = "UPDATE `tb_engagements` SET `status`=? WHERE `engagement_id`=?";
                                //    connection.query(update_engagement, [constants.engagementStatus.CANCELLED_ACCEPTED_REQUEST, result_engagements[0].engagement_id], function (err, result_update) {
                                //        logging.logDatabaseQuery("Updating the status for the active engagements", err, result_update, null);
                                //    });
                                //
                                //    // Set the status of the driver to Free
                                //    var update_driver_status = "UPDATE `tb_users` SET `status`=? WHERE `user_id`=?  LIMIT 1";
                                //    connection.query(update_driver_status, [constants.userFreeStatus.FREE, result_engagements[0].driver_id], function(err, result_update)
                                //    {
                                //        logging.logDatabaseQuery("Set the status of the driver to Free", err, result_update, null);
                                //    });
                                //
                                //    // Send notification to the driver
                                //    var message = "Request cancelled by the customer";
                                //    var flag = 2;
                                //    var payload = {"flag": constants.notificationFlags.REQUEST_CANCELLED, "engagement_id": result_engagements[0].engagement_id, "user_id": customer_id};
                                //    utils.sendNotification(result_engagements[0].driver_id, message, flag, payload);
                                //});
                            }
                        }

                        //logging.flushSession(session_id);
                    }
                });
            }
        });
    }
};


// API
// Driver cancels the ride after accepting it
exports.cancel_the_ride = function(req, res) {
    logging.startSection("cancel_the_ride");
    logging.logRequest(req);

    var access_token = req.body.access_token;
    var engagement_id = req.body.engagement_id;
    var customer_id = req.body.customer_id;

    var manvalues = [access_token, engagement_id, customer_id];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                sendAuthenticationError(res);
            } else {
                var driver_id = result[0].user_id;

                // Update the engagement
                // Fetch and update the session
                var get_session = "SELECT `session_id`, `status` FROM `tb_engagements` WHERE `engagement_id`=?";
                connection.query(get_session, [engagement_id], function(err, engagements) {
                    logging.logDatabaseQuery("Get the status and session for this engagement", err, engagements, null);

                    // This is a temporary fix for a bug that is there in the android app for the drivers
                    if (engagements[0].status == constants.engagementStatus.ENDED) {
                        // Send a response to the driver
                        var response = {
                            "log": "Ride cancelled successfully",
                            "flag": constants.responseFlags.RIDE_CANCELLED_BY_DRIVER
                        };
                        res.send(JSON.stringify(response));
                        return;
                    }

                    var update_engagement = "UPDATE `tb_engagements` SET `status`=? WHERE `engagement_id`=?";
                    var values = [constants.engagementStatus.ACCEPTED_THEN_REJECTED, engagement_id];
                    connection.query(update_engagement, values, function(err, result_update) {
                        logging.logDatabaseQuery("Updated the status of engagement to accepted and then rejected", err, result_update, null);
                    });

                    logging.addEventToSession(engagements[0].session_id, {
                        event: "The driver is cancelling the ride for the user",
                        driver: driver_id,
                        engagement: engagement_id
                    });

                    var update_session = "UPDATE `tb_session` SET `is_active`=?, `ride_acceptance_flag`=? WHERE `session_id`=?";
                    var values = [constants.sessionStatus.INACTIVE, constants.rideAcceptanceFlag.ACCEPTED_THEN_REJECTED, engagements[0].session_id];
                    connection.query(update_session, values, function(err, result_update) {
                        logging.logDatabaseQuery("Updating the status and ride acceptance flag of session", err, result_update, null);

                        logging.addEventToSession(engagements[0].session_id, {
                            event: "Updated the status to inactive and ride acceptance flag to ACCEPTED_THEN_REJECTED for the session"
                        });
                    });

                    // Update the information for driver and set his status to free
                    var update_driver = "UPDATE `tb_users` SET `status`=?, `total_cancel_by_driver`=`total_cancel_by_driver`+? WHERE `user_id`=?";
                    var values = [constants.userFreeStatus.FREE, 1, driver_id];
                    connection.query(update_driver, values, function(err, result_update) {
                        logging.logDatabaseQuery("Updated the status of driver to free", err, result_update, null);

                        logging.addEventToSession(engagements[0].session_id, {
                            event: "Updated the status of driver to free",
                            driver: driver_id
                        });
                    });

                    // Send notification to the customer
                    var message = "Sorry! The ride has been cancelled by the driver.";
                    var flag = 1;
                    var payload = {
                        "flag": constants.notificationFlags.RIDE_REJECTED_BY_DRIVER
                    };
                    utils.sendNotification(customer_id, message, flag, payload);

                    logging.addEventToSession(engagements[0].session_id, {
                        event: "Sending notification to the customer that the driver has rejected the ride",
                        customer: customer_id,
                        payload: payload
                    });

                    // Send a response to the driver
                    var response = {
                        "log": "Ride cancelled successfully",
                        "flag": constants.responseFlags.RIDE_CANCELLED_BY_DRIVER
                    };
                    res.send(JSON.stringify(response));

                    logging.addEventToSession(engagements[0].session_id, {
                        event: "Sending response to the driver",
                        response: response
                    });

                    //logging.flushSession(session_id);

                    mailer.sendMailForCancelledRide(driver_id, customer_id);
                });
            }
        });
    }
};

// API
// Start the ride
// Start a ride for the given engagement
exports.start_ride = function(req, res) {
    logging.startSection("start_ride");
    logging.logRequest(req);

    var access_token = req.body.access_token;
    var engagement_id = req.body.engagement_id;
    var customer_id = req.body.customer_id;
    var pickup_latitude = req.body.pickup_latitude;
    var pickup_longitude = req.body.pickup_longitude;

    var manvalues = [access_token, engagement_id, customer_id, pickup_latitude, pickup_longitude];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                sendAuthenticationError(res);
            } else {
                var driver_id = result[0].user_id;

                var get_session = "SELECT `session_id`,`manual_destination_latitude`,`manual_destination_longitude`,`manual_destination_address` FROM `tb_engagements` WHERE `engagement_id`=? LIMIT 1";
                connection.query(get_session, [engagement_id], function(err, result_session) {
                    var session_id = result_session[0].session_id;

                    logging.addEventToSession(session_id, {
                        event: "Getting the pickup location from google"
                    });

                    // Update the pickup information for the engagement
                    utils.getLocationAddress(pickup_latitude, pickup_longitude, function(pickup_address) {
                        logging.addEventToSession(session_id, {
                            event: "Got the pickup location from google"
                        });

                        var date = new Date();
                        var update_engagement = "UPDATE `tb_engagements` SET `status`=?,`pickup_time`=?,`pickup_location_address`=?  WHERE `engagement_id`=? LIMIT 1";
                        values = [constants.engagementStatus.STARTED, date, pickup_address, engagement_id];
                        connection.query(update_engagement, values, function(err, result_update) {
                            var response = {
                                "log": "ride_started",
                                "manual_destination_latitude": result_session[0].manual_destination_latitude,
                                "manual_destination_longitude": result_session[0].manual_destination_longitude,
                                "manual_destination_address": result_session[0].manual_destination_address,
                                "flag": constants.responseFlags.RIDE_STARTED
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));

                            logging.addEventToSession(session_id, {
                                event: "Sending response to the driver",
                                response: response
                            });
                        });
                    });

                    // Send notification to the customer
                    var message = "Your ride has started";
                    var flag = 3;
                    var payload = {
                        "flag": constants.notificationFlags.RIDE_STARTED
                    };
                    utils.sendNotification(customer_id, message, flag, payload);

                    logging.addEventToSession(session_id, {
                        event: "Sending notification to the customer",
                        customer: customer_id,
                        payload: payload
                    });

                    //logging.flushSession(session_id);

                    // Send e-mail to the support team
                    mailer.sendMailForStartedRide(customer_id, driver_id);
                });
            }
        });
    }
};




// Capture the data being generated in the ride
exports.captureRideData = function(req, res) {
    logging.startSection('Capturing the data being sent in the ride');
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var engagementId = parseInt(req.body.engagement_id);
    var latitude = parseFloat(req.body.current_latitude);
    var longitude = parseFloat(req.body.current_longitude);
    var distance = parseFloat(req.body.distance_travelled);
    var rideTime = parseInt(req.body.ride_time);
    var waitTime = parseInt(req.body.wait_time);

    var insertData =
        'INSERT INTO `tb_rides_data` ' +
        '(`engagement_id`, `latitude`, `longitude`, `distance_till_now`, `ride_time_till_now`, `wait_time_till_now`) ' +
        'VALUES (? ,? ,? ,? ,? ,?)';
    var values = [engagementId, latitude, longitude, distance, rideTime, waitTime];
    connection.query(insertData, values, function(err, result) {
        logging.logDatabaseQuery('Logging the data generated in ride', err, result);

        res.send({
            flag: constants.responseFlags.ACTION_COMPLETE,
            log: 'Data received'
        });
    });
};




// End the ride
// Yipeee! we had a full ride
exports.end_ride = function(req, res) {
    logging.startSection("end_ride");
    logging.logRequest(req);

    var access_token = req.body.access_token;
    var engagement_id = req.body.engagement_id;
    //var customer_id = req.body.customer_id;
    var drop_latitude = req.body.latitude;
    var drop_longitude = req.body.longitude;
    var distance_travelled = req.body.distance_travelled;
    var wait_time = req.body.wait_time;
    var ride_time = req.body.ride_time;
    if (ride_time === undefined)
        ride_time = 0;

    var manvalues = [access_token, engagement_id, drop_latitude, drop_longitude, wait_time, ride_time];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                sendAuthenticationError(res);
            } else {
                var driver_id = result[0].user_id;

                getCustomersInvolvedInEngagement(engagement_id, function(err, resultCustomersEngagementsData) {
                    if (err) {
                        responses.errorResponse(res);
                        return;
                    }
                    var wait_time_seconds = resultCustomersEngagementsData[0].wait_time_seconds;

                    var splitFareUsersCount = resultCustomersEngagementsData.length;

                    var arrEngagements = [];
                    var session_id = resultCustomersEngagementsData[0].session_id;
                    var car_type = resultCustomersEngagementsData[0].car_type;
                    var users = [];

                    for (var i = 0; i < splitFareUsersCount; i++) {
                        arrEngagements.push(resultCustomersEngagementsData[i].engagement_id);
                        users.push({
                            "user_name": resultCustomersEngagementsData[i].user_name,
                            "user_image": resultCustomersEngagementsData[i].user_image
                        });
                    }



                    var changeStatus =
                        'UPDATE tb_engagements ' +
                        'SET status = ? ' +
                        'WHERE engagement_id IN(' + arrEngagements.toString() + ')';
                    connection.query(changeStatus, [constants.engagementStatus.ENDED], function(err, result) {
                        logging.logDatabaseQuery('Changing the status of the engagement to ended', err, result);

                        console.log('GETTING THE FRESH CALL FOR THE ENDING THE ENGAGEMENT');
                        console.log('GETTING THE FRESH CALL FOR THE ENDING THE ENGAGEMENT');
                        console.log('GETTING THE FRESH CALL FOR THE ENDING THE ENGAGEMENT');
                        console.log('GETTING THE FRESH CALL FOR THE ENDING THE ENGAGEMENT');
                        console.log('GETTING THE FRESH CALL FOR THE ENDING THE ENGAGEMENT');
                        console.log('GETTING THE FRESH CALL FOR THE ENDING THE ENGAGEMENT');
                        console.log('GETTING THE FRESH CALL FOR THE ENDING THE ENGAGEMENT');
                        console.log('GETTING THE FRESH CALL FOR THE ENDING THE ENGAGEMENT');

                        // duplicate call case in case the response doesn't reach the user


                        var fare_details_query = "SELECT * FROM `tb_fare` WHERE `car_type`=? LIMIT 1";
                        connection.query(fare_details_query, [car_type], function(err, fare_details) {





                            var fare_subsidized = fare_details[0];
                            //var fare_unsubsidized = fare_details[1];
                            //
                            //

                            //wait time calculation
                            //
                            //assuming wait_time_fee is in fee per min and total fare is in dollers


                            var wait_time_charges = Math.ceil(resultCustomersEngagementsData[0].wait_time_seconds / 60) * fare_subsidized.wait_time_fare_per_min;
                            console.log("Wait time  charges : " + wait_time_charges + "$ =============" + fare_subsidized.wait_time_fare_per_min + "=============" + resultCustomersEngagementsData[0].wait_time_seconds);

                            var total_fare = fare_subsidized.fare_fixed;
                            if (distance_travelled >= fare_subsidized.fare_threshold_distance) {
                                total_fare += (distance_travelled - fare_subsidized.fare_threshold_distance) * fare_subsidized.fare_per_km;
                            }
                            ride_time = Math.ceil(ride_time);
                            if (ride_time >= fare_subsidized.fare_threshold_time) {
                                total_fare += (ride_time - fare_subsidized.fare_threshold_time) * fare_subsidized.fare_per_min;
                            }

                            total_fare = total_fare + wait_time_charges;

                            total_fare = total_fare / splitFareUsersCount;

                            total_fare = parseFloat(total_fare).toFixed(2);


                            //var actual_fare = fare_unsubsidized.fare_fixed;
                            //if (distance_travelled >= fare_unsubsidized.fare_threshold_distance) {
                            //    actual_fare += (distance_travelled - fare_unsubsidized.fare_threshold_distance) * fare_unsubsidized.fare_per_km;
                            //}
                            //if (ride_time >= fare_unsubsidized.fare_threshold_time) {
                            //    actual_fare += (ride_time - fare_unsubsidized.fare_threshold_time) * fare_unsubsidized.fare_per_min;
                            //}

                            actual_fare = total_fare;

                            var update_driver_info = "UPDATE `tb_users` " +
                                "SET `status`=?,`total_rides_as_driver`=`total_rides_as_driver`+?,`total_distance_as_driver`=`total_distance_as_driver`+? " +
                                "WHERE `user_id`=? LIMIT 1";
                            var values = [constants.userFreeStatus.FREE, 1, distance_travelled, driver_id];
                            connection.query(update_driver_info, values, function(err, result_update) {
                                logging.logDatabaseQuery("Make driver free", err, result_update, null);

                                //logging.addEventToSession(session_id, {
                                //    event: "updating the status of the driver to free",
                                //    engagement: driver_id
                                //});

                            });


                            for (var i = 0; i < splitFareUsersCount; i++) {
                                (function(i) {
                                    processEndAnEngagement(resultCustomersEngagementsData[i].engagement_id, resultCustomersEngagementsData[i].user_id, total_fare, actual_fare, distance_travelled, ride_time, wait_time, drop_latitude, drop_longitude, driver_id, wait_time_charges , wait_time_seconds);
                                    // Send a response to the driver that the ride has completed

                                    //return;

                                    //if(err){
                                    //    sendErrorResponse(res);
                                    //    return;
                                    //}
                                    //
                                    //if(i == splitFareUsersCount - 1){
                                    //    var response = {
                                    //        flag: constants.responseFlags.RIDE_ENDED,
                                    //        fare: total_fare * splitFareUsersCount,
                                    //        //to_pay: to_pay,
                                    //        //discount: discount,
                                    //        distance_travelled: distance_travelled,
                                    //        wait_time: wait_time,
                                    //        ride_time: ride_time
                                    //        //coupon: coupon
                                    //    };
                                    //    logging.logResponse(response);
                                    //    res.send(JSON.stringify(response));
                                    //
                                    //    //logging.addEventToSession(session_id, {
                                    //    //    event: "Sending response to the driver",
                                    //    //    driver: driver_id,
                                    //    //    response: response
                                    //    //});
                                    //
                                    //    // Deactivate the session for the current engagement
                                    deactivateSession(session_id);
                                    //}
                                })(i);
                            }

                            var response = {
                                flag: constants.responseFlags.RIDE_ENDED,
                                fare: total_fare * splitFareUsersCount,
                                //to_pay: to_pay,
                                //discount: discount,
                                distance_travelled: distance_travelled,
                                wait_time: wait_time_seconds,
                                wait_time_charges: wait_time_charges,
                                ride_time: ride_time,
                                users: users
                                    //coupon: coupon
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));

                            logging.addEventToSession(session_id, {
                                event: "Sending response to the driver",
                                driver: driver_id,
                                response: response
                            });

                        });



                    });

                });
            }
        });
    }
};

function sendRideCompleteNotificationToCustomer(sessionId, userId, rideSummary) {
    showRatingPopup(userId, rideSummary.coupon, function(err, showPopup) {
        // Send a notification to the customer
        var message = "Ride ended";
        var flag = 5;
        var payload = {
            flag: constants.notificationFlags.RIDE_ENDED,
            fare: rideSummary.total_fare.toString(),
            to_pay: rideSummary.to_pay.toString(),
            discount: rideSummary.discount.toString(),
            distance_travelled: rideSummary.distance_travelled.toString(),
            wait_time: rideSummary.wait_time.toString(),
            wait_time_charges: rideSummary.wait_time_charges.toString(),

            ride_time: rideSummary.ride_time.toString(),
            coupon: rideSummary.coupon == null ? {} : rideSummary.coupon,
            rate_us: showPopup === false ? 0 : 1
        };
        utils.sendNotification(userId, message, flag, payload);

        logging.addEventToSession(sessionId, {
            event: "Sending notification to the customer",
            customer: userId,
            payload: payload
        });
    });
}

function showRatingPopup(userId, couponApplied, callback) {
    if (couponApplied !== null) {
        return process.nextTick(callback.bind(null, null, true));
    }

    var triggerNumbers = [2, 7];
    var getPaidRideCount =
        'SELECT COUNT(*) AS `num_paid_rides` ' +
        'FROM `tb_engagements` ' +
        'WHERE `user_id` = ? AND `status` = 3 AND `account_id` = 0';
    connection.query(getPaidRideCount, [userId], function(err, result) {
        logging.logDatabaseQuery('Getting the number of paid rides made by the user', err, result);

        if (triggerNumbers.indexOf(result[0].num_paid_rides) > 0) {
            return process.nextTick(callback.bind(null, null, true));
        }

        return process.nextTick(callback.bind(null, null, false));
    });
}

function updateRideDistanceFromGoogle(engagementId, pickupLat, pickupLong, dropLat, dropLong) {
    var googleQuery = 'https://maps.googleapis.com/maps/api/distancematrix/json?' +
        'origins=' + pickupLat + ',' + pickupLong +
        '&destinations=' + dropLat + ',' + dropLong;
    request(googleQuery, function(error, response, body) {
        var distance = 0;
        if (!error && response.statusCode == 200) {
            body = JSON.parse(body);
            if (body.rows.length > 0) {
                distance = body.rows[0].elements[0].distance.value / 1000;

                var updateEngagement = "UPDATE `tb_engagements` SET `ride_distance_from_google`=? WHERE `engagement_id`=?";
                var values = [distance, engagementId];
                connection.query(updateEngagement, values, function(err, result) {
                    logging.logDatabaseQuery("Updating the ride distance for the engagement using google distance matrix", err, result);
                });
            }
        }
    });
}

function giveAdvancedPermissions(userId) {
    var updatePermissions =
        "UPDATE `tb_users` " +
        "SET `can_schedule` = 1, `can_change_location` = 1 " +
        "WHERE `user_id` = ? AND (`can_schedule` = 0 OR `can_change_location` = 0)";
    connection.query(updatePermissions, [userId], function(err, result) {
        logging.logDatabaseQuery('Changing the permission level for the users', err, result);

        if (result.affectedRows > 0) {
            mailer.sendMailForAdvancedPermissions(userId);
        } else {
            console.log('THE PERMISSIONS ARE ALREADY THERE.');
        }
    });
}



// Send notification to the customer that the waiting state has started or ended
exports.start_end_wait = function(req, res) {
    logging.startSection("start_end_wait");
    logging.logRequest(req);

    var access_token = req.body.access_token;
    var customer_id = req.body.customer_id;
    var flag = req.body.flag;

    var manvalues = [access_token, customer_id, flag];
    var checkblank = utils.checkBlank(manvalues);
    if (checkblank == 1) {
        sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                sendAuthenticationError(res);
            } else {
                var message = "";
                var payload = "";
                var notificationFlag = 7;
                if (flag == 1) {
                    message = "Waiting time has started";
                    payload = {
                        "message": "Waiting time has started",
                        "flag": constants.notificationFlags.WAITING_STARTED
                    };
                } else {
                    message = "Waiting time has ended";
                    payload = {
                        "message": "Waiting time has ended",
                        "flag": constants.notificationFlags.WAITING_ENDED
                    };
                }
                utils.sendNotification(customer_id, message, notificationFlag, payload);


                var response = {
                    "log": "Sent",
                    "flag": constants.responseFlags.WAITING
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            }
        });
    }
};


// Get the status of the user for the ride, if any
exports.get_current_user_status = function(req, res) {
    logging.startSection("Getting the status for the user");
    logging.logRequest(req);

    var access_token = req.body.access_token;

    var manValues = [access_token];
    var checkData = utils.checkBlank(manValues);
    if (checkData == 1) {
        sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                sendAuthenticationError(res);
            } else {
                var user_id = result[0].user_id;
                var current_user_status = result[0].current_user_status;
                getCurrentUserStatus(user_id, current_user_status, function(response) {
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));
                });
            }
        });
    }
};


function getCurrentUserStatus(user_id, current_user_status, callback) {


    var ride_module = require('./ride_parallel_dispatcher');
    if (current_user_status == constants.userCurrentStatus.DRIVER_ONLINE) {

        getEngagementDataForDriver(user_id, function(flag, data) {
            if (flag == 0) {
                var response = {
                    "active_requests": data,
                    "flag": constants.responseFlags.ACTIVE_REQUESTS
                };
                callback(response);
            } else {
                var response = {
                    "last_engagement_info": data,
                    "flag": constants.responseFlags.ENGAGEMENT_DATA
                };
                callback(response);
            }
        });
    } else if (current_user_status == constants.userCurrentStatus.CUSTOMER_ONLINE) {

        isASessionActive(user_id, function(session) {
            if (session.session_id < 0) {

                // Get last active engagement of customer where engagement status > accepted

                var get_engagement = "SELECT `engagement_id` FROM `tb_engagements` WHERE `user_id`=? && `status` = ? ORDER BY `engagement_id` DESC LIMIT 1";
                connection.query(get_engagement, [user_id, constants.engagementStatus.STARTED], function(err, result_engagement) {
                    logging.logDatabaseQuery("Getting the active engagement11", err, result_engagement, null);

                    if (result_engagement.length > 0) {

                        var engagement_id = result_engagement[0].engagement_id;

                        var get_data =
                            "SELECT a.driver_id, a.pickup_latitude, a.pickup_longitude, a.engagement_id, a.status, a.session_id, a.wait_time_seconds , a.last_wait_time_started , a.wait_time_started , " +
                            "b.user_name, b.phone_no, b.user_image, b.driver_car_image, b.driver_car_no, b.current_location_latitude, b.current_location_longitude, b.total_rating_got_driver, b.total_rating_driver " +
                            "FROM tb_engagements a, tb_users b " +
                            "WHERE a.engagement_id=? && a.driver_id=b.user_id ORDER BY `engagement_id` DESC LIMIT 1";

                        connection.query(get_data, [engagement_id], function(err, data) {
                            logging.logDatabaseQuery("Getting data for the engagement", err, data);



                                            //calculating wait_time

                var totalwaittime;
                var time = new Date(data[0].last_wait_time_started);
                if (data[0].wait_time_started == 1) {
                    totalwaittime = data[0].wait_time_seconds + (new Date() - time) / 1000
                } else {
                    totalwaittime = data[0].wait_time_seconds;
                }
                console.log(totalwaittime)

                data[0].wait_time = totalwaittime;
                delete data[0].wait_time_seconds
                delete data[0].last_wait_time_started

                            data[0].rating = data[0].total_rating_driver / data[0].total_rating_got_driver;
                            delete data[0].total_rating_user;
                            delete data[0].total_rating_got_user;

                            var freeRideData = {
                                free_ride: 0
                            };
                            checkFreeRide(user_id, freeRideData, function(err) {
                                data[0].free_ride = freeRideData.free_ride;
                                //process.nextTick(callback.bind(null, data));
                                var response = {
                                    "last_engagement_info": data,
                                    "flag": constants.responseFlags.ENGAGEMENT_DATA
                                };
                                callback(response);
                            });
                        });

                    } else {

                        ride_module.getLastRideForRating(user_id, function(err, lastRide) {
                            var response = {
                                "log": "No active session",
                                "flag": constants.responseFlags.NO_ACTIVE_SESSION,
                                "last_ride_response": lastRide
                            };
                            callback(response);
                        });

                    }
                });


            } else {
                if (session.ride_acceptance_flag) {
                    getActiveEngagementDataForCustomer(user_id, session.session_id, function(engagement_data) {
                        console.log(JSON.stringify(engagement_data));
                        if (engagement_data != null) {
                            var response = {
                                "last_engagement_info": engagement_data,
                                "flag": constants.responseFlags.ENGAGEMENT_DATA
                            };
                            callback(response);
                        } else {
                            ride_module.getLastRideForRating(user_id, function(err, lastRide) {
                                var response = {
                                    "log": "No active session",
                                    "flag": constants.responseFlags.NO_ACTIVE_SESSION,
                                    "last_ride_response": lastRide
                                };
                                callback(response);
                            });
                        }
                    });
                } else {
                    getEngagementDataFromSessionId(session.session_id, function(result) {
                        var response = {
                            "log": "Assigning driver",
                            "session_id": session.session_id,
                            "flag": constants.responseFlags.ASSIGNING_DRIVERS,
                            "car_type": result[0].car_type
                        };
                        callback(response);
                    });

                }
            }
        })

    } else if (current_user_status == constants.userCurrentStatus.OFFLINE) {

        var response = {
            "log": 'User was offline',
            "flag": constants.responseFlags.USER_OFFLINE
        };
        callback(response);
    }
}


function isASessionActive(user_id, callback) {
    var get_session = "SELECT `session_id`, `ride_acceptance_flag` " +
        "FROM `tb_session` " +
        "WHERE `user_id`=? && `is_active`=? && `date` > timestamp(DATE_SUB(NOW(), INTERVAL 2 HOUR))  ORDER BY `date` DESC LIMIT 1";
    var values = [user_id, constants.sessionStatus.ACTIVE];
    connection.query(get_session, values, function(err, result_session) {
        logging.logDatabaseQuery("Getting any active session for the user", err, result_session, null);

        if (result_session.length > 0) {
            callback(result_session[0]);
        } else {
            callback({
                "session_id": -1
            });
        }
    });
}


function getEngagementDataForDriver(driverId, callback) {
    var activeEngagement = [];
    var requests = [];

    async.parallel(
        [
            getActiveEngagementIfAny.bind(null, driverId),
            getActiveRequestsIfAny.bind(null, driverId)
        ],
        function(err) {
            if (err) {
                console.log("Error when fetching the engagement data for the driver when restoring the app.");
                return;
            }

            if (activeEngagement.length > 0) {
                console.log(activeEngagement);
                callback(1, activeEngagement);
            } else {
                callback(0, requests);
            }
        }
    );

    // TODO remove callback hell from here
    function getActiveEngagementIfAny(driverId, callback) {
        var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED];
        var getActiveEngagement =
            "SELECT a.user_id,a.pickup_time AS start_ride_time,a.ride_time, a.pickup_latitude, a.pickup_longitude, a.engagement_id, a.status, a.session_id,a.manual_destination_latitude,a.manual_destination_longitude, a.wait_time_seconds , a.wait_time_started , a.last_wait_time_started ," +
            "b.user_name, b.phone_no, b.user_image, b.total_rating_got_user, b.total_rating_user " +
            "FROM tb_engagements a, tb_users b " +
            "WHERE a.driver_id=? && a.status IN (" + status.toString() + ") && a.user_id=b.user_id ORDER BY `engagement_id` DESC LIMIT 1";
        connection.query(getActiveEngagement, [driverId], function(err, engagement) {
            logging.logDatabaseQuery("Getting active engagement if any", err, engagement);

            if (engagement.length === 0) {
                process.nextTick(callback.bind(null));
                return;
            }

            //calculating wait_time

            var totalwaittime;
            var time = new Date(engagement[0].last_wait_time_started);
            if (engagement[0].wait_time_started == 1) {
                totalwaittime = engagement[0].wait_time_seconds + (new Date() - time) / 1000
            } else {
                totalwaittime = engagement[0].wait_time_seconds;
            }
            console.log(totalwaittime)

            engagement[0].wait_time = totalwaittime;
            delete engagement[0].wait_time_seconds
            delete engagement[0].last_wait_time_started

            engagement[0].rating = engagement[0].total_rating_user / engagement[0].total_rating_got_user;
            delete engagement[0].total_rating_user;
            delete engagement[0].total_rating_got_user;

            var fetchPickupTimeIfAny = "SELECT `pickup_time`, `pickup_id` FROM `tb_schedules` WHERE `pickup_id` IN (SELECT `pickup_id` FROM `tb_session` WHERE `session_id` = ?)";
            connection.query(fetchPickupTimeIfAny, [engagement[0].session_id], function(err, pickup) {
                logging.logDatabaseQuery("Get the pickup time for this session, if any.", err, pickup);

                var pickupId = 0;
                var pickupTime = "";
                if (pickup.length > 0) {
                    pickupId = pickup[0].pickup_id;
                    pickupTime = pickup[0].pickup_time;
                }
                engagement[0].is_scheduled = pickupId == 0 ? 0 : 1;
                engagement[0].pickup_time = pickupTime === "" ? pickupTime : pickupTime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                var date = new Date();

                engagement[0].ride_time = engagement[0].start_ride_time == '0000-00-00 00:00:00' ? 0 : date.getTime() - engagement[0].start_ride_time.getTime();

                if (engagement[0].start_ride_time != '0000-00-00 00:00:00') {
                    engagement[0].start_ride_time = engagement[0].start_ride_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                }

                engagement[0].ride_time = engagement[0].ride_time / 1000;

                engagement[0].ride_time = parseInt(Math.abs(engagement[0].ride_time));
                engagement[0].ride_time -= totalwaittime ;

                var freeRideData = {
                    free_ride: 0
                };
                checkFreeRide(engagement[0].user_id, freeRideData, function(err) {
                    engagement[0].free_ride = freeRideData.free_ride;
                    activeEngagement.push(engagement[0]);
                    process.nextTick(callback.bind(null));
                });
            });
        });
    }

    function getActiveRequestsIfAny(driverId, callback) {

        var fetchActiveRequests = "SELECT `engagement_id`, `user_id`, `pickup_latitude`, `pickup_longitude`, `pickup_latitude` AS latitude, `pickup_longitude` AS longitude,`pickup_location_address`, `request_made_on`,`manual_destination_latitude`,`manual_destination_longitude` " +
            "FROM `tb_engagements` WHERE `driver_id`=? && `status`=? && TIMESTAMPDIFF(MINUTE,`request_made_on`, NOW()) < 1 group by user_id order by engagement_id desc";
        var values = [driverId, constants.engagementStatus.REQUESTED];
        console.log("NOW()")
        console.log(new Date())
        connection.query(fetchActiveRequests, values, function(err, result_requests) {

            console.log("==================== getActiveRequestsIfAny")
            console.log(result_requests)

            var result_requests_length = result_requests.length;
            for (var i = 0; i < result_requests_length; i++) {
                result_requests[i].current_time = result_requests[i].request_made_on.toISOString().replace(/T/, ' ').replace(/\..+/, '');
            }

            logging.logDatabaseQuery("Getting all requests for the driver", err, result_requests);
            requests = result_requests;
            process.nextTick(callback.bind(null));
        });
    }
}


function getActiveEngagementDataForCustomer(customer_id, session_id, callback) {
    var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED];
    var get_engagement = "SELECT `engagement_id` FROM `tb_engagements` WHERE `session_id`=? && `status` IN (" + status.toString() + ")";
    connection.query(get_engagement, [session_id], function(err, result_engagement) {
        logging.logDatabaseQuery("Getting the active engagement", err, result_engagement, null);

        if (result_engagement.length > 0) {
            var engagement_id = result_engagement[0].engagement_id;
            var get_data =
                "SELECT a.driver_id, a.pickup_latitude, a.pickup_longitude, a.engagement_id, a.status, a.session_id, a.wait_time_seconds , a.wait_time_started , a.last_wait_time_started , " +
                "b.user_name, b.phone_no, b.user_image, b.driver_car_image, b.driver_car_no, b.current_location_latitude, b.current_location_longitude, b.total_rating_got_driver, b.total_rating_driver " +
                "FROM tb_engagements a, tb_users b " +
                "WHERE a.engagement_id=? && a.driver_id=b.user_id ORDER BY `engagement_id` DESC LIMIT 1";
            connection.query(get_data, [engagement_id], function(err, data) {
                logging.logDatabaseQuery("Getting data for the engagement", err, data);


                //calculating wait_time

                var totalwaittime;
                var time = new Date(data[0].last_wait_time_started);
                if (data[0].wait_time_started == 1) {
                    totalwaittime = data[0].wait_time_seconds + (new Date() - time) / 1000
                } else {
                    totalwaittime = data[0].wait_time_seconds;
                }
                console.log(totalwaittime)

                data[0].wait_time = totalwaittime;
                delete data[0].wait_time_seconds
                delete data[0].last_wait_time_started

                data[0].rating = data[0].total_rating_driver / data[0].total_rating_got_driver;
                delete data[0].total_rating_user;
                delete data[0].total_rating_got_user;

                var freeRideData = {
                    free_ride: 0
                };
                checkFreeRide(customer_id, freeRideData, function(err) {
                    data[0].free_ride = freeRideData.free_ride;
                    process.nextTick(callback.bind(null, data));
                });
            });
        } else {
            deactivateSession(session_id);
            callback(null);
        }
    });
};


/*
 ALL THE APIs TO GET THINGS DONE MANUALLY ARE HERE
 */
exports.create_manual_engagement = function(req, res) {
    logging.startSection("create_manual_engagement");
    logging.logRequest(req);

    var customer_id = req.body.customer_id;
    var latitude = req.body.pickup_latitude;
    var longitude = req.body.pickup_longitude;
    var driver_id = req.body.driver_id;

    var manualDestinationLatitude = req.body.manual_destination_latitude;
    var manualDestinationLongitude = req.body.manual_destination_longitude;
    var manualDestinationAddress = req.body.manual_destination_address;

    var isBlank = utils.checkBlank([customer_id, latitude, longitude, driver_id]);
    if (isBlank) {
        sendParameterMissingResponse(res);
    } else {
        var get_session = "SELECT `is_active`, `ride_acceptance_flag`, `session_id` FROM `tb_session` WHERE `user_id`=? ORDER BY `session_id` DESC LIMIT 1";
        connection.query(get_session, [customer_id], function(err, sessions) {
            logging.logDatabaseQuery("Getting previous session for the customer", err, sessions, null);

            if (sessions.length > 0 && sessions[0].is_active == constants.sessionStatus.ACTIVE) {
                var response = {
                    error: 'The previous session for the customer is already active'
                };
                res.send(response);
            } else {
                var get_driver_status = "SELECT `status` FROM `tb_users` WHERE `user_id`=?";
                connection.query(get_driver_status, [driver_id], function(err, drivers) {
                    logging.logDatabaseQuery("Getting the status of the driver", err, drivers, null);

                    if (drivers[0].status == constants.userFreeStatus.BUSY) {
                        var response = {
                            error: 'The driver is busy'
                        };
                        res.send(response);
                    } else {
                        addManualEngagement(customer_id, latitude, longitude, driver_id, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, res);
                    }
                });
            }
        });
    }
};

function addManualEngagement(customer_id, latitude, longitude, driver_id, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, res) {
    var get_customer_info = "SELECT `user_id`, `user_name`, `user_email`, `phone_no` " +
        "FROM `tb_users` WHERE `user_id`=?";
    connection.query(get_customer_info, [customer_id], function(err, customer) {
        logging.logDatabaseQuery("Getting the information for the customer", err, customer, null);

        var get_driver_info = "SELECT `user_id`, `user_name`, `user_email`, `phone_no`, `current_location_latitude`, `current_location_longitude`,`car_type` " +
            "FROM `tb_users` WHERE `user_id`=?";
        connection.query(get_driver_info, [driver_id], function(err, driver) {
            logging.logDatabaseQuery("Getting the information for the customer", err, driver, null);

            utils.getLocationAddress(latitude, longitude, function(pickup_address) {
                // Create a new session
                var create_session = "INSERT INTO `tb_session` (`user_id`, `is_active`, `requested_drivers`, `ride_acceptance_flag`) " +
                    "VALUES (?, ?, ?, ?)";
                var values = [customer_id, constants.sessionStatus.ACTIVE, 5, constants.rideAcceptanceFlag.ACCEPTED];
                connection.query(create_session, values, function(err, session) {
                    logging.logDatabaseQuery("Creating a new session", err, session, null);


                    utils.generateSplitFareKey(function(err, splitFareKey) {
                        if (err) {
                            sendErrorResponse(res);
                            return;
                        }

                        var create_engagement = "INSERT INTO `tb_engagements` " +
                            "(`user_id`, `driver_id`, `pickup_latitude`, `pickup_location_address`, `pickup_longitude`, `status`, `driver_accept_latitude`, `driver_accept_longitude`, `session_id`, `accept_time`," +
                            "`manual_destination_latitude`,`manual_destination_longitude`,`manual_destination_address`,`split_fare_key`,`car_type`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        var values = [customer_id, driver_id,
                            latitude, pickup_address, longitude,
                            constants.engagementStatus.ACCEPTED,
                            driver[0].current_location_latitude, driver[0].current_location_longitude,
                            session.insertId, new Date(), manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, splitFareKey, driver[0].car_type
                        ];
                        //console.log("VALUES FOR ENGAGEMENT: " + JSON.stringify(values));
                        connection.query(create_engagement, values, function(err, engagement) {
                            logging.logDatabaseQuery("Adding a active engagement to the session", err, engagement, null);

                            if (err) {
                                responses.errorResponse(res);
                                return;
                            }

                            // INSERT into split fare requests
                            var sql = "INSERT INTO `tb_split_fare_requests`(`split_fare_key`,`sent_by`,`sent_to`,`status`) VALUES(?,?,?,?)";
                            connection.query(sql, [splitFareKey, 0, customer_id, constants.splitFareRequestStatus.SPLITFARE_REQUEST_ACCEPTED], function(err, resultInsertSplitFareRequest) {
                                logging.logDatabaseQueryError("Add split fare request to DB", err, resultInsertSplitFareRequest);
                                if (err) {

                                    responses.errorResponse(res);
                                    return;
                                }

                                var response = {
                                    "log": "A manual active engagement has been created",
                                    "engagement": engagement.insertId
                                };
                                logging.logResponse(response);
                                if (typeof res != 'undefined') {
                                    res.send(JSON.stringify(response));
                                }

                                // Send a push notification to the customer and the driver that a
                                // manual engagement has been created for them
                                var message_user = 'A ' + config.get('projectName') + ' driver has been assigned to you';
                                var payload_user = {
                                    flag: constants.notificationFlags.CHANGE_STATE,
                                    message: 'A ' + config.get('projectName') + ' driver has been assigned to you'
                                };
                                var notificationFlag_user = 1;
                                utils.sendNotification(customer_id, message_user, notificationFlag_user, payload_user);

                                var message_driver = 'Please pick the passenger just assigned to you';
                                var payload_driver = {
                                    flag: constants.notificationFlags.CHANGE_STATE,
                                    message: 'Please pick the passenger just assigned to you'
                                };
                                var notificationFlag_driver = 1;
                                utils.sendNotification(driver_id, message_driver, notificationFlag_driver, payload_driver);

                            });


                        });

                        // Set the status of the driver to busy
                        var set_driver_busy_query = "UPDATE `tb_users` SET `status`=? WHERE `user_id`=?";
                        connection.query(set_driver_busy_query, [constants.userFreeStatus.BUSY, driver_id], function(err, results) {
                            logging.logDatabaseQuery("Setting driver status to busy", err, results, null);
                        });

                    });

                });
            });
        });
    });
};

// TODO this function has been created to handle the two different versions of the apps for the drivers
// TODO As we are using a new flag here, the function above will be removed when all the drivers are
// TODO at versions > 136
function addAutomaticManualEngagement(sessionId, customerId, driverId, latitude, longitude, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, callback) {
    //var customer = null;
    var driver = null;
    var address = "";

    async.parallel(
        [
            //getCustomerInfo.bind(null, customerId),
            getDriverInfo.bind(null, driverId),
            getLocationAddress.bind(null, latitude, longitude, address)
        ],
        function(err) {
            if (err) {
                console.log("An error has occurred when creating a manual engagement between the customer and driver");
                if (typeof callback == 'function') {
                    callback(err, null);
                }
                return;
            }

            // Change the ride acceptance flag for the session
            var updateSession = "UPDATE `tb_session` SET `ride_acceptance_flag` = ? WHERE `session_id` = ?";
            connection.query(updateSession, [constants.rideAcceptanceFlag.ACCEPTED, sessionId], function(err, result) {
                logging.logDatabaseQuery("Updating the ride acceptance flag for the session.", err, result);
            });

            // create a new engagement


            utils.generateSplitFareKey(function(err, splitFareKey) {
                if (err) {
                    sendErrorResponse(res);
                    return;
                }

                var createEngagement = "INSERT INTO `tb_engagements` " +
                    "(`user_id`, `driver_id`, `pickup_latitude`, `pickup_location_address`, `pickup_longitude`, `status`, " +
                    "`driver_accept_latitude`, `driver_accept_longitude`, `session_id`,`manual_destination_latitude`,`manual_destination_longitude`," +
                    "`manual_destination_address`,`split_fare_key`,`car_type`) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                var values = [customerId, driverId,
                    latitude, address, longitude,
                    constants.engagementStatus.ACCEPTED,
                    driver.current_location_latitude, driver.current_location_longitude,
                    sessionId, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, splitFareKey, driver.car_type
                ];

                connection.query(createEngagement, values, function(err, engagement) {
                    logging.logDatabaseQuery("Adding a active engagement to the session", err, engagement, null);

                    // Send notification to the customer that the ride has been accepted
                    sendRideAcceptanceNotificationToCustomer(customerId, driverId, sessionId, engagement.insertId);

                    // Send a push notification to the driver that a
                    // manual engagement has been created for them
                    var messageDriver = 'Please pick the passenger just assigned to you';
                    var payloadDriver = {
                        flag: constants.notificationFlags.MANUAL_ENGAGEMENT,
                        message: 'Please pick the passenger just assigned to you'
                    };
                    var notificationFlagDriver = 1;
                    utils.sendNotification(driverId, messageDriver, notificationFlagDriver, payloadDriver);

                    callback(err, engagement.insertId);
                });

                // Set the status of the driver to busy
                var makeDriverBusy = "UPDATE `tb_users` SET `status`=? WHERE `user_id`=?";
                connection.query(makeDriverBusy, [constants.userFreeStatus.BUSY, driverId], function(err, results) {
                    logging.logDatabaseQuery("Setting driver status to busy", err, results, null);
                });


            });
        }
    );

    //function getCustomerInfo(customerId, callback) {
    //    var query = "SELECT `user_id`, `user_name`, `user_email`, `phone_no` " +
    //        "FROM `tb_users` WHERE `user_id`=?";
    //    connection.query(query, [customerId], function (err, resultCustomer) {
    //        logging.logDatabaseQuery("Getting the information for the customer", err, resultCustomer, null);
    //        if(!err){
    //            customer = resultCustomer[0];
    //        }
    //        return callback(err);
    //    });
    //}

    function getDriverInfo(driverId, callback) {
        var getDriverInfo = "SELECT `user_id`, `user_name`, `user_email`, `phone_no`, `current_location_latitude`, `current_location_longitude`,`car_type` " +
            "FROM `tb_users` WHERE `user_id`=?";
        connection.query(getDriverInfo, [driverId], function(err, resultDriver) {
            logging.logDatabaseQuery("Getting the information for the driver", err, resultDriver, null);
            if (!err) {
                driver = resultDriver[0];
            }
            return callback(err);
        });
    }

    function getLocationAddress(latitude, longitude, address, callback) {
        utils.getLocationAddress(latitude, longitude, function(pickupAddress) {
            address = pickupAddress.slice(0);
            callback();
        });
    }
};


exports.acknowledge_manual_engagement = acknowledgeManualEngagement;

function acknowledgeManualEngagement(req, res) {
    logging.startSection("acknowledge_manual_engagement");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var engagementId = req.body.engagement_id;
    var customerId = req.body.customer_id;

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }

        var response = {
            flag: constants.responseFlags.ACTION_COMPLETE,
            log: "Acknowledged"
        };
        res.send(response);

        // Send a mail to Jugnoo support team to relay acknowledgement
        mailer.sendMailForManualEngagementAck(customerId, user[0].user_id);

        // The manual engagement created was acknowledged by the driver,
        // hence, clear the timeout created to send a "critical" mail to support
        if (g_maunal_engage_timeouts.hasOwnProperty(engagementId.toString())) {
            clearTimeout(g_maunal_engage_timeouts[engagementId.toString()]);
            delete g_maunal_engage_timeouts[engagementId.toString()];
        }
    });
}


exports.clear_engagements_for_driver = function(req, res) {
    logging.startSection("clear_engagements_for_driver");
    logging.logRequest(req);

    var driver_id = req.body.driver_id;

    var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED];
    var fetch_engagements = "SELECT `engagement_id`, `user_id`, `driver_id`, `status`, `session_id` " +
        "FROM `tb_engagements` " +
        "WHERE `driver_id` = ? AND `status` IN (" + status.toString() + ") ORDER BY `engagement_id` DESC";
    connection.query(fetch_engagements, [driver_id], function(err, engagements) {
        logging.logDatabaseQuery("Getting the active engagements for the driver", err, engagements, null);

        if (engagements.length > 0) {
            // Update the engagement
            var update_engagement = "UPDATE `tb_engagements` SET `status`=? WHERE `engagement_id`=?";
            var newstatus = constants.engagementStatus.ACCEPTED_THEN_REJECTED;
            var values = [newstatus, engagements[0].engagement_id];
            connection.query(update_engagement, values, function(err, result_update) {
                logging.logDatabaseQuery("Updated the status of engagement to " + utils.engagementStatusToString(newstatus), err, result_update, null);
            });

            // Update the session
            var update_session = "UPDATE `tb_session` SET `is_active`=?, `ride_acceptance_flag`=? WHERE `session_id`=?";
            var values = [constants.sessionStatus.INACTIVE, constants.rideAcceptanceFlag.ACCEPTED_THEN_REJECTED, engagements[0].session_id];
            connection.query(update_session, values, function(err, result_update) {
                logging.logDatabaseQuery("Updating the status and ride acceptance flag of session", err, result_update, null);
            });

            // Send notification to the customer
            //if (engagements[0].status == constants.engagementStatus.ACCEPTED){
            //    var message = "Sorry! The ride has been cancelled by the driver.";
            //    var flag = 1;
            //    var payload = {"flag": constants.notificationFlags.RIDE_REJECTED_BY_DRIVER};
            //    utils.sendNotification(customer_id, message, flag, payload);
            //}

            var message = "Sorry! The ride has been cancelled by the driver.";
            var flag = 1;
            var payload = {
                flag: constants.notificationFlags.CHANGE_STATE,
                message: 'Sorry! The ride has been cancelled by the driver.'
            };
            utils.sendNotification(engagements[0].user_id, message, flag, payload);
        }
        // Update the information for driver and set his status to free
        var update_driver = "UPDATE `tb_users` SET `status`=? WHERE `user_id`=?";
        var values = [constants.userFreeStatus.FREE, driver_id];
        connection.query(update_driver, values, function(err, result_update) {
            logging.logDatabaseQuery("Updated the status of driver to free", err, result_update, null);
        });

        var message_driver = 'All the engagements have been cleared';
        var payload_driver = {
            flag: constants.notificationFlags.CHANGE_STATE,
            message: 'All the engagements have been cleared'
        };
        var notificationFlag_driver = 1;
        utils.sendNotification(driver_id, message_driver, notificationFlag_driver, payload_driver);
    });

    var response = {
        log: "Clearing all the engagements for driver with ID: " + driver_id
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
};


exports.clear_engagements_for_customer = function(req, res) {
    logging.startSection("clear_engagements_for_customer");
    logging.logRequest(req);

    var customer_id = req.body.customer_id;


    // Get the status of the request
    var session_info = "SELECT `is_active`, `ride_acceptance_flag`, `session_id` FROM `tb_session` WHERE `user_id`=? ORDER BY `session_id` DESC LIMIT 1";
    connection.query(session_info, [customer_id], function(err, sessions) {
        logging.logDatabaseQuery("Get session for the customer", err, sessions, null);

        if (sessions.length > 0) {
            // If the session is not active and has timed out, send request cancelled response
            if (sessions[0].is_active == constants.sessionStatus.INACTIVE) {
                var response = {
                    log: "The previous session for the user is not active"
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            } else if (sessions[0].is_active == constants.sessionStatus.ACTIVE) {
                if (sessions[0].ride_acceptance_flag == constants.rideAcceptanceFlag.NOT_YET_ACCEPTED) {
                    // Change the status of the session
                    deactivateSession(sessions[0].session_id);

                    // Change the status of all active requests made
                    var update_engagements = "UPDATE `tb_engagements` SET `status`=? WHERE `session_id`=? && `status`=?";
                    var values = [constants.engagementStatus.CANCELLED_BY_CUSTOMER, sessions[0].session_id, constants.engagementStatus.REQUESTED];
                    connection.query(update_engagements, values, function(err, result_update) {
                        logging.logDatabaseQuery("Updating the status for the active engagements", err, result_update, null);
                    });
                } else if (sessions[0].ride_acceptance_flag == constants.rideAcceptanceFlag.ACCEPTED) {
                    // Change the status of the session
                    deactivateSession(sessions[0].session_id);

                    // Fetch this particular engagement for this session
                    var status = [constants.engagementStatus.ACCEPTED, constants.engagementStatus.STARTED];
                    var get_engagement = "SELECT `engagement_id`, `driver_id` FROM `tb_engagements` WHERE `session_id`=? && `status` IN (" + status.toString() + ")";
                    var values = [sessions[0].session_id];
                    connection.query(get_engagement, values, function(err, engagements) {
                        logging.logDatabaseQuery("Getting the active engagements for the session", err, engagements, null);

                        if (engagements.length > 0) {
                            var update_engagement = "UPDATE `tb_engagements` SET `status`=? WHERE `engagement_id`=?";
                            connection.query(update_engagement, [constants.engagementStatus.CANCELLED_ACCEPTED_REQUEST, engagements[0].engagement_id], function(err, result_update) {
                                logging.logDatabaseQuery("Updating the status for the active engagements", err, result_update, null);
                            });

                            // Set the status of the driver to Free
                            var update_driver_status = "UPDATE `tb_users` SET `status`=? WHERE `user_id`=?  LIMIT 1";
                            connection.query(update_driver_status, [constants.userFreeStatus.FREE, engagements[0].driver_id], function(err, result_update) {
                                logging.logDatabaseQuery("Set the status of the driver to Free", err, result_update, null);
                            });
                        }

                        // Send notification to the drivers
                        var message_driver = "Request cancelled by the customer";
                        var flag_driver = 2;
                        var payload_driver = {
                            flag: constants.notificationFlags.CHANGE_STATE,
                            message: 'Request cancelled by the customer'
                        };
                        utils.sendNotification(engagements[0].driver_id, message_driver, flag_driver, payload_driver);

                        var message_user = 'The ride has been cancelled';
                        var payload_user = {
                            flag: constants.notificationFlags.CHANGE_STATE,
                            message: 'The ride has been cancelled'
                        };
                        var notificationFlag_user = 1;
                        utils.sendNotification(customer_id, message_user, notificationFlag_user, payload_user);
                    });
                }
                // Send the response to the customer
                var response = {
                    log: "Successfully cleared the session " + sessions[0].session_id + " for the customer"
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            }

            mailer.sendMailForCancelledRequest(customer_id);
        } else {
            var response = {
                log: "There were no sessions for this customer"
            };
            logging.logResponse(response);
            res.send(JSON.stringify(response));
        }
    });
};




function getLastRideForRating(customerId, callback) {
    var ride = null;

    // TODO remove callback hell from this code
    var getLastSuccessfulRide =
        'SELECT `engagement_id`, `driver_id`, `driver_rating`, `skip_rating_by_customer`, ' +
        '`money_transacted` as `fare`, `paid_by_customer` as `to_pay`, `discount`, `distance_travelled`, `ride_time`, `account_id`, `is_payment_successful` , wait_time_seconds  as wait_time ' +
        'FROM `tb_engagements` ' +
        'WHERE `user_id` = ? AND `status` = 3 ' + // time when feature was made live
        'ORDER BY `drop_time` DESC ' +
        'LIMIT 1';
    connection.query(getLastSuccessfulRide, [customerId], function(err, engagement) {
        logging.logDatabaseQuery('Getting the last completed engagement for the customer', err, engagement);

        if (err) {
            process.stderr.write('An error occurred when getting the last completed engagement for the customer\n');
            return process.nextTick(callback.bind(null, err, ride));
        }

        if (engagement.length === 0) {
            //console.log(here);
            return process.nextTick(callback.bind(null, err, ride));
        }

        engagement = engagement[0];

        console.log("======================================================================================")
        console.log(engagement)
        console.log("======================================================================================")

        if (engagement.driver_rating !== -1 || engagement.skip_rating_by_customer === 1) {
            process.stdout.write('Has already rated/skipped the last ride\n');
            return process.nextTick(callback.bind(null, err, ride));
        }

        getCouponInformation(engagement.account_id, function(err, coupon) {
            engagement.coupon = coupon;

            ride = engagement;
            delete ride.driver_rating;
            delete ride.skip_rating_by_customer;
            delete ride.account_id;

            showRatingPopup(customerId, coupon, function(showRatingPopup) {
                ride.rate_us = showRatingPopup ? 1 : 0;

                getDriverInformation(engagement.driver_id, function(err, driver) {
                    ride.driver_info = driver;
                    delete ride.driver_id;

                    return process.nextTick(callback.bind(null, err, ride));
                });
            });
        });

        function getDriverInformation(driverId, callback) {
            var getInfo =
                'SELECT `user_id` AS `id`, `user_name` AS `name`, `user_image` ' +
                'FROM `tb_users` ' +
                'WHERE `user_id` = ?';
            connection.query(getInfo, [driverId], function(err, driver) {
                logging.logDatabaseQuery('Getting the information for the driver for the last ride', err, driver);

                return process.nextTick(callback.bind(null, err, driver[0]));
            });
        }
    });
};

function getCouponInformation(accountId, callback) {
    if (accountId === 0) {
        return process.nextTick(callback.bind(null, null, null));
    }

    var getCouponInfo =
        'SELECT * ' +
        'FROM ' +
        '(SELECT * FROM `tb_accounts` WHERE `account_id` = ?) as `account` ' +
        'JOIN ' +
        '`tb_coupons` as `coupons` ' +
        'WHERE account.coupon_id = coupons.coupon_id';
    connection.query(getCouponInfo, [accountId], function(err, coupon) {
        logging.logDatabaseQuery('Getting the information for the applied coupon', err, coupon);

        return process.nextTick(callback.bind(null, err, coupon[0]));
    });
}


exports.getCustomersInvolvedInEngagement = getCustomersInvolvedInEngagement;

function getCustomersInvolvedInEngagement(engagement_id, callback) {

    var sql = "SELECT a.user_id,a.engagement_id,a.car_type,a.split_fare_key,a.session_id,a.paid_by_customer+a.tip AS to_pay,a.payment_method,a.is_payment_successful,a.defaulter_flag,a.wait_time_seconds,a.wait_time_started," +
        "b.user_name,b.user_image , b.is_corporate FROM tb_engagements a,tb_users b WHERE a.user_id = b.user_id && a.split_fare_key = " +
        "(SELECT `split_fare_key` FROM `tb_engagements` WHERE `engagement_id`=? LIMIT 1)";
    connection.query(sql, [engagement_id], function(err, resultCustomers) { // status chk constants.engagementStatus.STARTED, removed
        logging.logDatabaseQuery('Get customers involved in engagements', err, resultCustomers);
        if (err) {
            return callback(err, resultCustomers);
        }
        callback(null, resultCustomers);
    });
}


function processEndAnEngagement(engagement_id, customer_id, total_fare, actual_fare, distance_travelled, ride_time, wait_time, drop_latitude, drop_longitude, driver_id, wait_time_charges,wait_time_seconds) {

    var get_engagement =
        "SELECT `session_id`, `accept_time`, `pickup_latitude`, `pickup_longitude`, `pickup_time`, `distance_travelled`, `money_transacted`," +
        "`wait_time`, `ride_time`, `status`, `account_id` FROM `tb_engagements` " +
        "WHERE `engagement_id`=? LIMIT 1";
    connection.query(get_engagement, [engagement_id], function(err, engagement) {
        logging.addEventToSession(engagement[0].session_id, {
            event: "Ending the ride for the engagement",
            engagement: engagement_id
        });

        engagement = engagement[0];

        //var update_driver_info = "UPDATE `tb_users` " +
        //    "SET `status`=? WHERE `user_id`=? LIMIT 1";
        //var values = [constants.userFreeStatus.FREE, driver_id];
        //connection.query(update_driver_info, values, function (err, result_update) {
        //    logging.logDatabaseQuery("Make driver free", err, result_update, null);
        //
        //    logging.addEventToSession(engagement.session_id, {
        //        event: "updating the status of the driver to free",
        //        engagement: driver_id
        //    });
        //
        //});

        // TODO change this to get the fare details from a variable


        // Whether the coupon exists or not is fetched at 4:00 hrs IST with respect to the accept time for the driver
        var get_coupons =
            "SELECT * " +
            "FROM " +
            "(SELECT * FROM `tb_accounts` WHERE `user_id`=? && `status`=? && TIMESTAMPADD(MINUTE, 240, `expiry_date`) > ? ORDER BY `expiry_date` ASC LIMIT 1) as `account` " +
            "JOIN " +
            "`tb_coupons` as `coupons` " +
            "WHERE account.coupon_id = coupons.coupon_id";
        connection.query(get_coupons, [customer_id, constants.couponStatus.ACTIVE, engagement.accept_time], function(err, coupons) {
            logging.logDatabaseQuery("Getting the coupons for the user", err, coupons, null);

            logging.addEventToSession(engagement.session_id, {
                event: "Getting the coupons for the customer",
                coupons: coupons
            });


            var discount = 0;
            var to_pay = total_fare;
            var money_owed = (total_fare) / 5;

            var account_id = 0;
            if (coupons.length > 0) {
                discount = total_fare * coupons[0].discount / 100 < coupons[0].maximum ? Math.floor(total_fare) * coupons[0].discount / 100 : coupons[0].maximum;
                to_pay -= discount;
                account_id = coupons[0].account_id;
            }
            //else {
            //    // The user took a paid ride
            //    // Give the user permissions to schedule later and change the pickup location
            //    //giveAdvancedPermissions(customer_id);
            //}

            logging.addEventToSession(engagement.session_id, {
                event: "fetching the ride distance using Google's distancematrix API"
            });

            updateRideDistanceFromGoogle(engagement_id, engagement.pickup_latitude, engagement.pickup_longitude, drop_latitude, drop_longitude);

            logging.addEventToSession(engagement.session_id, {
                event: "Getting the drop location from google"
            });

            var date = new Date();
            utils.getLocationAddress(drop_latitude, drop_longitude, function(drop_location_address) {
                logging.addEventToSession(engagement.session_id, {
                    event: "Received the drop location from google",
                    address: drop_location_address
                });

                // Update the engagement for this ride
                var update_engagement = "UPDATE `tb_engagements` " +
                    "SET `status`=?," +
                    "`drop_time`=?,`drop_latitude`=?,`drop_longitude`=?,`drop_location_address`=?," +
                    "`distance_travelled`=?,`ride_time`=?,`wait_time`=?,`money_transacted`=?,`actual_fare`=?,`account_id`=?," +
                    "`paid_by_customer`=?, `discount`=? , wait_time_charges = ? " +
                    "WHERE `engagement_id`=? LIMIT 1";
                var values = [constants.engagementStatus.ENDED,
                    date, drop_latitude, drop_longitude, drop_location_address,
                    distance_travelled, ride_time, wait_time, total_fare, actual_fare, account_id,
                    to_pay, discount, wait_time_charges,
                    engagement_id
                ];
                connection.query(update_engagement, values, function(err, result_update) {
                    logging.logDatabaseQuery("Updating the engagement for the ride completed", err, result_update, null);

                    logging.addEventToSession(engagement.session_id, {
                        event: "updating the status of the engagement to ENDED",
                        engagement: engagement_id
                    });
                });

                // Update the finances for the driver
                var update_driver_info = "UPDATE `tb_users` " +
                    "SET `todays_earnings`=`todays_earnings`+?,`total_earnings`= `total_earnings`+?,`total_money_owed`=`total_money_owed`+? " +
                    "WHERE `user_id`=? LIMIT 1";
                var values = [total_fare, total_fare, money_owed, driver_id];
                connection.query(update_driver_info, values, function(err, result_update) {
                    logging.logDatabaseQuery("Updating the finances for the driver", err, result_update, null);

                    logging.addEventToSession(engagement.session_id, {
                        event: "updating the status of the driver to free",
                        engagement: driver_id
                    });
                });

                var get_rides = "SELECT `user_name`, `total_rides_as_user`, `referred_by` FROM `tb_users` WHERE `user_id`=?";
                connection.query(get_rides, [customer_id], function(err, user) {
                    logging.logDatabaseQuery("Getting the number of rides for the customer", err, user);

                    logging.addEventToSession(engagement.session_id, {
                        event: "Getting the number of rides for the customer",
                        rides: user[0].total_rides_as_user
                    });

                    if (user[0].total_rides_as_user == 0 && user[0].referred_by != 0) {
                        var referring_user = {};

                        function getReferringUser(callback) {
                            var get_user = "SELECT `user_id`, `user_name`, `user_email`, `user_name` FROM `tb_users` WHERE `user_id`=?";
                            connection.query(get_user, [user[0].referred_by], function(err, result_user) {
                                logging.logDatabaseQuery("Getting the information for referring person", err, result_user);
                                referring_user = result_user[0];
                                callback();
                            });
                        }

                        function giveCouponAndSendMail() {
                            logging.addEventToSession(engagement.session_id, {
                                event: "Adding coupons for ride for the referring customer",
                                referred_by: referring_user.user_id
                            });

                            var insert_coupon =
                                "INSERT INTO `tb_accounts` " +
                                "(`user_id`, `coupon_id`, `added_on`, `expiry_date`, `reason`) " +
                                "VALUES (?, 1, NOW(), concat_ws(' ',DATE(DATE_ADD(NOW(), INTERVAL 30 DAY)), '18:29:59'), 'referral ride')";
                            connection.query(insert_coupon, [referring_user.user_id], function(err, result) {
                                if (err) {
                                    logging.logDatabaseQuery("Inserting coupon for: " + referring_user.user_id + "failed.", err, result);
                                } else {
                                    mailer.sendMailForFirstReferralRide(user[0].user_name, referring_user);
                                    //messenger.sendMessageForFirstReferralRide(user[0].user_name, referring_user);
                                }
                            });
                        }

                        async.series([getReferringUser], giveCouponAndSendMail);
                    }

                    // Update the finances for the customer
                    var update_customer_info = "UPDATE `tb_users` " +
                        "SET `todays_money_spent`=`todays_money_spent`+?,`total_money_spent`= `total_money_spent`+?," +
                        "`total_rides_as_user`=`total_rides_as_user`+?,`total_distance_as_user`=`total_distance_as_user`+?, " +
                        "`last_ride_on` = NOW() " +
                        "WHERE `user_id`=? LIMIT 1";
                    var values = [total_fare, total_fare, 1, distance_travelled, customer_id];
                    connection.query(update_customer_info, values, function(err, result_update) {
                        logging.logDatabaseQuery("Update the finances for the customer", err, result_update, null);
                    });
                });


                if (account_id > 0) {
                    var update_coupon = "UPDATE `tb_accounts` SET `status`=?, `redeemed_on`=NOW() WHERE `account_id`=?";
                    var values = [constants.couponStatus.REDEEMED, account_id];
                    connection.query(update_coupon, values, function(err, result) {
                        logging.logDatabaseQuery("Updating the status of the coupon", err, result, null);

                        logging.addEventToSession(engagement.session_id, {
                            event: "Updating the status of an entry in account to redeemed",
                            account: account_id
                        });
                    });
                }

                // Send a notification to the customer
                var coupon = coupons.length > 0 ? coupons[0] : null;
                var ride_summary = {
                    total_fare: total_fare,
                    to_pay: to_pay,
                    discount: discount,
                    distance_travelled: distance_travelled,
                    wait_time: wait_time_seconds,
                    ride_time: ride_time,
                    wait_time_charges: wait_time_charges,
                    coupon: coupon
                };
                sendRideCompleteNotificationToCustomer(engagement.session_id, customer_id, ride_summary);

                // Send mail to the customer and the support time
                var drop_info = {
                    latitude: drop_latitude,
                    longitude: drop_longitude,
                    address: drop_location_address
                };
                var fare_info = {
                    distance: distance_travelled,
                    wait_time: wait_time,
                    ride_time: ride_time,
                    total_fare: total_fare,
                    coupon_used: coupons.length > 0 ? true : false,
                    discount: discount,
                    to_pay: to_pay
                };
                mailer.sendMailForCompletedRide(engagement_id, customer_id, driver_id,
                    drop_info, fare_info);


                //logging.flushSession(engagement.session_id);
            });
        });
    });


}

function getEngagementDataFromSessionId(session_id, callback) {
    var sql = "SELECT `car_type` FROM `tb_engagements` WHERE `session_id`=? LIMIT 1";
    connection.query(sql, [session_id], function(err, result) {
        callback(result);
    });
}

//function makeDriverFree(req, res){
//
//}
