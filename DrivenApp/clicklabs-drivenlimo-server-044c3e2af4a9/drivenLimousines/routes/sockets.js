/**
 * Created by harsh on 12/14/14.
 */


var socketEvents = require('./socketEvents');

(function setUpSocketIoServer(){
    socketIoServer.on('connection', function(socket){
        console.log("A new connection has been made with the server");
    });
})();

(function sendTestMessages(){
    setInterval(function(){
        socketIoServer.emit('testMessage', 'Test ping coming from the server');
    }, 5000);
})();


//socketIoServer.configure(function(){
//    socketIoServer.set('authorization', function(handshakeData, callback){
//        callback(null, true);
//    });
//});

socketIoServer.of('/driverConnection').on('connection', function(socket){
    socketEvents.loadCommonEvents(socket);
    socketEvents.loadDriverEvents(socket);
});