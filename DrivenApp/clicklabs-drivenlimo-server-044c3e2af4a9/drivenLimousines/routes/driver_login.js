var generatePassword    = require('password-generator');

var utils       = require('./commonfunction');
var logging     = require('./logging');
var constants   = require('./constants');
var responses   = require('./responses');


exports.emailLogin          = emailLogin;
exports.accessTokenLogin    = accessTokenLogin;
exports.logout              = logout;
exports.forgotPassword      = forgotPassword;




function emailLogin(req, res)
{
    logging.startSection('email_login');
    logging.logRequest(req);

    var email       = req.body.email;
    var password    = req.body.password;
    var latitude    = req.body.latitude;
    var longitude   = req.body.longitude;
    var country     = req.body.country;
    var os_version  = req.body.os_version;
    var device_name = req.body.device_name;
    var appVersion  = req.body.app_version;
    var deviceToken = req.body.device_token;
    var deviceType  = req.body.device_type;

    var manValues = [email, password, latitude, longitude, deviceType];
    var checkdata = utils.checkBlank(manValues);

    var response = {};

    if (checkdata == 1)
    {
        responses.sendParameterMissingError(res);
        return;
    }

    var md5 = require('MD5');
    var hash = md5(password);
    var getUserInfo =
        'SELECT `user_id`, `user_email`, `reg_as` ' +
        'FROM `tb_users` ' +
        'WHERE `user_email` = ? LIMIT 1';
    connection.query(getUserInfo, [email], function(err, user)
    {
        logging.logDatabaseQuery('Getting the user information using email', err, user);

        if (user.length == 0 || user[0].reg_as == 0)
        {
            response = {
                flag    : constants.responseFlags.CUSTOMER_LOGGING_IN,
                error   : 'You are not a drivenLimousines driver yet.'};
            logging.logResponse(response);
            res.send(response);
            return;
        }

        var userId = user[0].user_id;

        var checkPassword =
            'SELECT `user_id`, `user_email`, `user_name`, `user_image`, `phone_no`, `access_token`, `referral_code`, `reg_as`, `current_user_status`,`verification_status`, `is_available`,`car_type` ' +
            'FROM `tb_users` ' +
            'WHERE `user_id`=? && `password`=? LIMIT 1';
        connection.query(checkPassword, [userId, hash], function(err, user)
        {
            checkAppVersion(userId, appVersion, deviceType, function(updateAppPopUp)
            {
                if (user.length == 0)
                {
                    response = {
                        flag    : constants.responseFlags.INCORRECT_PASSWORD,
                        error   : 'Incorrect password',
                        popup   : updateAppPopUp};
                    logging.logResponse(response);
                    res.send(response);
                    return;
                }

                user = user[0];

                var currentUserStatus;
                var updateUserStatus = '';

                var randomString = email+utils.generateRandomString();
                var newAccessToken = utils.encrypt(randomString);

                if (user.reg_as == 1 && user.verification_status == 1)
                {
                    currentUserStatus = constants.userCurrentStatus.DRIVER_ONLINE;
                    updateUserStatus =
                        'UPDATE `tb_users` ' +
                        'SET `access_token`=?,`current_user_status` = ?, `timestamp` = NOW() ,`is_available`=? '+
                        'WHERE `user_id` = ? LIMIT 1';
                    connection.query(updateUserStatus,[newAccessToken,currentUserStatus, 1, userId],function(err,result){
                        logging.logDatabaseQuery('Updating the current user status to \'online as driver\'', err, result);
                    });
                }
                else if(user.verification_status == 0){
                    var response1 = {"error": "Please enter OTP", "flag": 0, "popup": updateAppPopUp, "phone_no": user[0].phone_no};
                    logging.logResponse(response1);
                    res.send(JSON.stringify(response1));
                    return;
                }
                else{
                    response = {
                        flag    : constants.responseFlags.CUSTOMER_LOGGING_IN,
                        error   : 'You are not verified drivenLimousines driver.'};
                    logging.logResponse(response);
                    res.send(response);
                    return;
                }

                updateUserParams(deviceToken, latitude, longitude, country, os_version, device_name, deviceType, userId);

                exceptional_driver(userId,function(exceptional_driver){

                    var getFare = 'SELECT * FROM `tb_fare` WHERE `car_type`=? LIMIT 1';

                    connection.query(getFare, [user.car_type], function(err, fare_details){

                        response = {
                            flag      : constants.responseFlags.LOGIN_SUCCESSFUL,
                            user_data : {
                                access_token       : newAccessToken,
                                user_name          : user.user_name,
                                user_image         : user.user_image,
                                phone_no           : user.phone_no,
                                referral_code      : user.referral_code,
                                is_available       : user.is_available,
                                gcm_intent         : constants.GCM_INTENT_FLAG,
                                current_user_status: currentUserStatus,
                                fare_details       : fare_details,
                                exceptional_driver : exceptional_driver,
                                user_email         : user.user_email},
                            popup     : updateAppPopUp};
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                    });
                });
            });
        });
    });
}




function accessTokenLogin(req, res)
{
    logging.startSection('access_token_login');
    logging.logRequest(req);

    var f_login = false, login_response = {};
    var f_status = false, status_response = {};

    var accessToken     = req.body.access_token;

    var response = {};
    utils.authenticateUser(accessToken, function(result) {
        if(result == 0){
            response = {
                flag    : constants.responseFlags.INVALID_ACCESS_TOKEN,
                error   : 'Invalid access token'};
            logging.logResponse(response);
            res.send(response);
            return;
        }

        var user_id = result[0].user_id;
        var current_user_status = result[0].current_user_status;
        var ride_module = require('./ride_parallel_dispatcher');

        fetchLoginInformation(req, function (response) {
            f_login = true;
            login_response = response;
            sendAccumulatedResponse();
        });

        ride_module.getCurrentUserStatus(user_id, current_user_status, function (response) {
            f_status = true;
            status_response = response;
            sendAccumulatedResponse();
        });

        function sendAccumulatedResponse() {
            if (f_login && f_status) {
                var response = {};
                if(login_response.flag === constants.responseFlags.INVALID_ACCESS_TOKEN){
                    response = {
                        flag    : constants.responseFlags.INVALID_ACCESS_TOKEN,
                        error   : 'Invalid access token'};
                }
                else{
                    response = {
                        flag    : constants.responseFlags.LOGIN_SUCCESSFUL,
                        login   : login_response,
                        status  : status_response
                    };
                }
                logging.logResponse(response);
                res.send(response);
            }
        }
    });
};

function fetchLoginInformation(req, callback)
{
    var accessToken     = req.body.access_token;
    var latitude        = req.body.latitude;
    var longitude       = req.body.longitude;
    var deviceType      = req.body.device_type;
    var deviceToken     = req.body.device_token;
    var appVersion      = req.body.app_version;

    var getUserInfo =
        'SELECT `user_id`, `user_email`, `user_name`, `user_image`, `phone_no`, `access_token`, `referral_code`, `reg_as`, `current_user_status`, `is_available`,`car_type` ' +
        'FROM `tb_users` ' +
        'WHERE `access_token`=? LIMIT 1';
    connection.query(getUserInfo, [accessToken], function(err, user)
    {
        logging.logDatabaseQuery('Authenticating the user using the access token', err, user);
        if (user.length == 0) {
            var response = {
                flag    : constants.responseFlags.INVALID_ACCESS_TOKEN,
                error   : 'Invalid access token'};
            return process.nextTick(callback.bind(null, response));
        }

        user = user[0];

        checkAppVersion(user.user_id, appVersion, deviceType, function(updateAppPopUp)
        {
            var updateUserInfo =
                'UPDATE `tb_users` ' +
                'SET `user_device_token` = ?, `current_location_latitude` = ?, `current_location_longitude` = ?, `device_type` = ? ' +
                'WHERE `user_id`=? ' +
                'LIMIT 1';
            connection.query(updateUserInfo, [deviceToken, latitude, longitude, deviceType, user.user_id], function(err, result) {
                logging.logDatabaseQuery('Updating the information for the user', err, result, null);
            });

            exceptional_driver(user.user_id, function(exceptional_driver){
                var getFare = 'SELECT * FROM `tb_fare` WHERE `car_type`=? LIMIT 1';

                connection.query(getFare, [user.car_type], function(err, fare_details){

                    console.log(fare_details);
                    response = {
                        user_data   : {
                            access_token       : accessToken,
                            user_name          : user.user_name,
                            user_image         : user.user_image,
                            phone_no           : user.phone_no,
                            referral_code      : user.referral_code,
                            is_available       : user.is_available,
                            gcm_intent         : constants.GCM_INTENT_FLAG,
                            current_user_status: user.current_user_status,
                            fare_details       : fare_details,
                            exceptional_driver : exceptional_driver,
                            user_email         : user.user_email},
                        popup       : updateAppPopUp};

                    return process.nextTick(callback.bind(null, response));
                });
            });
        });
    });
}




function logout(req, res)
{
    var accessToken = req.body.access_token;

    var manValues = [accessToken];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0)
        {
            responses.sendAuthenticationError(res);
            return;
        }

        user = user[0];

        var userId = user.user_id;

            var logoutUser =
                'UPDATE `tb_users` ' +
                'SET `current_user_status` = ?, `user_device_token` = ? ' +
                'WHERE `user_id` = ?';
            connection.query(logoutUser, [constants.userCurrentStatus.OFFLINE, '', userId], function(err, result)
            {
                var response = {
                    flag    : constants.responseFlags.LOGOUT_SUCCESSFUL,
                    log     : 'successfully logged off.'};
                logging.logResponse(response);
                res.send(response);
            });
    });
}




function forgotPassword(req, res)
{
    logging.startSection('forgot_password');
    logging.logRequest(req);

    var email       = req.body.email;
    var manValues   = [email];
    var isBlank     = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.sendParameterMissingError(res);
        return;
    }

    var getUserInfo =
        'SELECT `user_id`, `user_name`, `reg_as` ' +
        'FROM `tb_users` ' +
        'WHERE `user_email` = ? ' +
        'LIMIT 1';
    connection.query(getUserInfo, [email], function(err, user) {
        logging.logDatabaseQuery('Getting user information from email', err, user);

        var response = {};

        if(user.length == 0){
            response = {
                flag    : constants.responseFlags.NO_SUCH_USER,
                error   : 'This email is not registered with us.'};
            logging.logResponse(response);
            res.send(response);
            return;
        }

        user = user[0];
        if(user.reg_as == 0){
            response = {
                flag    : constants.responseFlags.CUSTOMER_LOGGING_IN,
                error   : 'You are not registered as a driver with us.'};
            logging.logResponse(response);
            res.send(response);
            return;
        }

        var password = generatePassword(6, false, /\d/);
        var md5 = require('MD5');
        var encryptedPass = md5(password);
        var name = user.user_name.split(' ')[0];

        var to = [];
        to.push(email);
        var cc = [];
        var bcc = [];

        var sub = 'Your new password for drivenLimousines';
        var msg =
            'Hi ' + name + ',\n\n' +
            'You seem to have forgotten your password for your account.\n' +
            'Your new password is: ' + password + '\n\n\n' +
            'Best,\n' +
            'drivenLimousines Team\n' ;
        utils.sendPlainTextEmail(to, cc, bcc, sub, msg, function(err, result){
            if(!err){
                var updatePassword =
                    'UPDATE tb_users ' +
                    'SET password = ? ' +
                    'WHERE user_email = ? LIMIT 1';
                connection.query(updatePassword, [encryptedPass, email], function(err, result) {
                    response = {
                        flag    : constants.responseFlags.ACTION_COMPLETE,
                        message : 'New password has been sent to your mail'};
                    logging.logResponse(response);
                    res.send(response);
                });
            }
            else{
                response = {
                    flag    : constants.responseFlags.ACTION_FAILED,
                    error   : 'The new password couldn\'t be sent to your email at this moment. Please try again.'};
                logging.logResponse(response);
                res.send(response);
            }
        });
    });
};



/*
 * -----------------------------------------------------------------------------
 * Check whether update popup has to be shown or not
 * INPUT : userId, version
 * OUTPUT : popup shown for update
 * -----------------------------------------------------------------------------
 */
function checkAppVersion(userId, userAppVersion, deviceType, callback)
{
    var getVersionInfo =
        'SELECT `id`, `current_ios_version`, `current_android_version`, `last_force_android_version`, `last_force_ios_version`, `current_ios_is_force`, `current_android_is_force` ' +
        'FROM `tb_version` LIMIT 1';
    connection.query(getVersionInfo, function(err, result){
        logging.logDatabaseQuery('Getting the version information for the app', err, result);

        var updateAppVersion =
            'UPDATE `tb_users` ' +
            'SET `app_versioncode` = ? ' +
            'WHERE `user_id`=? LIMIT 1';
        connection.query(updateAppVersion, [userAppVersion, userId], function(err, result) {
        });

        if(result.length > 0){
            var popup = {
                title   : 'Update Version',
                text    : 'Update app with new version!'
            };
            if(deviceType==0){          // android
                if(userAppVersion < result[0].current_android_version){
                    popup.cur_version = result[0].current_android_version;
                    if(result[0].current_android_is_force == 1){
                        popup.is_force = 1;
                    }
                    else if(userAppVersion < result[0].last_force_android_version){
                        popup.is_force = 1;
                    }
                    else{
                        popup.is_force = 0;
                    }
                    return callback(popup);
                }
                else{
                    return callback(0);
                }
            }
            else{                       // iOS
                if(userAppVersion < result[0].current_ios_version){
                    popup.cur_version = result[0].current_ios_version;
                    if(result[0].current_ios_is_force == 1){
                        popup.is_force = 1;
                    }
                    else if(userAppVersion < result[0].last_force_ios_version){
                        popup.is_force = 1;
                    }
                    else{
                        popup.is_force = 0;
                    }
                    return callback(popup);
                }
                else{
                    return callback(0);
                }
            }            
        }
    });

}

function updateUserParams(deviceToken, latitude, longitude, country, osVersion, deviceName, deviceType, userId)
{
    var date = new Date();
    var sql =
        "UPDATE tb_users " +
        "SET user_device_token = ?, current_location_latitude = ?, current_location_longitude = ?, country = ?, " +
        "os_version = ?, device_name = ?, device_type = ?, last_login = ? " +
        "WHERE user_id = ? " +
        "LIMIT 1";
    connection.query(sql, [deviceToken, latitude, longitude, country, osVersion, deviceName, deviceType, date, userId], function(err, result) {
    });
}


function exceptional_driver(user_id,callback){
    var sql="SELECT `user_id` FROM `tb_exceptional_users` WHERE `user_id`=? LIMIT 1";
    connection.query(sql,[user_id],function(err,result){ 
        if(result.length > 0){
            return callback(1);
        }
        else{
            return callback(0);
        }
    });                
}


exports.editDriverProfile  =  function (req,res){
    logging.startSection("edit_driver_profile");
    logging.logRequest(req);

    var accessToken     = req.body.access_token;
    var userName     = req.body.user_name;
    var imageFlag = req.body.image_flag;
    var bankAccountStripeToken = req.body.bank_account_stripe_token;

    var manValues   = [accessToken,userName,imageFlag];
    var isBlank     = utils.checkBlank(manValues);
    if (isBlank == 1)
    {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function (user){
        if (user == 0)
        {
            responses.sendAuthenticationError(res);
            return;
        }
        var userId = user[0].user_id;
        if(imageFlag==1){
            var randomText = utils.generateRandomString()
            req.files.image.name = randomText + "-" + req.files.image.name;
            utils.uploadImageFileToS3Bucket(req.files.image, config.get('s3BucketCredentials.folder.userProfileImages'), function(result_image) {
                if (result_image != 0) {
                    var newUserImage = config.get('s3BucketCredentials.s3URL')+'/'+config.get('s3BucketCredentials.folder.userProfileImages')+'/'+result_image;
                    var sql = "UPDATE `tb_users` SET `user_name`=?,`user_image`=? WHERE `user_id`=? LIMIT 1";
                    connection.query(sql,[userName,newUserImage,userId],function(err,result){
                        if(err){
                            logging.logDatabaseQueryError("Error when editing driver profile", err, result);
                            responses.sendError(res);
                            return;
                        }
                        var response ={
                            "log" : "Profile has been updated successfully",
                            "user_image" : newUserImage
                        };
                        res.send(response);
                    });
                }
            });
        }
        else{
            var sql = "UPDATE `tb_users` SET `user_name`=? WHERE `user_id`=? LIMIT 1";
            connection.query(sql,[userName,userId],function(err,result){
                if(err){
                    logging.logDatabaseQueryError("Error when editing driver profile", err, result);
                    responses.sendError(res);
                    return;
                }
                var response ={
                    "log" : "Profile has been updated successfully"
                };
                res.send(response);
            });
        }

        if(bankAccountStripeToken != ''){
            var sql ="UPDATE `tb_users` SET `bank_account_stripe_token`=? WHERE `user_id`=? LIMIT 1";
            connection.query(sql,[bankAccountStripeToken,userId],function(err,result){
                if(err){
                    logging.logDatabaseQueryError("Error when editing driver profile", err, result);
                }
            });
        }

    });
}



