var request = require('request');
var async = require('async');

var utils = require('./commonfunction');
var logging = require('./logging');
var mailer = require('./mailer');
var messenger = require('./messenger');
var constants = require('./constants');
var responses = require('./responses');
var stationing = require('./stationing');


function sendErrorResponse(res) {
    var response = {
        "error": 'something went wrong',
        "flag": constants.responseFlags.ERROR_IN_EXECUTION
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
}

function sendParameterMissingResponse(res) {
    var response = {
        "error": "some parameter missing",
        "flag": constants.responseFlags.PARAMETER_MISSING
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
}

function sendAuthenticationError(res) {
    var response = {
        "error": 'invalid access token',
        "flag": constants.responseFlags.INVALID_ACCESS_TOKEN
    };
    logging.logResponse(response);
    res.send(JSON.stringify(response));
}

exports.sendAuthenticationError = sendAuthenticationError;
exports.sendParameterMissingResponse = sendParameterMissingResponse;

/*
 ALL THE APIs TO GET THINGS DONE MANUALLY ARE HERE
 */
exports.create_driver_dispatcher = function(req, res) {
    logging.startSection("create_driver_dispatcher");
    logging.logRequest(req);

    var access_token = req.body.access_token;
    var customer_id = req.body.customer_id; // 32 : Dummy user_id account in tb_users

    var customer_name = req.body.customer_name;
    var customer_phone = req.body.customer_phone;

    var latitude = req.body.pickup_latitude;
    var longitude = req.body.pickup_longitude;

    var manualDestinationLatitude = req.body.manual_destination_latitude;
    var manualDestinationLongitude = req.body.manual_destination_longitude;
    var manualDestinationAddress = req.body.manual_destination_address;

    var isBlank = utils.checkBlank([customer_id, latitude, longitude]);
    if (isBlank) {
        sendParameterMissingResponse(res);
    } else {
        utils.authenticateUser(access_token, function(result) {
            if (result == 0) {
                sendAuthenticationError(res);
            } else {
                var driver_id = result[0].user_id;
                var get_driver_status = "SELECT `status` FROM `tb_users` WHERE `user_id`=?";
                connection.query(get_driver_status, [driver_id], function (err, drivers) {
                    logging.logDatabaseQuery("Getting the status of the driver", err, drivers, null);

                    if (drivers[0].status == constants.userFreeStatus.BUSY) {
                        var response = {
                            error: 'The driver is busy'
                        };
                        res.send(response);
                    } else {
                        addManualEngagement(customer_id, customer_name, customer_phone, latitude, longitude, driver_id, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, res);
                    }
                });
            }
        });
    }
};

function addManualEngagement(customer_id, customer_name, customer_phone, latitude, longitude, driver_id, manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, res) {
    var get_customer_info = "SELECT `user_id`, `user_name`, `user_email`, `phone_no` " +
        "FROM `tb_users` WHERE `user_id`=?";
    connection.query(get_customer_info, [customer_id], function(err, customer) {
        logging.logDatabaseQuery("Getting the information for the customer", err, customer, null);

        var get_driver_info = "SELECT `user_id`, `user_name`, `user_email`, `phone_no`, `current_location_latitude`, `current_location_longitude`,`car_type` " +
            "FROM `tb_users` WHERE `user_id`=?";
        connection.query(get_driver_info, [driver_id], function(err, driver) {
            logging.logDatabaseQuery("Getting the information for the customer", err, driver, null);

            utils.getLocationAddress(latitude, longitude, function(pickup_address) {

                    utils.generateSplitFareKey(function(err, splitFareKey) {
                        if (err) {
                            sendErrorResponse(res);
                            return;
                        }

                        var driver_dipatch = "INSERT INTO `tb_driver_dispatcher` (`driver_id`,`customer_id`,`customer_name`,`customer_phone`,`latitude`,`longitude`)" +
                            " VALUES (?,?,?,?,?,?)";
                        connection.query(driver_dipatch, [driver_id, customer_id, customer_name, customer_phone, latitude, longitude], function (err, dispatcherDrivers) {
                            logging.logDatabaseQuery("INSERTING tb_driver_disptacher INFROMATION", err, dispatcherDrivers, null);


                            var create_engagement = "INSERT INTO `tb_engagements` " +
                                "(`driver_dispatcher_id`,`user_id`, `driver_id`, `pickup_latitude`, `pickup_location_address`, `pickup_longitude`, `status`, `driver_accept_latitude`, `driver_accept_longitude`, `session_id`, `accept_time`," +
                                "`manual_destination_latitude`,`manual_destination_longitude`,`manual_destination_address`,`split_fare_key`,`car_type`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                            var values = [dispatcherDrivers.insertId, customer_id, driver_id,
                                latitude, pickup_address, longitude,
                                constants.engagementStatus.ACCEPTED,
                                driver[0].current_location_latitude, driver[0].current_location_longitude,
                                0, new Date(), manualDestinationLatitude, manualDestinationLongitude, manualDestinationAddress, splitFareKey, driver[0].car_type
                            ];
                            //console.log("VALUES FOR ENGAGEMENT: " + JSON.stringify(values));
                            connection.query(create_engagement, values, function (err, engagement) {
                                console.log(err)
                                logging.logDatabaseQuery("Adding a active engagement to the session", err, engagement, null);
                                if (err) {
                                    responses.errorResponse(res);
                                    return;
                                }
                                // INSERT into split fare requests
                                var sql = "INSERT INTO `tb_split_fare_requests`(`split_fare_key`,`sent_by`,`sent_to`,`status`) VALUES(?,?,?,?)";
                                connection.query(sql, [splitFareKey, 0, customer_id, constants.splitFareRequestStatus.SPLITFARE_REQUEST_ACCEPTED], function (err, resultInsertSplitFareRequest) {
                                    logging.logDatabaseQueryError("Add split fare request to DB", err, resultInsertSplitFareRequest);
                                    if (err) {
                                        responses.errorResponse(res);
                                        return;
                                    }

                                    var response = {
                                        "log": "A manual active engagement has been created",
                                        "engagement": engagement.insertId,
                                        "latitude": latitude,
                                        "longitude": longitude
                                    };
                                    logging.logResponse(response);
                                    if (typeof res != 'undefined') {
                                        res.send(JSON.stringify(response));
                                    }


                                    var message_driver = 'Please pick the passenger just assigned to you';
                                    var payload_driver = {
                                        flag: constants.notificationFlags.CHANGE_STATE,
                                        message: 'Please pick the passenger just assigned to you'
                                    };
                                    var notificationFlag_driver = 1;
                                    utils.sendNotification(driver_id, message_driver, notificationFlag_driver, payload_driver);

                                });
                                // Set the status of the driver to busy
                                var set_driver_busy_query = "UPDATE `tb_users` SET `status`=? WHERE `user_id`=?";
                                connection.query(set_driver_busy_query, [constants.userFreeStatus.BUSY, driver_id], function (err, results) {
                                    logging.logDatabaseQuery("Setting driver status to busy", err, results, null);
                                });

                            });



                        });
                    });
            });
        });
    });
};

