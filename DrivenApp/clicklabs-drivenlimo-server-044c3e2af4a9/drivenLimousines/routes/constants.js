/**
 * The node-module to hold the constants for the server
 */


function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}

var debugging = false;

define(exports, "SUPER_ADMIN_PASSWORD",         'auyq38yr9fsdjfw38');

define(exports, "NUM_DRIVER_REQUESTS_BATCH",    5);
define(exports, "NUM_ADHOC_FIRST_BATCH",        5);
define(exports, "NUM_DEDICATED_FIRST_BATCH",    0);
define(exports, "NUM_DEDICATED_REQUESTS_BATCH", 2);
define(exports, "PICKUP_RADIUS",                2500);               // in meters
define(exports, "PICKUP_RADIUS_MANUAL",         12000);               // in meters
define(exports, "DEDICATED_PICKUP_RADIUS",      12000);               // in meters
define(exports, "NUM_TRIES_ALLOCATE_DRIVER",    4);
define(exports, "TRIES_TIMEOUT",                60000);              // in milliseconds
define(exports, "SCHEDULER_ALARM_TIME",         30);                 // in minutes
define(exports, "MANUAL_NUM_TRIES",             2);
define(exports, "PROXIMITY_RADIUS",             400);
define(exports, "EMPTY_ACCURACY",              -999);
define(exports, "SERVICE_QUALITY_WINDOW",       60);                // in minutes
define(exports, "SCHEDULE_CANCEL_WINDOW",       40);                // in minutes
define(exports, "SCHEDULING_LIMIT",             60);                // in minutes
define(exports, "SCHEDULE_NOTIFICATION_TIME",   60);                // in minutes
define(exports, "SCHEDULE_PROCESS_INTERVAL",    4);                 // in minutes

define(exports, "GCM_INTENT_FLAG",              1);
define(exports, "NUKKAD_ENABLE_FLAG",           0);
define(exports, "NUKKAD_BUTTON_ICON",           'https://jugnoo.in/items/jugnoo_shop_button_icon.png');
define(exports, "NUKKAD_BUTTON_ICON_IOS",       'https://jugnoo.in/items/jugnoo_shop_button_icon_ios.png');

// Free rides on registration
define(exports, "REGISTRATION_FREE_RIDE",           1);         // 0 for off and 1 for on
define(exports, "REGISTRATION_FREE_RIDE_VALIDITY",  2);         // in days

// enums
exports.batchType = {};
define(exports.batchType, "NEAREST",          0);
define(exports.batchType, "ADHOC",            1);
define(exports.batchType, "DEDICATED",        2);
define(exports.batchType, "MANUAL",           3);

exports.userCurrentStatus = {};
define(exports.userCurrentStatus, "OFFLINE",            0);
define(exports.userCurrentStatus, "DRIVER_ONLINE",      1);
define(exports.userCurrentStatus, "CUSTOMER_ONLINE",    2);

exports.userFreeStatus = {};
define(exports.userFreeStatus, "FREE",    0);
define(exports.userFreeStatus, "BUSY",    1);

exports.userVerificationStatus = {};
define(exports.userVerificationStatus, "NotVerified",    0);
define(exports.userVerificationStatus, "Verified",       1);
define(exports.userVerificationStatus, "Blocked",        1);

exports.userRegistrationStatus = {};
define(exports.userRegistrationStatus, "CUSTOMER",          0);
define(exports.userRegistrationStatus, "DRIVER",            1);
define(exports.userRegistrationStatus, "DEDICATED_DRIVER",  2);

exports.engagementStatus = {};
define(exports.engagementStatus, "REQUESTED",                   0 );    // request has been sent
define(exports.engagementStatus, "ACCEPTED",                    1 );    // request has been accepted by the driver
define(exports.engagementStatus, "STARTED",                     2 );    // ride has started
define(exports.engagementStatus, "ENDED",                       3 );    // ride has ended
define(exports.engagementStatus, "REJECTED_BY_DRIVER",          4 );    // request rejected by driver
define(exports.engagementStatus, "CANCELLED_BY_CUSTOMER",       5 );    // request cancelled by customer
define(exports.engagementStatus, "TIMEOUT",                     6 );    // request timed out
define(exports.engagementStatus, "ACCEPTED_BY_OTHER_DRIVER",    7 );    // request was accepted by another driver
define(exports.engagementStatus, "ACCEPTED_THEN_REJECTED",      8 );    // request was accepted and then rejected
define(exports.engagementStatus, "CLOSED",                      9 );    // request was closed when the driver accepted other request
define(exports.engagementStatus, "CANCELLED_ACCEPTED_REQUEST",  10);    // request was cancelled after it was accepted by a driver

exports.deviceType = {};
define(exports.deviceType, "ANDROID",   0);
define(exports.deviceType, "iOS",       1);

exports.sessionStatus = {};
define(exports.sessionStatus, "INACTIVE",   0);
define(exports.sessionStatus, "ACTIVE",     1);
define(exports.sessionStatus, "TIMED_OUT",  2);

exports.notificationFlags = {};
define(exports.notificationFlags, "REQUEST",                        0 );  //driver
define(exports.notificationFlags, "REQUEST_TIMEOUT",                1 );  //driver
define(exports.notificationFlags, "REQUEST_CANCELLED",              2 );  //driver
define(exports.notificationFlags, "RIDE_STARTED",                   3 );  //customer
define(exports.notificationFlags, "RIDE_ENDED",                     4 );  //customer
define(exports.notificationFlags, "RIDE_ACCEPTED",                  5 );  //customer
define(exports.notificationFlags, "RIDE_ACCEPTED_BY_OTHER_DRIVER",  6 );  //driver
define(exports.notificationFlags, "RIDE_REJECTED_BY_DRIVER",        7 );  //customer
define(exports.notificationFlags, "NO_DRIVERS_AVAILABLE",           8 );  //customer
define(exports.notificationFlags, "WAITING_STARTED",                9 );  //customer
define(exports.notificationFlags, "WAITING_ENDED",                  10);  //customer
define(exports.notificationFlags, "SPLIT_FARE_REQUEST",             11);
define(exports.notificationFlags, "CHANGE_STATE",                   20);
define(exports.notificationFlags, "DISPLAY_MESSAGE",                21);
define(exports.notificationFlags, "TOGGLE_LOCATION_UPDATES",        22);
define(exports.notificationFlags, "MANUAL_ENGAGEMENT",              23);
define(exports.notificationFlags, "HEARTBEAT",                      40);
define(exports.notificationFlags, "STATION_CHANGED",                50);
define(exports.notificationFlags, "PAYMENT_INFO_TO_DRIVER",         51);

exports.responseFlags = {};
define(exports.responseFlags, "PARAMETER_MISSING",              100);
define(exports.responseFlags, "INVALID_ACCESS_TOKEN",           101);
define(exports.responseFlags, "ERROR_IN_EXECUTION",             102);
define(exports.responseFlags, "SHOW_ERROR_MESSAGE",             103);
define(exports.responseFlags, "SHOW_MESSAGE",                   104);
define(exports.responseFlags, "ASSIGNING_DRIVERS",              105);
define(exports.responseFlags, "NO_DRIVERS_AVAILABLE",           106);
define(exports.responseFlags, "RIDE_ACCEPTED",                  107);        // Duplicate payload in case of data loss
define(exports.responseFlags, "RIDE_ACCEPTED_BY_OTHER_DRIVER",  108);
define(exports.responseFlags, "RIDE_CANCELLED_BY_DRIVER",       109);
define(exports.responseFlags, "REQUEST_REJECTED",               110);
define(exports.responseFlags, "REQUEST_TIMEOUT",                111);
define(exports.responseFlags, "REQUEST_CANCELLED",              112);
define(exports.responseFlags, "SESSION_TIMEOUT",                113);
define(exports.responseFlags, "RIDE_STARTED",                   114);
define(exports.responseFlags, "RIDE_ENDED",                     115);
define(exports.responseFlags, "WAITING",                        116);
define(exports.responseFlags, "USER_OFFLINE",                   130);
define(exports.responseFlags, "NO_ACTIVE_SESSION",              131);
define(exports.responseFlags, "ENGAGEMENT_DATA",                132);
define(exports.responseFlags, "ACTIVE_REQUESTS",                133);
define(exports.responseFlags, "COUPONS",                        140);
define(exports.responseFlags, "SCHEDULED_PICKUPS",              141);
define(exports.responseFlags, "ACK_RECEIVED",                   142);
define(exports.responseFlags, "ACTION_COMPLETE",                143);
define(exports.responseFlags, "ACTION_FAILED",                  144);
define(exports.responseFlags, "LOGIN_SUCCESSFUL",               150);
define(exports.responseFlags, "INCORRECT_PASSWORD",             151);
define(exports.responseFlags, "CUSTOMER_LOGGING_IN",            152);
define(exports.responseFlags, "LOGOUT_SUCCESSFUL",              153);
define(exports.responseFlags, "LOGOUT_FAILURE",                 154);
define(exports.responseFlags, "NO_SUCH_USER",                   155);
define(exports.responseFlags, "STATION_ASSIGNED",               170);
define(exports.responseFlags, "NO_STATION_ASSIGNED",            171);
define(exports.responseFlags, "NO_STATION_AVAILABLE",           172);
define(exports.responseFlags, "DRIVER_LOGGING_IN",              173);       // When driver tries to login to customer app
define(exports.responseFlags, "WAITING_STARTED",                174);
define(exports.responseFlags, "WAITING_ENDED",                 	175);
define(exports.responseFlags, "WAIT_FOR_ACTIVATION_FLAG",       176);


exports.rideAcceptanceFlag = {};
define(exports.rideAcceptanceFlag, "NOT_YET_ACCEPTED",          0);
define(exports.rideAcceptanceFlag, "ACCEPTED",                  1);
define(exports.rideAcceptanceFlag, "ACCEPTED_THEN_REJECTED",    2);

exports.couponType = {};
define(exports.couponType, "FREE_RIDE",      1);

exports.couponStatus = {};
define(exports.couponStatus, "EXPIRED",      0);
define(exports.couponStatus, "ACTIVE",       1);
define(exports.couponStatus, "REDEEMED",     2);

exports.promoType = {};
define(exports.promoType, "REGISTRATION",    0);
define(exports.promoType, "IN_APP",          1);
define(exports.promoType, "ALL_TIME",        2);

exports.infoSection = {};
define(exports.infoSection, "ABOUT",         0);
define(exports.infoSection, "FAQs",          1);
define(exports.infoSection, "PRIVACY",       2);
define(exports.infoSection, "TERMS",         3);
define(exports.infoSection, "FARE",          4);
define(exports.infoSection, "SCHEDULES_TNC", 5);

exports.blockingReasons = {};
define(exports.blockingReasons, "SUSPECTED_NUMBER",     0);
define(exports.blockingReasons, "INVALID_EMAIL",        1);
define(exports.blockingReasons, "TERMS_VIOLATION",      2);


define(exports, "NOTIFICATION_BLAST_PASSWORD",  "jugnoo#notify#pass1");

exports.notificationFilters = {};
define(exports.notificationFilters, "TEST",                 0);
define(exports.notificationFilters, "ALL",                  1);
define(exports.notificationFilters, "COUPON_BUT_NO_RIDE",   2);
define(exports.notificationFilters, "NO_COUPON_NO_RIDE",    3);
define(exports.notificationFilters, "COUPONS_EXPIRING",     4);


define(exports, "EMAIL_BLAST_PASSWORD",         "jugnoo#email#pass1");

exports.emailFilters = {};
define(exports.emailFilters, "TEST",                 0);
define(exports.emailFilters, "ALL",                  1);
define(exports.emailFilters, "COUPON_BUT_NO_RIDE",   2);
define(exports.emailFilters, "NO_COUPON_NO_RIDE",    3);

define(exports, "SCHEDULE_CURRENT_TIME_DIFF",  60);     // in minutes   //60 earlier
define(exports, "SCHEDULE_DAYS_LIMIT",          1);     // in days
define(exports, "SCHEDULE_CANCEL_LIMIT",       60);     // in minutes    // 60 earlier

exports.scheduleStatus = {};
define(exports.scheduleStatus, "IN_QUEUE",    0);      // the ride will be scheduled at the appropriate time
define(exports.scheduleStatus, "IN_PROCESS",  1);      // the schedule has been picked up for processing
define(exports.scheduleStatus, "PROCESSED",   2);      // the schedule has already been processed, and is in past the current time
define(exports.scheduleStatus, "COULD_NOT_PROCESS",   3);      // the schedule has already been processed, and is in past the current time


exports.FILE_TYPE = {};
define(exports.FILE_TYPE, "RIDE_PATH",         0);      // the ride will be scheduled at the appropriate time

// All constants and enums related to the stationing logic for drivers
define(exports, "STATION_NULL",               0);
define(exports, "STATIONING_ACK_WINDOW",      180);         // in seconds
define(exports, "STATION_LEAVING_WINDOW",     300);         // in seconds
define(exports, "STATION_PROXIMITY",          500);         // in meters
define(exports, "STATIONING_ARRIVAL_BUFFER",  2.0);         // ratio of buffer for arrival time
define(exports, "STATIONING_ARRIVAL_DEFAULT", 600);         // default arrival window of 10 minutes in case google query doesn't work

exports.driverStationingStatus = {};
define(exports.driverStationingStatus, "NO_STATION",    0);
define(exports.driverStationingStatus, "ASSIGNED",      1);
define(exports.driverStationingStatus, "ACKNOWLEDGED",  2);
define(exports.driverStationingStatus, "REACHED",       3);
define(exports.driverStationingStatus, "LEAVING",       4);
define(exports.driverStationingStatus, "LEFT",          5);


exports.splitFareRequestStatus = {};
define(exports.splitFareRequestStatus, "SPLITFARE_REQUEST_SENDING_SUCCESSFUL",              0);
define(exports.splitFareRequestStatus, "SPLITFARE_REQUEST_ACCEPTED",                        1); // Accepted and in ride will be same
define(exports.splitFareRequestStatus, "SPLITFARE_REQUEST_REJECTED",                        2);
define(exports.splitFareRequestStatus, "SPLITFARE_REQUEST_TIMEOUT",                         3);


exports.UserResponseToSplitFareRequest = {};
define(exports.UserResponseToSplitFareRequest, "ACCEPTED",              0);
define(exports.UserResponseToSplitFareRequest, "REJECTED",              1);
define(exports.UserResponseToSplitFareRequest, "TIMEOUT",               2);


exports.SplitFareRequestDisplayMessage = {};
define(exports.SplitFareRequestDisplayMessage, "ACCEPTED",              'You have accepted the split fare request successfully');
define(exports.SplitFareRequestDisplayMessage, "REJECTED",              'You have rejected the split fare request successfully');
define(exports.SplitFareRequestDisplayMessage, "TIMEOUT",               'Request timeout');
define(exports.SplitFareRequestDisplayMessage, "RIDEALREADYENDED",      'Ride has been already completed');
define(exports.SplitFareRequestDisplayMessage, "REQUESTALREADYACCEPTED",'Request has been already accepted');


exports.splitFareRequestResponse = {};
define(exports.splitFareRequestResponse, "SPLITFARE_REQUEST_SENT",            'Request already sent');
define(exports.splitFareRequestResponse, "USER_NOT_REGISTERED",               'No customer registered with this phone number');
define(exports.splitFareRequestResponse, "USER_NOT_LOGGED_IN",                'User not logged in');
define(exports.splitFareRequestResponse, "BLOCKED_USER",                      'User has been blocked');
define(exports.splitFareRequestResponse, "USER_ALREADY_IN_RIDE",              'User is in ride');
define(exports.splitFareRequestResponse, "SPLITFARE_REQUEST_SENDING_SUCCESSFUL",'Request sent successfully');
define(exports.splitFareRequestResponse, "SENDING_REQUEST_TO_OWN",              'You cannot send request to yourself');


exports.splitFareRequestTimer = {};
define(exports.splitFareRequestTimer, "TIMEINSECONDS",              30);

exports.PaymentStatus = {};
define(exports.PaymentStatus, "UNSUCCESSFUL",      0);
define(exports.PaymentStatus, "SUCCESSFUL",        1);



exports.userBlockingStatus = {};
define(exports.userBlockingStatus, "BLOCKED",      1);


// Keep this list sorted by the driver-IDs
exports.rogueDrivers = [];
exports.rogueDrivers.push(168);     // Shiv Kumar
exports.rogueDrivers.push(169);     // Somveer
exports.rogueDrivers.push(363);     // Jackey Dhigan
exports.rogueDrivers.push(400);     // Dharmendar
exports.rogueDrivers.push(477);     // Parvinder Singh
exports.rogueDrivers.push(583);     // Shiv Kumar

// Keep this list sorted by the customer-IDs
exports.rogueCustomers = [];