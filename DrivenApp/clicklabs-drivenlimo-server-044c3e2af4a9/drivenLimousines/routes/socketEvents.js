/**
 * Created by harsh on 12/15/14.
 */


var responses = require('./responses');
var logging =   require('./logging');
var constants = require('./constants');
var utils =     require('./commonfunction');

var driversToSocketMap;
var socketsToDriverMap;


exports.loadCommonEvents = function(socket){
    socket.on('connect', function(){
        createConnection(socket);
    });
    socket.on('disconnect', function(){
        deleteConnection(socket);
    });
    socket.on('authenticate', function(data){
        var accessToken = data.access_token;
        authenticateConnection(socket, accessToken);
    });
};

exports.loadDriverEvents = function(socket){
};


function getDriversToSocketMap(){
    if(typeof driversToSocketMap === 'undefined'){
        driversToSocketMap = new Map();
    }
    return driversToSocketMap;
}

function getSocketsToDriverMap(){
    if(typeof socketsToDriverMap === 'undefined'){
        socketsToDriverMap = new Map();
    }
    return socketsToDriverMap;
}

function createConnection (socket){
    var socketId = socket.id();
    getSocketsToDriverMap().set(
        socketId,
        {
            driverId: 0,
            authenticated: false
        }
    );
}

function authenticateConnection (socket, accessToken){
    var socketId = socket.id();

    utils.authenticateUser(accessToken, function(driver){
        if(driver == 0){
            socket.emit('authenticationFailure', 'Connection closed');
            getSocketsToDriverMap().delete(socketId);
            socket.close();
            return;
        }

        socket.emit('authenticationSuccess', 'Connection authenticated');
        var driverId = driver[0].user_id;
        getSocketsToDriverMap().set(
            socketId,
            {
                driverId: driverId,
                authenticated: true
            }
        );

        getDriversToSocketMap().set(
            driverId,
            socketId
        )
    });
}

exports.sendMessage = function (driverId, message, callback){
    var socketId = getDriversToSocketMap().get(driverId);
    if(typeof socketId === 'undefined'){
        var error = new Error('No socket available for the driver');
        callback(error);
    }

    socketIoServer.sockets.socket(socketId).emit(message.key, message.data);
    callback(null);
};

function deleteConnection (socket){
    var socketId = socket.id();
    var driverId = getSocketsToDriverMap().get(socketId).driverId;
    if(typeof driverId === 'undefined'){
        getDriversToSocketMap().delete(driverId);
    }
    getSocketsToDriverMap().delete(socketId);
}