/**
 * Created by harsh on 11/14/14.
 */

// See if the last session was cancelled within the previous minute
function hasCancelledInLimit(timestamp){
    timestamp = timestamp.toISOString().replace(/T/, ' ').replace(/\..+/, '');
    var timestamp = timestamp.split(/[- :]/);
    var timestamp = new Date(timestamp[0], timestamp[1]-1, timestamp[2], timestamp[3], timestamp[4], timestamp[5]);
    if(timestamp.getTime() < 60000){
        return true;
    }
    else{
        return false;
    }
}




function getRequestsConvertedToRides(requests, start_date, for_all_day, callback){
    var dates = [];
    var get_number_requests = "";
    if(for_all_day === 1){
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(`date`,'%Y-%m-%d') as `date` " +
            "FROM `tb_session` WHERE DATE(`date`) > ? && `ride_acceptance_flag`!=0 GROUP BY DATE(`date`)";
    }
    else{
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(`date`,'%Y-%m-%d') as `date` " +
            "FROM `tb_session` WHERE DATE(`date`) > ? && TIME(`date`) < TIME(NOW()) && `ride_acceptance_flag`!=0 GROUP BY DATE(`date`)";
    }
    connection.query(get_number_requests, [start_date], function(err, data) {
        logging.logDatabaseQuery("Getting the number of request converted to rides", err, data);

        var i = 0;
        for (i = 0; i < data.length; i++) {
            requests.push(data[i].number_requests);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(requests, dates, start_date, end_date);

        callback();
    });
}

function getRequestsCancelledByUser(requests, start_date, for_all_day, callback){
    var dates = [];
    var get_number_requests = "";
    if(for_all_day === 1){
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(filtered.date,'%Y-%m-%d') as `date` " +
            "FROM (SELECT * FROM `tb_session` WHERE DATE(`date`) > ? && `cancelled_by_user`=1 GROUP BY `user_id`, HOUR(`date`)) as `filtered` " +
            "GROUP BY DATE(filtered.date)";
    }
    else{
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(filtered.date,'%Y-%m-%d') as `date` " +
            "FROM (SELECT * FROM `tb_session` WHERE DATE(`date`) > ? && TIME(`date`) < TIME(NOW()) && `cancelled_by_user`=1 GROUP BY `user_id`, HOUR(`date`)) as `filtered` " +
            "GROUP BY DATE(filtered.date)";
    }
    connection.query(get_number_requests, [start_date], function(err, data) {
        logging.logDatabaseQuery("Getting the number of requests cancelled by the user grouped by hour", err, data);

        var i = 0;
        for (i = 0; i < data.length; i++) {
            requests.push(data[i].number_requests);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(requests, dates, start_date, end_date);

        callback();
    });
}

function getRequestsCancelledByDriver(requests, start_date, for_all_day, callback){
    var dates = [];
    var get_number_requests = "";
    if(for_all_day === 1){
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(`date`,'%Y-%m-%d') as `date` " +
            "FROM `tb_session` WHERE DATE(`date`) > ? && `ride_acceptance_flag`=2 GROUP BY DATE(`date`)";
    }
    else{
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(`date`,'%Y-%m-%d') as `date` " +
            "FROM `tb_session` WHERE DATE(`date`) > ? && TIME(`date`) < TIME(NOW()) && `ride_acceptance_flag`=2 GROUP BY DATE(`date`)";
    }
    connection.query(get_number_requests, [start_date], function(err, data) {
        logging.logDatabaseQuery("Getting the number of requests cancelled by the driver grouped by hour", err, data);

        var i = 0;
        for (i = 0; i < data.length; i++) {
            requests.push(data[i].number_requests);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(requests, dates, start_date, end_date);

        callback();
    });
}

function getRequestsUnprocessed(requests, start_date, for_all_day, callback){
    var dates = [];
    var get_number_requests = "";
    if(for_all_day === 1){
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(filtered.date,'%Y-%m-%d') as `date` " +
            "FROM (SELECT * FROM `tb_session` WHERE DATE(`date`) > ? && `ride_acceptance_flag`=0 && `cancelled_by_user`=0 GROUP BY `user_id`, HOUR(`date`)) as `filtered` " +
            "GROUP BY DATE(filtered.date)";
    }
    else{
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(filtered.date,'%Y-%m-%d') as `date` " +
            "FROM (SELECT * FROM `tb_session` WHERE DATE(`date`) > ? && TIME(`date`) < TIME(NOW()) && `ride_acceptance_flag`=0 && `cancelled_by_user`=0 GROUP BY `user_id`, HOUR(`date`)) as `filtered` " +
            "GROUP BY DATE(filtered.date)";
    }

    connection.query(get_number_requests, [start_date], function(err, data) {
        logging.logDatabaseQuery("Getting the number of requests that were unprocessed", err, data);

        var i = 0;
        for (i = 0; i < data.length; i++) {
            requests.push(data[i].number_requests);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(requests, dates, start_date, end_date);

        callback();
    });
}

function getRequests(requests, start_date, for_all_day, callback){
    var dates = [];
    var get_number_requests = "";
    if(for_all_day === 1){
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(filtered.date,'%Y-%m-%d') as `date` " +
            "FROM (SELECT * FROM `tb_session` WHERE DATE(`date`) > ? GROUP BY `user_id`, HOUR(`date`)) as `filtered` " +
            "GROUP BY DATE(filtered.date)";
    }
    else{
        get_number_requests =
            "SELECT COUNT(*) as `number_requests`, DATE_FORMAT(filtered.date,'%Y-%m-%d') as `date` " +
            "FROM (SELECT * FROM `tb_session` WHERE DATE(`date`) > ? && TIME(`date`) < TIME(NOW()) GROUP BY `user_id`, HOUR(`date`)) as `filtered` " +
            "GROUP BY DATE(filtered.date)";
    }

    connection.query(get_number_requests, [start_date], function(err, data) {
        logging.logDatabaseQuery("Getting the number of requests that were unprocessed", err, data);

        var i = 0;
        for (i = 0; i < data.length; i++) {
            requests.push(data[i].number_requests);
            dates.push(data[i].date);
        }

        var end_date = new Date();
        end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
        ensureConsistencyOfNumbers(requests, dates, start_date, end_date);

        callback();
    });
}

exports.service_quality_data = function(req, res){
    logging.startSection("get_data_for_rides");
    logging.logRequest(req);

    var dates = [];
    var total_requests = [];
    var converted_requests = [];
    var user_cancel_requests = [];
    var driver_cancel_requests = [];
    var unprocessed_requests = [];

    var start_date = '2014-10-27';
    var end_date = new Date();
    end_date = end_date.getFullYear() + "-" + (end_date.getMonth() + 1) + "-" + end_date.getDate();
    getConsistentDates(dates, start_date, end_date);

    var asyncTasks = [];
    asyncTasks.push(getRidesCompleted.bind(null, converted_requests, start_date, 1));
    asyncTasks.push(getRequestsCancelledByUser.bind(null, user_cancel_requests, start_date, 1));
    asyncTasks.push(getRequestsCancelledByDriver.bind(null, driver_cancel_requests, start_date, 1));
    asyncTasks.push(getRequestsUnprocessed.bind(null, unprocessed_requests, start_date, 1));
    aysncTasks.push(getRequests.bind(null, total_requests, start_date, 1));

    async.parallel(asyncTasks, function(){
        var i=0;
        for (i=0; i<dates.length; i++){
            var date = dates[i].split('-');
            dates[i] = date[2] + '-' + date[1];
        }

        for(i=0; i<converted_requests.length; i++){
            total_requests.push(converted_requests[i] + user_cancel_requests[i] + unprocessed_requests[i]);
        }

        var response = {
            chart: {
                style: {
                    color: "#b9bbbb"
                },
                renderTo: "container",
                backgroundColor: "transparent",
                lineColor: "rgba(35,37,38,100)",
                plotShadow: false
            },
            credits: {
                enabled: false
            },
            title: {
                text: "Service quality data for each day"
            },
            xAxis: {
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
                    text: "Days"
                },
                categories: dates
            },
            yAxis: {
                min: 0,
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
                    text: "Numbers"
                }
            },
            legend: {
                itemStyle: {
                    color: "#b9bbbb"
                },
                layout: "vertical",
                align: "right",
                verticalAlign: "middle",
                borderWidth: 0
            },
            series: []
        };

        response.series.push({color: "#108ec5", name: "Total Requests", data: total_requests});
        response.series.push({color: "#009900", name: "Converted to Rides", data: converted_requests});
        response.series.push({color: "#99CC33", name: "Cancelled by user", data: user_cancel_requests});
        response.series.push({color: "#ee5728", name: "Cancelled by driver", data: driver_cancel_requests});
        response.series.push({color: "#339900", name: "Requests not processed", data: unprocessed_requests});

        //logging.logResponse(response);
        res.send(JSON.stringify(response));
    });
};


exports.flush_inactive_sessions = function(req, res){
    var required_statuses = [constants.sessionStatus.INACTIVE, constants.sessionStatus.TIMED_OUT];
    var get_session = "SELECT `session_id` " +
        "FROM `tb_session` " +
        "WHERE `is_active` IN (" + required_statuses.toString() + ") && `date` > timestamp(DATE_SUB(NOW(), INTERVAL 10 MINUTE))";
    connection.query(get_session, [], function(err, sessions){
        logging.logDatabaseQuery("Getting the sessions that timeout or were made inactive in last 10 minutes", err, sessions);

        for(var i=0; i<sessions.length; i++){
            logging.flushSession(sessions[i].session_id);
            logging.deleteSession(sessions[i].session_id);
        }

        res.send("flushing sessions: " + JSON.stringify(sessions));
    });
};


//
//SOCKET.IO APP.JS
//

// creating the socket.io server
var io = require('socket.io');
// making the socket.io server global to write code in new module
socketIoServer = io(httpsServer);
var sockets = require('./routes/sockets.js');

// `user_id` IN (308,551,587,601,605,620,736,749,770,784,797,874,922,1312,1327,1907,1909,1932,2672,2852,3345,3507,4352,6282)

function sendHeartbeatNotification(){
    var getDriverDeviceTokens =
        "SELECT drivers.user_id, drivers.device_type, drivers.user_device_token FROM " +
        "(SELECT `user_id`, `device_type`, `user_device_token` " +
        "FROM `tb_users` " +
        "WHERE `app_versioncode` >= 137 AND `reg_as` IN (1,2) AND `current_user_status` = 1 AND `current_location_latitude` > 0.001 AND `current_location_longitude` > 0.001) AS `drivers` " +
        "JOIN " +
        "`tb_timings` AS `timings` " +
        "ON drivers.user_id = timings.driver_id " +
        "WHERE timings.start_time < NOW() AND timings.end_time >= NOW()";
    connection.query(getDriverDeviceTokens, [], function(err, drivers){
        if(err){
            logging.logDatabaseQueryError("Getting the device token for all the drivers", err, drivers);
            return;
        }

        var i = 0;
        for (i = 0; i < drivers.length; i++){
            var uuid = uuidGenerator.v4();
            sendHeartbeat(drivers[i], uuid);
            logHeartbeat(drivers[i].user_id, uuid);
        }
    });
}
