
var async       = require('async');
var request     = require('request');

var constants   = require('./constants');
var logging     = require('./logging');
var utils       = require('./commonfunction');


exports.runTheFunction = function(req, res){
    logging.startSection("run_once");
    logging.logRequest(req);

    var password = req.body.password;
    if(password !== constants.SUPER_ADMIN_PASSWORD){
        res.send('You are not authorized to perform this action');
        return;
    }

    var functionToRun;

    functionToRun = emptyFunction;

    //functionToRun = changeDuplicateReferralCodes;
    functionToRun = updateLastRideToCurrentDistance;
    //functionToRun = transferRequestAcks;
    //functionToRun = insertLastRideTimestamp;
    //functionToRun = insertLastReferredOn;

    functionToRun();
    res.send("Running the function: " + functionToRun.name);
};


function emptyFunction(){
    console.log("Running the empty function");
}




// One time function to insert the last ride timestamp for all the users
// This will be updated dynamically for the users when ride ends after this
function insertLastRideTimestamp(){
    var getLastRideTime =
        'SELECT MAX(`drop_time`) AS `last_ride_timestamp`, user_id ' +
        'FROM `tb_engagements` ' +
        'WHERE `status` = 3 ' +
        'GROUP BY `user_id`';
    connection.query(getLastRideTime, [], function(err, engagements){
        logging.logDatabaseQuery('Getting the last drop time for all users who took a ride', err, engagements.length);

        var insertTimestamp =
            'UPDATE `tb_users` ' +
            'SET `last_ride_on` = ? ' +
            'WHERE user_id = ?';
        var i = 0;
        for(i = 0; i < engagements.length; i++){
            connection.query(insertTimestamp, [engagements[i].last_ride_timestamp, engagements[i].user_id], function(err, result){
                if(err){
                    logging.logDatabaseQuery('Updating the last ride timestamp for the user', err, result);
                }
            });
        }
    });
}




// One time function to insert the last referred timestamp for all the users
// This will be updated dynamically when a new registration occurs using the referral code for the user
function insertLastReferredOn(){
    var getLastReferredOn =
        'SELECT MAX(`date_registered`) AS `last_referred_on`, `referred_by` ' +
        'FROM `tb_users` ' +
        'WHERE `referred_by` != 0 ' +
        'GROUP BY `referred_by`';
    connection.query(getLastReferredOn, [], function(err, referrals){
        logging.logDatabaseQuery('Getting the last referred on date for the users', err, referrals.length);

        var insertTimestamp =
            'UPDATE `tb_users` ' +
            'SET `last_referred_on` = ? ' +
            'WHERE user_id = ?';
        var i = 0;
        for(i = 0; i < referrals.length; i++){
            connection.query(insertTimestamp, [referrals[i].last_referred_on, referrals[i].referred_by], function(err, result){
                if(err){
                    logging.logDatabaseQuery('Updating the last ride timestamp for the user', err, result);
                }
            });
        }
    });
}




// One time function to transfer the data from tb_request_acks to tb_engagements
function transferRequestAcks(){
    var getAllData =
        'SELECT `engagement_id`, `request_made`, `request_received`, `network` ' +
        'FROM `tb_request_acks`';
    connection.query(getAllData, [], function(err, engagementData){
        logging.logDatabaseQuery('Getting the data to be transferred to tb_engagements', err, engagementData.length);

        var updateEngagements =
            'UPDATE `tb_engagements` ' +
            'SET `request_made_on` = ?, `request_received_on` = ?, `network_driver` = ? ' +
            'WHERE `engagement_id` = ?';
        var i = 0;
        for(i = 0; i < engagementData.length; i++){
            (function(i){
                var values = [engagementData[i].request_made, engagementData[i].request_received,
                    engagementData[i].network, engagementData[i].engagement_id];
                connection.query(updateEngagements, values, function(err, result) {
                    if(err){
                        logging.logDatabaseQuery('Updating the data for the engagements', err, result);
                    }
                    console.log(engagementData[i].engagement_id);
                });
            })(i);
        }
    });
}




// One time function to update the distance for the drivers from last drop location to accept location for the next ride
function updateLastRideToCurrentDistance(){
    console.log('Updating the last drop to current accept distance');
    var getDedicatedDrivers =
        'SELECT user_id AS id ' +
        'FROM tb_users ' +
        'WHERE reg_as = 2';
    connection.query(getDedicatedDrivers, [], function(err, drivers){
        logging.logDatabaseQuery('Getting all the dedicated drivers', err, drivers);

        async.eachSeries(
            drivers,
            updateLastRideToCurrentDistanceForDriver,
            function(err){
                if(err){
                    console.log('An error occurred when updating the updateLastRideToCurrentDistance for the driver');
                }
                else{
                    console.log('All drivers processed successfully for updateLastRideToCurrentDistance');
                }
            }
        );
    });
};

function updateLastRideToCurrentDistanceForDriver(driver, callback){
    console.log('driver: ' + JSON.stringify(driver));
    var driversProcessed = [69, 308, 550, 551, 587, 605, 620, 797, 868, 922, 975, 1044, 1907, 2039, 2522, 2852, 3136, 3345];
    if(driversProcessed.indexOf(driver.id) > -1){
        return process.nextTick(callback.bind(null, null));
    }

    var getRides =
        'SELECT engagement_id, driver_id, pickup_time, drop_latitude, drop_longitude, ' +
        'driver_accept_latitude AS accept_latitude, driver_accept_longitude AS accept_longitude, accept_distance_from_last_drop AS existing_distance ' +
        'FROM tb_engagements ' +
        'WHERE driver_id = ? AND status = 3 AND pickup_time > \'2014-11-30\' ' +
        'ORDER BY driver_id, pickup_time DESC';
    connection.query(getRides, [driver.id], function(err, engagements){
        logging.logDatabaseQuery('Getting the number of engagements for the drivers', err, engagements.length);

        if(err){
            return process.nextTick(callback.bind(null, err));
        }

        updateLastRideToCurrentDistanceForRides(engagements, function(err){
            return process.nextTick(callback.bind(null, err));
        });
    });
}

function updateLastRideToCurrentDistanceForRides(engagements, callback){
    var numRequested = 0;
    var numReceived  = 0;

    var i = 0;
    for(i = 0; i < engagements.length - 1; i++){
        if(!engagements[i].existing_distance){
            (function (index){
                var engagementId    = engagements[index].engagement_id;
                var acceptLatitude  = engagements[index].accept_latitude;
                var acceptLongitude = engagements[index].accept_longitude;
                var dropLatitude    = engagements[index+1].drop_latitude;
                var dropLongitude   = engagements[index+1].drop_longitude;

                numRequested++;

                setTimeout(
                    function fetchAndUpdateDistance(){
                        var googleQuery =
                            'https://maps.googleapis.com/maps/api/distancematrix/json?' +
                            'origins=' + dropLatitude + ',' + dropLongitude +
                            '&destinations=' + acceptLatitude + ',' + acceptLongitude;
                        console.log(engagementId + '::::: ' + googleQuery);
                        request(googleQuery, function (error, response, body)
                        {
                            numReceived++;
                            var distance = 0;
                            if (!error && response.statusCode == 200)
                            {
                                body = JSON.parse(body);
                                if (body.rows.length > 0)
                                {
                                    distance = body.rows[0].elements[0].distance.value;

                                    var updateEngagement =
                                        'UPDATE `tb_engagements` ' +
                                        'SET `accept_distance_from_last_drop` = ? ' +
                                        'WHERE `engagement_id` = ?';
                                    connection.query(updateEngagement, [distance, engagementId], function(err, result){
                                        logging.logDatabaseQuery("Updating the LastRideToCurrentDistance using google distance matrix", err, result);
                                    });
                                }
                            }
                            checkAllReceivedAndReturn();
                        });
                    },
                    2000 * numRequested // run a google query every 2 seconds
                );

                function checkAllReceivedAndReturn(){
                    if(numRequested === numReceived){
                        process.nextTick(callback.bind(null, null));
                    }
                }
            }
            )(i);
        }
    }
}




// One time function to change the referral codes for the users with duplicate referral codes
// The codes were duplicate for some old registrations owing to a bug in the code which was fixed then
function changeDuplicateReferralCodes(){
    var getDuplicateCodes =
        'SELECT referral_code ' +
        'FROM tb_users ' +
        'GROUP BY referral_code ' +
        'HAVING COUNT(*)  > 1';
    connection.query(getDuplicateCodes, [], function(err, codes){
        logging.logDatabaseQuery('Getting referral codes with multiple users', err, codes);

        var i = 0;
        for(i = 0; i < codes.length; i++){
            changeCodeForUsers(codes[i].referral_code);
        }
    });
}

function changeCodeForUsers(referralCode){
    var getUsers =
        'SELECT user_id, user_name, date_registered ' +
        'FROM tb_users ' +
        'WHERE referral_code = ? ' +
        'ORDER BY date_registered ASC';
    connection.query(getUsers, [referralCode], function(err, users){
        logging.logDatabaseQuery('Getting users for the referral code ' + referralCode, err, users);

        // Don't want to change the referral code for the oldest user
        var i = 0;
        for(i = 1; i < users.length; i++){
            changeCodeForUser(users[i].user_id, users[i].user_name);
        }
    });
}

function changeCodeForUser(userId, userName){
    utils.generateUniqueReferralCode(userName, function(newReferralcode){
        console.log(userId + ' ::::: ' + userName + ' ::::: ' + newReferralcode);
        var changeCode =
            'UPDATE tb_users ' +
            'SET referral_code = ? ' +
            'WHERE user_id = ?';
        connection.query(changeCode, [newReferralcode, userId], function(err, result){
            logging.logDatabaseQuery('Updating the referral code for the user', err, result);
        });
    });
}




function addPickupDistanceForEngagements()
{
    logging.startSection("add_pickup_distance");

    var status = [constants.engagementStatus.ENDED];
    var get_engagements = "SELECT `engagement_id`, `pickup_latitude`, `pickup_longitude`, `driver_accept_latitude` as accept_latitude, `driver_accept_longitude` as accept_longitude " +
        "FROM `tb_engagements` WHERE `status` IN (" + status.toString() + ")";
    connection.query(get_engagements, [], function(err, engagements) {
        logging.logDatabaseQuery("Getting all the engagements that completed", err, null, null);

        console.log("Total to be updated: " + engagements.length);
        var start_at = 0;
        var intervalID = setInterval(
            function(){
                var i = start_at;
                for (i = start_at; i < engagements.length && i < start_at + 10; i++) {
                    addPickupDistanceForEngagement(engagements[i]);
                }
                if(i == engagements.length){
                    clearInterval(intervalID);
                }
                start_at = i;
                console.log("We are at: " + start_at);
            }, 2000);
    });
}

function addPickupDistanceForEngagement(engagement){
    var accept_distance = 0;
    if(Math.abs(engagement.accept_latitude) > 0.001 && Math.abs(engagement.accept_longitude) > 0.001){
        var google_query = 'https://maps.googleapis.com/maps/api/distancematrix/json?' +
            'origins=' + engagement.pickup_latitude + ',' + engagement.pickup_longitude +
            '&destinations=' + engagement.accept_latitude + ',' + engagement.accept_longitude;
        request(google_query, function (error, response, body)
        {
            var distance = 0;
            if (!error && response.statusCode == 200)
            {
                body = JSON.parse(body);
                if (body.rows.length > 0)
                {
                    distance = body.rows[0].elements[0].distance.value;

                    var update_engagement = "UPDATE `tb_engagements` SET `accept_distance`=? WHERE `engagement_id`=?";
                    var values = [distance, engagement.engagement_id];
                    connection.query(update_engagement, values, function(err, result){
                        //logging.logDatabaseQuery("Updating the accept distance for the driver using google distance matrix for: " + engagement.engagement_id, err, result, null);
                    });
                }
            }
            else{
                console.log('Error: ' + error);
                console.log('Status: ' + response.statusCode);
            }
        });
    }
}

