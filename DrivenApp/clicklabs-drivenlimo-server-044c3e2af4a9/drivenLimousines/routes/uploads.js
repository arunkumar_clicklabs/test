/**
 * Created by harsh on 12/5/14.
 */

var fs = require('fs');
var logging = require('./logging');
var constants = require('./constants');

exports.upload_file = function(req, res){
    logging.startSection("Upload a file to the server");
    logging.logRequest(req);

    //console.log('---------------------------------------');
    //console.log('| PARAMETERS WHEN UPLOADING THE FILES |');
    //console.log('---------------------------------------');
    //console.log(JSON.stringify(req.body));
    //console.log(JSON.stringify(req.files));

    var fileType = req.body.file_type;
    console.log("fileType: " + fileType);
    var file = null;

    if( fileType == constants.FILE_TYPE.RIDE_PATH){
        file = req.files.ride_path;
    }
    console.log("file: " + JSON.stringify(file));

    var path = file.path; //will be put into a temp directory
    //var mimeType = file.type;

    fs.readFile(path, {encoding: 'utf8'}, function(err, fileBuffer) {
        if (err) {
            process.stderr.write('The file couldn\'t be uploaded to the server.\n');
            process.stderr.write('request body: ' + JSON.stringify(req.body) + '\n');
            process.stderr.write('request files: ' + JSON.stringify(req.files) + '\n');
            res.send('The file couldn\'t be saved');
            removeTemporaryFiles(req.files);
            return callback(err);
        }
        else {
            var filename = file.name;
            var localPath = __dirname + config.get('directoryToUploadFiles.ridesData') + filename;

            fs.writeFile(localPath, fileBuffer, {encoding: 'utf8'}, function (err){
                if (err){
                    res.send("An error occurred in saving the file");
                    return;
                }
                res.send("The file was saved to " + localPath);
                removeTemporaryFiles(req.files);
            });
        }
    });
};

function removeTemporaryFiles(files){
    for(var file in files){
        //console.log('FILE INFO WHEN DELETING\n');
        //console.log(JSON.stringify(files[file], undefined, 2));
        fs.unlink(files[file].path, function(err){
            if(err){
                process.stderr.write('An error occurred when deleting the file\n');
                process.stderr.write('file: ' + JSON.stringify(file) + '\n');
            }
        });
    }
}