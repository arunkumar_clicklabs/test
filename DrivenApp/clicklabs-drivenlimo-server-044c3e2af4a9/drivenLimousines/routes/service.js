var logging = require('./logging');
var constants = require('./constants');
var utils = require('./commonfunction');
var mailer = require('./mailer');
var fs = require('fs');

exports.get_information = function(req, res){
    var section = req.body.section;

    var isBlank = utils.checkBlank([section]);
    if(isBlank == 1){
        var response = {"error": "some parameter missing", "flag": constants.responseFlags.PARAMETER_MISSING};
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    }
    else{
        var file_name = '';
        switch(true){
            case (section == constants.infoSection.ABOUT):
                file_name = __dirname + '/../data/about.html';
                break;
            case (section == constants.infoSection.FAQs):
                file_name = __dirname + '/../data/faqs.html';
                break;
            case (section == constants.infoSection.PRIVACY):
                file_name = __dirname + '/../data/privacy_policy.html';
                break;
            case (section == constants.infoSection.TERMS):
                file_name = __dirname + '/../data/terms_conditions.html';
                break;
            case (section == constants.infoSection.FARE):
                file_name = __dirname + '/../data/fares.html';
                break;
            case (section == constants.infoSection.SCHEDULES_TNC):
                file_name = __dirname + '/../data/tnc_schedules.html';
                break;
        }

        fs.readFile(file_name, {encoding: 'utf8'}, function (err, data) {
            if(err){
                console.log("Error when reading the file: " + JSON.stringify(err));
                res.send(JSON.stringify({error: "Error reading the data"}));
            }
            else{
                res.send(JSON.stringify({data: data}));
            }
        });
    }
};


exports.send_notification = function(req, res){
    logging.logDatabaseQuery("send_notification");
    logging.logRequest(req);

    var user_id = req.body.user_id;
    var message = req.body.message;

    var isBlank = utils.checkBlank([user_id, message]);
    if(isBlank){
        var response = {"error": "some parameter missing", "flag": constants.responseFlags.PARAMETER_MISSING};
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    }
    else{
        var payload = {
            flag    : constants.notificationFlags.DISPLAY_MESSAGE,
            persist : true,
            message : message};
        var notificationFlag = 1;
        utils.sendNotification(user_id, message, notificationFlag, payload);

        res.send(JSON.stringify({log:'notification sent to the user'}));
    }
};

