/**
 * Created by harsh on 12/5/14.
 */

/**
 * Created by harsh on 12/3/14.
 */

var fs = require('fs');

var logging = require('./logging');
var utils = require('./commonfunction');
var constants = require('./constants');

exports.blast_emails_to_users = function(req, res){
    logging.logDatabaseQuery("blast_emails_to_users");
    logging.logRequest(req);

    var subject = req.body.subject;
    var fileName = req.body.file_name;
    var filter = parseInt(req.body.filter);
    var password = req.body.password;

    var isBlank = utils.checkBlank([subject, fileName, filter, password]);
    if(isBlank){
        var response = {"error": "some parameter missing", "flag": constants.responseFlags.PARAMETER_MISSING};
        logging.logResponse(response);
        res.send(JSON.stringify(response));
        return;
    }

    if (password != constants.EMAIL_BLAST_PASSWORD) {
        res.send("The password is incorrect");
        return;
    }

    var getEmails = "";
    var values = [];
    switch(filter) {
        case constants.emailFilters.TEST:
            getEmails = "SELECT `user_email` FROM `tb_users` WHERE (`user_name` LIKE '%chinmay agarwal%' OR `user_name` LIKE '%harsh choudhary%') && `user_email` NOT LIKE '%@facebook.com%'";
            values = [];
            break;
        case constants.emailFilters.ALL:
            getEmails = "SELECT `user_email` FROM `tb_users` WHERE `is_blocked` = 0 && `reg_as` = 0 && `user_email` NOT LIKE '%@facebook.com%'";
            values = [];
            break;
        case constants.emailFilters.COUPON_BUT_NO_RIDE:
            getEmails =
                "SELECT emails.user_email FROM " +
                "(SELECT `user_id`, `user_email` FROM `tb_users` WHERE `is_blocked` = 0 && `reg_as` = 0 && `user_email` NOT LIKE '%@facebook.com%' AND `user_email` NOT LIKE '%disabled@jugnoo.in%') as `emails` " +
                "JOIN " +
                "(SELECT `user_id` FROM `tb_accounts` WHERE `status` = 1 and `user_id` NOT IN (SELECT `user_id` FROM `tb_engagements` WHERE `status` = 3 && DATE(`pickup_time`) >= DATE_SUB(NOW(), INTERVAL 3 DAY))) AS `users` " +
                "WHERE emails.user_id = users.user_id";
            break;
        case constants.emailFilters.NO_COUPON_NO_RIDE:
            getEmails =
                "SELECT * FROM " +
                    "(SELECT `user_id`, `user_email` FROM `tb_users` WHERE `user_email` NOT LIKE '%facebook.com%' AND `user_email` NOT LIKE '%disabled@jugnoo.in%' " +
                    "AND `user_id` NOT IN (SELECT `user_id` FROM `tb_engagements` WHERE `status` = 3 AND DATE(`pickup_time`) > DATE_SUB(NOW(), INTERVAL 7 DAY))) AS `no_rides_users` " +
                "LEFT JOIN " +
                    "(SELECT `user_id`, count(*) AS `num_coupons` FROM `tb_accounts` WHERE `status` = 1 GROUP BY `user_id`) AS `users_with_coupons` " +
                "ON users_with_coupons.user_id = no_rides_users.user_id " +
                "WHERE ISNULL(users_with_coupons.num_coupons)";
            break;
    }

    if(getEmails === "") {
        res.send("The filter that you are using is not in the system yet.");
        return;
    }

    connection.query(getEmails, values, function(err, emails){
        if(err){
            logging.logDatabaseQuery("Getting the email IDs for the users under the filter: " + filter, err);
        }
        console.log("A total of " + emails.length + " users will receive the notification");

        fileName = __dirname + config.get('directoryToUploadFiles.data') + fileName;
        fs.readFile(fileName, {encoding: 'utf8'}, function(err, data){
            if(err){
                res.send("Error reading file " + fileName + ", error: " + err);
                return;
            }

            var to = "";
            var i = 0;
            for(i = 0; i < emails.length; i++){
                to = emails[i].user_email;
                sendMail(to, i);
            }

            function sendMail(to, i) {
                var sending_to = to.slice(0);
                setTimeout(function() {
                    utils.sendHtmlContent(sending_to, data, subject, function(result){
                    });
                }, 2000*i);
            }
        });

        res.send("The email will be sent to " + emails.length + " devices");
        return;
    });
};
