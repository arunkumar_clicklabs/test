var utils = require('./commonfunction');
var logging = require('./logging');
var braintree = require('braintree');
var constants = require('./constants');
var responses = require('./responses');

var gateway = braintree.connect({
    environment: braintree.Environment.Sandbox, //Production,
    merchantId: config.get('braintreeCredentials.merchantId'),
    publicKey: config.get('braintreeCredentials.publicKey'),
    privateKey: config.get('braintreeCredentials.privateKey')
});


exports.createCustomer = createCustomer;
exports.addCreditCard = addCreditCard;
exports.listCreditCards = listCreditCards;
exports.ChangeDefaultCreditCard = ChangeDefaultCreditCard;
exports.DeleteCreditCard = DeleteCreditCard;
exports.paymentOnRideCompletion = paymentOnRideCompletion;
exports.getBraintreeToken = getBraintreeToken;
exports.checkPaymentStatus = checkPaymentStatus;



function getBraintreeToken(req, res) {
    logging.startSection("get_braintree_token");
    logging.logRequest(req);


    gateway.clientToken.generate({}, function(err, response) {
        console.log(err)
        logging.logResponse(response);
        res.send(JSON.stringify(response));
    });
};


function createCustomer(firstName, lastName, nounce, callback) {
    gateway.customer.create({
        firstName: firstName,
        lastName: lastName,
        creditCard: {
            paymentMethodNonce: nounce,
            options: {
                verifyCard: true,
                //failOnDuplicatePaymentMethod: true,
                makeDefault: true
            }
        }
    }, function(err, result) {

        if (err) {
            return callback(err, result);
        }

        if (result.success == true) {
            return callback(null, result);
        } else {
            return callback(null, result);
        }
    });
}


function addCreditCard(req, res) {
    logging.startSection("add_credit_card");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var nounce = req.body.nounce;

    var manValues = [accessToken, nounce];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.sendAuthenticationError(res);
            return;
        }
        var userId = user[0].user_id;
        var userName = user[0].user_name;
        var sql = "SELECT `braintree_customer_id` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
        connection.query(sql, [userId], function(err, result) {
            logging.logDatabaseQuery("Get existing braintree_customer_id", err, result, null);
            if (result[0].braintree_customer_id === '') {
                createCustomer(userName, userName, nounce, function(err, result) {
                    if (err) {
                        responses.sendError(res);
                        return;
                    }
                    if (result.success == false) {
                        var response = {
                            "error": result.message,
                            "flag": constants.responseFlags.DUPLICATE_CARD
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                        return;
                    }

                    var sql = "UPDATE `tb_users` SET `credit_card_flag`=?,`braintree_customer_id`=? WHERE `user_id`=? LIMIT 1";
                    connection.query(sql, [1, result.customer.id, userId], function(err, result) {
                        logging.logDatabaseQuery("Updating braintree_customer_id for user", err, result, null);
                    });

                    var sql = "INSERT INTO `tb_user_credit_cards`(`user_id`, `card_token`, `last4_digits`, `default_card`) VALUES (?,?,?,?)";
                    connection.query(sql, [userId, result.customer.creditCards[0].token, result.customer.creditCards[0].verification.creditCard.last4, 1], function(err, result) {
                        logging.logDatabaseQuery("Credit card added", err, result, null);
                        if (err) {
                            responses.sendError(res);
                            return;
                        }
                        var response = {
                            "log": "Card added successfully"
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                    });

                });
            } else {
                gateway.creditCard.create({
                    customerId: result[0].braintree_customer_id,
                    paymentMethodNonce: nounce,
                    options: {
                        verifyCard: true,
                        //failOnDuplicatePaymentMethod: true,
                        makeDefault: true
                    }
                }, function(err, result) {

                    if (err) {
                        responses.sendError(res);
                        return;
                    }

                    if (result.success == false) {
                        var response = {
                            "error": result.message,
                            "flag": constants.responseFlags.DUPLICATE_CARD
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                        return;
                    }

                    var sql = "UPDATE `tb_user_credit_cards` SET `default_card`=? WHERE `user_id`=? && `default_card`=? LIMIT 1";
                    connection.query(sql, [0, userId, 1], function(err, resultChangeDefaultCard) {
                        logging.logDatabaseQuery("Changing default status of existing default card", err, result, null);
                        var sql = "INSERT INTO `tb_user_credit_cards`(`user_id`, `card_token`, `last4_digits`, `default_card`) VALUES (?,?,?,?)";
                        connection.query(sql, [userId, result.creditCard.token, result.creditCard.last4, 1], function(err, result) {
                            logging.logDatabaseQuery("Inserting new credit card", err, result, null);
                            var response = {
                                "log": "Card added successfully"
                            };
                            logging.logResponse(response);
                            res.send(JSON.stringify(response));
                        });
                    });

                });
            }
        });
    })
}


function listCreditCards(req, res) {
    logging.startSection("list_credit_cards");
    logging.logRequest(req);

    var accessToken = req.body.access_token;

    var manValues = [accessToken];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.sendAuthenticationError(res);
            return;
        }

        var userId = user[0].user_id;

        var sql = "SELECT `credit_card_flag` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
        connection.query(sql, [userId], function(err, result) {
            logging.logDatabaseQuery("Get existing braintree_customer_id", err, result, null);

            if (result[0].credit_card_flag === 1) {

                var sql = "SELECT `id`,`last4_digits`,`default_card`,`card_added_on` FROM `tb_user_credit_cards` WHERE `user_id`=?";
                connection.query(sql, [userId], function(err, result) {

                    logging.logDatabaseQuery("Get existing credit cards of user", err, result, null);
                    if (err) {
                        responses.sendError(res);
                        return;
                    }

                    var length = result.length;

                    for (var i = 0; i < length; i++) {
                        result[i].card_added_on = result[i].card_added_on.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    }

                    var response = {
                        "list_credit_cards": result
                    };
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));

                });
            } else {
                var response = {
                    "list_credit_cards": []
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            }
        });
    })
}


function ChangeDefaultCreditCard(req, res) {
    logging.startSection("change_default_credit_card");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var cardId = req.body.card_id;

    var manValues = [accessToken, cardId];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.sendAuthenticationError(res);
            return;
        }

        var userId = user[0].user_id;

        var sql = "UPDATE `tb_user_credit_cards` SET `default_card`=? WHERE `user_id`=? && `default_card`=? LIMIT 1";
        connection.query(sql, [0, userId, 1], function(err, resultChangeDefaultCard) {

            logging.logDatabaseQuery("Changing default status of existing default card", err, resultChangeDefaultCard, null);

            if (err) {
                responses.sendError(res);
                return;
            }

            var sql = "UPDATE `tb_user_credit_cards` SET `default_card`=? WHERE `id`=? LIMIT 1";
            connection.query(sql, [1, cardId], function(err, result) {

                logging.logDatabaseQuery("Set credit card as default", err, result, null);

                if (err) {
                    responses.sendError(res);
                    return;
                }

                var response = {
                    "log": "Default credit card updated successfully"
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            });
        });

    })
}


function DeleteCreditCard(req, res) {
    logging.startSection("delete_credit_card");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var cardId = req.body.card_id;

    var manValues = [accessToken, cardId];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.sendAuthenticationError(res);
            return;
        }

        var userId = user[0].user_id;


        var sql = "SELECT count(`id`) AS count FROM `tb_user_credit_cards` LIMIT 1";
        connection.query(sql, function(err, result) {

            logging.logDatabaseQuery("Number of credit cards of a user", err, result, null);

            if (err) {
                responses.sendError(res);
                return;
            }

            if (result[0].count === 1) {
                var response = {
                    "error": "Deletion of last credit card from your account is not allowed"
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
                return;
            }

            var sql = "DELETE FROM `tb_user_credit_cards` WHERE `id`=? && `default_card`=? LIMIT 1";
            connection.query(sql, [cardId, 0], function(err, result) {

                logging.logDatabaseQuery("Deleting credit card", err, result, null);

                if (err) {
                    responses.sendError(res);
                    return;
                }

                if (result.affectedRows === 0) {
                    var response = {
                        "error": "Default credit card cannot be deleted"
                    };
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));
                    return;
                }

                var response = {
                    "log": "Credit card deleted successfully"
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
            });
        });

    });
}


function paymentOnRideCompletion(req, res) {
    logging.startSection("payment_on_ride_completion");
    logging.logRequest(req);

    var accessToken = req.body.access_token;
    var engagementId = req.body.engagement_id;
    var paymentMethod = req.body.payment_method; // 0 - cash / 1- card // 2 - corporate account
    var amountToPay = req.body.to_pay;
    var tip = req.body.tip;


    var manValues = [accessToken, engagementId, paymentMethod, amountToPay, tip];
    var isBlank = utils.checkBlank(manValues);
    if (isBlank == 1) {
        responses.sendParameterMissingError(res);
        return;
    }

    utils.authenticateUser(accessToken, function(user) {
        if (user == 0) {
            responses.sendAuthenticationError(res);
            return;
        }

        var isCorporateUser = user[0].is_corporate;

        var userId = user[0].user_id;
        var IsPaymentSuccessful = 0;

        if (isCorporateUser == 1) {
            paymentMethod = 2;
            IsPaymentSuccessful = 1;
            // send push to driver to collect cash amount from customer
            var sql = "UPDATE `tb_engagements` SET `tip`=?,`is_payment_successful`=?,`payment_method`=? WHERE `engagement_id`=? LIMIT 1";
            connection.query(sql, [tip, IsPaymentSuccessful, paymentMethod, engagementId], function(err, result) {
                logging.logDatabaseQuery("Updating tip and is_payment_successful fields in engagement info by corporate account", err, result, null);

                if (err) {
                    responses.sendError(res);
                    return;
                }
                var response = {
                    "log": "Your payment is done by your corporate account"
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));



                alog(amountToPay);
                updateCorporateAdminBalance(amountToPay, userId);

                //sendPushToDriverToCollectPayableAmountFromCustomer(engagementId,paymentMethod,amountToPay);
                return;
            });


        }

        if (paymentMethod == 0) { // Cash
            IsPaymentSuccessful = 1;
            // send push to driver to collect cash amount from customer
            var sql = "UPDATE `tb_engagements` SET `tip`=?,`is_payment_successful`=?,`payment_method`=? WHERE `engagement_id`=? LIMIT 1";
            connection.query(sql, [tip, IsPaymentSuccessful, paymentMethod, engagementId], function(err, result) {
                logging.logDatabaseQuery("Updating tip and is_payment_successful fields in engagement info", err, result, null);

                if (err) {
                    responses.sendError(res);
                    return;
                }
                var response = {
                    "log": "Please make cash payment to driver"
                };
                logging.logResponse(response);
                res.send(JSON.stringify(response));
                //sendPushToDriverToCollectPayableAmountFromCustomer(engagementId,paymentMethod,amountToPay);
                return;
            });

        }
        if (paymentMethod == 1) { // Card
            // error in payment from card case pending
            // Make payment
            // send push to driver to indicate payment done from customer
            // add tip, update payment mode in engagement

            var sql = "SELECT a.braintree_customer_id,b.card_token FROM tb_users a,tb_user_credit_cards b WHERE a.user_id=b.user_id  && b.default_card = ? && a.user_id=? LIMIT 1";
            connection.query(sql, [1, userId], function(err, result) {

                logging.logDatabaseQuery("Getting existing braintree_customer_id and default card for card payment", err, result, null);
                if (err) {
                    responses.sendError(res);
                    return;
                }

                if (result.length === 0) {
                    var response = {
                        "error": "No credit card found in your account",
                        "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
                    };
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));
                    return;
                }

                if (parseFloat(amountToPay) == 0.0) {

                    IsPaymentSuccessful = 1;
                    var response = {
                        "log": "Payment Successful"
                    };
                    logging.logResponse(response);
                    res.send(JSON.stringify(response));

                    var sql = "UPDATE `tb_engagements` SET `is_payment_successful`=?,`payment_method`=? WHERE `engagement_id`=? LIMIT 1";
                    connection.query(sql, [IsPaymentSuccessful, paymentMethod, engagementId], function(err, result) {
                        logging.logDatabaseQuery("Updating is_payment_successful fields in engagement info", err, result, null);

                        //sendPushToDriverToCollectPayableAmountFromCustomer(engagementId,paymentMethod,amountToPay);

                    });

                    return;
                }

                amountToPay = amountToPay * 100; // Convert Euros to cents

                amountToPay = parseInt(amountToPay);

                gateway.transaction.sale({
                    customerId: result[0].braintree_customer_id,
                    amount: amountToPay,
                    paymentMethodToken: result[0].card_token
                }, function(err, result) {

                    console.log(err)
                    console.log(result)

                    if (err) {
                        var response = {
                            "error": "Error in payment",
                            "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                        return
                    }


                    //// have to changeeeeeeeeee
                    if (result.success !== true) {
                        IsPaymentSuccessful = 1;
                        var response = {
                            "error": "Error in payment",
                            "flag": constants.responseFlags.SHOW_ERROR_MESSAGE
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                    }

                    if (result.success === true) {
                        IsPaymentSuccessful = 1;
                        var response = {
                            "log": "Payment Successful"
                        };
                        logging.logResponse(response);
                        res.send(JSON.stringify(response));
                    }

                    var sql = "UPDATE `tb_engagements` SET `tip`=?,`is_payment_successful`=?,`payment_method`=?,`transaction_id`=? WHERE `engagement_id`=? LIMIT 1";
                    connection.query(sql, [tip, IsPaymentSuccessful, paymentMethod, result.transaction.id, engagementId], function(err, result) {
                        logging.logDatabaseQuery("Updating tip and is_payment_successful fields in engagement info", err, result, null);

                        return;

                        //sendPushToDriverToCollectPayableAmountFromCustomer(engagementId,paymentMethod,amountToPay);

                    });


                });


            });


        }

        //if(paymentMethod == 0){     // Invalid choice
        //        var response = {
        //            "error": "Invalid choice",
        //            "flag" : constants.responseFlags.SHOW_ERROR_MESSAGE
        //        };
        //        logging.logResponse(response);
        //        res.send(JSON.stringify(response));
        //        return;
        //
        //}


    });
}

function updateCorporateAdminBalance(money, userId) {

    var sql = " select admin_id from tb_corporate_user where user_id = ? limit 1";
    connection.query(sql, [userId], function(err, result) {

        var updateSql = "update tb_corporate_admin set total_money = total_money + ? where user_id = ? limit 1"
        connection.query(updateSql, [money, result[0].admin_id], function(err, result) {
            alog(err);
            alog(result);
            logging.logDatabaseQuery("Updating corporate admin email", err, result, null);


        });

    });


}


function sendPushToDriverToCollectPayableAmountFromCustomer(engagementId, paymentMethod, amountToPay) {

    var sql = "SELECT `driver_id` FROM `tb_engagements` WHERE `engagement_id`=? LIMIT 1";
    connection.query(sql, [engagementId], function(err, result) {

        logging.logDatabaseQuery("Getting driver_id for engagement", err, result, null);
        var driverId = result[0].driver_id;

        var message = "Please collect " + amountToPay + " in cash from customer";

        if (paymentMethod == 1) { // By card
            message = "Payment of " + amountToPay + " has been collected from customer";
        }

        var flag = constants.notificationFlags.PAYMENT_INFO_TO_DRIVER;

        var payload = {
            "engagement_id": engagementId,
            "paymentMethod": paymentMethod,
            "amountToPay": amountToPay,
            "flag": flag
        };

        utils.sendNotification(driverId, message, flag, payload);
    });

}


function checkPaymentStatus(req, res) {
    var engagementId = req.body.engagement_id;

}
