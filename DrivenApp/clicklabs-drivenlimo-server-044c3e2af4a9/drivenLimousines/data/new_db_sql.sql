ALTER TABLE `tb_engagements` ADD `wait_time_started` TINYINT NOT NULL COMMENT 'is wait time started or not by driver' ;
ALTER TABLE `tb_engagements` ADD `wait_time_seconds` INT NOT NULL COMMENT 'total time in sec of wait time' ;
ALTER TABLE `tb_engagements` ADD `wait_time_charges` INT NOT NULL , ADD `last_wait_time_started` DATETIME NOT NULL ;
ALTER TABLE  `tb_engagements` ADD  `user_feedback` VARCHAR( 300 ) NOT NULL ;
ALTER TABLE `tb_fare` ADD `wait_time_fare_per_min` INT NOT NULL ;






# verify driver sql

UPDATE `drivenlimousines_dev`.`tb_users` SET `last_referred_on` = NULL, `last_ride_on` = NULL, `verification_status` = '1', `reg_as` = '1', `make_me_driver_flag` = '0' WHERE `tb_users`.`user_id` = 513;
INSERT INTO `drivenlimousines_dev`.`tb_exceptional_users` (`id`, `user_id`) VALUES (NULL, '513');
INSERT INTO `drivenlimousines_dev`.`tb_home_locations` (`id`, `driver_id`, `latitude`, `longitude`) VALUES (NULL, '513', '0', '0');
INSERT INTO `drivenlimousines_dev`.`tb_timings` (`driver_id`, `start_time`, `end_time`, `timing_id`) VALUES ('513', '00:00:00', '23:23:23', NULL);


# Corporate Account

ALTER TABLE  `tb_users` ADD  `corporate_email` VARCHAR( 100 ) NOT NULL ;
CREATE TABLE IF NOT EXISTS `tb_corporate_admin` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(70) NOT NULL,
  `access_token` varchar(300) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  `password` varchar(300) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
) ;

CREATE TABLE IF NOT EXISTS `tb_corporate_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `corporate_email` varchar(70) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_id` (`admin_id`,`corporate_email`),
  KEY `admin_id_2` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


ALTER TABLE `tb_corporate_user`
  ADD CONSTRAINT `tb_corporate_user_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `tb_corporate_admin` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE  `tb_corporate_user` ADD  `verification_token` INT NOT NULL ;
ALTER TABLE  `tb_users` ADD  `is_corporate` TINYINT NOT NULL ;