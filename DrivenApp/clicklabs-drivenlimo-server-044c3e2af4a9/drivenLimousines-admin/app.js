
/**
 * Module dependencies.
 */
process.env.NODE_ENV = 'testing';
process.env.NODE_CONFIG_DIR = 'config/';
config = require('config');

var express = require('express');
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var errorhandler = require('errorhandler');
var logger = require('morgan');
var methodOverride = require('method-override');
var multipart       = require('connect-multiparty');
var multipartMiddleware = multipart();
var md5 = require('MD5');
QueryBuilder = require('datatable');

var admin_panel=require('./routes/admin_panel');
var drivers = require('./routes/drivers');
var customer = require('./routes/customer');
var dashboard = require('./routes/dashboard');
var report = require('./routes/reports');
var help = require('./routes/help');
var dispatcher = require('./routes/dispatcher');
var payment = require('./routes/payment');
var promocode = require('./routes/promocode');
constants = require('./routes/constants');

if(process.env.NODE_ENV != 'localhost') {
    mysqlLib = require('./routes/mysqlLib');
} else {
    mysqlLib = require('./routes/mysqlLocalLib');
}

corporateSuperadminApi = require('./routes/corporate_superadmin');
corporateadmin = require('./routes/corporate_admin');
corporatereports = require('./routes/corporate_reports');
corporate_dashboard = require('./routes/corporate_dashboard');

var app = express();

// all environments
app.set('port', process.env.PORT || config.get('PORT'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(favicon(__dirname + '/views/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(errorhandler());
}


app.get('/test', function (req, res) {
    res.render('test');
});


//.......................ADMIN PANEL API's.............................
app.post('/admin_login',admin_panel.admin_login);
app.post('/change_password',admin_panel.change_password);
app.post('/list_all_cars',admin_panel.list_all_cars);
app.post('/update_car_fare',admin_panel.update_car_fare);
app.get('/all_rides',admin_panel.all_rides);
app.get('/ongoing_rides',admin_panel.ongoing_rides);
app.post('/force_complete_engagement',admin_panel.force_complete_engagement);
app.post('/forgot_password',admin_panel.forgotPasswordFromEmail);
app.post('/change_forgot_password',admin_panel.forgotPassword);

app.post('/unapproved_driver_list',drivers.unapproved_driver_list);
app.post('/approve_driver', multipartMiddleware);
app.post('/approve_driver',drivers.approve_driver);
app.post('/add_driver',drivers.add_driver);
app.post('/delete_user',drivers.delete_user);
app.post('/get_all_driver',drivers.get_all_driver);
app.post('/edit_driver_info', multipartMiddleware);
app.post('/edit_driver_info',drivers.update_driver_information);
app.post('/block_unblock_user',drivers.block_unblock_user);

app.post('/get_all_customer',customer.get_all_customer);

//.......................DASHBOARD API's.............................
app.post('/driver_heat_map',dashboard.drivers_heat_map);
app.post('/dashboard_report',dashboard.dashboard_report);

//.......................REPORT API's.............................
app.post('/heat_map_rides_data',report.heat_map_rides_data);
app.post('/heat_map_earnings',report.heat_map_earnings);

//.......................Help's.............................
app.post('/read_about_us',help.read_about_us);
app.post('/write_about_us',help.write_about_us);
app.post('/read_faq',help.read_faq);
app.post('/write_faq',help.write_faq);
app.post('/read_privacy_policy',help.read_privacy_policy);
app.post('/write_privacy_policy',help.write_privacy_policy);
app.post('/read_tnc',help.read_tnc);
app.post('/write_tnc',help.write_tnc);

//.......................DISPATCHER API's.............................
app.post('/searchcustomer_from_phonenumber',dispatcher.searchcustomer_from_phonenumber);
app.post('/register_a_user',dispatcher.register_a_user);
app.post('/find_driver_in_area',dispatcher.find_driver_in_area);
app.post('/create_manual_engagement',dispatcher.create_manual_engagement);
app.post('/driver_response',dispatcher.driver_response);

//.......................PAYMENT API's.............................
app.post('/driver_payment_details',payment.driver_payment_details);
app.post('/payDriver',payment.payDriver);

//.......................PROMOTION CODE API's.............................
app.post('/viewAllPromoCode',promocode.viewAllPromoCode);
app.post('/viewPromoCodeById',promocode.viewPromoCodeById);
app.post('/addPromoCode',promocode.addPromoCode);
app.post('/editPromoCode',promocode.editPromoCode);
app.post('/deletePromoCode',promocode.deletePromoCode);


//.......................Corporate admin API's.............................
app.post('/add_corporate_admin',corporateSuperadminApi.addCorporateAdmin);
app.post('/list_all_coraporate_admins',corporateSuperadminApi.getAllCorporateAdmins);
app.post('/get_all_customer_corporate_admin',corporateSuperadminApi.getAllCostomersCorporateAdmin);
app.post('/get_payment_manually',corporateSuperadminApi.getPaymentManually);


app.post('/corporate_admin_login',corporateadmin.corporate_admin_login);
app.post('/corporate_all_rides',corporateadmin.corporate_all_rides);
app.post('/corporate_ongoing_rides',corporateadmin.corporate_ongoing_rides);
app.post('/getAllCustomersOfCorporateAdmin',corporateadmin.getAllCustomersOfCorporateAdmin);


app.post('/corporate_heat_map_earnings',corporatereports.corporate_heat_map_earnings);
app.post('/corporate_heat_map_rides_data',corporatereports.corporate_heat_map_rides_data);

app.get('/corporate_all_rides',corporateadmin.corporate_all_rides);
app.get('/corporate_ongoing_rides',corporateadmin.corporate_ongoing_rides);
app.post('/corporate_block_unblock_user',corporateadmin.corporate_block_unblock_user);
app.post('/corporate_delete_user',corporateadmin.corporate_delete_user);
app.post('/corporate_change_password',corporateadmin.corporate_change_password);
app.post('/add_corporate_user',corporateadmin.add_corporate_user);
app.post('/corporate_forgot_password',corporateadmin.corporate_forgotPasswordFromEmail);
app.post('/corporate_change_forgot_password',corporateadmin.corporate_forgotPassword);


app.post('/corporate_dashboard_report',corporate_dashboard.corporate_dashboard_report);




http.createServer(app).listen(app.get('port'), function(){
    constants.refreshMessageKeyAndFlag (function(response){
        console.log(response);
    });
    console.log('Express server listening on port ' + app.get('port'));
});