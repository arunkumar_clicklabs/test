var commonFunc = require('./commonfunction');
var logging = require('./logging');
var responses = require('./responses');
var async = require('async');

/*
 * -----------------------------------------------
 * HEAT MAP OF FREE AND BUSY DRIVERS
 * -----------------------------------------------
 */

exports.drivers_heat_map = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT `user_id` AS `driver_id`,`user_name`,`phone_no`,`user_email`,`current_location_latitude`,`current_location_longitude`,`status`,`is_available` FROM `tb_users` ";
                sql += "WHERE `reg_as`=? AND `current_user_status`=? AND `is_deleted`<>?";
                connection.query(sql, [constants.registeredAsFlag.DRIVER, constants.userCurrentStatus.DRIVER_ONLINE, constants.DELETED_STATUS], function (err, driverheatmapresult) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching unapproved driver : ", err, driverheatmapresult);
                        responses.sendError(res);
                        return;
                    }
                    if (result.length > 0) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"driver_data": driverheatmapresult}
                        };
                    } else {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"driver_data": []}
                        };
                    }
                    res.send(JSON.stringify(response));
                    return;
                });
            }
        });
    }
};

/*
 * ---------------------
 * DASHBOARD's REPORTS
 * ---------------------
 */
exports.dashboard_report = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var start_time = req.body.start_time;
    var end_time = req.body.end_time;
    var manvalues = [access_token,start_time,end_time];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                async.parallel([
                    //calling total earning and rides function
                    function(callback) {
                        total_rides_and_earnings(req,res,function(total_earnings_result){
                            callback(null,total_earnings_result)
                        });
                    },
                    //calling total users function
                    function(callback) {
                            total_users(req,res,function(total_users_result){
                                callback(null,total_users_result)
                            });
                    },
                    //calling total users for today function
                    function(callback) {
                        total_users_registered_today(start_time,end_time,req,res,function(total_users_today_result){
                                callback(null,total_users_today_result)
                            });
                    },
                    //calling total rides for today function
                    function(callback) {
                        total_rides_and_earnings_today(start_time,end_time,req,res,function(totalridestoday_result){
                                callback(null,totalridestoday_result)
                            });
                    }],
                    function(err, results) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {
                                "total_data": results[0],
                                "total_users":results[1],
                                "total_users_registered_today":results[2],
                                "specified_dates_data":results[3]
                            }
                        };
                        res.send(JSON.stringify(response));
                    });
            }
        });
    }
};
/*
 * --------------------------------
 * TOTAL EARNINGS OF THE SYSTEM
 * --------------------------------
 */

function total_rides_and_earnings(req, res,callback) {

    var sql = "SELECT count(`engagement_id`) as `total_rides`,IF(SUM(`money_transacted`) IS NULL,0,SUM(`money_transacted`)) as `total_earnings` FROM `tb_engagements` ";
    sql += "WHERE `status`=?";
    connection.query(sql, [constants.engagementStatus.ENDED], function (err, total_earnings) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total earnings : ", err, total_earnings);
            responses.sendError(res);
            return;
        }
        if (total_earnings.length > 0) {
            var response = {"rides":total_earnings[0].total_rides,"earnings":total_earnings[0].total_earnings.toFixed(2)};
        } else {
            var response = '';
        }
        callback(response)
    });
}

/*
 * ------------------------------
 * TOTAL USERS OF THE SYSTEM
 * ------------------------------
 */
function total_users(req, res,callback) {

    var sql = "SELECT count(`user_id`) as `total_users` FROM `tb_users` ";
    sql += "WHERE `reg_as`=? AND `verification_status`=?";
    connection.query(sql, [constants.registeredAsFlag.CUSTOMER,constants.userVerificationStatus.VERIFY], function (err, total_user_result) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total customers : ", err, total_user_result);
            responses.sendError(res);
            return;
        }
            if (total_user_result.length > 0) {
                var response = total_user_result[0].total_users;
            } else {
                var response = '';
            }
        callback(response)
    });
}
/*
 * -----------------------------------------------
 * TOTAL NUMBER OF RIDES AND EARNINGS FOR TODAY
 * -----------------------------------------------
 */
function total_rides_and_earnings_today(start_date,end_date,req, res,callback) {

    var sql = "SELECT count(`engagement_id`) as `total_rides_for_today`,IF(SUM(`money_transacted`) IS NULL,0,SUM(`money_transacted`)) as `total_earnings_for_today` FROM `tb_engagements` ";
    sql += "WHERE `status`=? AND `current_time`>=? AND `current_time`<=?";
    connection.query(sql, [constants.engagementStatus.ENDED,start_date,end_date], function (err, totatridestoday_result) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total rides for today : ", err, totatridestoday_result);
            responses.sendError(res);
            return;
        }
        if (totatridestoday_result.length > 0) {
            var response = {"rides":totatridestoday_result[0].total_rides_for_today,"earnings":totatridestoday_result[0].total_earnings_for_today.toFixed(2)};
        } else {
            var response = '';
        }
        callback(response)
    });
}
/*
 * -----------------------------------------------
 * TOTAL NUMBER OF USERS REGISTERED FOR TODAY
 * -----------------------------------------------
 */
function total_users_registered_today(start_date,end_date,req, res,callback) {

    var sql = "SELECT count(`user_id`) as `total_users_for_today` FROM `tb_users` ";
    sql += "WHERE `verification_status`=? AND `date_registered`>=? AND `date_registered`<=?";
    connection.query(sql, [constants.userVerificationStatus.VERIFY,start_date,end_date], function (err, totatuserstoday_result) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total users for today : ", err, totatuserstoday_result);
            responses.sendError(res);
            return;
        }
        if (totatuserstoday_result.length > 0) {
            var response = totatuserstoday_result[0].total_users_for_today;
        } else {
            var response = '';
        }
        callback(response)
    });
}