var commonFunc = require('./commonfunction');
var logging = require('./logging');
var responses = require('./responses');
var async = require('async');

/*
 * -----------------------------------------------
 * VIEW ALL PROMO CODES
 * -----------------------------------------------
 */

exports.viewAllPromoCode = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var fetchallpromotions = "SELECT * FROM `tb_promotions` where `id`>1"
                connection.query(fetchallpromotions, [], function(err, promotions) {
                    if (promotions.length > 0) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"promotion_codes": promotions}
                        };
                    } else {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"promotion_codes": []}
                        };
                    }
                    res.send(JSON.stringify(response));
                })
            }
        });
    }
};

/*
 * -----------------------------------------------
 * VIEW PROMO CODE BY ID
 * -----------------------------------------------
 */
exports.viewPromoCodeById = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var promo_id = req.body.promo_id;
    var manvalues = [access_token,promo_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT * FROM `tb_promotions` where id = ? limit 1";
                connection.query(sql, [promo_id], function(err, promotions) {
                    if (promotions.length > 0) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"promotion_codes": promotions}
                        };
                    } else {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"promotion_codes": []}
                        };
                    }
                    res.send(JSON.stringify(response));
                });
            }
        });
    }
};

/*
 * ---------------------
 * ADD PROMO CODE
 * ---------------------
 */
exports.addPromoCode = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var start_date = req.body.start_date;
    var end_date = req.body.end_date;
    var promo_code = req.body.promo_code;
    var num_coupons = req.body.num_coupons;
    var validity_window = req.body.validity_window;
    var max_number = req.body.max_number;
    var type = req.body.type;
    var description = req.body.description;
    var manvalues = [access_token, description, type, num_coupons,validity_window,max_number, promo_code, end_date, start_date];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "INSERT into `tb_promotions`(comments,type,master_id,promotion_code,end_date,start_date,num_coupons,validity_window,max_number,coupon_id)"+
                    "values(?,?,?,?,?,?,?,?,?,?)";
                connection.query(sql, [description, type, promo_code, promo_code, end_date, start_date, num_coupons, validity_window, max_number,1], function(err, insertpromoo) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in inserting promo code : ", err, insertpromoo);
                        if (err.code == "ER_DUP_ENTRY") {
                            var response = {
                                "message": constants.responseMessages.SHOW_PROMOCODE_ERROR_MESSAGE,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return
                        }
                    }
                    if (insertpromoo.insertId > 0) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {}
                        }
                    }
                    else {
                        var response = {
                            "message": constants.responseMessages.SHOW_ERROR_MESSAGE,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {}
                        };
                    }
                    res.send(JSON.stringify(response));
                })
            }
        });
    }
};

/*
 * ---------------------
 * EDIT PROMO CODE
 * ---------------------
 */
exports.editPromoCode = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var start_date = req.body.start_date;
    var end_date = req.body.end_date;
    var promo_code = req.body.promo_code;
    var num_coupons = req.body.num_coupons;
    var validity_window = req.body.validity_window;
    var max_number = req.body.max_number;
    var type = req.body.type;
    var description = req.body.description;
    var promo_id = req.body.promo_id;
    var manvalues = [promo_id,access_token, description, type, num_coupons,validity_window,max_number, promo_code, end_date, start_date];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "UPDATE  `tb_promotions` SET comments = ? , type = ?,master_id = ?,promotion_code = ?,"
                    +"end_date = ?,start_date = ?,num_coupons =?,validity_window=?, max_number=?"+
                    " where id = ? LIMIT 1"
                console.log(sql);
                connection.query(sql, [description, type, promo_code, promo_code, end_date, start_date, num_coupons,validity_window,max_number,promo_id], function(err, editpromocode) {
                    if (editpromocode.affectedRows > 0) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {}
                        }
                    }
                    else {
                        var response = {
                            "message": constants.responseMessages.SHOW_ERROR_MESSAGE,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {}
                        };
                    }
                    res.send(JSON.stringify(response));
                });
            }
        });
    }
};

/*
 * ---------------------
 * DELETE PROMO CODE
 * ---------------------
 */
exports.deletePromoCode = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var promo_id = req.body.promo_id;
    var manvalues = [promo_id,access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "DELETE from `tb_promotions` where id=?";
                connection.query(sql, [promo_id], function(err, deleted) {
                    if (deleted.affectedRows > 0) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {}
                        }
                    }
                    else {
                        var response = {
                            "message": constants.responseMessages.SHOW_ERROR_MESSAGE,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {}
                        };
                    }
                    res.send(JSON.stringify(response));
                });
            }
        });
    }
};