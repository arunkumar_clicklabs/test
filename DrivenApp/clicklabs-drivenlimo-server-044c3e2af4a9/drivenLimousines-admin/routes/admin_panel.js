var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses   = require('./responses');
var logging = require('./logging');

/*
 * -----------------------------------------------
 * ADMIN LOGIN VIA EMAIL AND PASSWORD
 * -----------------------------------------------
 */

exports.admin_login = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var email = req.body.email;
    var password = req.body.password;
    var manvalues = [email, password];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        var encrypted_pass = md5(password);
        var sql = "SELECT `admin_id`,`access_token`,`email`,`password` FROM `tb_admin` WHERE `email`=? LIMIT 1";
        connection.query(sql, [email], function(err, result_check) {
            if(err) {
                logging.logDatabaseQueryError("Error in fetching unapproved driver : ", err, result_check);
                responses.sendError(res);
                return;
            } else {
                if (result_check.length == 0) {
                    var response = {
                        "message": constants.responseMessages.INVALID_EMAIL_ID,
                        "status": constants.responseFlags.INVALID_EMAIL_ID,
                        "data" : {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    if(result_check[0].password != encrypted_pass) {
                        var response = {
                            "message": constants.responseMessages.INCORRECT_PASSWORD,
                            "status": constants.responseFlags.WRONG_PASSWORD,
                            "data" : {}
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else {
                        final = {"access_token": result_check[0].access_token};
                        var response = {
                            "message": constants.responseMessages.LOGIN_SUCCESSFULLY,
                            "status": constants.responseFlags.LOGIN_SUCCESSFULLY,
                            "data" : {
                                "admin_id" : result_check[0].admin_id,
                                "access_token" : result_check[0].access_token,
                                "email" : result_check[0].email
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    }
                }
            }
        });
    }
};


/*
 * -----------------------
 * CHANGE ADMIN PASSWORD
 * -----------------------
 */

exports.change_password = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var old_password = req.body.old_password;
    var new_password = req.body.new_password;
    var manvalues = [access_token, old_password, new_password];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var encrypted_old_pass = md5(old_password);
                var encrypted_new_pass = md5(new_password);
                var sql = "SELECT `password` FROM `tb_admin` WHERE `access_token`=? LIMIT 1";
                connection.query(sql, [access_token], function(err, result_check) {
                    if(err) {
                        logging.logDatabaseQueryError("Error in fetching admin info : ", err, result_check);
                        responses.sendError(res);
                        return;
                    } else {
                        if(result_check[0].password != encrypted_old_pass) {
                            var response = {
                                "message": constants.responseMessages.WRONG_PASSWORD,
                                "status": constants.responseFlags.WRONG_PASSWORD,
                                "data" : {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        } else {
                            var sql = "UPDATE `tb_admin` SET `password`=? WHERE `access_token`=? LIMIT 1";
                            connection.query(sql, [encrypted_new_pass, access_token], function(err, result_check) {
                                if(err) {
                                    logging.logDatabaseQueryError("Error in updating admin new password : ", err, result_check);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    var response = {
                                        "message": constants.responseMessages.PASSWORD_CHANGED_SUCCESSFULLY,
                                        "status": constants.responseFlags.PASSWORD_CHANGED_SUCCESSFULLY,
                                        "data" : {}
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};

/*
 * -----------------------------------------------
 * LIST ALL CARS
 * -----------------------------------------------
 */

exports.list_all_cars = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            }
            var sql = "SELECT `id` AS `car_id`,`fare_fixed`,`fare_per_km`,`fare_per_min`,";
            sql += "`car_type`,`car_name` FROM `tb_fare`";
            connection.query(sql, [], function(err, getAllCars) {
                if(err) {
                    logging.logDatabaseQueryError("Error in fetching all cars : ", err, getAllCars);
                    responses.sendError(res);
                    return;
                } else {
                    if (getAllCars.length > 0) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"car_list": getAllCars}
                        };
                    } else {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"car_list": []}
                        };
                    }
                    res.send(JSON.stringify(response));
                    return;
                }
            });
        });
    }
};

/*
 * -----------------------------------------------
 * UPDATE CAR FARE
 * -----------------------------------------------
 */

exports.update_car_fare = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var car_id = req.body.car_id;
    var fare_fixed = req.body.fare_fixed;
    var fare_per_km = req.body.fare_per_km;
    var fare_per_min = req.body.fare_per_min;
    var manvalues = [access_token, car_id, fare_fixed, fare_per_km, fare_per_min];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT `id` FROM `tb_fare` WHERE `id`=? LIMIT 1";
                connection.query(sql, [car_id], function(err, checkCarId) {
                    if(err) {
                        logging.logDatabaseQueryError("Error in checking car id : ", err, checkCarId);
                        responses.sendError(res);
                        return;
                    } else {
                        if (checkCarId.length == 0) {
                            var response = {
                                "message": constants.responseMessages.INVALID_CAR_ID,
                                "status": constants.responseFlags.INVALID_CAR_ID,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        } else {
                            var updateSql = "UPDATE `tb_fare` SET `fare_fixed`=?,`fare_per_km`=?,`fare_per_min`=? ";
                            updateSql += "WHERE `id`=? LIMIT 1";
                            var binparams = [fare_fixed, fare_per_km, fare_per_min, car_id];
                            connection.query(updateSql, binparams, function(err, updateCarFare) {
                                if(err) {
                                    logging.logDatabaseQueryError("Error in updating car fare : ", err, updateCarFare);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    var response = {
                                        "message": constants.responseMessages.CAR_FARE_UPDATED_SUCCESSFULLY,
                                        "status": constants.responseFlags.CAR_FARE_UPDATED_SUCCESSFULLY,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};


/*
 * -----------------------------------------------
 * GET ALL RIDES
 * -----------------------------------------------
 */

exports.all_rides = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    var tableDefinition = {
        sSelectSql: "" +
        "tb_engagements.`current_time` as ride_date,"+
        "tb_engagements.pickup_location_address," +
        "tb_engagements.drop_location_address,"+
        "tb_engagements.pickup_time,"+
        "tb_engagements.drop_time,"+
        "tb_engagements.tip,"+
        "tb_engagements.actual_fare as fare,"+
        "tb_engagements.status,"+

        "customer_table.user_name as `customer_name`,"+
        "driver_table.user_name as `driver_name`",

        sFromSql: "tb_engagements, tb_users as customer_table, tb_users as driver_table",
        sWhereAndSql: "(tb_engagements.status = "+constants.engagementStatus.ENDED+" ||  tb_engagements.status = "+constants.engagementStatus.REJECTED_BY_DRIVER+
                      " || tb_engagements.status = "+constants.engagementStatus.CANCELLED_BY_CUSTOMER+" || tb_engagements.status = "+constants.engagementStatus.ACCEPTED_THEN_REJECTED+
                      " || tb_engagements.status = "+constants.engagementStatus.CANCELLED_ACCEPTED_REQUEST+")"+
                      "&& tb_engagements.user_id = customer_table.user_id && tb_engagements.driver_id = driver_table.user_id",

        aSearchColumns: [
            "tb_engagements.current_time","tb_engagements.pickup_location_address","tb_engagements.drop_location_address",
            "tb_engagements.pickup_time","tb_engagements.drop_time",
            "tb_engagements.actual_fare", "tb_engagements.status","customer_table.user_name","driver_table.user_name"
        ],

        sCountColumnName: "tb_engagements.engagement_id",
        aoColumnDefs: [
            {mData: "ride_date", bSearchable: true},
            {mData: "pickup_location_address", bSearchable: true},
            {mData: "drop_location_address", bSearchable: true},
            {mData: "pickup_time", bSearchable: true},
            {mData: "drop_time", bSearchable: true},
            {mData: "fare", bSearchable: true},
            {mData: "status", bSearchable: true},
            {mData: "customer_name", bSearchable: true},
            {mData: "driver_name", bSearchable: true}
        ]
    };


    var queryBuilder = new QueryBuilder(tableDefinition);
    var requestQuery = req.query;
    var queries = queryBuilder.buildQuery(requestQuery);
    queries = queries.join(" ");
    connection.query(queries, function (err, resultAllRidesData) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching all rides : ", err, resultAllRidesData);
            responses.sendError(res);
            return;
        }
        var resultAllRides = resultAllRidesData.pop();
        var resultAllRidesLength = resultAllRides.length;
        if(resultAllRidesLength > 0) {

            var arrResultRides = new Array();
            for (var i = 0; i < resultAllRidesLength; i++) {
                arrResultRides[i] = new Array();
                arrResultRides[i][0] = commonFunc.convertTimeIntoLocal(resultAllRides[i].ride_date,req.query.timezone);
                arrResultRides[i][1] = resultAllRides[i].customer_name;
                arrResultRides[i][2] = resultAllRides[i].driver_name;
                arrResultRides[i][3] = resultAllRides[i].pickup_location_address;
                arrResultRides[i][4] = commonFunc.convertTimeIntoLocal(resultAllRides[i].pickup_time,req.query.timezone);
                arrResultRides[i][5] = resultAllRides[i].drop_location_address;
                arrResultRides[i][6] = commonFunc.convertTimeIntoLocal(resultAllRides[i].drop_time,req.query.timezone);
                arrResultRides[i][7] = resultAllRides[i].tip;
                arrResultRides[i][7] = resultAllRides[i].fare;
                if (resultAllRides[i].status == constants.engagementStatus.ENDED) {
                    arrResultRides[i][9] = "Completed";
                } else if (resultAllRides[i].status == constants.engagementStatus.REJECTED_BY_DRIVER) {
                    arrResultRides[i][9] = "Rejected by driver";
                } else if (resultAllRides[i].status == constants.engagementStatus.ACCEPTED_THEN_REJECTED) {
                    arrResultRides[i][9] = "Rejected by driver";
                }else{
                    arrResultRides[i][9] = "Rejected by driver";
                }
            }
            var response = {
                "iTotalDisplayRecords":resultAllRidesData[0][0]['COUNT(*)'],
                "iTotalRecords":resultAllRidesData[0][0]['COUNT(*)'],
                "sEcho": 0,
                "aaData": arrResultRides
            };
            res.send(JSON.stringify(response));
            return;
        } else {
            var response = {"iTotalDisplayRecords":0,"iTotalRecords":resultAllRidesData[0][0]['COUNT(*)'],"sEcho":requestQuery.sEcho,"aaData":[]};
            res.send(JSON.stringify(response));
            return;
        }
    });
};


/*
 * -----------------------------------------------
 * ONGOING RIDES
 * -----------------------------------------------
 */

exports.ongoing_rides = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var tableDefinition = {
        sSelectSql: "" +
        "tb_engagements.`engagement_id`,"+
        "tb_engagements.`current_time` as ride_date,"+
        "tb_engagements.pickup_location_address," +
        "tb_engagements.drop_location_address,"+
        "tb_engagements.pickup_time,"+
        "tb_engagements.drop_time,"+
        "tb_engagements.actual_fare as fare,"+
        "tb_engagements.status,"+

        "customer_table.user_name as `customer_name`,"+
        "driver_table.user_name as `driver_name`",

        sFromSql: "tb_engagements, tb_users as customer_table, tb_users as driver_table",
        sWhereAndSql: "(tb_engagements.status = "+constants.engagementStatus.STARTED+")"+
        "&& tb_engagements.user_id = customer_table.user_id && tb_engagements.driver_id = driver_table.user_id",

        aSearchColumns: [
            "tb_engagements.current_time","tb_engagements.pickup_location_address","tb_engagements.drop_location_address",
            "tb_engagements.pickup_time","tb_engagements.drop_time",
            "tb_engagements.actual_fare", "tb_engagements.status","customer_table.user_name","driver_table.user_name"
        ],

        sCountColumnName: "tb_engagements.engagement_id",
        aoColumnDefs: [
            {mData: "ride_date", bSearchable: true},
            {mData: "pickup_location_address", bSearchable: true},
            {mData: "drop_location_address", bSearchable: true},
            {mData: "pickup_time", bSearchable: true},
            {mData: "drop_time", bSearchable: true},
            {mData: "fare", bSearchable: true},
            {mData: "status", bSearchable: true},
            {mData: "customer_name", bSearchable: true},
            {mData: "driver_name", bSearchable: true}
        ]
    };
    var queryBuilder = new QueryBuilder(tableDefinition);
    var requestQuery = req.query;
    var queries = queryBuilder.buildQuery(requestQuery);
    queries = queries.join(" ");
    connection.query(queries, function (err, resultAllRidesData) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching all ongoing rides : ", err, resultAllRidesData);
            responses.sendError(res);
            return;
        }
        var resultAllRides = resultAllRidesData.pop();
        var resultAllRidesLength = resultAllRides.length;
        if(resultAllRidesLength > 0) {

            var arrResultRides = new Array();
            for (var i = 0; i < resultAllRidesLength; i++) {
                arrResultRides[i] = new Array();
                arrResultRides[i][0] = commonFunc.convertTimeIntoLocal(resultAllRides[i].ride_date,req.query.timezone);
                arrResultRides[i][1] = resultAllRides[i].customer_name;
                arrResultRides[i][2] = resultAllRides[i].driver_name;
                arrResultRides[i][3] = resultAllRides[i].pickup_location_address;
                arrResultRides[i][4] = commonFunc.convertTimeIntoLocal(resultAllRides[i].pickup_time,req.query.timezone);
                arrResultRides[i][5] = resultAllRides[i].drop_location_address;
                arrResultRides[i][6] = commonFunc.convertTimeIntoLocal(resultAllRides[i].drop_time,req.query.timezone);
                //arrResultRides[i][7] = resultAllRides[i].fare;
                if(resultAllRides[i].status == constants.engagementStatus.STARTED) {
                    arrResultRides[i][7] = "On Going";
                }
                arrResultRides[i][8] = '<input type="button" class="btn btn-danger completeride" id="'+resultAllRides[i].engagement_id+'" value="Force Complete Ride"/>';
            }
            var response = {
                "iTotalDisplayRecords":resultAllRidesData[0][0]['COUNT(*)'],
                "iTotalRecords":resultAllRidesData[0][0]['COUNT(*)'],
                "sEcho": 0,
                "aaData": arrResultRides
            };
            res.send(JSON.stringify(response));
            return;
        } else {
            var response = {"iTotalDisplayRecords":0,"iTotalRecords":resultAllRidesData[0][0]['COUNT(*)'],"sEcho":requestQuery.sEcho,"aaData":[]};
            res.send(JSON.stringify(response));
            return;
        }
    });
};

/*
 * ----------------------------------
 * COMPLETE ENGAGEMENT FORCEFULLY
 * ----------------------------------
 */

exports.force_complete_engagement = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var engagement_id = req.body.engagement_id;
    var manvalues = [access_token, engagement_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT `session_id`,`driver_id` FROM `tb_engagements` WHERE `engagement_id`=? LIMIT 1";
                connection.query(sql, [engagement_id], function(err, sessionID) {
                    if(err) {
                        logging.logDatabaseQueryError("Error in checking sessionID : ", err, sessionID);
                        responses.sendError(res);
                        return;
                    } else {
                        var driversql = "UPDATE `tb_users` SET `status`=? ";
                        driversql += "WHERE `user_id`=? LIMIT 1";
                        var bindparams = [constants.userFreeStatus.FREE,sessionID[0].driver_id];
                        connection.query(driversql, bindparams, function (err, updateDriverStatus) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in updating Driver Status : ", err, updateDriverStatus);
                                responses.sendError(res);
                                return;
                            } else {
                                var updateSql = "UPDATE `tb_engagements` SET `money_transacted`=?,`status`=?,`is_payment_successful`= 1 ";
                                updateSql += "WHERE `engagement_id`=? LIMIT 1";
                                var bindparams = [0, constants.engagementStatus.ENDED, engagement_id];
                                connection.query(updateSql, bindparams, function (err, updateEngagement) {
                                    if (err) {
                                        logging.logDatabaseQueryError("Error in updating engagement : ", err, updateEngagement);
                                        responses.sendError(res);
                                        return;
                                    } else {
                                        var deactivate_session = "UPDATE `tb_session` SET `is_active`=? WHERE `session_id`=? LIMIT 1";
                                        var values = [constants.sessionStatus.INACTIVE, sessionID[0].session_id];
                                        connection.query(deactivate_session, values, function (err, result) {
                                            if (err) {
                                                logging.logDatabaseQueryError("Deactivating the session", err, result);
                                                responses.sendError(res);
                                                return;
                                            }
                                            else {
                                                var response = {
                                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                                    "data": {}
                                                };
                                                res.send(JSON.stringify(response));
                                                return;
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};

/*
 * ----------------------------------
 * FORGOT PASSWORD
 * ----------------------------------
 */

exports.forgotPassword = function(req, res) {
    var md5 = require('MD5');
    res.header("Access-Control-Allow-Origin", "*");
    var token = req.body.token;
    var password = req.body.password;
    var email = req.body.email;
    var manValues = [token, password, email];
    var checkdata = commonFunc.checkBlank(manValues);
    if (checkdata == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminUsingEMAIL(email, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                password = md5(password);
                var verificationToken = md5(token);
                var engagements = "UPDATE tb_admin SET password = ?,verification_token = ? WHERE ";
                engagements += "verification_token = ? AND email = ? LIMIT 1";
                connection.query(engagements, [password, verificationToken, token, email], function (err, forgotPasswordResult) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in changing forgot password : ", err, forgotPasswordResult);
                        responses.sendError(res);
                        return;
                    } else {
                        if (forgotPasswordResult.affectedRows > 0) {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {}
                            };
                        } else {
                            var response = {
                                "message": constants.responseMessages.SHOW_ERROR_MESSAGE,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                        }
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    };
};

/*
 * ----------------------------------
 * FORGOT PASSWORD FROM EMAIL
 * ----------------------------------
 */
exports.forgotPasswordFromEmail = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var email = req.body.email;
    var manValues = [email];
    var checkdata = commonFunc.checkBlank(manValues);
    if (checkdata == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminUsingEMAIL(email, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var md5 = require('MD5');
                var token = md5(result[0].email);
                var sql = "UPDATE tb_admin set verification_token=? WHERE email=? LIMIT 1";
                connection.query(sql, [token, email], function(err, response) {

                    var link = config.get('forgotpasswordSuperAdminPageLink');
                    link += "?token=" + token + "&email=" + email;

                    commonFunc.emailFormatting(email,'','','',link, function(returnMessage){
                        commonFunc.sendHtmlContent(email,returnMessage,"[Driven Limousines] Forgot Password", function (result) {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        });
                    })
                });
            }
        });
    }
};