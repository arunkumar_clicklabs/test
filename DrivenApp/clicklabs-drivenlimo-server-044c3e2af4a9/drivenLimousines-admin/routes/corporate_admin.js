var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses   = require('./responses');
var logging = require('./logging');

/*
 * -----------------------------------------------
 * CORPORATE ADMIN LOGIN VIA EMAIL AND PASSWORD
 * -----------------------------------------------
 */

exports.corporate_admin_login = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var email = req.body.email;
    var password = req.body.password;
    var manvalues = [email, password];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        var encrypted_pass = md5(password);
        var sql = "SELECT * FROM `tb_corporate_admin` WHERE `email`=? LIMIT 1";
        connection.query(sql, [email], function(err, result_check) {
            if(err) {
                logging.logDatabaseQueryError("Error in fetching corporate admin information : ", err, result_check);
                responses.sendError(res);
                return;
            } else {
                if (result_check.length == 0) {
                    var response = {
                        "message": constants.responseMessages.INVALID_EMAIL_ID,
                        "status": constants.responseFlags.INVALID_EMAIL_ID,
                        "data" : {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    if(result_check[0].password != encrypted_pass) {
                        var response = {
                            "message": constants.responseMessages.INCORRECT_PASSWORD,
                            "status": constants.responseFlags.WRONG_PASSWORD,
                            "data" : {}
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else {
                        final = {"access_token": result_check[0].access_token};
                        var response = {
                            "message": constants.responseMessages.LOGIN_SUCCESSFULLY,
                            "status": constants.responseFlags.LOGIN_SUCCESSFULLY,
                            "data" : result_check
                        };
                        res.send(JSON.stringify(response));
                        return;
                    }
                }
            }
        });
    }
};



/*
 * -----------------------------------------------
 * GET ALL RIDES
 * -----------------------------------------------
 */

exports.corporate_all_rides = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    commonFunc.authenticateCorporateAdminAccessToken(req.query.access_token, function (authenticateCorporateAdminAccessTokenResult) {
        if (authenticateCorporateAdminAccessTokenResult == 0) {
            responses.authenticationErrorResponse(res);
            return;
        } else {
            var tableDefinition = {
                sSelectSql: "" +
                "tb_engagements.`current_time` as ride_date," +
                "tb_engagements.pickup_location_address," +
                "tb_engagements.drop_location_address," +
                "tb_engagements.pickup_time," +
                "tb_engagements.drop_time," +
                "tb_engagements.actual_fare as fare," +
                "tb_engagements.status," +
                "tb_engagements.tip," +

                "customer_table.user_name as `customer_name`," +
                "driver_table.user_name as `driver_name`",

                sFromSql: "tb_engagements, tb_users as customer_table, tb_users as driver_table ,tb_corporate_user",
                sWhereAndSql: "(tb_engagements.status = " + constants.engagementStatus.ENDED + ")" +
                "&& tb_engagements.user_id = customer_table.user_id && tb_engagements.driver_id = driver_table.user_id && tb_corporate_user.user_id = customer_table.user_id && tb_corporate_user.admin_id = "+authenticateCorporateAdminAccessTokenResult[0].user_id,

                aSearchColumns: [
                    "tb_engagements.current_time","tb_engagements.pickup_location_address","tb_engagements.drop_location_address",
                    "tb_engagements.pickup_time","tb_engagements.drop_time",
                    "tb_engagements.actual_fare", "tb_engagements.status","customer_table.user_name","driver_table.user_name"
                ],

                sCountColumnName: "tb_engagements.engagement_id",
                aoColumnDefs: [
                    {mData: "ride_date", bSearchable: true},
                    {mData: "pickup_location_address", bSearchable: true},
                    {mData: "drop_location_address", bSearchable: true},
                    {mData: "pickup_time", bSearchable: true},
                    {mData: "drop_time", bSearchable: true},
                    {mData: "fare", bSearchable: true},
                    {mData: "status", bSearchable: true},
                    {mData: "customer_name", bSearchable: true},
                    {mData: "driver_name", bSearchable: true}
                ]
            };


            var queryBuilder = new QueryBuilder(tableDefinition);
            var requestQuery = req.query;
            var queries = queryBuilder.buildQuery(requestQuery);
            queries = queries.join(" ");
            connection.query(queries, function (err, resultAllRidesData) {
                if (err) {
                    logging.logDatabaseQueryError("Error in fetching all rides : ", err, resultAllRidesData);
                    responses.sendError(res);
                    return;
                }
                var resultAllRides = resultAllRidesData.pop();
                var resultAllRidesLength = resultAllRides.length;
                if (resultAllRidesLength > 0) {

                    var arrResultRides = new Array();
                    for (var i = 0; i < resultAllRidesLength; i++) {
                        arrResultRides[i] = new Array();
                        arrResultRides[i][0] = commonFunc.convertTimeIntoLocal(resultAllRides[i].ride_date, req.query.timezone);
                        arrResultRides[i][1] = resultAllRides[i].customer_name;
                        arrResultRides[i][2] = resultAllRides[i].driver_name;
                        arrResultRides[i][3] = resultAllRides[i].pickup_location_address;
                        arrResultRides[i][4] = commonFunc.convertTimeIntoLocal(resultAllRides[i].pickup_time, req.query.timezone);
                        arrResultRides[i][5] = resultAllRides[i].drop_location_address;
                        arrResultRides[i][6] = commonFunc.convertTimeIntoLocal(resultAllRides[i].drop_time, req.query.timezone);
                        arrResultRides[i][7] = resultAllRides[i].tip;
                        arrResultRides[i][8] = resultAllRides[i].fare;
                        if (resultAllRides[i].status == constants.engagementStatus.ENDED) {
                            arrResultRides[i][9] = "Completed";
                        } else if (resultAllRides[i].status == constants.engagementStatus.REJECTED_BY_DRIVER) {
                            arrResultRides[i][9] = "Rejected by driver";
                        } else if (resultAllRides[i].status == constants.engagementStatus.ACCEPTED_THEN_REJECTED) {
                            arrResultRides[i][9] = "Rejected by driver";
                        }else{
                            arrResultRides[i][9] = "Rejected by driver";
                        }
                    }
                    var response = {
                        "iTotalDisplayRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "sEcho": 0,
                        "aaData": arrResultRides
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    var response = {
                        "iTotalDisplayRecords": 0,
                        "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "sEcho": requestQuery.sEcho,
                        "aaData": []
                    };
                    res.send(JSON.stringify(response));
                    return;
                }
            });
        }
    });
};


/*
 * -----------------------------------------------
 * GET ALL CORPORATE USERS
 * -----------------------------------------------
 */

exports.getAllCustomersOfCorporateAdmin = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateCorporateAdminAccessToken(access_token, function(result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT u.`user_id` AS `customer_id`,u.`user_name`,u.`user_email`,u.`phone_no`,";
                sql += "u.`total_rides_as_user`,u.`total_rating_got_user`,u.`total_rating_user`,"
                sql += "u.`req_stat` AS `registration_status`,u.`is_blocked`,u.`is_deleted` , tca.user_name as corporate_admin , tca.email as corporate_admin_email  FROM `tb_users` u inner join tb_corporate_user tcu on tcu.user_id = u.user_id inner join tb_corporate_admin tca on tca.user_id = tcu.admin_id   WHERE ";
                sql += "u.`reg_as`=? AND u.`make_me_driver_flag`=? AND u.`is_deleted`<>? and u.is_corporate = ? and tcu.admin_id = ? ";
                var bindparams = [constants.registeredAsFlag.CUSTOMER, constants.makeMeDriverFlag.APPROVED_DRIVER_STATUS, constants.DELETED_STATUS, constants.CORPORATE_USER_STATUS, result[0].user_id];
                connection.query(sql, bindparams, function(err, getAllCustomer) {
                    console.log(err)

                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching all customers of corporate admin : ", err, getAllCustomer);
                        responses.sendError(res);
                        return;
                    } else {
                        if (getAllCustomer.length > 0) {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {
                                    "customer_list": getAllCustomer
                                }
                            };
                        } else {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {
                                    "customer_list": []
                                }
                            };
                        }
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    }
};

/*
 * -----------------------------------------------
 * ONGOING RIDES
 * -----------------------------------------------
 */

exports.corporate_ongoing_rides = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    commonFunc.authenticateAdminAccessToken(req.query.access_token, function (result) {
        if (result == 0) {
            responses.authenticationErrorResponse(res);
            return;
        } else {
            var tableDefinition = {
                sSelectSql: "" +
                "tb_engagements.`engagement_id`," +
                "tb_engagements.`current_time` as ride_date," +
                "tb_engagements.pickup_location_address," +
                "tb_engagements.drop_location_address," +
                "tb_engagements.pickup_time," +
                "tb_engagements.drop_time," +
                "tb_engagements.actual_fare as fare," +
                "tb_engagements.status," +

                "customer_table.user_name as `customer_name`," +
                "driver_table.user_name as `driver_name`",

                sFromSql: "tb_engagements, tb_users as customer_table, tb_users as driver_table",
                sWhereAndSql: "(tb_engagements.status = " + constants.engagementStatus.STARTED + ")" +
                "&& tb_engagements.user_id = customer_table.user_id && tb_engagements.driver_id = driver_table.user_id && tb_corporate_user.user_id = customer_table.user_id && tb_corporate_user.admin_id = "+authenticateCorporateAdminAccessTokenResult[0].user_id,

                aSearchColumns: [
                    "tb_engagements.current_time","tb_engagements.pickup_location_address","tb_engagements.drop_location_address",
                    "tb_engagements.pickup_time","tb_engagements.drop_time",
                    "tb_engagements.actual_fare", "tb_engagements.status","customer_table.user_name","driver_table.user_name"
                ],

                sCountColumnName: "tb_engagements.engagement_id",
                aoColumnDefs: [
                    {mData: "ride_date", bSearchable: true},
                    {mData: "pickup_location_address", bSearchable: true},
                    {mData: "drop_location_address", bSearchable: true},
                    {mData: "pickup_time", bSearchable: true},
                    {mData: "drop_time", bSearchable: true},
                    {mData: "fare", bSearchable: true},
                    {mData: "status", bSearchable: true},
                    {mData: "customer_name", bSearchable: true},
                    {mData: "driver_name", bSearchable: true}
                ]
            };
            var queryBuilder = new QueryBuilder(tableDefinition);
            var requestQuery = req.query;
            var queries = queryBuilder.buildQuery(requestQuery);
            queries = queries.join(" ");
            connection.query(queries, function (err, resultAllRidesData) {
                if (err) {
                    logging.logDatabaseQueryError("Error in fetching all ongoing rides : ", err, resultAllRidesData);
                    responses.sendError(res);
                    return;
                }
                var resultAllRides = resultAllRidesData.pop();
                var resultAllRidesLength = resultAllRides.length;
                if (resultAllRidesLength > 0) {

                    var arrResultRides = new Array();
                    for (var i = 0; i < resultAllRidesLength; i++) {
                        arrResultRides[i] = new Array();
                        arrResultRides[i][0] = commonFunc.convertTimeIntoLocal(resultAllRides[i].ride_date, req.query.timezone);
                        arrResultRides[i][1] = resultAllRides[i].customer_name;
                        arrResultRides[i][2] = resultAllRides[i].driver_name;
                        arrResultRides[i][3] = resultAllRides[i].pickup_location_address;
                        arrResultRides[i][4] = commonFunc.convertTimeIntoLocal(resultAllRides[i].pickup_time, req.query.timezone);
                        arrResultRides[i][5] = resultAllRides[i].drop_location_address;
                        arrResultRides[i][6] = commonFunc.convertTimeIntoLocal(resultAllRides[i].drop_time, req.query.timezone);
                        //arrResultRides[i][7] = resultAllRides[i].fare;
                        if (resultAllRides[i].status == constants.engagementStatus.STARTED) {
                            arrResultRides[i][7] = "On Going";
                        }
                        arrResultRides[i][8] = '<input type="button" class="btn btn-danger completeride" id="' + resultAllRides[i].engagement_id + '" value="Force Complete Ride"/>';
                    }
                    var response = {
                        "iTotalDisplayRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "sEcho": 0,
                        "aaData": arrResultRides
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    var response = {
                        "iTotalDisplayRecords": 0,
                        "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "sEcho": requestQuery.sEcho,
                        "aaData": []
                    };
                    res.send(JSON.stringify(response));
                    return;
                }
            });
        }
    });
};

/*
 * -----------------------------------------------
 * DECLINE REQUEST OR DELETE USER
 * -----------------------------------------------
 */

exports.corporate_delete_user = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var user_id = req.body.user_id;
    var manvalues = [access_token, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateCorporateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {

                isASessionActive(user_id, function(activeSessionResult) {

                    if(activeSessionResult.session_id < 0) {

                        var sql = "SELECT `user_email`,`reg_as`,`make_me_driver_flag` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
                        connection.query(sql, [user_id], function (err, checkDriverData) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in checking user id : ", err, checkDriverData);
                                responses.sendError(res);
                                return;
                            } else {
                                if (checkDriverData.length == 0) {
                                    var response = {
                                        "message": constants.responseMessages.USER_NOT_FOUND,
                                        "status": constants.responseFlags.USER_NOT_FOUND,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                } else {
                                    var str = '';
                                    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    var size = chars.length;
                                    for (var i = 0; i < 4; i++) {
                                        var randomnumber = Math.floor(Math.random() * size);
                                        str = chars[randomnumber] + str;
                                    }
                                    var new_email = str + "DELETED" + checkDriverData[0].user_email;
                                    var sql = "UPDATE `tb_users` SET `user_email`=?,`make_me_driver_flag`=?,`is_deleted`=?";
                                    sql += " WHERE `user_id`=? LIMIT 1";
                                    var bindparams = [new_email, constants.makeMeDriverFlag.INVALID, constants.DELETED_STATUS, user_id];
                                    connection.query(sql, bindparams, function (err, updateDriverStatus) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in updating user deleted status : ", err, updateDriverStatus);
                                            responses.sendError(res);
                                            return;
                                        } else {
                                            var message = "You have been deleted by admin.";
                                            commonFunc.emailFormatting(checkDriverData[0].user_email, message, '', '', '', function (returnMessage) {

                                                commonFunc.sendHtmlContent(checkDriverData[0].user_email, returnMessage, "[Driven Limousines] Removal", function (result) {

                                                    var response = {
                                                        "message": constants.responseMessages.USER_DELETED_SUCCESSFULLY,
                                                        "status": constants.responseFlags.USER_DELETED_SUCCESSFULLY,
                                                        "data": {}
                                                    };
                                                    res.send(JSON.stringify(response));
                                                    return;
                                                });
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    }
                    else{
                        var response = {
                            "message": constants.responseMessages.DRIVER_BUSY_NOW,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {}
                        };
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    }
};

/*
 * ---------------------------
 * BLOCK/UNBLOCK USER
 * ---------------------------
 */

exports.corporate_block_unblock_user = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var user_id = req.body.user_id;
    var block_status = req.body.block_status;
    var manvalues = [access_token, user_id, block_status];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateCorporateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                if (!(block_status == constants.BLOCK_STATUS || block_status == constants.UNBLOCK_STATUS)) {
                    var response = {
                        "message": constants.responseMessages.INVALID_BLOCK_STATUS,
                        "status": constants.responseFlags.INVALID_BLOCK_STATUS,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    isASessionActive(user_id, function(activeSessionResult) {

                        if(activeSessionResult.session_id < 0) {
                            var sql = "SELECT `make_me_driver_flag`,`user_email` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
                            connection.query(sql, [user_id], function (err, checkData) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in checking driver id : ", err, checkData);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    if (checkData.length == 0) {
                                        var response = {
                                            "message": constants.responseMessages.USER_NOT_FOUND,
                                            "status": constants.responseFlags.USER_NOT_FOUND,
                                            "data": {}
                                        };
                                        res.send(JSON.stringify(response));
                                        return;
                                    } else {
                                        var sql = "UPDATE `tb_users` SET `is_blocked`=? WHERE `user_id`=? LIMIT 1";
                                        connection.query(sql, [block_status, user_id], function (err, updateDriverStatus) {
                                            if (err) {
                                                logging.logDatabaseQueryError("Error in updating user block status : ", err, updateDriverStatus);
                                                responses.sendError(res);
                                                return;
                                            } else {
                                                var message='';
                                                if(block_status==0){
                                                    message = "You have been unblocked by admin.";
                                                }
                                                else if (block_status==1){
                                                    message = "You have been blocked by admin.";
                                                }
                                                commonFunc.emailFormatting(checkData[0].user_email, message, '', '', '', function(returnMessage) {

                                                    commonFunc.sendHtmlContent(checkData[0].user_email, returnMessage, "[Driven Limousines] Block/Unblock", function (result) {

                                                        var response = {
                                                            "message": constants.responseMessages.STATUS_CHANGED_SUCCESSFULLY,
                                                            "status": constants.responseFlags.STATUS_CHANGED_SUCCESSFULLY,
                                                            "data": {}
                                                        };
                                                        res.send(JSON.stringify(response));
                                                        return;
                                                    });
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        }
                        else{
                            var response = {
                                "message": constants.responseMessages.DRIVER_BUSY_NOW,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        }
                    });
                }
            }
        });
    }
};


function isASessionActive(user_id, callback){
    var get_session = "SELECT `session_id`, `ride_acceptance_flag` " +
        "FROM `tb_session` " +
        "WHERE `user_id`=? && `is_active`=? && `date` > timestamp(DATE_SUB(NOW(), INTERVAL 2 HOUR))  ORDER BY `date` DESC LIMIT 1";
    var values = [user_id, constants.sessionStatus.ACTIVE];
    connection.query(get_session, values, function(err, result_session) {
        logging.logDatabaseQueryError("Getting any active session for the user", err, result_session, null);

        if(result_session.length > 0) {
            callback(result_session[0]);
        }
        else {
            callback({"session_id":-1});
        }
    });
}

/*
 * -----------------------
 * CHANGE ADMIN PASSWORD
 * -----------------------
 */

exports.corporate_change_password = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var old_password = req.body.old_password;
    var new_password = req.body.new_password;
    var manvalues = [access_token, old_password, new_password];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateCorporateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var encrypted_old_pass = md5(old_password);
                var encrypted_new_pass = md5(new_password);
                var sql = "SELECT `password` FROM `tb_corporate_admin` WHERE `access_token`=? LIMIT 1";
                connection.query(sql, [access_token], function(err, result_check) {
                    if(err) {
                        logging.logDatabaseQueryError("Error in fetching admin info : ", err, result_check);
                        responses.sendError(res);
                        return;
                    } else {
                        if(result_check[0].password != encrypted_old_pass) {
                            var response = {
                                "message": constants.responseMessages.WRONG_PASSWORD,
                                "status": constants.responseFlags.WRONG_PASSWORD,
                                "data" : {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        } else {
                            var sql = "UPDATE `tb_corporate_admin` SET `password`=? WHERE `access_token`=? LIMIT 1";
                            connection.query(sql, [encrypted_new_pass, access_token], function(err, result_check) {
                                if(err) {
                                    logging.logDatabaseQueryError("Error in updating admin new password : ", err, result_check);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    var response = {
                                        "message": constants.responseMessages.PASSWORD_CHANGED_SUCCESSFULLY,
                                        "status": constants.responseFlags.PASSWORD_CHANGED_SUCCESSFULLY,
                                        "data" : {}
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};

/*
 * -----------------------
 * ADD CORPORATE USER
 * -----------------------
 */

exports.add_corporate_user = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var email = req.body.email;
    var manvalues = [access_token, email];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateCorporateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var emails = email.split(',');
                console.log(emails)
                var emailLength = emails.length;
                    var i=0;
                    emails.forEach(function(email){
                        var verification_token = commonFunc.generateRandomString();
                        var sql = "INSERT INTO `tb_corporate_user` (`admin_id`,`corporate_email`,`verification_token`) VALUES (?,?,?)";
                        connection.query(sql, [result[0].user_id, email, verification_token], function (err, result_check) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in inserting admin info : ", err, result_check);
                                //responses.sendError(res);
                                //return;
                            } else {

                                var msg = "code = " + verification_token;
                                commonFunc.sendHtmlContent(email, msg, "[Driven Limousines] Code", function (result) {


                                });
                            }
                        });
                    })
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": {}
                };
                res.send(JSON.stringify(response));
                return;

            }
        });
    }
};

/*
 * ----------------------------------
 * FORGOT PASSWORD
 * ----------------------------------
 */

exports.corporate_forgotPassword = function(req, res) {
    var md5 = require('MD5');
    res.header("Access-Control-Allow-Origin", "*");
    var token = req.body.token;
    var password = req.body.password;
    var email = req.body.email;
    var manValues = [token, password, email];
    var checkdata = commonFunc.checkBlank(manValues);
    if (checkdata == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateCorporateAdminUsingEMAIL(email, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                password = md5(password);
                var verificationToken = md5(token);
                var engagements = "UPDATE tb_corporate_admin SET password = ?,password_token = ? WHERE ";
                engagements += "password_token = ? AND email = ? LIMIT 1";
                connection.query(engagements, [password, verificationToken, token, email], function (err, forgotPasswordResult) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in changing forgot password : ", err, forgotPasswordResult);
                        responses.sendError(res);
                        return;
                    } else {
                        if (forgotPasswordResult.affectedRows > 0) {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {}
                            };
                        } else {
                            var response = {
                                "message": constants.responseMessages.SHOW_ERROR_MESSAGE,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                        }
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    };
};

/*
 * ----------------------------------
 * FORGOT PASSWORD FROM EMAIL
 * ----------------------------------
 */
exports.corporate_forgotPasswordFromEmail = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var email = req.body.email;
    var manValues = [email];
    var checkdata = commonFunc.checkBlank(manValues);
    if (checkdata == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateCorporateAdminUsingEMAIL(email, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                    var md5 = require('MD5');
                    var token = md5(result[0].email);
                    var sql = "UPDATE `tb_corporate_admin` set `password_token`=? WHERE `email`=? LIMIT 1";
                    connection.query(sql, [token, email], function (err, response) {


                        var link = config.get('forgotpasswordCorporatePageLink');
                        link += "?token=" + token + "&email=" + email;

                        commonFunc.sendHtmlContent(email, link, "[Driven Limousines] Forgot Password", function (result) {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        });
                    });
                }
        });
    }
};