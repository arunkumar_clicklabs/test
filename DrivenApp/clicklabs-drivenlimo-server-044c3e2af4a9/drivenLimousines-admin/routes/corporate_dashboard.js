var commonFunc = require('./commonfunction');
var logging = require('./logging');
var responses = require('./responses');
var async = require('async');


/*
 * ---------------------
 * CORPORATE DASHBOARD's REPORTS
 * ---------------------
 */
exports.corporate_dashboard_report = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var start_time = req.body.start_time;
    var end_time = req.body.end_time;
    var manvalues = [access_token,start_time,end_time];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateCorporateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var corporate_user_id = result[0].user_id;
                async.parallel([
                    //calling total earning and rides function
                    function(callback) {
                        total_rides_and_earnings(corporate_user_id,req,res,function(total_earnings_result){
                            callback(null,total_earnings_result)
                        });
                    },
                    //calling total users function
                    function(callback) {
                            total_users(corporate_user_id,req,res,function(total_users_result){
                                callback(null,total_users_result)
                            });
                    },
                    //calling total users for today function
                    function(callback) {
                        total_users_registered_today(corporate_user_id,start_time,end_time,req,res,function(total_users_today_result){
                                callback(null,total_users_today_result)
                            });
                    },
                    //calling total rides for today function
                    function(callback) {
                        total_rides_and_earnings_today(corporate_user_id,start_time,end_time,req,res,function(totalridestoday_result){
                                callback(null,totalridestoday_result)
                            });
                    },
                        //calling total active users
                        function(callback) {
                            total_active_users(corporate_user_id,start_time,end_time,req,res,function(totalridestoday_result){
                                callback(null,totalridestoday_result)
                            });
                        }
                    ],
                    function(err, results) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {
                                "total_data": results[0],
                                "total_users":results[1],
                                "total_users_registered_today":results[2],
                                "specified_dates_data":results[3],
                                "total_active_users": results[4]
                            }
                        };
                        res.send(JSON.stringify(response));
                    });
            }
        });
    }
};
/*
 * --------------------------------
 * TOTAL EARNINGS OF THE SYSTEM
 * --------------------------------
 */

function total_rides_and_earnings(corporate_user_id,req, res,callback) {

    var sql = "SELECT count(engagements.`engagement_id`) as `total_rides`,IF(SUM(engagements.`money_transacted`) IS NULL,0,SUM(engagements.`money_transacted`)) as `total_earnings` FROM `tb_engagements` engagements ";
    sql += " INNER JOIN `tb_corporate_user` tcp WHERE engagements.`status`=? AND tcp.`admin_id`=? AND tcp.`user_id`=engagements.`user_id`";
    connection.query(sql, [constants.engagementStatus.ENDED,corporate_user_id], function (err, total_earnings) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total earnings : ", err, total_earnings);
            responses.sendError(res);
            return;
        }
        if (total_earnings.length > 0) {
            var response = {"rides":total_earnings[0].total_rides,"earnings":total_earnings[0].total_earnings.toFixed(2)};
        } else {
            var response = '';
        }
        callback(response)
    });
}

/*
 * ------------------------------
 * TOTAL USERS OF THE SYSTEM
 * ------------------------------
 */
function total_users(corporate_user_id,req, res,callback) {

    var sql = "SELECT count(`user_id`) as `total_users` FROM `tb_corporate_user` ";
    sql += "WHERE `admin_id`=?";
    connection.query(sql, [corporate_user_id], function (err, total_user_result) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total customers : ", err, total_user_result);
            responses.sendError(res);
            return;
        }
            if (total_user_result.length > 0) {
                var response = total_user_result[0].total_users;
            } else {
                var response = '';
            }
        callback(response)
    });
}
/*
 * -----------------------------------------------
 * TOTAL NUMBER OF RIDES AND EARNINGS FOR TODAY
 * -----------------------------------------------
 */
function total_rides_and_earnings_today(corporate_user_id,start_date,end_date,req, res,callback) {

    var sql = "SELECT count(eng.`engagement_id`) as `total_rides_for_today`,IF(SUM(eng.`money_transacted`) IS NULL,0,SUM(eng.`money_transacted`)) as `total_earnings_for_today` FROM `tb_engagements` eng ";
    sql += "INNER JOIN `tb_corporate_user` tcp WHERE eng.`status`=? AND tcp.`admin_id`=? AND tcp.`user_id`=eng.`user_id` AND eng.`current_time`>=? AND eng.`current_time`<=?";
    connection.query(sql, [constants.engagementStatus.ENDED,corporate_user_id,start_date,end_date], function (err, totatridestoday_result) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total rides for today : ", err, totatridestoday_result);
            responses.sendError(res);
            return;
        }
        if (totatridestoday_result.length > 0) {
            var response = {"rides":totatridestoday_result[0].total_rides_for_today,"earnings":totatridestoday_result[0].total_earnings_for_today.toFixed(2)};
        } else {
            var response = '';
        }
        callback(response)
    });
}
/*
 * -----------------------------------------------
 * TOTAL NUMBER OF USERS REGISTERED FOR TODAY
 * -----------------------------------------------
 */
function total_users_registered_today(corporate_user_id,start_date,end_date,req, res,callback) {

    var sql = "SELECT count(tbu.`user_id`) as `total_users_for_today` FROM `tb_users` tbu";
    sql += " INNER JOIN `tb_corporate_user` tcp WHERE tcp.`admin_id`=? AND tbu.`verification_status`=? AND tbu.`date_registered`>=? AND tbu.`date_registered`<=?";
    connection.query(sql, [corporate_user_id,constants.userVerificationStatus.VERIFY,start_date,end_date], function (err, totatuserstoday_result) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total users for today : ", err, totatuserstoday_result);
            responses.sendError(res);
            return;
        }
        if (totatuserstoday_result.length > 0) {
            var response = totatuserstoday_result[0].total_users_for_today;
        } else {
            var response = '';
        }
        callback(response)
    });
}

/*
 * -----------------------------------------------
 * TOTAL ACTIVE USERS
 * -----------------------------------------------
 */
function total_active_users(corporate_user_id,start_date,end_date,req, res,callback) {

    var sql = "SELECT count(tbu.`user_id`) as `total_active_users` FROM `tb_users` tbu";
    sql += " INNER JOIN `tb_corporate_user` tcp WHERE tcp.`admin_id`=? AND tbu.`verification_status`=? AND tbu.`user_id`=tcp.`user_id`";
    connection.query(sql, [corporate_user_id,constants.userVerificationStatus.VERIFY], function (err, totatuserstoday_result) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total active users : ", err, totatuserstoday_result);
            responses.sendError(res);
            return;
        }
        if (totatuserstoday_result.length > 0) {
            var response = totatuserstoday_result[0].total_active_users;
        } else {
            var response = '';
        }
        callback(response)
    });
}