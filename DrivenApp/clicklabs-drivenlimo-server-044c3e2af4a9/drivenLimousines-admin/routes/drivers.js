var commonFunc = require('./commonfunction');
var math = require('math');
var logging = require('./logging');
var responses = require('./responses');


/*
 * -----------------------------------------------
 * ADD DRIVER
 * -----------------------------------------------
 */

exports.add_driver = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var email = req.body.email
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.authenticateDriverUserUsingEmail(email, function (resultUserTable) {

                    if (resultUserTable != 0) {

                        var response = {
                            "message": constants.responseMessages.EMAIL_ALREADY_EXISTS,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {}
                        };
                        res.send(JSON.stringify(response));
                        return;
                    }
                    else {
                        var msg = 'Click button below to download app';

                        commonFunc.emailFormatting(email, msg, '', '', '', function (returnMessage) {

                            commonFunc.sendHtmlContent(email, returnMessage, "[Driven Limousines] App Links", function (result) {
                                var data;
                                if (result == 1) {
                                    data = "Mail Sent";
                                }
                                else {
                                    data = "Mail Not Sent";
                                }
                                var response = {
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data": data

                                }
                                res.send(JSON.stringify(response));
                                return;
                            });

                        });
                    }
                });

            }
        });
    }
};


/*
 * -----------------------------------------------
 * GET UNAPPROVED DRIVER LIST
 * -----------------------------------------------
 */

exports.unapproved_driver_list = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT `user_id` AS `driver_id`,`user_name`,`phone_no`,`user_email`,`driver_paypal_id` FROM `tb_users` ";
                sql += "WHERE `make_me_driver_flag`=? AND `is_deleted`<>?";
                connection.query(sql, [constants.makeMeDriverFlag.UNAPPROVED_DRIVER_STATUS, constants.DELETED_STATUS], function (err, result) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching unapproved driver : ", err, checkDriverData);
                        responses.sendError(res);
                        return;
                    }
                    if (result.length > 0) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"unapproved_members": result}
                        };
                    } else {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {"unapproved_members": []}
                        };
                    }
                    res.send(JSON.stringify(response));
                    return;
                });
            }
        });
    }
};

/*
 * -----------------------------------------------
 * APPROVE UNAPPROVED DRIVER
 * -----------------------------------------------
 */

exports.approve_driver = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var driver_id = req.body.driver_id;
    var driver_vehicle_number = req.body.driver_vehicle_number;
    var driver_license_number = req.body.driver_license_number;
    var driver_car_type = req.body.driver_car_type;
    var social_security_number = req.body.social_security_number;
    var driver_paypal_id = req.body.driver_paypal_id;
    var phone_no = req.body.phone_no;
    var driver_vehicle_image = req.body.driver_vehicle_image;
    var manvalues = [access_token, driver_id, driver_vehicle_number, driver_license_number, driver_car_type,
        social_security_number, driver_paypal_id, phone_no];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        if (!(driver_car_type == constants.EXECUTIVE_CAR_FLAG || driver_car_type == constants.PRESTIGE_CAR_FLAG || driver_car_type == constants.SUV_CAR_FLAG || driver_car_type == constants.MPV_CAR_FLAG)) {
            var response = {
                "message": constants.responseMessages.INVALID_CAR_TYPE,
                "status": constants.responseFlags.INVALID_CAR_TYPE,
                "data": {}
            };
            res.send(JSON.stringify(response));
            return;
        }
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {

                var sql = "SELECT `make_me_driver_flag`,`reg_as`,`user_email` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
                connection.query(sql, [driver_id], function (err, checkDriverData) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in checking driver id : ", err, checkDriverData);
                        responses.sendError(res);
                        return;
                    } else {

                        if (checkDriverData.length == 0) {
                            var response = {
                                "message": constants.responseMessages.DRIVER_NOT_FOUND,
                                "status": constants.responseFlags.DRIVER_NOT_FOUND,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        }
                        if (!(checkDriverData[0].reg_as == constants.registeredAsFlag.DRIVER || checkDriverData[0].make_me_driver_flag == constants.makeMeDriverFlag.UNAPPROVED_DRIVER_STATUS)) {
                            var response = {
                                "message": constants.responseMessages.NOT_A_DRIVER,
                                "status": constants.responseFlags.NOT_A_DRIVER,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        }


                        if (driver_vehicle_image === 'undefined' || typeof driver_vehicle_image === undefined) {
                            var driver_vehicle_image_path = checkDriverData[0].driver_car_image;
                            var sql = "UPDATE `tb_users` SET `driver_car_no`=?,`driver_license_number`=?,`driver_social_security_number`=?,";
                            sql += "`car_type`=?,`phone_no`=?,`driver_paypal_id`=?,`driver_car_image`=?,`make_me_driver_flag`=?,";
                            sql += "`req_stat`=?,`reg_as`=? WHERE `user_id`=? LIMIT 1";
                            var bindparams = [driver_vehicle_number, driver_license_number, social_security_number, driver_car_type,
                                phone_no, driver_paypal_id, driver_vehicle_image_path, constants.makeMeDriverFlag.APPROVED_DRIVER_STATUS,
                                constants.REGISTRATION_DRIVER_STATUS, constants.registeredAsFlag.DRIVER, driver_id];
                            connection.query(sql, bindparams, function (err, updateDriverStatus) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in updating driver status : ", err, updateDriverStatus);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    addDriverData(checkDriverData[0].user_email, driver_id);

                                }
                            });
                        } else {

                            var vehicle_image_name = req.files.driver_vehicle_image.name;
                            var vehicle_image_size = req.files.driver_vehicle_image.size;

                            var timestamp = new Date().getTime().toString();
                            var str = '';
                            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                            var size = chars.length;
                            for (var i = 0; i < 4; i++) {
                                var randomnumber = math.floor(math.random() * size);
                                str = chars[randomnumber] + str;
                            }

                            console.log("ddddddd")

                            vehicle_image_name = vehicle_image_name.replace(/\s/g, '');
                            var driver_vehicle_image_name = str + timestamp + "-" + vehicle_image_name;
                            req.files.driver_vehicle_image.name = driver_vehicle_image_name;

                            commonFunc.uploadImageToS3Bucket(req.files.driver_vehicle_image, 'driver_vehicle_image', function (result_driver_vehicle_image) {
                                if (result_driver_vehicle_image == 0) {
                                    var response = {
                                        "message": constants.responseMessages.UPLOAD_ERROR,
                                        "status": constants.responseFlags.UPLOAD_ERROR,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                } else {
                                    var driver_vehicle_image_path = config.get('s3BucketCredentials.driverImageBaseURL') + driver_vehicle_image_name;
                                    var sql = "UPDATE `tb_users` SET `driver_car_no`=?,`driver_license_number`=?,`driver_social_security_number`=?,";
                                    sql += "`car_type`=?,`phone_no`=?,`driver_paypal_id`=?,`driver_car_image`=?,`make_me_driver_flag`=?,";
                                    sql += "`req_stat`=?,`reg_as`=? WHERE `user_id`=? LIMIT 1";
                                    var bindparams = [driver_vehicle_number, driver_license_number, social_security_number, driver_car_type,
                                        phone_no, driver_paypal_id, driver_vehicle_image_path, constants.makeMeDriverFlag.APPROVED_DRIVER_STATUS,
                                        constants.REGISTRATION_DRIVER_STATUS, constants.registeredAsFlag.DRIVER, driver_id];
                                    connection.query(sql, bindparams, function (err, updateDriverStatus) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in updating driver status : ", err, updateDriverStatus);
                                            responses.sendError(res);
                                            return;
                                        } else {
                                            addDriverData(checkDriverData[0].user_email, driver_id)

                                        }
                                    });

                                }
                            });
                        }
                        function addDriverData(email, driver_id) {

                            //FOR HOME LOCATION (tb_home_locations)
                            var homeLocationSql = "SELECT `driver_id` FROM `tb_home_locations` WHERE `driver_id`=? LIMIT 1";
                            connection.query(homeLocationSql, [driver_id], function (err, homeLocationResult) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in checking driver location : ", err, homeLocationResult);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    if (homeLocationResult.length == 0) {
                                        var insertHomeLocationSql = "INSERT INTO `tb_home_locations`(`driver_id`,`latitude`,`longitude`) VALUES (?,?,?)";
                                        var insertHomeLocationBindparams = [driver_id, constants.INITIAL_LATITUDE, constants.INITIAL_LONGITUDE];
                                        connection.query(insertHomeLocationSql, insertHomeLocationBindparams, function (err, insertHomeLocationResult) {
                                            if (err) {
                                                logging.logDatabaseQueryError("Error in inserting driver location : ", err, insertHomeLocationResult);
                                                responses.sendError(res);
                                                return;
                                            }
                                        });
                                    }
                                }
                            });

                            //FOR EXCEPTIONAL USER (tb_exceptional_users)
                            var exceptionalUserSql = "SELECT `user_id` FROM `tb_exceptional_users` WHERE `user_id`=? LIMIT 1";
                            connection.query(exceptionalUserSql, [driver_id], function (err, exceptionalUserResult) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in checking driver exceptional user : ", err, exceptionalUserResult);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    if (exceptionalUserResult.length == 0) {
                                        var insertExceptionalUserSql = "INSERT INTO `tb_exceptional_users`(`user_id`) VALUES (?)";
                                        var insertExceptionalUserBindparams = [driver_id];
                                        connection.query(insertExceptionalUserSql, insertExceptionalUserBindparams, function (err, insertExceptionalUserResult) {
                                            if (err) {
                                                logging.logDatabaseQueryError("Error in inserting exceptional user : ", err, insertExceptionalUserResult);
                                                responses.sendError(res);
                                                return;
                                            }
                                        });
                                    }
                                }
                            });

                            //FOR tb_timings
                            var timingUserSql = "SELECT `driver_id` FROM `tb_timings` WHERE `driver_id`=? LIMIT 1";
                            connection.query(timingUserSql, [driver_id], function (err, timingUserResult) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in checking driver timing user : ", err, timingUserResult);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    if (timingUserResult.length == 0) {
                                        var insertTimingUserSql = "INSERT INTO `tb_timings`(`driver_id`) VALUES (?)";
                                        var insertTimingUserBindparams = [driver_id];
                                        connection.query(insertTimingUserSql, insertTimingUserBindparams, function (err, insertTimingUserResult) {
                                            if (err) {
                                                logging.logDatabaseQueryError("Error in inserting timing user : ", err, insertTimingUserResult);
                                                responses.sendError(res);
                                                return;
                                            }
                                        });
                                    }
                                }
                            });

                            var message = "Congrats!! Your driver account has been approved by admin.";
                            commonFunc.emailFormatting(email, message, config.get('playStoreURL'), config.get('appStoreURL'), '', function (returnMessage) {

                                commonFunc.sendHtmlContent(email, returnMessage, "[Driven Limousines] Approval", function (result) {

                                    var response = {
                                        "message": constants.responseMessages.DRIVER_APPROVED_SUCCESSFULLY,
                                        "status": constants.responseFlags.DRIVER_APPROVED_SUCCESSFULLY,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                });
                            });
                        }
                    }
                });
            }
        });
    }
}



/*
 * -----------------------------------------------
 * DECLINE REQUEST OR DELETE USER
 * -----------------------------------------------
 */

exports.delete_user = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var user_id = req.body.user_id;
    var manvalues = [access_token, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {

                isASessionActive(user_id, function(activeSessionResult) {

                    if(activeSessionResult.session_id < 0) {

                        var sql = "SELECT `user_email`,`reg_as`,`make_me_driver_flag` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
                        connection.query(sql, [user_id], function (err, checkDriverData) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in checking user id : ", err, checkDriverData);
                                responses.sendError(res);
                                return;
                            } else {
                                if (checkDriverData.length == 0) {
                                    var response = {
                                        "message": constants.responseMessages.USER_NOT_FOUND,
                                        "status": constants.responseFlags.USER_NOT_FOUND,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                } else {
                                    var str = '';
                                    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    var size = chars.length;
                                    for (var i = 0; i < 4; i++) {
                                        var randomnumber = math.floor(math.random() * size);
                                        str = chars[randomnumber] + str;
                                    }
                                    var new_email = str + "DELETED" + checkDriverData[0].user_email;
                                    var sql = "UPDATE `tb_users` SET `user_email`=?,`make_me_driver_flag`=?,`is_deleted`=?";
                                    sql += " WHERE `user_id`=? LIMIT 1";
                                    var bindparams = [new_email, constants.makeMeDriverFlag.INVALID, constants.DELETED_STATUS, user_id];
                                    connection.query(sql, bindparams, function (err, updateDriverStatus) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in updating user deleted status : ", err, updateDriverStatus);
                                            responses.sendError(res);
                                            return;
                                        } else {
                                            var message = "You have been deleted by admin.";
                                            commonFunc.emailFormatting(checkDriverData[0].user_email, message, '', '', '', function (returnMessage) {

                                                commonFunc.sendHtmlContent(checkDriverData[0].user_email, returnMessage, "[TaxiMust] Removal", function (result) {

                                                    var response = {
                                                        "message": constants.responseMessages.USER_DELETED_SUCCESSFULLY,
                                                        "status": constants.responseFlags.USER_DELETED_SUCCESSFULLY,
                                                        "data": {}
                                                    };
                                                    res.send(JSON.stringify(response));
                                                    return;
                                                });
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    }
                    else{
                        var response = {
                            "message": constants.responseMessages.DRIVER_BUSY_NOW,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {}
                        };
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    }
};

/*
 * -----------------------------------------------
 * GET ALL DRIVER
 * -----------------------------------------------
 */
exports.get_all_driver = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT `user_id` AS `driver_id`,`user_name`,`user_email`,`phone_no`,`car_type`,";
                sql += "`driver_car_no`,`driver_license_number`,`driver_social_security_number`,`total_rating_got_driver`,";
                sql += "`total_rating_driver`,`total_rides_as_driver`,`driver_paypal_id`,`total_earnings`,`driver_car_image`,";
                sql += "`status`,`is_blocked`,`is_deleted` FROM `tb_users` WHERE ";
                sql += "`reg_as`=? AND `make_me_driver_flag`=? AND `is_deleted`<>?";
                var bindparams = [constants.registeredAsFlag.DRIVER, constants.makeMeDriverFlag.APPROVED_DRIVER_STATUS, constants.DELETED_STATUS];
                connection.query(sql, bindparams, function (err, getAllDriver) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching all driver : ", err, getAllDriver);
                        responses.sendError(res);
                        return;
                    } else {
                        if (getAllDriver.length > 0) {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {"driver_list": getAllDriver}
                            };
                        } else {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {"driver_list": []}
                            };
                        }
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    }
};

/*
 * -----------------------------------------------
 * EDIT DRIVER INFO
 * -----------------------------------------------
 */
exports.update_driver_information = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var driver_id = req.body.driver_id;
    var driver_vehicle_number = req.body.driver_vehicle_number;
    var driver_license_number = req.body.driver_license_number;
    var driver_car_type = req.body.driver_car_type;
    var social_security_number = req.body.social_security_number;
    var driver_paypal_id = req.body.driver_paypal_id;
    var phone_no = req.body.phone_no;
    var driver_vehicle_image = req.body.driver_vehicle_image;
    var manvalues = [access_token, driver_id, driver_vehicle_number, driver_license_number, driver_car_type,
        social_security_number, driver_paypal_id, phone_no];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        if (!(driver_car_type == constants.LIMO_CAR_FLAG || driver_car_type == constants.VAN_CAR_FLAG)) {
            var response = {
                "message": constants.responseMessages.INVALID_CAR_TYPE,
                "status": constants.responseFlags.INVALID_CAR_TYPE,
                "data": {}
            };
            res.send(JSON.stringify(response));
            return;
        }
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                isASessionActive(user_id, function(activeSessionResult) {

                    if(activeSessionResult.session_id < 0) {

                        var sql = "SELECT `make_me_driver_flag`,`reg_as`,`driver_car_image` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
                connection.query(sql, [driver_id], function (err, checkDriverData) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in checking driver id : ", err, checkDriverData);
                        responses.sendError(res);
                        return;
                    } else {
                        if (checkDriverData.length == 0) {
                            var response = {
                                "message": constants.responseMessages.DRIVER_NOT_FOUND,
                                "status": constants.responseFlags.DRIVER_NOT_FOUND,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        }
                        if (!(checkDriverData[0].reg_as == constants.registeredAsFlag.DRIVER || checkDriverData[0].make_me_driver_flag == constants.makeMeDriverFlag.UNAPPROVED_DRIVER_STATUS)) {
                            var response = {
                                "message": constants.responseMessages.NOT_A_DRIVER,
                                "status": constants.responseFlags.NOT_A_DRIVER,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        }
                        var vehicle_image_name = req.files.driver_vehicle_image.name;
                        var vehicle_image_size = req.files.driver_vehicle_image.size;
                        if (vehicle_image_name === '' || vehicle_image_name === "" || vehicle_image_name == undefined || vehicle_image_size == 0) {
                            var driver_vehicle_image_path = checkDriverData[0].driver_car_image;
                        } else {
                            var timestamp = new Date().getTime().toString();
                            var str = '';
                            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                            var size = chars.length;
                            for (var i = 0; i < 4; i++) {
                                var randomnumber = math.floor(math.random() * size);
                                str = chars[randomnumber] + str;
                            }
                            vehicle_image_name = vehicle_image_name.replace(/\s/g, '');
                            var driver_vehicle_image_name = str + timestamp + "-" + vehicle_image_name;
                            req.files.driver_vehicle_image.name = driver_vehicle_image_name;
                            commonFunc.uploadImageToS3Bucket(req.files.driver_vehicle_image, 'driver_vehicle_image', function (result_driver_vehicle_image) {
                                if (result_driver_vehicle_image == 0) {
                                    var response = {
                                        "message": constants.responseMessages.UPLOAD_ERROR,
                                        "status": constants.responseFlags.UPLOAD_ERROR,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                } else {
                                    var driver_vehicle_image_path = config.get('s3BucketCredentials.driverImageBaseURL') + driver_vehicle_image_name;
                                }
                            });
                        }
                        var sql = "UPDATE `tb_users` SET `driver_car_no`=?,`driver_license_number`=?,`driver_social_security_number`=?,";
                        sql += "`car_type`=?,`phone_no`=?,`driver_paypal_id`=?,`driver_car_image`=?";
                        sql += " WHERE `user_id`=? LIMIT 1";
                        var bindparams = [driver_vehicle_number, driver_license_number, social_security_number, driver_car_type,
                            phone_no, driver_paypal_id, driver_vehicle_image_path, driver_id];
                        connection.query(sql, bindparams, function (err, updateDriverStatus) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in updating driver status : ", err, updateDriverStatus);
                                responses.sendError(res);
                                return;
                            } else {
                                var response = {
                                    "message": constants.responseMessages.DRIVER_INFO_UPDATED_SUCCESSFULLY,
                                    "status": constants.responseFlags.DRIVER_INFO_UPDATED_SUCCESSFULLY,
                                    "data": {}
                                };
                                res.send(JSON.stringify(response));
                                return;
                            }
                        });
                    }
                });
                    }
                    else{
                        var response = {
                            "message": constants.responseMessages.DRIVER_BUSY_NOW,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {}
                        };
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    }
}

/*
 * ---------------------------
 * BLOCK/UNBLOCK USER
 * ---------------------------
 */

exports.block_unblock_user = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var user_id = req.body.user_id;
    var block_status = req.body.block_status;
    var manvalues = [access_token, user_id, block_status];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                if (!(block_status == constants.BLOCK_STATUS || block_status == constants.UNBLOCK_STATUS)) {
                    var response = {
                        "message": constants.responseMessages.INVALID_BLOCK_STATUS,
                        "status": constants.responseFlags.INVALID_BLOCK_STATUS,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    isASessionActive(user_id, function(activeSessionResult) {

                        if(activeSessionResult.session_id < 0) {
                    var sql = "SELECT `make_me_driver_flag`,`user_email` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
                    connection.query(sql, [user_id], function (err, checkData) {
                        if (err) {
                            logging.logDatabaseQueryError("Error in checking driver id : ", err, checkData);
                            responses.sendError(res);
                            return;
                        } else {
                            if (checkData.length == 0) {
                                var response = {
                                    "message": constants.responseMessages.USER_NOT_FOUND,
                                    "status": constants.responseFlags.USER_NOT_FOUND,
                                    "data": {}
                                };
                                res.send(JSON.stringify(response));
                                return;
                            } else {
                                var sql = "UPDATE `tb_users` SET `is_blocked`=? WHERE `user_id`=? LIMIT 1";
                                connection.query(sql, [block_status, user_id], function (err, updateDriverStatus) {
                                    if (err) {
                                        logging.logDatabaseQueryError("Error in updating user block status : ", err, updateDriverStatus);
                                        responses.sendError(res);
                                        return;
                                    } else {
                                        var message='';
                                        if(block_status==0){
                                             message = "You have been unblocked by admin.";
                                        }
                                        else if (block_status==1){
                                             message = "You have been blocked by admin.";
                                        }
                                        commonFunc.emailFormatting(checkData[0].user_email, message, '', '', '', function(returnMessage) {

                                            commonFunc.sendHtmlContent(checkData[0].user_email, returnMessage, "[TaxiMust] Block/Unblock", function (result) {

                                                var response = {
                                                    "message": constants.responseMessages.STATUS_CHANGED_SUCCESSFULLY,
                                                    "status": constants.responseFlags.STATUS_CHANGED_SUCCESSFULLY,
                                                    "data": {}
                                                };
                                                res.send(JSON.stringify(response));
                                                return;
                                            });
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            else{
                    var response = {
                        "message": constants.responseMessages.DRIVER_BUSY_NOW,
                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                }
            });
                }
            }
        });
    }
};


function isASessionActive(user_id, callback){
    var get_session = "SELECT `session_id`, `ride_acceptance_flag` " +
        "FROM `tb_session` " +
        "WHERE `user_id`=? && `is_active`=? && `date` > timestamp(DATE_SUB(NOW(), INTERVAL 2 HOUR))  ORDER BY `date` DESC LIMIT 1";
    var values = [user_id, constants.sessionStatus.ACTIVE];
    connection.query(get_session, values, function(err, result_session) {
        logging.logDatabaseQueryError("Getting any active session for the user", err, result_session, null);

        if(result_session.length > 0) {
            callback(result_session[0]);
        }
        else {
            callback({"session_id":-1});
        }
    });
}