var commonFunc = require('./commonfunction');
var logging = require('./logging');
var responses = require('./responses');

/*
 * -----------------------------------------------
 * HEAT MAP OF EARNINGS
 * -----------------------------------------------
 */

exports.heat_map_earnings = function(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var start_date = req.body.start_time;
    var end_date = req.body.end_time;
    var timezone = req.body.timezone;
    var manvalues = [access_token, start_date, end_date , timezone];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var total_earnings=[];
                var dates = [];
                var start_date1 = new Date(start_date);
                var end_date1 = new Date(end_date);
                var start_date_duplicate = start_date1.setTime(start_date1.getTime()-(timezone*60*1000));
                var end_date_duplicate = end_date1.setTime(end_date1.getTime()-(timezone*60*1000));
                getConsistentDatesIncluding(dates, start_date_duplicate, end_date_duplicate);
                var sql = "SELECT `money_transacted`, DATE_ADD(`current_time`, INTERVAL ? MINUTE) as `current_time` FROM `tb_engagements` " +
                    "WHERE `current_time`>= ? AND `current_time` <= ? AND `status`=?";
                connection.query(sql, [-timezone, start_date,end_date,constants.engagementStatus.ENDED], function (err, data) {
                    //logging.logDatabaseQueryError("Getting the Demand Data", err, data);
                    var prev_date = dates[0];
                    var earnings=0;
                    var j=0;
                    var flag;
                    var date_length = data.length;
                    var dataObjectearnings={};
                    for (var i = 0 ; i < date_length; i++) {
                        if (new Date(data[i].current_time).toDateString()==prev_date){
                            earnings+=data[i].money_transacted;
                            flag = 0;
                        }
                        else{
                            dataObjectearnings[prev_date]=Number(earnings.toFixed(2));
                            earnings=data[i].money_transacted;
                            prev_date = new Date(data[i].current_time).toDateString();
                            flag = 1;
                        }
                    }

                    if (flag == 0){
                        dataObjectearnings[prev_date]=Number(earnings.toFixed(2));
                    }

                    dates.forEach(function(date){
                        if(dataObjectearnings[date]) {
                            total_earnings.push(dataObjectearnings[date]);
                        }else {total_earnings.push(0);}
                    });


                    var response = {
                        chart: {
                            style: {
                                color: "#b9bbbb"
                            },
                            renderTo: "earningcharts",
                            backgroundColor: "transparent",
                            lineColor: "rgba(35,37,38,100)",
                            plotShadow: false
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: "Earning Data"
                        },
                        xAxis: {
                            title: {
                                style: {
                                    color: "#b9bbbb"
                                },
                                text: "Dates"
                            },
                            categories: dates,
                            labels: {
                                rotation: 270,
                                align: "right",
                                distance: 45,
                                enabled: false
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                style: {
                                    color: "#b9bbbb"
                                },
                                text: "Money Earned"
                            }
                        },
                        legend: {
                            itemStyle: {
                                color: "#b9bbbb"
                            },
                            layout: "vertical",
                            align: "right",
                            verticalAlign: "middle",
                            borderWidth: 0
                        },
                        series: []
                    };
                    response.series.push({color: "#108ec5", name: "Total Earnings", data: total_earnings});
                    res.send(JSON.stringify(response));
                });
            }
    });
    }
};

/*
 * -----------------------------------------------
 * HEAT MAP OF RIDES
 * -----------------------------------------------
 */

exports.heat_map_rides_data = function(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var start_date = req.body.start_time;
    var end_date = req.body.end_time;
    var timezone = req.body.timezone;
    var manvalues = [access_token, start_date, end_date, timezone];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var rides_requested = [];
                var rides_completed = [];
                var rides_accepted = [];
                var rides_cancelled = [];
                var rides_missed = [];
                var dates = [];

                var start_date1 = new Date(start_date);
                var end_date1 = new Date(end_date);
                var start_date_duplicate = start_date1.setTime(start_date1.getTime()-(timezone*60*1000));
                var end_date_duplicate = end_date1.setTime(end_date1.getTime()-(timezone*60*1000));
                getConsistentDatesIncluding(dates, start_date_duplicate, end_date_duplicate);
                //dates2 = removeYear(dates);
                //for (i = 0; i < dates2.length; i++) {
                //    var date = dates2[i].split('-');
                //    dates2[i] = date[1] + '-' + date[0];
                //}

                var sql = "SELECT `status`, DATE_ADD(`current_time`, INTERVAL ? MINUTE) as `current_time` FROM `tb_engagements` " +
                    "WHERE `current_time`>= ? AND `current_time` <= ? ORDER BY `current_time` ";
                connection.query(sql, [-timezone, start_date, end_date], function (err, data) {
                    //logging.logDatabaseQueryError("Getting the Demand Data", err, data);
                    var prev_date = dates[0];
                    var rides_requested_count= 0,rides_completed_count= 0, rides_accepted_count= 0,rides_cancelled_count= 0, rides_missed_count=0;
                    var j=0;
                    var flag;
                    var date_length = data.length;

                    var dateObjectrides_requested={},dateObjectrides_completed={},dateObjectrides_acceptedt={},dateObjectrides_cancelled={},dateObjectrides_missed={};
                    for (var i = 0 ; i < date_length; i++) {
                        if (new Date(data[i].current_time).toDateString()==prev_date){
                            if(data[i].status==constants.engagementStatus.REQUESTED){rides_requested_count++;}
                            else if(data[i].status==constants.engagementStatus.ACCEPTED){rides_accepted_count++;}
                            else if(data[i].status==constants.engagementStatus.ENDED){rides_completed_count++;}
                            else if(data[i].status==constants.engagementStatus.CANCELLED_BY_CUSTOMER){rides_cancelled_count++;}
                            else if(data[i].status==constants.engagementStatus.TIMEOUT){rides_missed_count++;}
                            flag = 0;
                        }
                        else{
                            dateObjectrides_requested[prev_date] = rides_requested_count;
                            dateObjectrides_completed[prev_date] = rides_completed_count;
                            dateObjectrides_acceptedt[prev_date] = rides_accepted_count;
                            dateObjectrides_cancelled[prev_date] = rides_cancelled_count;
                            dateObjectrides_missed[prev_date] = rides_missed_count;
                            rides_requested_count=0;rides_completed_count=0;rides_accepted_count=0;rides_cancelled_count=0;rides_missed_count=0;
                            if(data[i].status==constants.engagementStatus.REQUESTED){rides_requested_count=1;}
                            else if(data[i].status==constants.engagementStatus.ACCEPTED){rides_accepted_count=1;}
                            else if(data[i].status==constants.engagementStatus.ENDED){rides_completed_count=1;}
                            else if(data[i].status==constants.engagementStatus.CANCELLED_BY_CUSTOMER){rides_cancelled_count=1;}
                            else if(data[i].status==constants.engagementStatus.TIMEOUT){rides_missed_count=1;}
                            prev_date = new Date(data[i].current_time).toDateString();
                            flag = 1;
                        }
                    }

                    if (flag == 0){
                        dateObjectrides_requested[prev_date] = rides_requested_count;
                        dateObjectrides_completed[prev_date] = rides_completed_count;
                        dateObjectrides_acceptedt[prev_date] = rides_accepted_count;
                        dateObjectrides_cancelled[prev_date] = rides_cancelled_count;
                        dateObjectrides_missed[prev_date] = rides_missed_count;
                    }

                    dates.forEach(function(date){
                        if(dateObjectrides_requested[date]) {
                            rides_requested.push(dateObjectrides_requested[date]);
                        }else {rides_requested.push(0);}
                        if(dateObjectrides_completed[date]) {
                            rides_completed.push(dateObjectrides_completed[date]);
                        }else {rides_completed.push(0);}
                        if(dateObjectrides_acceptedt[date]) {
                            rides_accepted.push(dateObjectrides_acceptedt[date]);
                        }else {rides_accepted.push(0);}
                        if(dateObjectrides_cancelled[date]) {
                            rides_cancelled.push(dateObjectrides_cancelled[date]);
                        }else {rides_cancelled.push(0);}
                        if(dateObjectrides_missed[date]) {
                            rides_missed.push(dateObjectrides_missed[date]);
                        }else {rides_missed.push(0);}
                    });

                    var response = {
                        chart: {
                            style: {
                                color: "#b9bbbb"
                            },
                            renderTo: "ridecharts",
                            backgroundColor: "transparent",
                            lineColor: "rgba(35,37,38,100)",
                            plotShadow: false
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: "Ride Data"
                        },
                        xAxis: {
                            title: {
                                style: {
                                    color: "#b9bbbb"
                                },
                                text: "Dates"
                            },
                            categories: dates,
                            labels: {
                                rotation: 270,
                                align: "right",
                                distance: 45,
                                enabled: false
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                style: {
                                    color: "#b9bbbb"
                                },
                                text: "Numbers of Rides"
                            }
                        },
                        legend: {
                            itemStyle: {
                                color: "#b9bbbb"
                            },
                            layout: "vertical",
                            align: "right",
                            verticalAlign: "middle",
                            borderWidth: 0
                        },
                        series: []
                    };
                    response.series.push({color: "#108ec5", name: "Ride Requests", data: rides_requested});
                    //response.series.push({color: "#99CC33", name: "Rides Accepted", data: rides_accepted});
                    response.series.push({color: "#ee5728", name: "Rides Completed", data: rides_completed});
                    response.series.push({color: "#009900", name: "Rides Cancelled", data: rides_cancelled});
                    response.series.push({color: "#2277ff", name: "Rides Missed", data: rides_missed});
                    res.send(JSON.stringify(response));
                });
            }
        });
    }
};

function getConsistentDatesIncluding(dates, start_date, end_date){
    dates.push(new Date(start_date).toDateString());
    var i=0;
    while(commonFunc.timeDifferenceInDays(dates[i], end_date) > 0){
        dates.push(addDay(dates[i]));
        i++;
    }
}

function removeYear(dates) {
    newdates = [];
    for(var i = 0; i < dates.length; i++) {
        newdates.push(dates[i].split(/-(.+)?/)[1]);
    }
    return newdates;
}


function addDay(date){
    var newDate = new Date(date);
    newDate.setTime(newDate.getTime() + 86400000); // add a date
    return new Date(newDate).toDateString();
}