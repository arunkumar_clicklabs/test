var commonFunc = require('./commonfunction');
var logging = require('./logging');
var responses = require('./responses');
var fs = require('fs');


/*
 * -----------------------------------------------
 * READ ABOUT US HTML FILE
 * -----------------------------------------------
 */

exports.read_about_us = function(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var data = fs.readFileSync(config.get('filePath.about'), "utf8");
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": {"aboutus": data}
                };
                res.send(JSON.stringify(response));
                return;
            }
    });
    }
};

/*
 * -----------------------------------------------
 * WRITE ABOUT US HTML FILE
 * -----------------------------------------------
 */

exports.write_about_us = function(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var data = req.body.data;
    var manvalues = [access_token,data];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                fs.writeFile(config.get('filePath.about'), data, function (err) {
                    if (err) return console.log(err);
                    var response = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                });
            }
        });
    }
};

/*
 * -----------------------------------------------
 * READ FAQ HTML FILE
 * -----------------------------------------------
 */

exports.read_faq = function(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var data = fs.readFileSync(config.get('filePath.faq'), "utf8");
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": {"aboutus": data}
                };
                res.send(JSON.stringify(response));
                return;
            }
        });
    }
};

/*
 * -----------------------------------------------
 * WRITE FAQ HTML FILE
 * -----------------------------------------------
 */

exports.write_faq = function(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var data = req.body.data;
    var manvalues = [access_token,data];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                fs.writeFile(config.get('filePath.faq'), data, function (err) {
                    if (err) return console.log(err);
                    var response = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                });
            }
        });
    }
};

/*
 * -----------------------------------------------
 * READ PRIVACY POLICY HTML FILE
 * -----------------------------------------------
 */

exports.read_privacy_policy = function(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var data = fs.readFileSync(config.get('filePath.privacy_policy'), "utf8");
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": {"aboutus": data}
                };
                res.send(JSON.stringify(response));
                return;
            }
        });
    }
};

/*
 * -----------------------------------------------
 * WRITE PRIVACY POLICY HTML FILE
 * -----------------------------------------------
 */

exports.write_privacy_policy = function(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var data = req.body.data;
    var manvalues = [access_token,data];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                fs.writeFile(config.get('filePath.privacy_policy'), data, function (err) {
                    if (err) return console.log(err);
                    var response = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                });
            }
        });
    }
};

/*
 * -----------------------------------------------
 * READ TERMS AND CONDITIONS HTML FILE
 * -----------------------------------------------
 */

exports.read_tnc = function(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var data = fs.readFileSync(config.get('filePath.t&c'), "utf8");
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": {"aboutus": data}
                };
                res.send(JSON.stringify(response));
                return;
            }
        });
    }
};

/*
 * -----------------------------------------------
 * WRITE TERMS AND CONDITIONS HTML FILE
 * -----------------------------------------------
 */

exports.write_tnc = function(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var data = req.body.data;
    var manvalues = [access_token,data];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                fs.writeFile(config.get('filePath.t&c'), data, function (err) {
                    if (err) return console.log(err);
                    var response = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                });
            }
        });
    }
};