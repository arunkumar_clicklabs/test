var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');


/*
 * -----------------------------------------------
 * ADDING A CORPORATE ADMIN ACCOUNT 
 * -----------------------------------------------
 */
// 1. create a admin entry in the tb_corporate_admin table who could have their admin panels seprately
exports.addCorporateAdmin = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var email = req.body.email;
    var userName = req.body.user_name;
    var phoneNo = req.body.phone_no;
    var accessToken = req.body.access_token;
    var manvalues = [email, userName, phoneNo, accessToken];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(accessToken, function(result) {
            var self = this;
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            }
            addAdmin();
            return;
        });
    }


    function addAdmin() {
        var date = new Date();
        var accessTokenAdmin = commonFunc.generateRandomString();
        accessTokenAdmin = md5(accessTokenAdmin + date + accessTokenAdmin);
        var passwordToken = md5(accessTokenAdmin + date + email);
        var sql = " insert into tb_corporate_admin (user_name,email, access_token , phone_no ,password_token,password ) values(?,?,?,?,?,?) ";
        var sqlArguments = [userName, email, accessToken, phoneNo, passwordToken, accessTokenAdmin];
        connection.query(sql, sqlArguments, addAdminSqlHandler);
        return;

        function addAdminSqlHandler(err, result_add) {
            if (err) {
                console.log(err);
                if (err.code == "ER_DUP_ENTRY") {
                    var response = {
                        "message": constants.responseMessages.EMAIL_ALREADY_EXISTS,
                        "status": constants.responseFlags.EMAIL_ALREADY_EXISTS,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                }
                return;
            }

            var response = {
                "message": constants.responseMessages.ACTION_COMPLETE,
                "status": constants.responseFlags.ACTION_COMPLETE,
                "data": "Admin successfully added"
            };

            res.send(JSON.stringify(response));
            return;

            var url = "forgot password url";

            senderEmailforResetPasswordorporateAdmin(email, passwordToken)
            return;
        }
    }
};

function senderEmailforResetPasswordorporateAdmin(email, token) {

       var link = config.get('forgotpasswordPageLink');
                    link += "?token=" + token + "&email=" + email;

                        commonFunc.sendHtmlContent(email,link,"[Driven Limousines] Reset Password", function (result){})

}



/*
 * -----------------------------------------------
 * GET ALL CORPORATE USERS
 * -----------------------------------------------
 */

exports.getAllCostomersCorporateAdmin = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var corporateAdminId = req.body.corporate_admin_id;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function(result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT u.`user_id` AS `customer_id`,u.`user_name`,u.`user_email`,u.`phone_no`,";
                sql += "u.`total_rides_as_user`,u.`total_rating_got_user`,u.`total_rating_user`,"
                sql += "u.`req_stat` AS `registration_status`,u.`is_blocked`,u.`is_deleted` , tca.user_name as corporate_admin , tca.email as corporate_admin_email  FROM `tb_users` u inner join tb_corporate_user tcu on tcu.user_id = u.user_id inner join tb_corporate_admin tca on tca.user_id = tcu.admin_id   WHERE ";
                sql += "u.`reg_as`=? AND u.`make_me_driver_flag`=? AND u.`is_deleted`<>? and u.is_corporate = ? and tcu.admin_id = ? ";
                var bindparams = [constants.registeredAsFlag.CUSTOMER, constants.makeMeDriverFlag.APPROVED_DRIVER_STATUS, constants.DELETED_STATUS, constants.CORPORATE_USER_STATUS, corporateAdminId];
                connection.query(sql, bindparams, function(err, getAllCustomer) {
                    console.log(err)

                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching all driver : ", err, getAllCustomer);
                        responses.sendError(res);
                        return;
                    } else {
                        if (getAllCustomer.length > 0) {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {
                                    "customer_list": getAllCustomer
                                }
                            };
                        } else {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {
                                    "customer_list": []
                                }
                            };
                        }
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    }
};


/*
 * -----------------------------------------------
 * GET ALL CORPORATE ADMINS
 * -----------------------------------------------
 */

exports.getAllCorporateAdmins = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function(result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT tca.user_name , tca.email , tca.money_paid , tca.total_money , (tca.total_money - tca.money_paid ) as money_left, tca.phone_no  , tca.user_id , count(tca.user_id) as active_members from tb_corporate_admin tca left join tb_corporate_user tcu on tcu.admin_id = tca.user_id left join tb_users u on u.user_id = tcu.user_id group by tca.user_id ";

                var bindparams = [];
                connection.query(sql, bindparams, function(err, getAllAdmin) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching all driver : ", err, getAllAdmin);
                        responses.sendError(res);
                        return;
                    } else {
                        if (getAllAdmin.length > 0) {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {
                                    "admin_list": getAllAdmin
                                }
                            };
                        } else {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {
                                    "admin_list": []
                                }
                            };
                        }
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    }
};



/*
 * -----------------------------------------------
 * Mannualy getting the payment
 * -----------------------------------------------
 */

exports.getPaymentManually = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    var access_token = req.body.access_token;
    var admin_id = req.body.corporate_admin_id;
    var amount = req.body.amount;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateAdminAccessToken(access_token, function(result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "update tb_corporate_admin set money_paid = money_paid + ? where user_id = ? limit 1 ";

                var bindparams = [amount, admin_id];
                connection.query(sql, bindparams, function(err, updateAmount) {
                    if (err) {
                        logging.logDatabaseQueryError("Updating payment corporate admin : ", err, updateAmount);
                        responses.sendError(res);
                        return;
                    } else {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {}
                        };

                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    }
};
